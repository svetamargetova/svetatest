package consys.event.b2b.gwt.client;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;

import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.event.b2b.gwt.client.comp.IFrameContent;
import consys.event.b2b.gwt.client.module.B2BModule;
import consys.event.common.gwt.client.module.OverseerModule;

public class B2BEntryPoint extends DebugModuleEntryPoint {

    @Override
    public void initMocks() {
        ClientEvent event = new ClientEvent();
        event.setUuid("123");
        ClientCurrentEvent ce = new ClientCurrentEvent();
        ce.setEvent(event);
        Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, ce);
        ClientUser cu = new ClientUser();
        cu.setUuid("456");
        Cache.get().register(UserCacheAction.USER, cu);
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new B2BModule());
    }
    int i = 0;

    @Override
    public void registerForms(final Map<String, Widget> formMap) {
        formMap.put("IFramecontent testfile", new IFrameContent("http://127.0.0.1:8888/B2BDebug/event_profile.html"));

        //formMap.put("org - manage event", new ManageEvent());
        // formMap.put("org - manage event - org - rooms and tables", new
        // RoomsAndTablesList("something"));
    }
}
