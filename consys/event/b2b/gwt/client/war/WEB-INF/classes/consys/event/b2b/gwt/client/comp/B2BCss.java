package consys.event.b2b.gwt.client.comp;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface B2BCss extends CssResource {

    String hideScrollbars();
}
