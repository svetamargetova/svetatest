package consys.event.b2b.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.b2b.gwt.client.bo.ClientMeetingRoom;

public class UpdateMeetingRoomAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 4803803233859349123L;

    private ClientMeetingRoom cmr;

    public UpdateMeetingRoomAction() {
    }

    public UpdateMeetingRoomAction(ClientMeetingRoom cmr) {
	this.cmr = cmr;
    }

    public void setCmr(ClientMeetingRoom cmr) {
	this.cmr = cmr;
    }

    public ClientMeetingRoom getCmr() {
	return cmr;
    }

}
