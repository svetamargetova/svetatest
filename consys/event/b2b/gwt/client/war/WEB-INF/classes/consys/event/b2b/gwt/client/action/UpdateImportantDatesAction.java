package consys.event.b2b.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.b2b.gwt.client.bo.ClientImportantDates;

public class UpdateImportantDatesAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -8473610879873479701L;

    private ClientImportantDates dates;

    public UpdateImportantDatesAction(ClientImportantDates data) {
	this.dates = data;
    }

    public void setDates(ClientImportantDates dates) {
	this.dates = dates;
    }

    public ClientImportantDates getDates() {
	return dates;
    }

}
