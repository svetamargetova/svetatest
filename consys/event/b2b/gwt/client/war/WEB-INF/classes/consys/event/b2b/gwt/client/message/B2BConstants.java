package consys.event.b2b.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 *
 * @author pepa
 */
public interface B2BConstants extends Constants {

    String aboutEvent_text_name();

    String b2bModule_text_moduleName();

    String b2bModule_text_moduleRightAgent();

    String b2bModule_text_moduleRightAgentDescription();

    String b2bModule_text_moduleRightAdministrator();

    String b2bModule_text_moduleRightAdministratorDescription();

    String b2bModule_text_navigationModelAnonymousDasboard();

    String b2bModule_text_navigationModelAnonymousRegister();
        
    String b2bModule_text_navigationModelAgentCooperationProfile();    

    String b2bModule_text_navigationModelAgentDashboard();

    String b2bModule_text_navigationModelAgentFeedback();

    String b2bModule_text_navigationModelAgentMeetings();

    String b2bModule_text_navigationModelAgentOrganizationProfile();

    String b2bModule_text_navigationModelAgentParticipants();

    String b2bModule_text_navigationModelAgentProfile();

    String b2bModule_text_navigationModelAgentProfileVisitors();

    String b2bModule_text_navigationModelAgentSchedule();

    String b2bModule_text_navigationModelAgentStatistics();

    String b2bModule_text_navigationModelB2B();
    
    String b2bModule_text_navigationModelOrganizatorB2B();

    String b2bModule_text_navigationModelOrganizatorDashboard();

    String b2bModule_text_navigationModelOrganizatorFeedback();

    String b2bModule_text_navigationModelOrganizatorManageEvent();

    String b2bModule_text_navigationModelOrganizatorMeetings();

    String b2bModule_text_navigationModelOrganizatorParticipants();

    String b2bModule_text_navigationModelOrganizatorSchedule();

    String b2bModule_text_navigationModelOrganizatorStatistics();



}
