package consys.event.b2b.gwt.client.module;

import com.google.gwt.i18n.client.DateTimeFormat;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.module.event.EventNavigationItemVisiblityResolver;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.event.b2b.api.navigation.Navigation;
import consys.event.b2b.api.properties.B2BProperties;
import consys.event.b2b.api.right.EventB2BModuleRoleModel;
import consys.event.b2b.gwt.client.comp.IFrameContent;
import consys.event.b2b.gwt.client.utils.B2BMessageUtils;
import consys.event.b2b.gwt.client.utils.B2BResourceUtils;
import consys.event.common.api.right.Role;
import consys.event.common.gwt.client.event.ReloadEventMenuEvent;
import consys.event.registration.gwt.client.event.NewOrderEvent;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class B2BModule implements EventModule, NewOrderEvent.Handler {

    // konstanty
    public static final String NAVIGATION_PARENT_B2B = B2BMessageUtils.c.b2bModule_text_navigationModelB2B();
    public static final String B2B_NEW_ORDER_EVENT_TOKEN = "B2B_TOKEN";
    private static final String B2B_URL = "NO B2B";
    private boolean enabled;

    @Override
    public String getLocalizedModuleName() {
        return B2BMessageUtils.c.b2bModule_text_moduleName();
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
        EventB2BModuleRoleModel roles = new EventB2BModuleRoleModel();
        Map<String, Role> rightMap = roles.getModelInMap();

        // RIGHT_B2B_AGENT
        ModuleRole agent = new ModuleRole();
        agent.setId(rightMap.get(EventB2BModuleRoleModel.RIGHT_B2B_AGENT).getIdentifier());
        agent.setSystem(rightMap.get(EventB2BModuleRoleModel.RIGHT_B2B_AGENT).isSystem());
        agent.setShortcut(EventB2BModuleRoleModel.RIGHT_B2B_AGENT);
        agent.setLocalizedName(B2BMessageUtils.c.b2bModule_text_moduleRightAgent());
        agent.setLocalizedDescription(B2BMessageUtils.c.b2bModule_text_moduleRightAgent());
        moduleRoles.add(agent);

        // RIGHT_B2B_ORGANIZATOR
        ModuleRole organizator = new ModuleRole();
        organizator.setId(rightMap.get(EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR).getIdentifier());
        organizator.setSystem(rightMap.get(EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR).isSystem());
        organizator.setShortcut(EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR);
        organizator.setLocalizedName(B2BMessageUtils.c.b2bModule_text_moduleRightAdministrator());
        organizator.setLocalizedDescription(B2BMessageUtils.c.b2bModule_text_moduleRightAdministratorDescription());
        moduleRoles.add(organizator);
    }
    
    private String getB2BPrefix(){
        ClientCurrentEvent ce = (ClientCurrentEvent) Cache.get().get(CurrentEventCacheAction.CURRENT_EVENT);
        return B2B_URL +"/"+ce.getEvent().getUuid();
    }

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
        EventNavigationItem b2bBrokerage = new EventNavigationItem(NAVIGATION_PARENT_B2B, null, new ConsysAction() {

            @Override
            public void run() {

                
                
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorManageEvent(),
                        BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix()+"/organisator/manage/")));
                EventBus.get().fireEvent(e);
            }
        }, 40, true, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                enabled = StringUtils.equals(systemProperties.get(B2BProperties.B2B_MODULE_ENABLED), "true");
                return enabled;
            }
        }, EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR);
        
        EventNavigationItem b2bBrokerageOrganizer = new EventNavigationItem(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorB2B(), null, new ConsysAction() {

            @Override
            public void run() {

                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorManageEvent(),
                        BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix()+ "/organisator/manage/")));
                EventBus.get().fireEvent(e);
            }
        }, 41, true, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                enabled = StringUtils.equals(systemProperties.get(B2BProperties.B2B_MODULE_ENABLED), "true");
                return enabled;
            }
        }, EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR);

        initAnonymousNavigationModel(model, b2bBrokerage);
        initOrganizatorNavigationModel(model, b2bBrokerageOrganizer);
        initAgentNavigationModel(model, b2bBrokerage);
    }

    private void initAnonymousNavigationModel(EventNavigationModel model, EventNavigationItem parent) {
        // registracia
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAnonymousRegister(), 20,
                new ConsysAction() {

                    @Override
                    public void run() {
                        String eventName;
                        ClientCurrentEvent cce = Cache.get().get(CurrentEventCacheAction.CURRENT_EVENT);
                        if (cce != null) {
                            eventName = cce.getEvent().getFullName();
                        } else {
                            eventName = "";
                        }
                        String title = B2BMessageUtils.c.b2bModule_text_navigationModelAnonymousRegister();
                        String registrationMessage = B2BMessageUtils.m.b2bModule_text_navigationModelAnonymousRegisterInfo(eventName);
                        RegistrationForm rf = new RegistrationForm(null, registrationMessage, B2B_NEW_ORDER_EVENT_TOKEN);
                        FormUtils.fireNextRootBreadcrumb(title, rf);
                    }
                }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                return !rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_AGENT);
            }
        });
    }

    private void initAgentNavigationModel(EventNavigationModel model, EventNavigationItem parent) {
        // dashboard
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentDashboard(), Navigation.AGENT_DASHBOARD, 10,
                new ConsysAction() {

                    @Override
                    public void run() {
                        BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentDashboard(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                                new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation")));
                        EventBus.get().fireEvent(e);
                    }
                }, EventB2BModuleRoleModel.RIGHT_B2B_AGENT);

        // agent profile
        /*model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentProfile(), Navigation.AGENT_PROFILE, 15,
                new ConsysAction() {

                    @Override
                    public void run() {
                        BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentProfile(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                                new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation/profile/agent")));
                        EventBus.get().fireEvent(e);
                    }
                }, EventB2BModuleRoleModel.RIGHT_B2B_AGENT);*/

        // organization profile
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentOrganizationProfile(), Navigation.AGENT_ORGANIZATION_PROFILE,
                20, new ConsysAction() {

            @Override
            public void run() {
                
                
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentOrganizationProfile(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix()+"/organisation/profile/organisation")));
                EventBus.get().fireEvent(e);
            }
        }, EventB2BModuleRoleModel.RIGHT_B2B_AGENT);

        // cooperation profile
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentCooperationProfile(), Navigation.AGENT_COOPERATION_PROFILE,
                30, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentCooperationProfile(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation/cooperation-profile")));
                EventBus.get().fireEvent(e);
            }
        }, EventB2BModuleRoleModel.RIGHT_B2B_AGENT);

        // participants
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentParticipants(),
                40, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentParticipants(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation/participant")));
                EventBus.get().fireEvent(e);
            }
        }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                if (rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_AGENT)) {
                    return isTodayAfter(systemProperties.get(B2BProperties.B2B_BOOKINGS_START));
                }
                return false;
            }
        });

        // meetings
        /*model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentMeetings(),
                50, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentMeetings(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation/meeting")));
                EventBus.get().fireEvent(e);
            }
        }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                if (rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_AGENT)) {
                    return isTodayAfter(systemProperties.get(B2BProperties.B2B_BOOKINGS_START));
                }
                return false;
            }
        });*/

        // schedule        
        /*model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelAgentSchedule(),
                60, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelAgentSchedule(), BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisation/schedule")));
                EventBus.get().fireEvent(e);
            }
        }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                if (rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_AGENT)) {
                    return isTodayAfter(systemProperties.get(B2BProperties.B2B_BOOKINGS_START));
                }
                return false;
            }
        });*/
        
        
        // profile visitors

        // feedback

        // statistics       
    }

    private void initOrganizatorNavigationModel(EventNavigationModel model, EventNavigationItem parent) {
        // organizator dashboard
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorDashboard(),
                100,
                new ConsysAction() {

                    @Override
                    public void run() {
                        BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorDashboard(),
                                BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                                new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisator/")));
                        EventBus.get().fireEvent(e);
                    }
                }, EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR);

        // organizator participants
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorParticipants(),
                120,
                new ConsysAction() {

                    @Override
                    public void run() {
                        BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorParticipants(),
                                BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                                new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisator/participant/")));
                        EventBus.get().fireEvent(e);
                    }
                }, EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR);

        // meetings
        model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorMeetings(),
                130, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorMeetings(),
                        BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisator/meeting/")));
                EventBus.get().fireEvent(e);
            }            
        }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                if (rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR)) {
                    return isTodayAfter(systemProperties.get(B2BProperties.B2B_BOOKINGS_START));
                }
                return false;
            }
        });

	// organizator schedule
	model.insertFirstLevelAddon(parent, B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorSchedule(),
                140, new ConsysAction() {

            @Override
            public void run() {
                BreadcrumbContentEvent e = new BreadcrumbContentEvent(B2BMessageUtils.c.b2bModule_text_navigationModelOrganizatorSchedule(),
                        BreadcrumbContentEvent.LEVEL.NEXT_ROOT,
                        new ChangeContentEvent(new IFrameContent(getB2BPrefix() + "/organisator/schedule")));
                EventBus.get().fireEvent(e);
            }            
        }, new EventNavigationItemVisiblityResolver() {

            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                if (rights.contains(EventB2BModuleRoleModel.RIGHT_B2B_ADMINISTRATOR)) {
                    return isTodayAfter(systemProperties.get(B2BProperties.B2B_BOOKINGS_START));
                }
                return false;
            }
        });
        
        
	// organizator statistics

	// organizator feedback
	
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        EventBus.get().addHandler(NewOrderEvent.TYPE, this);
    }

    @Override
    public void registerModule() {
        B2BResourceUtils.b().css().ensureInjected();


    }

    @Override
    public void unregisterModule() {
    }

    @Override
    public void onOrderConfirmed(NewOrderEvent event) {
        if (enabled) {
            // znovunacitame menu
            EventBus.get().fireEvent(new ReloadEventMenuEvent());

            // nastavime dashboard
            EventBus.get().fireEvent(new UriFragmentTokenEvent(Navigation.AGENT_DASHBOARD));
        }
    }
    private static final DateTimeFormat FORMAT = DateTimeFormat.getFormat(B2BProperties.DATES_FORMAT);

    private static boolean isTodayAfter(String date) {
        if (date != null && date.length() > 0) {
            try {
                Date baseDate = FORMAT.parse(date);
                Date today = new Date();
                return baseDate.compareTo(today) <= 0;
            } catch (IllegalArgumentException e) {
                LoggerFactory.log(B2BModule.class, "Illegal parse of date: " + date);
                return false;
            }
        }
        return false;
    }
}
