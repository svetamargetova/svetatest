package consys.event.b2b.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.b2b.gwt.client.bo.ClientMeetingDetails;

public class UpdateMeetingDetailsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7075198330954392290L;

    private ClientMeetingDetails details;

    public UpdateMeetingDetailsAction(ClientMeetingDetails details) {
	this.setDetails(details);
    }

    public void setDetails(ClientMeetingDetails details) {
	this.details = details;
    }

    public ClientMeetingDetails getDetails() {
	return details;
    }

}
