package consys.event.b2b.gwt.client.bo;

public class ClientRoomsAndTablesAndTimes extends AbstractClientBO {

    private static final long serialVersionUID = -2545418470739208884L;

    private int doubleTables;
    private int quatreTables;

    public int getDoubleTables() {
	return this.doubleTables;
    }

    public void setDoubleTables(int doubleTables) {
	this.doubleTables = doubleTables;
    }

    public int getQuatreTables() {
	return this.quatreTables;
    }

    public void setQuatreTables(int quatreTables) {
	this.quatreTables = quatreTables;
    }

}
