package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

import consys.common.gwt.shared.action.Result;

public class AbstractClientBO implements IsSerializable, Result {

    private static final long serialVersionUID = -4564916835670739842L;

    private String uuid;
    private String name;
    private long id;

    public AbstractClientBO() {
    }

    public AbstractClientBO(AbstractClientBO clone) {
	this.uuid = clone.uuid;
	this.name = clone.name;
	this.id = clone.id;
    }

    public long getId() {
	return this.id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getUuid() {
	return this.uuid;
    }

    public void setUuid(String uuid) {
	this.uuid = uuid;
    }

}
