package consys.event.b2b.gwt.client.comp;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 *
 * @author pepa
 */
public interface B2BResources extends ClientBundle {

    public static final B2BResources INSTANCE = GWT.create(B2BResources.class);

    @Source("b2b.css")
    B2BCss css();
}
