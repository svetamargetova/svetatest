package consys.event.b2b.gwt.client.utils;

import com.google.gwt.core.client.GWT;

import consys.event.b2b.gwt.client.utils.css.B2BBundle;

/**
 * 
 * @author pepa
 */
public class B2BResourceUtils {

    private static B2BBundle bundle;

    static {
	bundle = (B2BBundle) GWT.create(B2BBundle.class);
	bundle.css().ensureInjected();
    }

    /** bundle */
    public static B2BBundle b() {
	return bundle;
    }
}
