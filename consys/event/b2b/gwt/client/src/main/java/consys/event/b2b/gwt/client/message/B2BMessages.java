package consys.event.b2b.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

public interface B2BMessages extends Messages {

    String b2bModule_text_navigationModelAnonymousRegisterInfo(String eventName);

}
