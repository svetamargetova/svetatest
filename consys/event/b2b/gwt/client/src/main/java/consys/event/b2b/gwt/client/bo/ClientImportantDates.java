package consys.event.b2b.gwt.client.bo;

import java.util.Date;

public class ClientImportantDates extends AbstractClientBO {

    private static final long serialVersionUID = -9187769905801357364L;

    private Date regFrom;
    private Date regTo;
    private Date selectFrom;
    private Date selectTo;
    private Date meetFrom;
    private Date meetTo;

    public ClientImportantDates() {
    }

    public ClientImportantDates(Date regFrom, Date regTo, Date selectFrom, Date selectTo, Date meetFrom, Date meetTo) {
	this.regFrom = regFrom;
	this.regTo = regTo;
	this.selectFrom = selectFrom;
	this.selectTo = selectTo;
	this.meetFrom = meetFrom;
	this.meetTo = meetTo;
    }

    public ClientImportantDates(ClientImportantDates data) {
	super(data);
	this.regFrom = data.regFrom;
	this.regTo = data.regTo;
	this.selectFrom = data.selectFrom;
	this.selectTo = data.selectTo;
	this.meetFrom = data.meetFrom;
	this.meetTo = data.meetTo;
    }

    public Date getRegFrom() {
	return this.regFrom;
    }

    public void setRegFrom(Date regFrom) {
	this.regFrom = regFrom;
    }

    public Date getRegTo() {
	return this.regTo;
    }

    public void setRegTo(Date regTo) {
	this.regTo = regTo;
    }

    public Date getSelectFrom() {
	return this.selectFrom;
    }

    public void setSelectFrom(Date selectFrom) {
	this.selectFrom = selectFrom;
    }

    public Date getSelectTo() {
	return this.selectTo;
    }

    public void setSelectTo(Date selectTo) {
	this.selectTo = selectTo;
    }

    public Date getMeetFrom() {
	return this.meetFrom;
    }

    public void setMeetFrom(Date meetFrom) {
	this.meetFrom = meetFrom;
    }

    public Date getMeetTo() {
	return this.meetTo;
    }

    public void setMeetTo(Date meetTo) {
	this.meetTo = meetTo;
    }

}
