package consys.event.b2b.gwt.client.comp;

import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUser;

/**
 * Obsah, ktery zobrazuje cizi stranku v iframe
 * @author pepa
 */
public class IFrameContent extends Composite {

    // konstanty
    private static final String FRAME_ID = "B2BFrame";
    private static final int EVERY_SECOND = 1000;
    private static final int INIT_HEIGHT = 1000;
    // komponenty
    private Frame frame;
    // data
    private String url;
    private Timer timer;
    private int lastHeight;

    public IFrameContent(String url) {
        ClientCurrentEvent ce = (ClientCurrentEvent) Cache.get().get(CurrentEventCacheAction.CURRENT_EVENT);
        ClientUser cu = (ClientUser) Cache.get().get(UserCacheAction.USER);

        this.url = url + "?eid=" + ce.getEvent().getUuid() + "&uid=" + cu.getUuid();
        B2BResources.INSTANCE.css().ensureInjected();

        frame = new Frame();
        frame.addStyleName(B2BResources.INSTANCE.css().hideScrollbars());
        frame.setWidth(LayoutManager.LAYOUT_CONTENT_WIDTH);
        frame.setHeight(INIT_HEIGHT + "px");
        frame.addLoadHandler(loadHandler());
        frame.getElement().setAttribute("frameBorder", "0");
        frame.getElement().setAttribute("scrolling", "no");
        frame.getElement().setId(FRAME_ID);

        initWidget(frame);
    }

    @Override
    protected void onLoad() {
        frame.setUrl(url);
    }

    @Override
    protected void onUnload() {
        if (timer != null) {
            timer.cancel();
        }
    }

    private LoadHandler loadHandler() {
        return new LoadHandler() {

            @Override
            public void onLoad(LoadEvent event) {
                int height;

                try {
                    height = Integer.parseInt(resizeByContent(FRAME_ID));
                } catch (NumberFormatException ex) {
                    height = -1;
                }

                if (height == -1) {
                    // nepodporovany prohlizec pro automatickou upravu vysky
                    frame.removeStyleName(B2BResources.INSTANCE.css().hideScrollbars());
                    frame.getElement().setAttribute("scrolling", "auto");
                } else {
                    // vyska se bude upravovat podle obsahu ramce
                    frame.getElement().getStyle().clearHeight();
                    lastHeight = height;
                    startTimer();
                }
            }
        };
    }

    /** spusti casovac, ktery kontrouje vysku obsahu ramce a pokud se od posledni kontroly zmenila, zvetsi ramec */
    private void startTimer() {
        if (timer == null) {
            timer = createTimer();
        }
        timer.scheduleRepeating(EVERY_SECOND);
    }

    private Timer createTimer() {
        return new Timer() {

            @Override
            public void run() {
                int height = getContentHeight(FRAME_ID);
                if (height != lastHeight) {
                    resizeByContent(FRAME_ID);
                    lastHeight = height;
                }
            }
        };
    }

    /** zjisti vysku obsahu ramce */
    private native int getContentHeight(String frameId) /*-{
     var currentfr=$doc.getElementById(frameId);
     if (currentfr) {
     currentfr.style.display="block";

     if (currentfr.contentDocument && currentfr.contentDocument.body.offsetHeight){
     //ns6 syntax
     return currentfr.contentDocument.body.offsetHeight;
     } else if (currentfr.Document && currentfr.Document.body.scrollHeight) {
     //ie5+ syntax
     return currentfr.Document.body.scrollHeight;
     }
     }
     }-*/;

    /** upravi vysku ramce podle jeho obsahu */
    // pro operu by se melo dodelat document.documentElement.clientHeight
    //currentfr.contentDocument.body is null (TODO: pepa, upravit na kontrolu a vymyslet co delat, kdyz je null)
    private native String resizeByContent(String frameId) /*-{
     // extra height in px to add to iframe in FireFox 1.0+ browsers
     var getFFVersion=navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1];
     var FFextraHeight=parseFloat(getFFVersion)>= 0.1 ? 16 : 0;

     // nacteme ramec
     var currentfr=$doc.getElementById(frameId);
     if (currentfr && !window.opera) {
     currentfr.style.display="block";

     if (currentfr.contentDocument && currentfr.contentDocument.body.offsetHeight){
     //ns6 syntax
     currentfr.height = Math.max(currentfr.contentDocument.body.offsetHeight + FFextraHeight,600);
     return currentfr.height;
     } else if (currentfr.Document && currentfr.Document.body.scrollHeight) {
     //ie5+ syntax
     currentfr.height = Math.max(currentfr.Document.body.scrollHeight,600);
     return currentfr.height;
     } else {
     return -1;
     }
     }
     }-*/;
}
