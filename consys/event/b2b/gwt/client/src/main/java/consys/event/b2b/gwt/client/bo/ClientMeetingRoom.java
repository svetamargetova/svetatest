package consys.event.b2b.gwt.client.bo;


public class ClientMeetingRoom extends AbstractClientBO {

    private static final long serialVersionUID = -7904938456884243068L;

    private int tablesCount;

    public ClientMeetingRoom() {
    }

    public ClientMeetingRoom(ClientMeetingRoom clientMeetingRoom) {
	super(clientMeetingRoom);
	this.tablesCount = clientMeetingRoom.tablesCount;
    }

    public int getTablesCount() {
	return this.tablesCount;
    }

    public void setTablesCount(int tablesCount) {
	this.tablesCount = tablesCount;
    }

}
