package consys.event.b2b.gwt.client.utils;

import com.google.gwt.core.client.GWT;

import consys.event.b2b.gwt.client.message.B2BConstants;
import consys.event.b2b.gwt.client.message.B2BMessages;

/**
 * 
 * @author pepa
 */
public class B2BMessageUtils {

    /** konstanty modulu b2b */
    public static final B2BConstants c = (B2BConstants) GWT.create(B2BConstants.class);
    /** systemove zpravy modulu b2b */
    public static final B2BMessages m = (B2BMessages) GWT.create(B2BMessages.class);
}
