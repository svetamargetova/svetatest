package consys.event.b2b.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.b2b.gwt.client.bo.ClientMeetingRoom;

public class LoadMeetingRoomAction extends EventAction<ClientMeetingRoom> {

    private static final long serialVersionUID = 92358312161540524L;

    private long id;

    public LoadMeetingRoomAction() {
    }

    public LoadMeetingRoomAction(long id) {
	this.id = id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public long getId() {
	return id;
    }

}
