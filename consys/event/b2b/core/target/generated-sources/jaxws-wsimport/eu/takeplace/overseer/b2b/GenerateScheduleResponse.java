
package eu.takeplace.overseer.b2b;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScheduledFrames" type="{http://takeplace.eu/overseer/b2b}ScheduledFrame" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "scheduledFrames"
})
@XmlRootElement(name = "GenerateScheduleResponse")
public class GenerateScheduleResponse {

    @XmlElement(name = "ScheduledFrames", required = true)
    protected List<ScheduledFrame> scheduledFrames;

    /**
     * Gets the value of the scheduledFrames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scheduledFrames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduledFrames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduledFrame }
     * 
     * 
     */
    public List<ScheduledFrame> getScheduledFrames() {
        if (scheduledFrames == null) {
            scheduledFrames = new ArrayList<ScheduledFrame>();
        }
        return this.scheduledFrames;
    }

}
