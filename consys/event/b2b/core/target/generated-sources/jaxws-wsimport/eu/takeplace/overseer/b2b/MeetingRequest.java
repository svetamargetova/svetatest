
package eu.takeplace.overseer.b2b;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MeetingRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeetingRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AgentSrcId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AgentDestId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeetingRequest", propOrder = {
    "id",
    "agentSrcId",
    "agentDestId"
})
public class MeetingRequest {

    @XmlElement(name = "Id")
    protected long id;
    @XmlElement(name = "AgentSrcId")
    protected long agentSrcId;
    @XmlElement(name = "AgentDestId")
    protected long agentDestId;

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the agentSrcId property.
     * 
     */
    public long getAgentSrcId() {
        return agentSrcId;
    }

    /**
     * Sets the value of the agentSrcId property.
     * 
     */
    public void setAgentSrcId(long value) {
        this.agentSrcId = value;
    }

    /**
     * Gets the value of the agentDestId property.
     * 
     */
    public long getAgentDestId() {
        return agentDestId;
    }

    /**
     * Sets the value of the agentDestId property.
     * 
     */
    public void setAgentDestId(long value) {
        this.agentDestId = value;
    }

}
