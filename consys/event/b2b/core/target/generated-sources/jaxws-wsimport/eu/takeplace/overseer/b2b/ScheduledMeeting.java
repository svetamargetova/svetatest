
package eu.takeplace.overseer.b2b;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScheduledMeeting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScheduledMeeting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TableId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="MeetingId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FrameId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScheduledMeeting", propOrder = {
    "tableId",
    "meetingId",
    "frameId"
})
public class ScheduledMeeting {

    @XmlElement(name = "TableId")
    protected long tableId;
    @XmlElement(name = "MeetingId")
    protected long meetingId;
    @XmlElement(name = "FrameId")
    protected long frameId;

    /**
     * Gets the value of the tableId property.
     * 
     */
    public long getTableId() {
        return tableId;
    }

    /**
     * Sets the value of the tableId property.
     * 
     */
    public void setTableId(long value) {
        this.tableId = value;
    }

    /**
     * Gets the value of the meetingId property.
     * 
     */
    public long getMeetingId() {
        return meetingId;
    }

    /**
     * Sets the value of the meetingId property.
     * 
     */
    public void setMeetingId(long value) {
        this.meetingId = value;
    }

    /**
     * Gets the value of the frameId property.
     * 
     */
    public long getFrameId() {
        return frameId;
    }

    /**
     * Sets the value of the frameId property.
     * 
     */
    public void setFrameId(long value) {
        this.frameId = value;
    }

}
