
package eu.takeplace.overseer.b2b;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.takeplace.overseer.b2b package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrepareSchedule_QNAME = new QName("http://b2b.overseer.takeplace.eu/", "prepareSchedule");
    private final static QName _PrepareScheduleResponse_QNAME = new QName("http://b2b.overseer.takeplace.eu/", "prepareScheduleResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.takeplace.overseer.b2b
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrepareSchedule.Request }
     * 
     */
    public PrepareSchedule.Request createPrepareScheduleRequest() {
        return new PrepareSchedule.Request();
    }

    /**
     * Create an instance of {@link MeetingFrame }
     * 
     */
    public MeetingFrame createMeetingFrame() {
        return new MeetingFrame();
    }

    /**
     * Create an instance of {@link GenerateScheduleResponse }
     * 
     */
    public GenerateScheduleResponse createGenerateScheduleResponse() {
        return new GenerateScheduleResponse();
    }

    /**
     * Create an instance of {@link GenerateScheduleRequest }
     * 
     */
    public GenerateScheduleRequest createGenerateScheduleRequest() {
        return new GenerateScheduleRequest();
    }

    /**
     * Create an instance of {@link AgentScheduleRestriction }
     * 
     */
    public AgentScheduleRestriction createAgentScheduleRestriction() {
        return new AgentScheduleRestriction();
    }

    /**
     * Create an instance of {@link ScheduledFrame }
     * 
     */
    public ScheduledFrame createScheduledFrame() {
        return new ScheduledFrame();
    }

    /**
     * Create an instance of {@link PrepareScheduleResponse }
     * 
     */
    public PrepareScheduleResponse createPrepareScheduleResponse() {
        return new PrepareScheduleResponse();
    }

    /**
     * Create an instance of {@link PrepareScheduleResponse.Return }
     * 
     */
    public PrepareScheduleResponse.Return createPrepareScheduleResponseReturn() {
        return new PrepareScheduleResponse.Return();
    }

    /**
     * Create an instance of {@link MeetingRequest }
     * 
     */
    public MeetingRequest createMeetingRequest() {
        return new MeetingRequest();
    }

    /**
     * Create an instance of {@link ScheduledMeeting }
     * 
     */
    public ScheduledMeeting createScheduledMeeting() {
        return new ScheduledMeeting();
    }

    /**
     * Create an instance of {@link AgentFrameRestriction }
     * 
     */
    public AgentFrameRestriction createAgentFrameRestriction() {
        return new AgentFrameRestriction();
    }

    /**
     * Create an instance of {@link PrepareSchedule }
     * 
     */
    public PrepareSchedule createPrepareSchedule() {
        return new PrepareSchedule();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareSchedule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://b2b.overseer.takeplace.eu/", name = "prepareSchedule")
    public JAXBElement<PrepareSchedule> createPrepareSchedule(PrepareSchedule value) {
        return new JAXBElement<PrepareSchedule>(_PrepareSchedule_QNAME, PrepareSchedule.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrepareScheduleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://b2b.overseer.takeplace.eu/", name = "prepareScheduleResponse")
    public JAXBElement<PrepareScheduleResponse> createPrepareScheduleResponse(PrepareScheduleResponse value) {
        return new JAXBElement<PrepareScheduleResponse>(_PrepareScheduleResponse_QNAME, PrepareScheduleResponse.class, null, value);
    }

}
