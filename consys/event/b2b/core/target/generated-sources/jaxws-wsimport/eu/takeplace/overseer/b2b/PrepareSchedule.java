
package eu.takeplace.overseer.b2b;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prepareSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prepareSchedule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="request" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MeetingFrames" type="{http://takeplace.eu/overseer/b2b}MeetingFrame" maxOccurs="unbounded"/>
 *                   &lt;element name="MeetingRequests" type="{http://takeplace.eu/overseer/b2b}MeetingRequest" maxOccurs="unbounded"/>
 *                   &lt;element name="AgentScheduleRestriction" type="{http://takeplace.eu/overseer/b2b}AgentScheduleRestriction" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prepareSchedule", namespace = "http://b2b.overseer.takeplace.eu/", propOrder = {
    "request"
})
public class PrepareSchedule {

    protected PrepareSchedule.Request request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link PrepareSchedule.Request }
     *     
     */
    public PrepareSchedule.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrepareSchedule.Request }
     *     
     */
    public void setRequest(PrepareSchedule.Request value) {
        this.request = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MeetingFrames" type="{http://takeplace.eu/overseer/b2b}MeetingFrame" maxOccurs="unbounded"/>
     *         &lt;element name="MeetingRequests" type="{http://takeplace.eu/overseer/b2b}MeetingRequest" maxOccurs="unbounded"/>
     *         &lt;element name="AgentScheduleRestriction" type="{http://takeplace.eu/overseer/b2b}AgentScheduleRestriction" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "meetingFrames",
        "meetingRequests",
        "agentScheduleRestriction"
    })
    public static class Request {

        @XmlElement(name = "MeetingFrames", required = true)
        protected List<MeetingFrame> meetingFrames;
        @XmlElement(name = "MeetingRequests", required = true)
        protected List<MeetingRequest> meetingRequests;
        @XmlElement(name = "AgentScheduleRestriction", required = true)
        protected List<AgentScheduleRestriction> agentScheduleRestriction;

        /**
         * Gets the value of the meetingFrames property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the meetingFrames property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMeetingFrames().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MeetingFrame }
         * 
         * 
         */
        public List<MeetingFrame> getMeetingFrames() {
            if (meetingFrames == null) {
                meetingFrames = new ArrayList<MeetingFrame>();
            }
            return this.meetingFrames;
        }

        /**
         * Gets the value of the meetingRequests property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the meetingRequests property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMeetingRequests().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MeetingRequest }
         * 
         * 
         */
        public List<MeetingRequest> getMeetingRequests() {
            if (meetingRequests == null) {
                meetingRequests = new ArrayList<MeetingRequest>();
            }
            return this.meetingRequests;
        }

        /**
         * Gets the value of the agentScheduleRestriction property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the agentScheduleRestriction property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAgentScheduleRestriction().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AgentScheduleRestriction }
         * 
         * 
         */
        public List<AgentScheduleRestriction> getAgentScheduleRestriction() {
            if (agentScheduleRestriction == null) {
                agentScheduleRestriction = new ArrayList<AgentScheduleRestriction>();
            }
            return this.agentScheduleRestriction;
        }

    }

}
