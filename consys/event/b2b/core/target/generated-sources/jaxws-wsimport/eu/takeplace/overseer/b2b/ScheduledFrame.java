
package eu.takeplace.overseer.b2b;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScheduledFrame complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScheduledFrame">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Meetings" type="{http://takeplace.eu/overseer/b2b}ScheduledMeeting" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScheduledFrame", propOrder = {
    "meetings"
})
public class ScheduledFrame {

    @XmlElement(name = "Meetings", required = true)
    protected List<ScheduledMeeting> meetings;

    /**
     * Gets the value of the meetings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the meetings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeetings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduledMeeting }
     * 
     * 
     */
    public List<ScheduledMeeting> getMeetings() {
        if (meetings == null) {
            meetings = new ArrayList<ScheduledMeeting>();
        }
        return this.meetings;
    }

}
