/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.dao;

import consys.event.b2b.core.bo.thumb.MeetingExportData;
import consys.event.b2b.core.bo.thumb.MeetingFramesGeneratorData;
import eu.takeplace.overseer.b2b.AgentScheduleRestriction;
import eu.takeplace.overseer.b2b.MeetingRequest;
import eu.takeplace.overseer.b2b.ScheduledFrame;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author palo
 */
public interface MeetingDao {
    
    public List<MeetingRequest> listAllActiveMeetingsForScheduleGenerator();
 
    public MeetingFramesGeneratorData listAllMeetingFrames();
    
    public List<AgentScheduleRestriction> listAllAgentsScheduleRestriction();      
            
    public MeetingExportData loadMeetingExportData(Integer meetingId) ;
    
    public List<MeetingExportData> listMeetingExportData(String uuid, String date);
   
    public List<MeetingExportData> listMeetingExportDataForOrganization(Integer id);
       
    public void insertSchedule(List<ScheduledFrame> scheduledFrames, MeetingFramesGeneratorData mfgd);
    
}
