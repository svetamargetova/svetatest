/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.event.b2b.core.bo.Organization;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.b2b.core.dao.OrganizationDao;
import consys.event.b2b.core.service.OrganizationService;
import consys.event.badges.core.export.BadgeParticipantInfo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class OrganizationServiceImpl implements OrganizationService{

    @Autowired
    private OrganizationDao participantDao;
    
    @Override
    public List<OrganizationThumb> listAllOrganizations() {
        return participantDao.listAllParticipants();
    }

    @Override
    public Organization loadOrganization(Integer id) throws NoRecordException {
        return participantDao.loadOrganizationDetailed(id);
    }

    @Override
    public List<BadgeParticipantInfo> listAllOrganizationAgents() {
        return participantDao.listAllOrganizationAgents();
    }

    @Override
    public List<OrganizationExportData> listOrganizationsToExport() {
        return participantDao.listOrganizationExportData();
    }
}
