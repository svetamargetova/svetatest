/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo.thumb;


import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author palo
 */
public class OrganizationThumb {
       
    private Integer organizationId;
    private String organizationName;
    private String organizationStreet;
    private String organizationCity;
    private String organizationZip;
    private String organizationCountry;
    private String organizationPhone;
    private String organizationWebsite;
    private String organizationLogo;
    private String agentFullname;
    private String agentPosition;
    private String agentProfileImgPrefix;
    private List<CooperationProfile> profiles;

    public OrganizationThumb() {
        profiles = Lists.newArrayList();
    }

    /**
     * @return the organizationName
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * @param organizationName the organizationName to set
     */
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    /**
     * @return the organizationStreet
     */
    public String getOrganizationStreet() {
        return organizationStreet;
    }

    /**
     * @param organizationStreet the organizationStreet to set
     */
    public void setOrganizationStreet(String organizationStreet) {
        this.organizationStreet = organizationStreet;
    }

    /**
     * @return the organizationCity
     */
    public String getOrganizationCity() {
        return organizationCity;
    }

    /**
     * @param organizationCity the organizationCity to set
     */
    public void setOrganizationCity(String organizationCity) {
        this.organizationCity = organizationCity;
    }

    /**
     * @return the organizationZip
     */
    public String getOrganizationZip() {
        return organizationZip;
    }

    /**
     * @param organizationZip the organizationZip to set
     */
    public void setOrganizationZip(String organizationZip) {
        this.organizationZip = organizationZip;
    }

    /**
     * @return the organizationCountry
     */
    public String getOrganizationCountry() {
        return organizationCountry;
    }

    /**
     * @param organizationCountry the organizationCountry to set
     */
    public void setOrganizationCountry(String organizationCountry) {
        this.organizationCountry = organizationCountry;
    }

    /**
     * @return the organizationPhone
     */
    public String getOrganizationPhone() {
        return organizationPhone;
    }

    /**
     * @param organizationPhone the organizationPhone to set
     */
    public void setOrganizationPhone(String organizationPhone) {
        this.organizationPhone = organizationPhone;
    }

    /**
     * @return the organizationWebsite
     */
    public String getOrganizationWebsite() {
        return organizationWebsite;
    }

    /**
     * @param organizationWebsite the organizationWebsite to set
     */
    public void setOrganizationWebsite(String organizationWebsite) {
        this.organizationWebsite = organizationWebsite;
    }

    /**
     * @return the organizationLogo
     */
    public String getOrganizationLogo() {
        return organizationLogo;
    }

    /**
     * @param organizationLogo the organizationLogo to set
     */
    public void setOrganizationLogo(String organizationLogo) {
        this.organizationLogo = organizationLogo;
    }

    /**
     * @return the agentFullname
     */
    public String getAgentFullname() {
        return agentFullname;
    }

    /**
     * @param agentFullname the agentFullname to set
     */
    public void setAgentFullname(String agentFullname) {
        this.agentFullname = agentFullname;
    }

    /**
     * @return the agentPosition
     */
    public String getAgentPosition() {
        return agentPosition;
    }

    /**
     * @param agentPosition the agentPosition to set
     */
    public void setAgentPosition(String agentPosition) {
        this.agentPosition = agentPosition;
    }

    /**
     * @return the agentProfileImgPrefix
     */
    public String getAgentProfileImgPrefix() {
        return agentProfileImgPrefix;
    }

    /**
     * @param agentProfileImgPrefix the agentProfileImgPrefix to set
     */
    public void setAgentProfileImgPrefix(String agentProfileImgPrefix) {
        this.agentProfileImgPrefix = agentProfileImgPrefix;
    }

    /**
     * @return the profiles
     */
    public List<CooperationProfile> getProfiles() {
        return profiles;
    }

    /**
     * @return the organizationId
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }
    
    
    
    
    
    public class CooperationProfile{
        private String name;

        public CooperationProfile(String name) {
            this.name = name;
        }
                

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }
    
    }
    
    
    
}
