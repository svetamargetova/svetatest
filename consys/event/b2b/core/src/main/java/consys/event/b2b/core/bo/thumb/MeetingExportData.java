package consys.event.b2b.core.bo.thumb;

import com.google.common.collect.Lists;
import consys.event.b2b.core.bo.CooperationProfileType;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public class MeetingExportData implements Comparable<MeetingExportData> {

    private Integer id;
    // Kde a kedy
    private Date meetingTime;
    private BigInteger tableIdx;
    private String tableName;
    // S kym a co    
    private String organizationTitle;
    private String organizationDescription;
    // profile
    private Integer profileId;
    private String profileTitle;
    private String profileDescription;
    private String agentName;
    private String agentPosition;
    private String agentImageProfilePrefix;
    private List<CooperationProfileType> profileTypes;
    // poznamky
    private String notes;

    /**
     * @return the meetingTime
     */
    public Date getMeetingTime() {
        return meetingTime;
    }

    /**
     * @param meetingTime the meetingTime to set
     */
    public void setMeetingTime(Date meetingTime) {
        this.meetingTime = meetingTime;
    }

    /**
     * @return the organizationTitle
     */
    public String getOrganizationTitle() {
        return organizationTitle;
    }

    /**
     * @param organizationTitle the organizationTitle to set
     */
    public void setOrganizationTitle(String organizationTitle) {
        this.organizationTitle = organizationTitle;
    }

    /**
     * @return the profileTitle
     */
    public String getProfileTitle() {
        return profileTitle;
    }

    /**
     * @param profileTitle the profileTitle to set
     */
    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
    }

    /**
     * @return the profileDescription
     */
    public String getProfileDescription() {
        return profileDescription;
    }

    /**
     * @param profileDescription the profileDescription to set
     */
    public void setProfileDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    /**
     * @return the agentName
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * @param agentName the agentName to set
     */
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    /**
     * @return the agentPosition
     */
    public String getAgentPosition() {
        return agentPosition;
    }

    /**
     * @param agentPosition the agentPosition to set
     */
    public void setAgentPosition(String agentPosition) {
        this.agentPosition = agentPosition;
    }

    /**
     * @return the agentImageProfilePrefix
     */
    public String getAgentImageProfilePrefix() {
        return agentImageProfilePrefix;
    }

    /**
     * @param agentImageProfilePrefix the agentImageProfilePrefix to set
     */
    public void setAgentImageProfilePrefix(String agentImageProfilePrefix) {
        this.agentImageProfilePrefix = agentImageProfilePrefix;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the tableIdx
     */
    public BigInteger getTableIdx() {
        return tableIdx;
    }

    /**
     * @param tableIdx the tableIdx to set
     */
    public void setTableIdx(BigInteger tableIdx) {
        this.tableIdx = tableIdx;
    }

    /**
     * @return the organizationDescription
     */
    public String getOrganizationDescription() {
        return organizationDescription;
    }

    /**
     * @param organizationDescription the organizationDescription to set
     */
    public void setOrganizationDescription(String organizationDescription) {
        this.organizationDescription = organizationDescription;
    }

    /**
     * @return the profileTypes
     */
    public List<CooperationProfileType> getProfileTypes() {
        if (profileTypes == null) {
            profileTypes = Lists.newArrayList();
        }
        return profileTypes;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the profileId
     */
    public Integer getProfileId() {
        return profileId;
    }

    /**
     * @param profileId the profileId to set
     */
    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public int compareTo(MeetingExportData o) {
        return meetingTime.compareTo(o.getMeetingTime());
    }
}
