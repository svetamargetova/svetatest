/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.b2b.core.bo.Organization;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.badges.core.export.BadgeParticipantInfo;
import java.util.List;

/**
 *
 * @author palo
 */
public interface OrganizationDao {
    
    public List<OrganizationThumb> listAllParticipants();
    
    public List<BadgeParticipantInfo> listAllOrganizationAgents();
    
    public Organization loadOrganizationDetailed(Integer id) throws NoRecordException;
    
    public List<OrganizationExportData> listOrganizationExportData();
    
}
