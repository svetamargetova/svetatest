/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo.thumb;

import consys.event.b2b.core.bo.*;
import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class OrganizationExportData implements ConsysObject {

    private Integer id;
    private String title;
    private String street;
    private String city;
    private String zip;
    private String country;
    private String phone;
    private String web;
    private String description;
    private UserEvent agent;    
    private String logo;
    private List<CooperationProfile> cooperationProfiles;    
    private List<AreaOfActivity> areas;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the agent
     */
    public UserEvent getAgent() {
        return agent;
    }

    /**
     * @param agent the agent to set
     */
    public void setAgent(UserEvent agent) {
        this.agent = agent;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the cooperationProfiles
     */
    public List<CooperationProfile> getCooperationProfiles() {
        if (cooperationProfiles == null) {
            cooperationProfiles = new ArrayList<CooperationProfile>();
        }
        return cooperationProfiles;
    }

    /**
     * @param cooperationProfiles the cooperationProfiles to set
     */
    public void setCooperationProfiles(List<CooperationProfile> cooperationProfiles) {
        this.cooperationProfiles = cooperationProfiles;
    }

   

    /**
     * @return the areas
     */
    public List<AreaOfActivity> getAreas() {
        if (areas == null) {
            areas = Lists.newArrayList();
        }        
        return areas;
    }

    /**
     * @param areas the areas to set
     */
    public void setAreas(List<AreaOfActivity> areas) {
        this.areas = areas;
    }
}
