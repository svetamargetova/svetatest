package consys.event.b2b.core.module;

import consys.common.core.bo.SystemProperty;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.b2b.api.properties.B2BProperties;
import consys.event.b2b.api.right.EventB2BModuleRoleModel;
import consys.event.common.api.right.ModuleRoleModel;
import consys.event.common.api.right.Role;
import consys.event.common.core.AbstractEventModuleRegistrator;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

public class B2BModuleRegistrator extends AbstractEventModuleRegistrator implements B2BProperties {

    private static final String SCHEMA = "b2b";
    private static final String MODULE = "b2b";

    @Override
    public String getSchemaName() {
	return SCHEMA;
    }

    @Override
    public void doAfterSessionFactoryCreate(Session session) {

    }

    @Override
    public List<SystemProperty> getModuleProperties() {
        SystemProperty moduleEnabled = new SystemProperty();
        moduleEnabled.setKey(B2BProperties.B2B_MODULE_ENABLED);
        moduleEnabled.setValue("false");
        moduleEnabled.setToClient(true);
        return Lists.newArrayList(moduleEnabled);
    }

    @Override
    public Module getModule() {
	Module m = new Module();
	m.setName(MODULE);
	ModuleRoleModel rights = new EventB2BModuleRoleModel();
	for (Role r : rights.getRoles()) {
	    ModuleRight right = new ModuleRight();
	    right.setName(r.getName());
	    right.setUniqueValue(r.getIdentifier());
	    m.getRights().add(right);
	}

	return m;
    }

    @Override
    public Map<String, String> getSQLScripts() {
	Map<String, String> map = Maps.newHashMap();
	return map;
    }

}
