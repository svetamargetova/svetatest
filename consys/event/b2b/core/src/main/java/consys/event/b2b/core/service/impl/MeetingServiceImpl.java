/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.b2b.core.bo.thumb.MeetingExportData;
import consys.event.b2b.core.bo.thumb.MeetingFramesGeneratorData;
import consys.event.b2b.core.dao.MeetingDao;
import consys.event.b2b.core.service.MeetingService;
import eu.takeplace.overseer.b2b.*;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author palo
 */
public class MeetingServiceImpl extends AbstractService implements MeetingService{

    private static final ObjectFactory OF = new ObjectFactory();
    
    @Autowired
    private MeetingDao meetingDao;
    
    private SchedulerWS_Service generatorService;

    public MeetingServiceImpl() {
        generatorService = new SchedulerWS_Service();
    }
    
    
    
    
    @Override
    public void generateSchedule() {
                                        
        PrepareSchedule.Request r = new PrepareSchedule.Request();       
        log().debug("B2B: Generator");
        // Vlozime poziadavky na meeting
        List<MeetingRequest> meetings = meetingDao.listAllActiveMeetingsForScheduleGenerator();
        log().debug("Meetings: {}",meetings.size());
        r.getMeetingRequests().addAll(meetings);
        
        // vlozime mozne timesloty
        MeetingFramesGeneratorData mfgd = meetingDao.listAllMeetingFrames();
        log().debug("Timeslosts: {}",mfgd.getMeetingFrames().size());
        r.getMeetingFrames().addAll(mfgd.getMeetingFrames());
        
        
        //  vlozime obmedzenia agentov
        List<AgentScheduleRestriction> restrictions = meetingDao.listAllAgentsScheduleRestriction();
        log().debug("Agent restrictions: {}",restrictions.size());
        r.getAgentScheduleRestriction().addAll(restrictions);
                
        // posleme
         
        
        PrepareScheduleResponse.Return result  = generatorService.getSchedulerWSPort().prepareSchedule(r);
        
        // spracujeme
        meetingDao.insertSchedule(result.getScheduledFrames(),mfgd); 
    }

    @Override
    public MeetingExportData loadMeetingExportData(BigInteger meetingId) {
        return meetingDao.loadMeetingExportData(meetingId.intValue());
    }

    @Override
    public List<MeetingExportData> listMeetingExportData(String uuid, String date) {
        return meetingDao.listMeetingExportData(uuid, date);
    }

    @Override
    public List<MeetingExportData> listMeetingExportDataForOrganization(Integer id) {
        return meetingDao.listMeetingExportDataForOrganization(id);
    }

    
    
}
