/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo;

/**
 *
 * @author palo
 */
public class CooperationProfileType {
    
    private Integer id;
    private String typeName;
    private boolean offer;
    private boolean request;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the offer
     */
    public boolean isOffer() {
        return offer;
    }

    /**
     * @param offer the offer to set
     */
    public void setOffer(boolean offer) {
        this.offer = offer;
    }

    /**
     * @return the request
     */
    public boolean isRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(boolean request) {
        this.request = request;
    }
    
}
