/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.dao.jdbc;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.b2b.core.bo.CooperationProfileType;
import consys.event.b2b.core.bo.Meeting;
import consys.event.b2b.core.bo.thumb.MeetingExportData;
import consys.event.b2b.core.bo.thumb.MeetingFramesGeneratorData;
import consys.event.b2b.core.bo.thumb.MeetingFramesGeneratorData.Table;
import consys.event.b2b.core.dao.MeetingDao;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import eu.takeplace.overseer.b2b.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public class MeetingDaoImpl extends GenericDaoImpl<Meeting> implements MeetingDao {

    @Override
    public List<MeetingRequest> listAllActiveMeetingsForScheduleGenerator() {

        List<Object[]> meetings = session().createSQLQuery("SELECT id, \"userId\", \"agentId\" FROM b2b.meeting where cancelled=0 and pending=0").list();
        List<MeetingRequest> out = Lists.newArrayList();

        for (Object[] m : meetings) {
            MeetingRequest meeting = new MeetingRequest();
            meeting.setId(((Integer) m[0]).longValue());
            meeting.setAgentSrcId(((BigInteger) m[1]).longValue());
            meeting.setAgentDestId(((BigInteger) m[2]).longValue());
            out.add(meeting);
        }
        return out;
    }

    @Override
    public MeetingFramesGeneratorData listAllMeetingFrames() {
        List<Object[]> frames = session().createSQLQuery("select ts.id as timeslotId,i.id as intervalId,r.id as roomId,r.title, r.seats2 from b2b.time_slot ts inner join b2b.interval i on ts.id_interval=i.id left join b2b.room r on r.date=i.date order by i.date").list();


        MeetingFramesGeneratorData out = new MeetingFramesGeneratorData();

        MeetingFrame m = null;

        Long tableIdcounter = 1L;

        for (Object[] o : frames) {
            if (m == null || m.getId() != ((BigInteger) o[0]).longValue()) {
                m = new MeetingFrame();
                m.setId(((BigInteger) o[0]).longValue());
                out.getMeetingFrames().add(m);
            }
            String roomName = (String) o[3];
            Integer seats = (Integer) o[4];
            for (int i = 1; i <= seats; i++, tableIdcounter++) {
                out.getTableIdToRoom().put(tableIdcounter, out.new Table(" Table " + i, i, (Integer) o[2]));
                m.getTableIds().add(tableIdcounter);
            }
        }
        return out;
    }

    @Override
    public List<AgentScheduleRestriction> listAllAgentsScheduleRestriction() {
        List<Object[]> frames = session().createSQLQuery("select id_agent, id_time_slot from b2b.agent_time_slot_restriction order by id_agent").list();
        AgentScheduleRestriction aa = null;
        List<AgentScheduleRestriction> out = Lists.newArrayList();
        for (Object[] objects : frames) {
            if (aa == null || aa.getAgentId() != ((BigInteger) objects[0]).longValue()) {
                aa = new AgentScheduleRestriction();
                aa.setAgentId(((BigInteger) objects[0]).longValue());
                out.add(aa);
            }

            AgentFrameRestriction afr = new AgentFrameRestriction();
            afr.setRestrictedFrameId(((BigInteger) objects[1]).longValue());
            aa.getFrameRestrictions().add(afr);
        }
        return out;
    }

    @Override
    public Meeting load(Long id) throws NoRecordException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    // query kde ja sem cilem - muj profil a organizacia
    String baseQueryOrganization = "select m.id , m.note, o.name, o.description as orgdesc, "
            + "ue.full_name, ue.profile_img_prefix,ue.position, "
            + "sm.id_table, ts.start_time,sm.table_name "            
            + "from b2b.meeting m "
            + "left join b2b.organisation o on o.user_id=m.\"userId\" "            
            + "inner join b2b.organisation op on op.user_id=m.\"agentId\" "
            + "left join b2b.scheduled_meeting sm on sm.id_meeting=m.id "
            + "left join b2b.time_slot ts on ts.id=sm.id_time_slot "
            + "inner join user_event ue on ue.id=m.\"userId\" "
            + " where op.id=:O_ID and m.cancelled=0 and m.pending=0 order by ts.start_time ";
    
    // query kde ja v roli userID
    String baseQueryAgent = "select m.id , m.note, o.name, o.description as orgdesc, "
            + "ue.full_name, ue.profile_img_prefix,ue.position, "
            + "sm.id_table, ts.start_time,sm.table_name, "
            + "op.id as profileId, op.title, op.description  "
            + "from b2b.meeting m "
            + "left join b2b.organisation_profile op on op.id=m.\"profileId\" "
            + "inner join b2b.organisation o on o.user_id=m.\"agentId\" "
            + "left join b2b.scheduled_meeting sm on sm.id_meeting=m.id "
            + "left join b2b.time_slot ts on ts.id=sm.id_time_slot "
            + "inner join user_event ue on ue.id=m.\"agentId\" "            
            + "left join b2b.organisation ao on ao.user_id=m.\"userId\""
            + "where ao.id=:ID and m.cancelled=0 and m.pending=0 order by ts.start_time";

    @Override
    public MeetingExportData loadMeetingExportData(Integer meetingId) {
        List<Object[]> meeting = session().createSQLQuery(baseQueryOrganization).setInteger("M_ID", meetingId).list();

        List<MeetingExportData> out = parseData(meeting);
        if (out.isEmpty()) {
            return null;
        } else {
            return out.get(0);
        }
    }

    private List<MeetingExportData> parseData(List<Object[]> meeting) {
        List<MeetingExportData> out = Lists.newArrayList();

        MeetingExportData currentData = null;
        for (Object[] m : meeting) {
            if (currentData == null || currentData.getId().compareTo((Integer) m[0]) != 0) {
                currentData = new MeetingExportData();
                currentData.setId((Integer) m[0]);
                currentData.setNotes(safe(m[1]));
                currentData.setOrganizationTitle(safe(m[2]));
                currentData.setOrganizationDescription(safe(m[3]));
                currentData.setAgentName((String) m[4]);
                currentData.setAgentImageProfilePrefix(safe(m[5]));
                currentData.setAgentPosition(safe(m[6]));
                if (m[7] != null) {
                    currentData.setTableIdx((BigInteger) m[7]);
                    long dateInSeconds = ((BigInteger) m[8]).longValue();
                    currentData.setMeetingTime(new Date(dateInSeconds * 1000));
                    currentData.setTableName(safe(m[9]));
                }
                out.add(currentData);
            }

            // spracovani profilu
            if (m.length > 10 && m[10] != null) {
                currentData.setProfileId((Integer) m[10]);
                currentData.setProfileTitle(safe(m[11]));
                currentData.setProfileDescription(safe(m[12]));
            }
        }
        return out;

    }

    @Override
    public List<MeetingExportData> listMeetingExportData(String userUuid, String date) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<MeetingExportData> listMeetingExportDataForOrganization(Integer id) {
        List<Object[]> meeting = session().createSQLQuery(baseQueryOrganization).setInteger("O_ID", id).list();
        List<MeetingExportData> meetings = parseData(meeting);
        meeting = session().createSQLQuery(baseQueryAgent).setInteger("ID", id).list();
        meetings.addAll(parseData(meeting));
        return meetings;
    }

    private static String safe(Object o) {
        return o == null ? "" : (String) o;
    }

    @Override
    public void insertSchedule(List<ScheduledFrame> scheduledFrames, MeetingFramesGeneratorData mfgd) {
        session().createSQLQuery("delete from b2b.scheduled_meeting").executeUpdate();

        for (ScheduledFrame scheduledFrame : scheduledFrames) {
            for (ScheduledMeeting m : scheduledFrame.getMeetings()) {
                Table t = mfgd.getTableIdToRoom().get(m.getTableId());
                session().
                        createSQLQuery("INSERT INTO b2b.scheduled_meeting(id_meeting, id_table, id_time_slot,table_name,id_room) VALUES (:meeting, :id_table, :time_slot,:table,:room)").
                        setBigInteger("meeting", BigInteger.valueOf(m.getMeetingId())).
                        setBigInteger("id_table", BigInteger.valueOf(t.getIdx())).
                        setBigInteger("time_slot", BigInteger.valueOf(m.getFrameId())).
                        setBigInteger("room", BigInteger.valueOf(t.getRoomId())).
                        setString("table", t.getName()).
                        executeUpdate();
            }
        }
    }
}
