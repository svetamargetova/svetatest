/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.service;

import consys.common.core.exception.NoRecordException;
import consys.event.b2b.core.bo.Organization;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.badges.core.export.BadgeParticipantInfo;
import java.util.List;

/**
 *
 * @author palo
 */
public interface OrganizationService {
    
    
    /**
     * Nacita vsetkych participantov podla Nazvu organizacie
     * 
     * @return 
     */
    public List<OrganizationThumb> listAllOrganizations();
    
    public List<BadgeParticipantInfo> listAllOrganizationAgents();
    
    public Organization loadOrganization(Integer id) throws NoRecordException;
    
    public List<OrganizationExportData> listOrganizationsToExport();
    
}
