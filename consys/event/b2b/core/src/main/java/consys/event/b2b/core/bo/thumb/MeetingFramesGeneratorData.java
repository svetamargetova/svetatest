/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo.thumb;

import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import eu.takeplace.overseer.b2b.MeetingFrame;
import java.util.List;
import java.util.Map;

/**
 *
 * @author palo
 */
public class MeetingFramesGeneratorData {
    
     List<MeetingFrame> meetingFrames = Lists.newArrayList();
     Map<Long,Table> tableIdToRoom = Maps.newHashMap();

    public MeetingFramesGeneratorData() {
    }

    public List<MeetingFrame> getMeetingFrames() {
        return meetingFrames;
    }

    public Map<Long, Table> getTableIdToRoom() {
        return tableIdToRoom;
    }

    public class Table{
        private String name;
        private Integer idx;
        private Integer roomId;

        public Table(String name, Integer idx, Integer roomId) {
            this.name = name;
            this.idx = idx;
            this.roomId = roomId;
        }

        public Integer getRoomId() {
            return roomId;
        }       

        public Integer getIdx() {
            return idx;
        }

        public String getName() {
            return name;
        }
        
        
               
    }
         
     
    
    
}
