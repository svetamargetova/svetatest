/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.dao.jdbc;

import com.google.common.collect.ImmutableList;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.b2b.core.bo.AreaOfActivity;
import consys.event.b2b.core.bo.CooperationProfile;
import consys.event.b2b.core.bo.CooperationProfileType;
import consys.event.b2b.core.bo.Organization;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.b2b.core.dao.OrganizationDao;
import consys.event.badges.core.export.BadgeParticipantInfo;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author palo
 */
public class OrganizationDaoImpl extends GenericDaoImpl<Organization> implements OrganizationDao {

    @Override
    public List<BadgeParticipantInfo> listAllOrganizationAgents() {
        List<Object[]> participants = session().createSQLQuery("SELECT ue.full_name,ue.position, o.\"name\" FROM b2b.organisation as o left join public.user_event ue on ue.id=o.user_id ORDER BY ue.last_name").list();
        List<BadgeParticipantInfo> out = Lists.newArrayList();
        for (Object[] agent : participants) {
            String name = (String) agent[0];
            String position = safe(agent[1]);
            String organization = (String) agent[2];
            if (position.length() > 0) {
                out.add(new BadgeParticipantInfo(name, String.format("%s, %s", position, organization)));
            } else {
                out.add(new BadgeParticipantInfo(name, organization));
            }
        }
        return out;
    }

    @Override
    public List<OrganizationThumb> listAllParticipants() {
        ImmutableList.Builder<OrganizationThumb> builder = ImmutableList.builder();

        List<Object[]> participants = session().createSQLQuery("SELECT o.id,o.\"name\", o.street, o.city, o.zip, o.country, o.phone, o.website, o.logo, ue.full_name,ue.position,ue.profile_img_prefix, op.title FROM b2b.organisation as o left join public.user_event ue on ue.id=o.user_id left join b2b.organisation_profile as op on op.\"organisationId\"=o.id ORDER BY o.\"name\"").list();
        OrganizationThumb thumb = null;
        Integer actualId = null;
        for (Object[] item : participants) {
            if (actualId == null || !((Integer) item[0]).equals(actualId)) {
                actualId = (Integer) item[0];
                thumb = new OrganizationThumb();
                thumb.setOrganizationId(actualId);
                thumb.setOrganizationName((String) item[1]);
                thumb.setOrganizationStreet((String) item[2]);
                thumb.setOrganizationCity((String) item[3]);
                thumb.setOrganizationZip((String) item[4]);
                thumb.setOrganizationCountry((String) item[5]);
                thumb.setOrganizationPhone((String) item[6]);
                thumb.setOrganizationWebsite(safe((String) item[7]));
                thumb.setOrganizationLogo(safe((String) item[8]));
                thumb.setAgentFullname((String) item[9]);
                thumb.setAgentPosition(safe((String) item[10]));
                thumb.setAgentProfileImgPrefix(safe((String) item[11]));
                builder.add(thumb);
            }
            if (item[12] != null) {
                thumb.getProfiles().add(thumb.new CooperationProfile((String) item[12]));
            }
        }

        return builder.build();
    }

    @Override
    public Organization loadOrganizationDetailed(Integer id) throws NoRecordException {

        List<Object[]> ol = session().createSQLQuery("SELECT o.name, o.street, o.city, o.zip, o.country, o.phone, o.website, o.description, o.validated, o.modified, o.logo, o.created, ue.full_name, ue.\"position\", ue.organization, ue.profile_img_prefix, op.id as opId, op.title, op.description as op_description, op.url, op.created as op_created, op.modified as op_modified,  pt.\"typeId\",pt.requested, pt.offered, cpt.title as type_title FROM b2b.organisation as o inner join user_event ue on ue.id=o.user_id left outer join b2b.organisation_profile as op on op.\"organisationId\"=o.id  left outer  join b2b.profile_type  pt on pt.\"profileId\"=op.id left outer join b2b.coop_profile_type cpt on cpt.id=pt.\"typeId\" WHERE o.id=:ID").setInteger("ID", id).list();

        if (ol.isEmpty()) {
            throw new NoRecordException();
        }

        Organization o = new Organization();
        // organizacia
        o.setId(id);
        o.setTitle(safe(ol.get(0)[0]));
        o.setStreet(safe(ol.get(0)[1]));
        o.setCity(safe(ol.get(0)[2]));
        o.setZip(safe(ol.get(0)[3]));
        o.setCountry(safe(ol.get(0)[4]));
        o.setPhone(safe(ol.get(0)[5]));
        o.setWeb(safe(ol.get(0)[6]));
        o.setDescription(safe(ol.get(0)[7]));
        o.setValidatedDate((BigInteger) ol.get(0)[8]);
        o.setModifiedDate((BigInteger) ol.get(0)[9]);
        o.setLogo(safe(ol.get(0)[10]));
        o.setCreatedDate((BigInteger) ol.get(0)[11]);

        // agent
        o.setAgent(new UserEvent());
        o.getAgent().setFullName((String) ol.get(0)[12]);
        o.getAgent().setPosition(safe(ol.get(0)[13]));
        o.getAgent().setOrganization(safe(ol.get(0)[14]));
        o.getAgent().setProfileImagePrefix(safe(ol.get(0)[15]));

        // profily
        List<Integer> profileIds = Lists.newArrayList();

        CooperationProfile cooperationProfile = null;


        for (Object[] item : ol) {
            if (item[16] == null) {
                continue;
            }
            if (cooperationProfile == null || !cooperationProfile.getId().equals(item[16])) {
                cooperationProfile = new CooperationProfile();
                cooperationProfile.setId((Integer) item[16]);
                cooperationProfile.setTitle(safe(item[17]));
                cooperationProfile.setDescription(safe(item[18]));
                cooperationProfile.setWeb(safe(item[19]));
                cooperationProfile.setCreated((BigInteger) item[20]);
                cooperationProfile.setModified((BigInteger) item[21]);
                o.getCooperationProfiles().add(cooperationProfile);
                profileIds.add(cooperationProfile.getId());
            }
            if (item[22] != null) {
                CooperationProfileType type = new CooperationProfileType();
                type.setId((Integer) item[22]);
                type.setRequest((Short) item[23] == 1 ? true : false);
                type.setOffer((Short) item[24] == 1 ? true : false);
                type.setTypeName((String) item[25]);
                cooperationProfile.getCooperationProfileTypes().add(type);
            }

        }

        // donacitame data ke kooperacnemu profilu - TAGY
        if (!profileIds.isEmpty()) {
            List<Object[]> tags = session().createSQLQuery("select \"profileId\",tag from b2b.profile_tag where \"profileId\" in ( :PIDS )").setParameterList("PIDS", profileIds).list();
            for (Object[] ptag : tags) {
                for (CooperationProfile cp : o.getCooperationProfiles()) {
                    if (cp.getId().equals(ptag[0])) {
                        cp.getTags().add((String) ptag[1]);
                    }
                }
            }
        }


        // donacitama data k organzacii - standard, tag
        List<Object[]> st = session().createSQLQuery("select s.standard,t.tag from b2b.organisation_standard as s, b2b.organisation_tag as t where s.\"organisationId\"=:ID and t.\"organisationId\"=:ID").setInteger("ID", o.getId()).list();
        for (Object[] s : st) {
            if (s[0] != null && !o.getStandarts().contains((String) s[0])) {
                o.getStandarts().add((String) s[0]);
            }

            if (s[1] != null && !o.getTags().contains((String) s[1])) {
                o.getTags().add((String) s[1]);
            }
        }

        // areas - parent + childs
        AreaOfActivity parent = null;
        List<Object[]> areas = session().createSQLQuery("select b.id, b.title ,a.id as cid,a.title as ctitle from b2b.organisation_area oa, b2b.areas a inner join  b2b.areas as b on a.id_parent=b.id where oa.\"organisationId\"=:ID and oa.\"areaId\"=a.id order by b.title,a.title").setInteger("ID", o.getId()).list();
        for (Object[] area : areas) {

            if (parent == null || !parent.getId().equals(area[0])) {
                parent = new AreaOfActivity();
                parent.setId((Integer) area[0]);
                parent.setTitle(safe(area[1]));
                o.getAreas().add(parent);
            }
            AreaOfActivity child = new AreaOfActivity();
            child.setId((Integer) area[2]);
            child.setTitle(safe(area[3]));
            parent.getChildrens().add(child);
        }

        return o;
    }

    @Override
    public Organization load(Long id) throws NoRecordException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private static String safe(Object o) {
        return o == null ? "" : (String) o;
    }

    @Override
    public List<OrganizationExportData> listOrganizationExportData() {
        List<Object[]> ol = session().createSQLQuery("SELECT o.id,o.name, o.street, o.city, o.zip, o.country, o.phone, o.website, o.description, o.logo, ue.full_name, ue.\"position\", ue.organization, ue.profile_img_prefix, op.id as opId, op.title, op.description as op_description, op.url, op.created as op_created, op.modified as op_modified,  pt.\"typeId\",pt.requested, pt.offered, cpt.title as type_title FROM b2b.organisation as o inner join user_event ue on ue.id=o.user_id left outer join b2b.organisation_profile as op on op.\"organisationId\"=o.id  left outer  join b2b.profile_type  pt on pt.\"profileId\"=op.id left outer join b2b.coop_profile_type cpt on cpt.id=pt.\"typeId\" order by o.name").list();

        List<OrganizationExportData> out = Lists.newArrayList();

        if (ol.isEmpty()) {
            return out;
        }

        OrganizationExportData currentOrganization = null;
        CooperationProfile cooperationProfile = null;

        for (Object[] org : ol) {

            if (currentOrganization == null || currentOrganization.getId().compareTo((Integer) org[0]) != 0) {
                currentOrganization = new OrganizationExportData();
                // organizacia
                currentOrganization.setId((Integer) org[0]);
                currentOrganization.setTitle(safe(org[1]));
                currentOrganization.setStreet(safe(org[2]));
                currentOrganization.setCity(safe(org[3]));
                currentOrganization.setZip(safe(org[4]));
                currentOrganization.setCountry(safe(org[5]));
                currentOrganization.setPhone(safe(org[6]));
                currentOrganization.setWeb(safe(org[7]));
                currentOrganization.setDescription(safe(org[8]));
                currentOrganization.setLogo(safe(org[9]));


                // agent
                currentOrganization.setAgent(new UserEvent());
                currentOrganization.getAgent().setFullName((String) org[10]);
                currentOrganization.getAgent().setPosition(safe(org[11]));
                currentOrganization.getAgent().setOrganization(safe(org[12]));
                currentOrganization.getAgent().setProfileImagePrefix(safe(org[13]));

                // profily                
                cooperationProfile = null;

                // areas - parent + childs
                AreaOfActivity parent = null;
                List<Object[]> areas = session().createSQLQuery("select b.id, b.title ,a.id as cid,a.title as ctitle from b2b.organisation_area oa, b2b.areas a inner join  b2b.areas as b on a.id_parent=b.id where oa.\"organisationId\"=:ID and oa.\"areaId\"=a.id order by b.title,a.title").setInteger("ID", currentOrganization.getId()).list();
                for (Object[] area : areas) {
                    if (parent == null || !parent.getId().equals(area[0])) {
                        parent = new AreaOfActivity();
                        parent.setId((Integer) area[0]);
                        parent.setTitle(safe(area[1]));
                        currentOrganization.getAreas().add(parent);
                    }
                    AreaOfActivity child = new AreaOfActivity();
                    child.setId((Integer) area[2]);
                    child.setTitle(safe(area[3]));
                    parent.getChildrens().add(child);
                }

                out.add(currentOrganization);

            }

            if (org[14] == null) {
                continue;
            }
            if (cooperationProfile == null || !cooperationProfile.getId().equals(org[14])) {
                cooperationProfile = new CooperationProfile();
                cooperationProfile.setId((Integer) org[14]);
                cooperationProfile.setTitle(safe(org[15]));
                cooperationProfile.setDescription(safe(org[16]));
                cooperationProfile.setWeb(safe(org[17]));
                cooperationProfile.setCreated((BigInteger) org[18]);
                cooperationProfile.setModified((BigInteger) org[19]);
                currentOrganization.getCooperationProfiles().add(cooperationProfile);
            }

            if (org[20] != null) {
                CooperationProfileType type = new CooperationProfileType();
                type.setId((Integer) org[20]);
                type.setRequest((Short) org[21] == 1 ? true : false);
                type.setOffer((Short) org[22] == 1 ? true : false);
                type.setTypeName((String) org[23]);
                cooperationProfile.getCooperationProfileTypes().add(type);
            }


        }

        List<OrganizationExportData> rout = Lists.newArrayList();

        for (OrganizationExportData o : out) {
            boolean alreadyThere = false;
            for (OrganizationExportData a : rout) {
                if (a.getTitle().equalsIgnoreCase(o.getTitle())) {
                    alreadyThere = true;
                    break;
                }
            }
            if (!alreadyThere) {
                rout.add(o);
            }
        }
        return rout;
    }
}
