/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.service;

import consys.event.b2b.core.bo.thumb.MeetingExportData;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author palo
 */
public interface MeetingService {
    
    
     public void generateSchedule();
     
     public MeetingExportData loadMeetingExportData(BigInteger meetingId);
     
     public List<MeetingExportData> listMeetingExportData(String uuid, String date);
     
     public List<MeetingExportData> listMeetingExportDataForOrganization(Integer id);
    
}
