/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class AreaOfActivity {
    private Integer id;
    private String title;
    private List<AreaOfActivity> childrens;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the childrens
     */
    public List<AreaOfActivity> getChildrens() {
        if(childrens == null){
            childrens = new ArrayList<AreaOfActivity>();
        }
        return childrens;
    }

    /**
     * @param childrens the childrens to set
     */
    public void setChildrens(List<AreaOfActivity> childrens) {
        this.childrens = childrens;
    }
    
    
}
