/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author palo
 */
public class CooperationProfile implements ConsysObject{
    
    private Integer id;
    private String title;
    private String description;
    private String web;
    private BigInteger created;
    private BigInteger modified;    
    private List<String> tags;
    private List<CooperationProfileType> cooperationProfileTypes;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the created
     */
    public BigInteger getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(BigInteger created) {
        this.created = created;
    }

    /**
     * @return the modified
     */
    public BigInteger getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(BigInteger modified) {
        this.modified = modified;
    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        if(tags == null){
            tags = Lists.newArrayList();
        }
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the cooperationProfileTypes
     */
    public List<CooperationProfileType> getCooperationProfileTypes() {
        if(cooperationProfileTypes == null){
            cooperationProfileTypes = Lists.newArrayList();
        }
        return cooperationProfileTypes;
    }

    /**
     * @param cooperationProfileTypes the cooperationProfileTypes to set
     */
    public void setCooperationProfileTypes(List<CooperationProfileType> cooperationProfileTypes) {
        this.cooperationProfileTypes = cooperationProfileTypes;
    }
    
    
}
