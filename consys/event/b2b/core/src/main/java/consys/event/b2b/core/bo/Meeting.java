/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.core.bo;

import consys.common.core.bo.ConsysObject;
import java.math.BigInteger;

/**
 *
 * @author palo
 */
public class Meeting implements ConsysObject{
    private BigInteger id;
    private BigInteger fromAgent;
    private BigInteger toAgent;

    /**
     * @return the id
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(BigInteger id) {
        this.id = id;
    }

    /**
     * @return the fromAgent
     */
    public BigInteger getFromAgent() {
        return fromAgent;
    }

    /**
     * @param fromAgent the fromAgent to set
     */
    public void setFromAgent(BigInteger fromAgent) {
        this.fromAgent = fromAgent;
    }

    /**
     * @return the toAgent
     */
    public BigInteger getToAgent() {
        return toAgent;
    }

    /**
     * @param toAgent the toAgent to set
     */
    public void setToAgent(BigInteger toAgent) {
        this.toAgent = toAgent;
    }
    
    
    
}
