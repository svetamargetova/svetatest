package consys.event.b2b.ws;


import consys.event.b2b.ws.bo.GenerateScheduleRequest;
import consys.event.b2b.ws.bo.GenerateScheduleResponse;
import consys.event.b2b.ws.bo.ObjectFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class B2BWebServiceEndpoint {

    public final static String NAMESPACE = "http://takeplace.eu/overseer/b2b";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();


    @PayloadRoot(localPart = "GenerateScheduleRequest", namespace = NAMESPACE)
    public GenerateScheduleResponse createEvent(GenerateScheduleRequest request) {
        GenerateScheduleResponse response = OBJECT_FACTORY.createGenerateScheduleResponse();
        
        return response;
    }




}
