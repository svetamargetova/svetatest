package consys.event.b2b.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.b2b.core.bo.AreaOfActivity;
import consys.event.b2b.core.bo.CooperationProfile;
import consys.event.b2b.core.bo.CooperationProfileType;
import consys.event.b2b.core.bo.Organization;
import consys.event.b2b.core.service.OrganizationService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrganizationDetailActionHandler implements ActionHandler {

    @Autowired
    private OrganizationService participantService;

    @Override
    public String getActionName() {
        return "b2b-organizationDetail";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        Integer oid = action.getIntParam("oid");
        try {

            Organization o = participantService.loadOrganization(oid);            
            result.writeStringField("name", o.getTitle());
            result.writeStringField("description", o.getDescription());
            result.writeStringField("city", o.getCity());
            result.writeStringField("country", o.getCountry());
            result.writeStringField("street", o.getStreet());
            result.writeStringField("zip", o.getZip());
            result.writeStringField("phone", o.getPhone());
            result.writeStringField("web", o.getWeb());
            result.writeStringField("logo", o.getLogo());
            result.writeNumberField("createDate", o.getCreatedDate().longValue());
            result.writeNumberField("modifiedDate", o.getModifiedDate().longValue());
            result.writeNumberField("validatedDate", o.getValidatedDate().longValue());
            result.writeStringField("agentName", o.getAgent().getFullName());
            result.writeStringField("agentPosition", o.getAgent().getPosition());
            result.writeStringField("agentImagePrefix", o.getAgent().getProfileImagePrefix());



            // Areas of activity
            result.writeArrayFieldStart("areasOfActivity");
            for (AreaOfActivity parent : o.getAreas()) {
                result.writeStartObject();
                result.writeStringField("title", parent.getTitle());
                
                result.writeArrayFieldStart("childrens");
                for (AreaOfActivity child : parent.getChildrens()) {
                    result.writeString(child.getTitle());
                }
                result.writeEndArray();
                
                result.writeEndObject();
            }
            result.writeEndArray();

            // Tags
            result.writeArrayFieldStart("tags");
            for (String tag : o.getTags()) {
                result.writeString(tag);
            }
            result.writeEndArray();

            // Standarts
            result.writeArrayFieldStart("standarts");
            for (String tag : o.getStandarts()) {
                result.writeString(tag);
            }
            result.writeEndArray();

            // Profiles
            result.writeArrayFieldStart("profiles");
            for (CooperationProfile profile : o.getCooperationProfiles()) {
                result.writeStartObject();
                result.writeStringField("title", profile.getTitle());
                result.writeStringField("description", profile.getDescription());
                result.writeStringField("web", profile.getWeb());
                result.writeNumberField("createDate", profile.getCreated().longValue());
                result.writeNumberField("modifiedDate", profile.getModified().longValue());
                // Tagy
                result.writeArrayFieldStart("tags");
                for (String tag : profile.getTags()) {
                    result.writeString(tag);
                }
                result.writeEndArray();
                
                // Kooperacne typy
                result.writeArrayFieldStart("cooperationTypes");
                for (CooperationProfileType type : profile.getCooperationProfileTypes()) {
                    result.writeStartObject();
                    result.writeStringField("name", type.getTypeName());
                    result.writeBooleanField("offer", type.isOffer());
                    result.writeBooleanField("request", type.isRequest());                    
                    result.writeEndObject();
                }
                result.writeEndArray();

                result.writeEndObject();
            }
            result.writeEndArray();


            // --- END Organization

            




        } catch (NoRecordException ex) {
            result.writeError(1, "No organization with id=" + oid);
        }
    }
}
