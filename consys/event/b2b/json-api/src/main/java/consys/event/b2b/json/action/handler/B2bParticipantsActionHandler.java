package consys.event.b2b.json.action.handler;

import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.b2b.core.service.OrganizationService;
import consys.event.b2b.json.result.ParticipantResult;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class B2bParticipantsActionHandler implements ActionHandler{
    
    @Autowired
    private OrganizationService participantService;

    @Override
    public String getActionName() {
        return "b2b-participants";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        List<OrganizationThumb> participants =  participantService.listAllOrganizations();        
        ParticipantResult.writePublicParticipants(result, participants);
    }
}
