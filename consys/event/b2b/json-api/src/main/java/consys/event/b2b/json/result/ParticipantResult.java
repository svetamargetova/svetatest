package consys.event.b2b.json.result;

import consys.common.json.Result;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import java.io.IOException;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantResult {


    public static void writePublicParticipants(Result result, List<OrganizationThumb> items) throws IOException{
        result.writeArrayFieldStart("participants");
        for (OrganizationThumb item : items) {
            result.writeStartObject();
            result.writeNumberField("organizationId", item.getOrganizationId());
            result.writeStringField("organizationName", item.getOrganizationName());
            result.writeStringField("organizationStreet", item.getOrganizationStreet());
            result.writeStringField("organizationCity", item.getOrganizationCity());
            result.writeStringField("organizationCountry", item.getOrganizationCountry());
            result.writeStringField("organizationZip", item.getOrganizationZip());
            result.writeStringField("organizationWebsite", item.getOrganizationWebsite());
            result.writeStringField("organizationLogo", item.getOrganizationLogo());
            result.writeStringField("organizationPhone", item.getOrganizationPhone());
            result.writeStringField("agentName", item.getAgentFullname());
            result.writeStringField("agentPosition", item.getAgentPosition());
            result.writeStringField("agentProfileImgPrefix", item.getAgentProfileImgPrefix());            
            result.writeArrayFieldStart("profiles");
            for(OrganizationThumb.CooperationProfile profile : item.getProfiles()){
                result.writeStartObject();
                result.writeStringField("name",profile.getName());
                result.writeEndObject();
            }            
            result.writeEndArray();
            result.writeEndObject();
        }
        result.writeEndArray();
    }   
}
