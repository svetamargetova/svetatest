package consys.event.scheduling.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import consys.event.common.core.bo.UserEvent;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.bo.UserEntityDetails;
import consys.event.scheduling.core.bo.thumb.EntityThumb;
import consys.event.scheduling.core.service.EntityService;
import consys.event.scheduling.core.service.FrameService;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ScheduleActionHandler implements ActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleActionHandler.class);
    @Autowired
    private FrameService frameService;
    @Autowired
    private EntityService entityService;

    @Override
    public String getActionName() {
        return "schedule";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        try {
            List<Room> rooms = frameService.listRooms();
            result.writeArrayFieldStart("rooms");
            for (Room room : rooms) {
                result.writeStartObject();
                result.writeNumberField("room-id", room.getId());
                result.writeStringField("name", room.getName());
                result.writeStringField("description", room.getDescription());
                result.writeEndObject();
            }
            result.writeEndArray();
        } catch (NoRecordException ex) {
            ErrorCode.error(result, ex, "There is are no rooms created yet.");
            return;
        }


        try {
            List<TimeFrameBlock> blocks = frameService.listTimeFrameBlocksDetailed();
            result.writeArrayFieldStart("blocks");
            for (TimeFrameBlock block : blocks) {
                result.writeStartObject();
                result.writeNumberField("block-id", block.getId());
                result.writeNumberField("room-id", block.getRoom().getId());
                result.writeDateField("date", block.getTimeFrame().getDay());
                result.writeStringField("chair-name", block.getChair() != null ? block.getChair().getFullName() : null);
                result.writeStringField("chair-uuid", block.getChair() != null ? block.getChair().getUuid() : null);
                result.writeStringField("chair-organization", block.getChair() != null ? block.getChair().getOrganization() : null);
                result.writeStringField("section-name",block.getSection() != null ? block.getSection().getName():null);
                result.writeNumberField("from-time", block.getTimeFrom());
                result.writeNumberField("to-time", block.getTimeTo());
                result.writeEndObject();
            }
            result.writeEndArray();
        } catch (NoRecordException ex) {
            logger.debug("No timeframe created?");
            return;
        }
        try {
            List<EntityThumb> list = entityService.listAllEntitiesWithDetail();
            result.writeArrayFieldStart("entities");
            for (EntityThumb entity : list) {
                result.writeStartObject();
                result.writeStringField("uuid", entity.getUuid());
                result.writeStringField("name", entity.getTitle());
                result.writeStringField("description", entity.getDescription());
                result.writeStringField("type", entity.getType().name());
                result.writeStringField("topic", entity.getTags());
                result.writeStringField("subtitle", entity.getSubtitle());
                result.writeNumberField("block-id", entity.getBlockId());
                result.writeNumberField("offset", entity.getBlockOffset());
                result.writeNumberField("length", entity.getEntityLength());
                result.writeArrayFieldStart("contributor");
                if (entity.getContributors() != null) {
                    for (UserEvent ue : entity.getContributors()) {
                        result.writeStartObject();
                        result.writeStringField("name", ue.getFullName());
                        result.writeStringField("organization", ue.getOrganization());
                        result.writeStringField("uuid", ue.getUuid());
                        result.writeEndObject();
                    }
                }
                result.writeEndArray();
                result.writeEndObject();
            }
            result.writeEndArray();

        } catch (NoRecordException ex) {
            logger.debug("No entitites created?");
            return;
        }
        try {
            List<UserEntityDetails> details = entityService.listUserEntityDetails(action.getUserUuid());
            result.writeArrayFieldStart("user-entity-details");
            for (UserEntityDetails ued : details) {
                result.writeStartObject();
                result.writeStringField("uuid", ued.getEntity().getUuid());
                result.writeBooleanField("favourite", ued.isFavourite());
                result.writeStringField("note", ued.getNote());
                result.writeEndObject();
            }
            result.writeEndArray();
        } catch (NoRecordException ex) {
            logger.debug("No favourites created?");
            return;
        } catch (RequiredPropertyNullException ex) {
            return;
        }





    }
}
