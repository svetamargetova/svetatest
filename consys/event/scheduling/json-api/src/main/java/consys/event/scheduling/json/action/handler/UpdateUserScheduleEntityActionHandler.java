package consys.event.scheduling.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import consys.event.scheduling.core.service.EntityService;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateUserScheduleEntityActionHandler implements ActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(UpdateUserScheduleEntityActionHandler.class);

    @Autowired
    private EntityService entityService;

    @Override
    public String getActionName() {
        return "update-user-schedule-entity";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        try {
            boolean favourite = action.getBooleanParam("favourite");
            String note = action.getStringParam("note");
            String entity = action.getStringParam("entity-uuid");
            entityService.updateUserEntityDetails(action.getUserUuid(), entity, favourite, note);
            result.writeBooleanField("success", true);
        } catch (NoRecordException ex) {
            ErrorCode.error(result, ex, "There is no entity or user with given uuids");
        } catch (RequiredPropertyNullException ex) {
            ErrorCode.error(result, ex, "entity-uuid");
        } catch (ServiceExecutionFailed ex) {
            ErrorCode.error(result, ex, "User not exits");
        }
    }
}
