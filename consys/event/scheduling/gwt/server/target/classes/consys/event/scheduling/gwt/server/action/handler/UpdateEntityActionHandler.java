package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.UpdateEntityAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateEntityActionHandler implements EventActionHandler<UpdateEntityAction, VoidResult> {

    // servica
    @Autowired
    private SchedulingService schedulingService;
    @Autowired
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Override
    public Class<UpdateEntityAction> getActionType() {
        return UpdateEntityAction.class;
    }

    @Override
    public VoidResult execute(UpdateEntityAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isInUserSection(action.getEntityId(), action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            schedulingService.updateEntity(action.getEntityId(), action.getLength(), action.getRoomsId());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (IllegalPropertyValue ex) {
            throw new BadInputException();
        }
    }
}
