package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadSectionActionHander implements EventActionHandler<LoadSectionAction, ClientSection> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<LoadSectionAction> getActionType() {
        return LoadSectionAction.class;
    }

    @Override
    public ClientSection execute(LoadSectionAction action) throws ActionException {
        try {
            Section section = schedulingSettingsService.loadSection(action.getId());

            ClientSection cs = new ClientSection();
            cs.setId(section.getId());
            cs.setName(section.getName());
            cs.setBgColor(section.getBgColor());
            cs.setFgColor(section.getFgColor());
            cs.setFontColor(section.getFontColor());

            List<String> uuids = schedulingSettingsService.listSectionRights(section.getId());
            cs.getAuthorizedUsers().addAll(uuids);

            return cs;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
