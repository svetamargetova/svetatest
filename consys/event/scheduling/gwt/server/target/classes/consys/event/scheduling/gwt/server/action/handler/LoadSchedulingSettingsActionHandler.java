package consys.event.scheduling.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadSchedulingSettingsAction;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadSchedulingSettingsActionHandler implements EventActionHandler<LoadSchedulingSettingsAction, ArrayListResult<ClientDayThumb>> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<LoadSchedulingSettingsAction> getActionType() {
        return LoadSchedulingSettingsAction.class;
    }

    @Override
    public ArrayListResult<ClientDayThumb> execute(LoadSchedulingSettingsAction action) throws ActionException {
        ArrayList<ClientDayThumb> result = new ArrayList<ClientDayThumb>();
        List<TimeFrame> days = schedulingSettingsService.listTimeFrames();
        for (TimeFrame tf : days) {
            result.add(new ClientDayThumb(tf.getId(), tf.getDay()));
        }
        return new ArrayListResult(result);
    }
}
