package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverview;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadTalkOverviewActionHandler implements EventActionHandler<LoadTalkOverviewAction, ClientSectionOverview> {

    // servica
    private SchedulingService schedulingService;

    @Autowired(required = true)
    public void setSchedulingService(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @Override
    public Class<LoadTalkOverviewAction> getActionType() {
        return LoadTalkOverviewAction.class;
    }

    @Override
    public ClientSectionOverview execute(LoadTalkOverviewAction action) throws ActionException {
        try {
            List<EntitySubmission> ess = schedulingService.listSubmissionsBySectionId(action.getSectionId());

            ArrayList<String> talks = new ArrayList<String>();
            for (EntitySubmission es : ess) {
                talks.add(es.getSubmission().getTitle());
            }

            ClientSectionOverview overview = new ClientSectionOverview();
            overview.setTalks(talks);
            return overview;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
