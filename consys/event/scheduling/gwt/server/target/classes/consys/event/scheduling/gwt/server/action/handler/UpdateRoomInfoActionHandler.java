package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.UpdateRoomInfoAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateRoomInfoActionHandler implements EventActionHandler<UpdateRoomInfoAction, VoidResult> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<UpdateRoomInfoAction> getActionType() {
        return UpdateRoomInfoAction.class;
    }

    @Override
    public VoidResult execute(UpdateRoomInfoAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            ClientRoomInfo cr = action.getRoom();
            Room room = new Room();
            room.setId(cr.getId());
            room.setName(cr.getName());
            schedulingSettingsService.updateRoom(room);
            return new VoidResult();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
