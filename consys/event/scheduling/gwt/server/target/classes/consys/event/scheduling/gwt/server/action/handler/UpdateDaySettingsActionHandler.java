package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.UpdateDaySettingsAction;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateDaySettingsActionHandler implements EventActionHandler<UpdateDaySettingsAction, VoidResult> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;
    // data
    private TimeFrame tf;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<UpdateDaySettingsAction> getActionType() {
        return UpdateDaySettingsAction.class;
    }

    @Override
    public VoidResult execute(UpdateDaySettingsAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            tf = schedulingSettingsService.loadTimeFrame(action.getId());
            final int oldFrom = tf.getTimeFrom();
            final int oldTo = tf.getTimeTo();

            tf.setDay(action.getDate());
            tf.setTimeFrom(action.getFrom().getTime());
            tf.setTimeTo(action.getTo().getTime());
            schedulingSettingsService.updateTimeFrame(tf);

            // kontrola splitu (presah a unikatnost) + serazeni pro lepsi praci
            validateSplits(tf.getTimeFrom(), tf.getTimeTo(), action.getSplits());

            // zjisteni stavu mistnosti (pridane, odebrane, stavajici)
            final ArrayList<Long> roomIds = action.getRooms();
            List<TimeFrameBlock> blocksToRemove = new ArrayList<TimeFrameBlock>();
            Set<Long> newRooms = new HashSet<Long>();
            Set<Long> oldRooms = new HashSet<Long>();
            Map<Integer, List<TimeFrameBlock>> mapSplits = new HashMap<Integer, List<TimeFrameBlock>>();
            checkRoomsAndBlocks(newRooms, oldRooms, roomIds, mapSplits, blocksToRemove);

            final boolean changedOnlyRooms = checkChangedOnlyRooms(oldFrom, oldTo, action.getSplits(), mapSplits);

            // TODO: zkontrolovat jestli nebyly nektere bloky rozdeleny (slouceny) -> smazat puvodni (reset entit) a vytvorit nove
            // TODO: zatim bez drbacky zrusit vsechno a nagenerovat znovu
            if (!changedOnlyRooms) {
                for (List<TimeFrameBlock> tfbs : mapSplits.values()) {
                    blocksToRemove.addAll(tfbs);
                }
            }

            // smazani bloku odebranych mistnosti a presahujicich bloku (+ reset entit co v nich byly)
            schedulingSettingsService.resetEntityForBlocks(blocksToRemove);
            schedulingSettingsService.deleteTimeFrameBlocks(blocksToRemove);

            // TODO: az bude s drbackou tak smazat, protoze se bude upravovat vzdy jen cast bloku
            if (!changedOnlyRooms) {
                generateAllBlocksToRooms(oldRooms, action.getSplits());
            }

            // pridat bloky do pridanych mistnosti
            generateAllBlocksToRooms(newRooms, action.getSplits());

            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (IllegalPropertyValue ex) {
            throw new BadInputException();
        }
    }

    /** validace splitu na unikatnost a presah + serazeni pro lepsi praci */
    private void validateSplits(final int from, final int to, List<ClientTime> splits) throws BadInputException {
        List<Integer> validatedTime = new ArrayList<Integer>();
        for (ClientTime ct : splits) {
            final int time = ct.getTime();
            if (time <= from || time >= to) {
                throw new BadInputException();
            }
            if (validatedTime.contains(ct.getTime())) {
                throw new BadInputException();
            }
            validatedTime.add(ct.getTime());
        }

        // serazeni podle casu, aby se daly snadneji generovat bloky
        Collections.sort(splits, new Comparator<ClientTime>() {

            @Override
            public int compare(ClientTime o1, ClientTime o2) {
                return o1.getTime() - o2.getTime();
            }
        });
    }

    /** zjisti stav mistnosti a bloku */
    private void checkRoomsAndBlocks(Set<Long> newRooms, Set<Long> oldRooms, ArrayList<Long> roomIds,
            Map<Integer, List<TimeFrameBlock>> mapSplits, List<TimeFrameBlock> blocksToRemove) {
        List<TimeFrameBlock> blocks = new ArrayList<TimeFrameBlock>();
        try {
            blocks = schedulingSettingsService.listTimeFrameBlocks(tf.getId());
        } catch (NoRecordException ex) {
            // casovy ramec nema prirazeny zadne mistnosti
        }

        newRooms.addAll(roomIds);

        for (TimeFrameBlock tfb : blocks) {
            final Long roomId = tfb.getRoom().getId();
            if (roomIds.contains(roomId)) {
                // stavajici mistnost - pridani do mapy splitu
                List<TimeFrameBlock> list = mapSplits.get(tfb.getTimeFrom());
                if (list == null) {
                    list = new ArrayList<TimeFrameBlock>();
                    mapSplits.put(tfb.getTimeFrom(), list);
                }
                list.add(tfb);
                oldRooms.add(roomId);
            } else {
                // zrusena mistnost
                blocksToRemove.add(tfb);
            }
            newRooms.remove(roomId);
        }
    }

    /** zkontroluje jestli se provadely jen zmeny pridani/ubrani mistnosti, ale casy a splity zustaly stejne  */
    private boolean checkChangedOnlyRooms(int oldFrom, int oldTo, ArrayList<ClientTime> splits, Map<Integer, List<TimeFrameBlock>> mapSplits) {
        if (tf.getTimeFrom() != oldFrom || tf.getTimeTo() != oldTo) {
            // zmenil se cas zacatku nebo konce
            return false;
        }
        Set<Integer> keys = new HashSet(mapSplits.keySet());
        for (ClientTime ct : splits) {
            if (!keys.contains(ct.getTime())) {
                // zmeneny splity
                return false;
            }
            keys.remove(ct.getTime());
        }
        if (keys.size() == 1 && keys.contains(oldFrom)) {
            return true;
        } else {
            // zmeneny splity
            return false;
        }
    }

    /** vygeneruje vsechny bloky do mistnosti */
    private void generateAllBlocksToRooms(Set<Long> roomIds, ArrayList<ClientTime> splits)
            throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue {
        for (Long roomId : roomIds) {
            Room room = schedulingSettingsService.loadRoom(roomId);

            int startTime = tf.getTimeFrom();
            for (ClientTime ct : splits) {
                TimeFrameBlock block = new TimeFrameBlock();
                block.setRoom(room);
                block.setTimeFrame(tf);
                block.setTimeFrom(startTime);
                block.setTimeTo(ct.getTime());
                schedulingSettingsService.createTimeFrameBlock(block);

                startTime = ct.getTime();
            }
            TimeFrameBlock block = new TimeFrameBlock();
            block.setRoom(room);
            block.setTimeFrame(tf);
            block.setTimeFrom(startTime);
            block.setTimeTo(tf.getTimeTo());
            schedulingSettingsService.createTimeFrameBlock(block);
        }
    }
}
