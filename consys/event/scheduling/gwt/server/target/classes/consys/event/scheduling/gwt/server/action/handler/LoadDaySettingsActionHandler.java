package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadDaySettingsAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadDaySettingsActionHandler implements EventActionHandler<LoadDaySettingsAction, ClientDay> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<LoadDaySettingsAction> getActionType() {
        return LoadDaySettingsAction.class;
    }

    @Override
    public ClientDay execute(LoadDaySettingsAction action) throws ActionException {
        ClientDay cd = new ClientDay();
        try {
            TimeFrame tf = schedulingSettingsService.loadTimeFrame(action.getId());
            cd.setId(action.getId());
            cd.setDate(tf.getDay());
            cd.setFrom(new ClientTime(tf.getTimeFrom()));
            cd.setTo(new ClientTime(tf.getTimeTo()));

            ArrayList<ClientRoomInfo> rooms = new ArrayList<ClientRoomInfo>();
            List<Room> rms = schedulingSettingsService.listRoomsForTimeFrame(tf.getId());
            for (Room r : rms) {
                ClientRoomInfo room = new ClientRoomInfo();
                room.setId(r.getId());
                room.setName(r.getName());
                rooms.add(room);
            }
            cd.setRooms(rooms);

            ArrayList<ClientTime> splits = new ArrayList<ClientTime>();
            try {
                List<TimeFrameBlock> blks = schedulingSettingsService.listTimeFrameBlocks(tf.getId());
                Set<Integer> bks = new HashSet<Integer>();
                // vytvoreni unikatnich splitu
                for (TimeFrameBlock b : blks) {
                    if (tf.getTimeFrom() != b.getTimeFrom()) {
                        bks.add(b.getTimeFrom());
                    }
                }
                // vytvoreni seznamu splitu
                for (Integer i : bks) {
                    splits.add(new ClientTime(i));
                }
            } catch (NoRecordException ex) {
                // nenastaveny zadne mistnosti, takze nejsou ani bloky
            }
            cd.setSplits(splits);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
        return cd;
    }
}
