package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.CreateSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateSectionActionHandler implements EventActionHandler<CreateSectionAction, ArrayListResult<Long>> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<CreateSectionAction> getActionType() {
        return CreateSectionAction.class;
    }

    @Override
    public ArrayListResult<Long> execute(CreateSectionAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            ArrayList<Long> result = new ArrayList<Long>();
            for (String name : action.getNames()) {
                Section section = new Section();
                section.setName(name);
                section.setBgColor(ClientSection.DEFAULT_BG_COLOR);
                section.setFgColor(ClientSection.DEFAULT_FG_COLOR);

                schedulingSettingsService.createSection(section);
                result.add(section.getId());
            }
            return new ArrayListResult(result);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
