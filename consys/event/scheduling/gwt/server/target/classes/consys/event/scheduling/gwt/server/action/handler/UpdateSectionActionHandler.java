package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.UpdateSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateSectionActionHandler implements EventActionHandler<UpdateSectionAction, VoidResult> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<UpdateSectionAction> getActionType() {
        return UpdateSectionAction.class;
    }

    @Override
    public VoidResult execute(UpdateSectionAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            ClientSection cs = action.getSection();
            Section section = schedulingSettingsService.loadSection(cs.getId());
            section.setBgColor(cs.getBgColor());
            section.setFgColor(cs.getFgColor());
            section.setFontColor(cs.getFontColor());

            List<UserEvent> ues = schedulingSettingsService.listUserEventByUuids(cs.getAuthorizedUsers());
            section.getPermittedUsers().clear();
            section.getPermittedUsers().addAll(ues);

            schedulingSettingsService.updateSection(section);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
