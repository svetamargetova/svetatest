package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.UuidProvider;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.UpdateEntityPositionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateEntityPositionActionHandler implements EventActionHandler<UpdateEntityPositionAction, LongResult> {

    // servica
    private SchedulingService schedulingService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingService(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<UpdateEntityPositionAction> getActionType() {
        return UpdateEntityPositionAction.class;
    }

    @Override
    public LongResult execute(UpdateEntityPositionAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isInUserSection(action.getBlockId(), action.getEntityId(), action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }
            
            if (!action.isBuffer()) {
                // pohyb v rozvrhu
                Long entityId = action.getEntityId();
                if (entityId == null) {
                    // jde o novy blind
                    TimeFrameBlock tfb = schedulingService.loadTimeFrameBlock(action.getBlockId());

                    EntityBlind eb = new EntityBlind();
                    eb.setFromMinutesInBlock(action.getTime().getTime() - tfb.getTimeFrom());
                    eb.setLengthMinutes(30);
                    eb.setTimeFrameBlock(tfb);
                    eb.setTitle(action.getBlindName());
                    eb.setUuid(UuidProvider.getUuid());
                    entityId = schedulingService.createEntityBlind(eb);
                }
                schedulingService.updateEntityPosition(entityId, action.getBlockId(), action.getTime().getTime());
                return new LongResult(entityId);
            } else {
                // presunuti do bufferu
                schedulingService.updateEntityBufferPosition(action.getEntityId());
                return new LongResult(action.getEntityId());
            }
        } catch (IllegalPropertyValue ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
