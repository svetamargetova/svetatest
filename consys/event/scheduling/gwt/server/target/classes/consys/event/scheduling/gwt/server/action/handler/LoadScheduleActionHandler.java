package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadScheduleAction;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import consys.event.scheduling.gwt.client.bo.ClientSchedule;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadScheduleActionHandler implements EventActionHandler<LoadScheduleAction, ClientSchedule> {

    // servica
    private SchedulingService schedulingService;
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingService(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<LoadScheduleAction> getActionType() {
        return LoadScheduleAction.class;
    }

    @Override
    public ClientSchedule execute(LoadScheduleAction action) throws ActionException {
        ClientSchedule cs = new ClientSchedule();

        // nacteni seznamu ramcu dnu
        List<TimeFrame> frames = schedulingSettingsService.listTimeFrames();
        ArrayList<ClientDayThumb> clientDays = new ArrayList<ClientDayThumb>();
        for (TimeFrame tf : frames) {
            clientDays.add(new ClientDayThumb(tf.getId(), tf.getDay()));
        }
        cs.setFrames(clientDays);

        // nacist id prav podle uzivatelovych prav
        try {
            List<Long> sectionIds = schedulingSettingsService.listUserSectionRights(action.getUserEventUuid());
            cs.setSectionIdRights(new ArrayList<Long>(sectionIds));
        } catch (RequiredPropertyNullException ex) {
            // toto drbne nekde driv, protoze by uuid uzivatele melo byt znamo vzdy
        }

        // nacteni vsech sekci, pro vykreslovani prehledu
        List<Section> sections = schedulingSettingsService.listSections();
        ArrayList<ClientSection> clientSections = new ArrayList<ClientSection>();
        for (Section s : sections) {
            ClientSection c = new ClientSection();
            c.setId(s.getId());
            c.setName(s.getName());
            c.setBgColor(s.getBgColor());
            c.setFgColor(s.getFgColor());
            c.setFontColor(s.getFontColor());

            // je potreba nacist opravnene uzivatele k sekcim??? tady by mohlo zustat prazdne, neni to potreba

            clientSections.add(c);
        }
        cs.setSections(clientSections);
        return cs;
    }
}
