package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.LoadBufferAction;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntity;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntityTalk;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadBufferActionHandler implements EventActionHandler<LoadBufferAction, ArrayListResult<ClientUnassignedEntity>> {

    // servica
    private SchedulingService schedulingService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingService(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<LoadBufferAction> getActionType() {
        return LoadBufferAction.class;
    }

    @Override
    public ArrayListResult<ClientUnassignedEntity> execute(LoadBufferAction action) throws ActionException {
        try {
            ArrayList<ClientUnassignedEntity> result = new ArrayList<ClientUnassignedEntity>();

            // overeni a vyhozeni id, ke kterym uzivatel nema pravo, pro admina umoznit nacist vsechno co chce
            if (!schedulingActionPermissionService.isUserSection(action.getSectionIds(), action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            List<EntitySubmission> list = schedulingService.loadBuffer(action.getSectionIds());
            for (EntitySubmission es : list) {
                ClientUnassignedEntityTalk cue = new ClientUnassignedEntityTalk();
                cue.setId(es.getId());
                cue.setSectionId(es.getSection().getId());
                cue.setAuthor(es.getSubmission().getAuthors());
                cue.setName(es.getSubmission().getTitle());
                result.add(cue);
            }
            return new ArrayListResult<ClientUnassignedEntity>(result);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
