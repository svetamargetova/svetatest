package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.utils.UuidProvider;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.CreateDaySettingsAction;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateDaySettingsActionHandler implements EventActionHandler<CreateDaySettingsAction, ArrayListResult<Long>> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<CreateDaySettingsAction> getActionType() {
        return CreateDaySettingsAction.class;
    }

    @Override
    public ArrayListResult<Long> execute(CreateDaySettingsAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            ArrayList<Long> result = new ArrayList<Long>();
            for (Date date : action.getDays()) {
                TimeFrame timeFrame = new TimeFrame();
                timeFrame.setDay(date);
                timeFrame.setUuid(UuidProvider.getUuid());
                timeFrame.setTimeFrom(480); // 8:00
                timeFrame.setTimeTo(960); // 16:00
                Long id = schedulingSettingsService.createTimeFrame(timeFrame);
                result.add(id);
            }
            return new ArrayListResult<Long>(result);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (IllegalPropertyValue ex) {
            throw new BadInputException();
        } catch (Exception ex) {
            throw new ServiceFailedException();
        }
    }
}
