package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.UpdateBlockAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateBlockActionHandler implements EventActionHandler<UpdateBlockAction, VoidResult> {

    // servica
    @Autowired
    private SchedulingService schedulingService;
    @Autowired
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Override
    public Class<UpdateBlockAction> getActionType() {
        return UpdateBlockAction.class;
    }

    @Override
    public VoidResult execute(UpdateBlockAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            schedulingService.updateBlock(action.getBlockId(), action.getSectionId(), action.getChairUuid());
            return new VoidResult();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (Exception ex) {
            throw new ServiceFailedException();
        }
    }
}
