package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.gwt.client.action.LoadEditableTalkOverviewAction;
import consys.event.scheduling.gwt.client.bo.ClientEditableSectionOverview;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadEditableTalkOverviewActionHandler implements EventActionHandler<LoadEditableTalkOverviewAction, ClientEditableSectionOverview> {

    // servica
    private SchedulingService schedulingService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingService(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<LoadEditableTalkOverviewAction> getActionType() {
        return LoadEditableTalkOverviewAction.class;
    }

    @Override
    public ClientEditableSectionOverview execute(LoadEditableTalkOverviewAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            List<EntitySubmission> ess = schedulingService.listSubmissionsBySectionId(action.getSectionId());
            List<Submission> submissions = schedulingService.listUnassignedSubmissions();

            ArrayList<CommonLongThumb> talks = new ArrayList<CommonLongThumb>();
            for (EntitySubmission es : ess) {
                talks.add(new CommonLongThumb(es.getId(), es.getSubmission().getTitle()));
            }

            ArrayList<CommonLongThumb> unassigneds = new ArrayList<CommonLongThumb>();
            for (Submission s : submissions) {
                unassigneds.add(new CommonLongThumb(s.getId(), s.getTitle()));
            }

            ClientEditableSectionOverview overview = new ClientEditableSectionOverview();
            overview.setTalks(talks);
            overview.setUnassignedTalks(unassigneds);
            return overview;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
