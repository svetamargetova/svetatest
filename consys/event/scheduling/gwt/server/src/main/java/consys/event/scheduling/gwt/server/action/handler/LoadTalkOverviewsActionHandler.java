package consys.event.scheduling.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewsAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverviews;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadTalkOverviewsActionHandler implements EventActionHandler<LoadTalkOverviewsAction, ClientSectionOverviews> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<LoadTalkOverviewsAction> getActionType() {
        return LoadTalkOverviewsAction.class;
    }

    @Override
    public ClientSectionOverviews execute(LoadTalkOverviewsAction action) throws ActionException {
        List<Section> sections = schedulingSettingsService.listSections();

        ArrayList<CommonLongThumb> thumbs = new ArrayList<CommonLongThumb>();
        for (Section s : sections) {
            thumbs.add(new CommonLongThumb(s.getId(), s.getName()));
        }

        ClientSectionOverviews overviews = new ClientSectionOverviews();
        overviews.setSectionThumbs(thumbs);
        return overviews;
    }
}
