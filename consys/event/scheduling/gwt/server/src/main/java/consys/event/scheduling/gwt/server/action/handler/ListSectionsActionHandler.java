package consys.event.scheduling.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.ListSectionsAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.bo.ClientSectionSettings;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListSectionsActionHandler implements EventActionHandler<ListSectionsAction, ClientSectionSettings> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<ListSectionsAction> getActionType() {
        return ListSectionsAction.class;
    }

    @Override
    public ClientSectionSettings execute(ListSectionsAction action) throws ActionException {
        List<Section> sections = schedulingSettingsService.listSections();

        ClientSectionSettings css = new ClientSectionSettings();

        if (!sections.isEmpty()) {
            boolean first = true;
            for (Section section : sections) {
                if (first) {
                    ClientSection cs = new ClientSection();
                    cs.setId(section.getId());
                    cs.setName(section.getName());
                    cs.setBgColor(section.getBgColor());
                    cs.setFgColor(section.getFgColor());
                    cs.setFontColor(section.getFontColor());
                    first = false;
                    css.setFirstSection(cs);
                } else {
                    css.getSectionThumbs().add(new CommonLongThumb(section.getId(), section.getName()));
                }
            }
        }

        return css;
    }
}
