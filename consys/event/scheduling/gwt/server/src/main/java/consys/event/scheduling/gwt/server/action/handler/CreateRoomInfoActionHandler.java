package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.CreateRoomInfoAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateRoomInfoActionHandler implements EventActionHandler<CreateRoomInfoAction, LongResult> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<CreateRoomInfoAction> getActionType() {
        return CreateRoomInfoAction.class;
    }

    @Override
    public LongResult execute(CreateRoomInfoAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }
            
            Room room = new Room();
            room.setName(action.getRoom().getName());
            Long id = schedulingSettingsService.createRoom(room);
            return new LongResult(id);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
