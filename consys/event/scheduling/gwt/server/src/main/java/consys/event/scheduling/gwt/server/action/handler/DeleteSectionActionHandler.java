package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.DeleteSectionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DeleteSectionActionHandler implements EventActionHandler<DeleteSectionAction, VoidResult> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;
    private SchedulingActionPermissionService schedulingActionPermissionService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Autowired(required = true)
    public void setSchedulingActionPermissionService(SchedulingActionPermissionService schedulingActionPermissionService) {
        this.schedulingActionPermissionService = schedulingActionPermissionService;
    }

    @Override
    public Class<DeleteSectionAction> getActionType() {
        return DeleteSectionAction.class;
    }

    @Override
    public VoidResult execute(DeleteSectionAction action) throws ActionException {
        try {
            // overeni opravneni uzivatele
            if (!schedulingActionPermissionService.isAdmin(action.getUserEventUuid())) {
                throw new ActionNotPermittedException();
            }

            Section section = schedulingSettingsService.loadSection(action.getId());
            schedulingSettingsService.deleteSection(section);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
