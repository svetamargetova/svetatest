package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.service.EntityService;
import consys.event.scheduling.gwt.client.action.UpdateFavouriteEntityAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateFavouriteEntityActionHandler implements EventActionHandler<UpdateFavouriteEntityAction, VoidResult>{

    @Autowired
    private EntityService entityService;

    @Override
    public Class<UpdateFavouriteEntityAction> getActionType() {
        return UpdateFavouriteEntityAction.class;
    }

    @Override
    public VoidResult execute(UpdateFavouriteEntityAction action) throws ActionException {
        try {
            entityService.updateUserEntityDetails(action.getUserEventUuid(), action.getEntityId(), action.isFavourite());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
    }

}
