package consys.event.scheduling.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.ListRoomInfoAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListRoomInfoActionHandler implements EventActionHandler<ListRoomInfoAction, ArrayListResult<ClientRoomInfo>> {

    // servica
    private SchedulingSettingsService schedulingSettingsService;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService schedulingSettingsService) {
        this.schedulingSettingsService = schedulingSettingsService;
    }

    @Override
    public Class<ListRoomInfoAction> getActionType() {
        return ListRoomInfoAction.class;
    }

    @Override
    public ArrayListResult<ClientRoomInfo> execute(ListRoomInfoAction action) throws ActionException {
        List<Room> rooms = schedulingSettingsService.listRooms();

        ArrayList<ClientRoomInfo> crs = new ArrayList<ClientRoomInfo>();
        for (Room r : rooms) {
            ClientRoomInfo cri = new ClientRoomInfo(r.getId(), r.getName());
            crs.add(cri);
        }

        return new ArrayListResult<ClientRoomInfo>(crs);
    }
}
