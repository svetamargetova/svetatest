package consys.event.scheduling.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.bo.UserEntityDetails;
import consys.event.scheduling.core.service.EntityService;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.gwt.client.action.LoadMyScheduleDayAction;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.event.scheduling.gwt.client.bo.ClientMyEntity;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadMyScheduleDayActionHandler implements EventActionHandler<LoadMyScheduleDayAction, ClientScheduleDay> {

    // servica
    @Autowired
    private SchedulingService schedulingService;
    @Autowired
    private SchedulingSettingsService schedulingSettingsService;
    @Autowired
    private EntityService entityService;
    
    @Override
    public Class<LoadMyScheduleDayAction> getActionType() {
        return LoadMyScheduleDayAction.class;
    }

    @Override
    public ClientScheduleDay execute(LoadMyScheduleDayAction action) throws ActionException {
        try {
            TimeFrame tf = schedulingService.loadTimeFrame(action.getId());
            List<TimeFrameBlock> blocks = schedulingSettingsService.listTimeFrameBlocks(action.getId());
            List<Room> rooms = schedulingService.listRooms(action.getId());
            List<Entity> entities = schedulingService.listEntities(action.getId());
            List<EntityPlenaryBlock> entityPlenaryBlocks = schedulingService.listPlenaryBlocks(action.getId());
            List<UserEntityDetails> userDetailsList = entityService.listUserEntityDetails(action.getUserEventUuid());

            // mistnosti
            ArrayList<ClientRoom> clientRooms = new ArrayList<ClientRoom>();
            Map<Long, ClientRoom> mapRooms = new HashMap<Long, ClientRoom>();
            for (Room r : rooms) {
                ClientRoom cr = new ClientRoom();
                cr.setId(r.getId());
                cr.setName(r.getName());
                clientRooms.add(cr);
                mapRooms.put(r.getId(), cr);
            }

            // plenarni casti
            Map<Long, ArrayList<Long>> mapPlenary = new HashMap<Long, ArrayList<Long>>();
            for (EntityPlenaryBlock plenary : entityPlenaryBlocks) {
                ArrayList<Long> plenaries = mapPlenary.get(plenary.getEntity().getId());
                if (plenaries == null) {
                    plenaries = new ArrayList<Long>();
                    mapPlenary.put(plenary.getEntity().getId(), plenaries);
                }
                plenaries.add(plenary.getTimeFrameBlock().getRoom().getId());
            }

            // entity
            Map<Long, ArrayList<ClientMyEntity>> mapEntities = new HashMap<Long, ArrayList<ClientMyEntity>>();
            for (Entity e : entities) {
                final Long blockId = e.getTimeFrameBlock().getId();
                ArrayList<ClientMyEntity> list = mapEntities.get(blockId);
                if (list == null) {
                    list = new ArrayList<ClientMyEntity>();
                    mapEntities.put(blockId, list);
                }

                ClientMyEntity myEntity = null;
                if (e instanceof EntitySubmission) {
                    EntitySubmission es = (EntitySubmission) e;
                    final int from = es.getFromMinutesInBlock() + es.getTimeFrameBlock().getTimeFrom();

                    ClientMyEntityTalk talk = new ClientMyEntityTalk();
                    talk.setId(es.getId());
                    talk.setFrom(new ClientTime(from));
                    talk.setTo(new ClientTime(from + es.getLengthMinutes()));
                    talk.setAuthor(es.getSubmission().getAuthors());
                    talk.setName(es.getSubmission().getTitle());

                    ArrayList<Long> rms = mapPlenary.get(es.getId());
                    if (rms != null) {
                        talk.setRoomIds(rms);
                    }

                    myEntity = talk;
                } else if (e instanceof EntityBlind) {
                    EntityBlind eb = (EntityBlind) e;
                    final int from = eb.getFromMinutesInBlock() + eb.getTimeFrameBlock().getTimeFrom();

                    ClientMyEntityBlind blind = new ClientMyEntityBlind();
                    blind.setId(eb.getId());
                    blind.setFrom(new ClientTime(from));
                    blind.setTo(new ClientTime(from + eb.getLengthMinutes()));
                    blind.setName(eb.getTitle());

                    ArrayList<Long> rms = mapPlenary.get(eb.getId());
                    if (rms != null) {
                        blind.setRoomIds(rms);
                    }

                    myEntity = blind;
                }
                UserEntityDetails ed = null;
                // pokusime sa najit info o tejto entite v oblubenych
                for(UserEntityDetails ued : userDetailsList){
                    if(ued.getEntity().getId().equals(myEntity.getId())){
                        ed = ued;
                        break;
                    }
                }

                // ak sme nasli tak nastaviem a odstranime
                if(ed != null){
                    myEntity.setNote(ed.getNote());
                    myEntity.setFavourite(ed.isFavourite());
                    userDetailsList.remove(ed);
                }                
                list.add(myEntity);
            }

            // bloky
            for (TimeFrameBlock tfb : blocks) {
                ClientBlock cb = new ClientBlock();
                cb.setBlockId(tfb.getId());
                if (tfb.getSection() != null) {
                    cb.setSectionId(tfb.getSection().getId());
                }
                cb.setChairUuid(tfb.getChair() == null ? null : tfb.getChair().getUuid());
                cb.setFrom(new ClientTime(tfb.getTimeFrom()));
                cb.setTo(new ClientTime(tfb.getTimeTo()));

                ArrayList<ClientMyEntity> list = mapEntities.get(tfb.getId());
                if (list != null) {
                    cb.setSubmissions(list);
                }

                ClientRoom cr = mapRooms.get(tfb.getRoom().getId());
                cr.getBlocks().add(cb);
            }

            // vysledny klientsky objekt
            ClientScheduleDay csd = new ClientScheduleDay();
            csd.setFrom(new ClientTime(tf.getTimeFrom()));
            csd.setTo(new ClientTime(tf.getTimeTo()));
            csd.setRooms(clientRooms);
            return csd;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
