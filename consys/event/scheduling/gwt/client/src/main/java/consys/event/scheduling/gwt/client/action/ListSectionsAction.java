package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionSettings;

/**
 * Akce nacteni seznamu thumbu sekci + prvni sekce cela
 * @author pepa
 */
public class ListSectionsAction extends EventAction<ClientSectionSettings> {

    private static final long serialVersionUID = -836868680913404783L;
}
