package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.scheduling.gwt.client.action.LoadEditableTalkOverviewAction;
import consys.event.scheduling.gwt.client.action.UpdateTalkOverviewAction;
import consys.event.scheduling.gwt.client.bo.ClientEditableSectionOverview;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author pepa
 */
public class EditFormTalkOverview extends RootPanel {

    // komponenty
    private SelectBox<Long> selectBox;
    private FlowPanel wrapper;
    // data
    private Long sectionId;
    private ClientEditableSectionOverview data;
    private HashMap<Long, String> addedThumbs;
    private HashMap<Long, String> removedThumbs;

    public EditFormTalkOverview(Long sectionId, String sectionName) {
        super(ESMessageUtils.c.editFormTalkOverview_title() + " " + sectionName);
        this.sectionId = sectionId;
    }

    @Override
    protected void onLoad() {
        addedThumbs = new HashMap<Long, String>();
        removedThumbs = new HashMap<Long, String>();
        EventDispatchEvent loadEvent = new EventDispatchEvent(
                new LoadEditableTalkOverviewAction(sectionId),
                new AsyncCallback<ClientEditableSectionOverview>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExcecutor
                    }

                    @Override
                    public void onSuccess(ClientEditableSectionOverview result) {
                        data = result;
                        init();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }

    private void init() {
        clear();

        selectBox = new SelectBox<Long>();
        selectBox.addStyleName(FLOAT_LEFT);
        selectBox.addStyleName(MARGIN_RIGHT_10);
        regenerateSelectBox();

        ActionLabel add = new ActionLabel(UIMessageUtils.c.const_add(), FLOAT_LEFT + " " + MARGIN_TOP_3);
        add.setClickHandler(addClickHandler());

        FlowPanel selectWrapper = new FlowPanel();
        selectWrapper.add(selectBox);
        selectWrapper.add(add);
        selectWrapper.add(StyleUtils.clearDiv());

        wrapper = new FlowPanel() {

            @Override
            public void add(Widget w) {
                if (getWidgetCount() == 1 && getWidget(0) instanceof Label) {
                    super.remove(getWidget(0));
                }
                super.add(w);
            }

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (getWidgetCount() == 0) {
                    add(new Label(ESMessageUtils.c.talkOverview_text_sectionIsEmpty()));
                }
                return result;
            }
        };
        wrapper.addStyleName(MARGIN_TOP_10);

        for (CommonLongThumb talk : data.getTalks()) {
            wrapper.add(new AssignedTalkUI(talk.getId(), talk.getName()));
        }
        if (data.getTalks().isEmpty()) {
            wrapper.add(new Label(ESMessageUtils.c.talkOverview_text_sectionIsEmpty()));
        }

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        BaseForm form = new BaseForm("170px");
        form.addOptional(0, ESMessageUtils.c.editFormTalkOverview_form_unassignedTalks(), selectWrapper);
        form.addContent(1, wrapper);
        form.addActionMembers(2, button, ActionLabel.breadcrumbBackCancel());
        addWidget(form);
    }

    /** click handler pro prirazeni prednasky ze seznamu neprirazenych */
    private ClickHandler addClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                SelectBoxItem<Long> item = selectBox.getSelectedItem();
                if (item != null) {
                    wrapper.add(new AssignedTalkUI(item.getItem(), item.getName()));
                    if (removedThumbs.get(item.getItem()) == null) {
                        addedThumbs.put(item.getItem(), item.getName());
                    } else {
                        removedThumbs.remove(item.getItem());
                    }
                    regenerateSelectBox();
                    selectBox.setSelectedItem(null);
                }
            }
        };
    }

    /** click handler pro potvrzovaci tlacitko */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UpdateTalkOverviewAction action = new UpdateTalkOverviewAction(sectionId);

                for (int i = 0; i < wrapper.getWidgetCount(); i++) {
                    Widget w = wrapper.getWidget(i);
                    if (w instanceof AssignedTalkUI) {
                        action.getTalkIds().add(((AssignedTalkUI) w).getId());
                    }
                }

                EventBus.get().fireEvent(new EventDispatchEvent(action,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava action event executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, EditFormTalkOverview.this));
            }
        };
    }

    /** naplni select box podle aktualni situace */
    private void regenerateSelectBox() {
        ArrayList<SelectBoxItem<Long>> boxs = new ArrayList<SelectBoxItem<Long>>();
        for (CommonLongThumb clt : data.getUnassignedTalks()) {
            if (addedThumbs.get(clt.getId()) == null) {
                boxs.add(new SelectBoxItem<Long>(clt.getId(), clt.getName()));
            }
        }
        for (Entry<Long, String> s : removedThumbs.entrySet()) {
            boxs.add(new SelectBoxItem<Long>(s.getKey(), s.getValue()));
        }
        selectBox.setItems(boxs);
    }

    /** jedna prirazena prednaska s moznosti odstraneni */
    private class AssignedTalkUI extends FlowPanel {

        // data
        private Long id;
        private String name;

        public AssignedTalkUI(final Long id, final String name) {
            this.id = id;
            this.name = name;
            setWidth("525px");

            Label titleLabel = StyleUtils.getStyledLabel(name, FONT_BOLD, MARGIN_BOT_10);
            titleLabel.setWidth("380px");
            titleLabel.addStyleName(FLOAT_LEFT);

            Remover remover = Remover.commonRemover(new ConsysAction() {

                @Override
                public void run() {
                    AssignedTalkUI.this.removeFromParent();
                    if (addedThumbs.get(id) == null) {
                        removedThumbs.put(id, name);
                    } else {
                        addedThumbs.remove(id);
                    }
                    regenerateSelectBox();
                }
            });
            remover.addStyleName(FLOAT_RIGHT);

            add(titleLabel);
            add(remover);
            add(StyleUtils.clearDiv());
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AssignedTalkUI other = (AssignedTalkUI) obj;
            if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
            return hash;
        }
    }
}
