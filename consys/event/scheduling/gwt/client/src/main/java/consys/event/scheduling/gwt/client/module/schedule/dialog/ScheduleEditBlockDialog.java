package consys.event.scheduling.gwt.client.module.schedule.dialog;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.event.scheduling.gwt.client.action.UpdateBlockAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import java.util.ArrayList;
import java.util.Map.Entry;

/**
 * Dialog pro editaci bloku
 * @author pepa
 */
public abstract class ScheduleEditBlockDialog extends AbstractScheduleEditDialog {

    // komponenty
    private ChairPanel chairPanel;
    // data
    private Long blockId;
    private Long oldSectionId;
    private String oldChairUuid;
    private AbsolutePanel content;

    public ScheduleEditBlockDialog(Long blockId, Long oldSectionId, String oldChairUuid, AbsolutePanel content) {
        this.blockId = blockId;
        this.oldSectionId = oldSectionId;
        this.oldChairUuid = oldChairUuid;
        this.content = content;

        chairPanel = new ChairPanel();
    }

    @Override
    protected void onLoad() {
        if (oldChairUuid != null && ScheduleData.get().chairmans().get(oldChairUuid) == null) {
            EventBus.get().fireEvent(new DispatchEvent(
                    new LoadUserThumbAction(oldChairUuid),
                    new AsyncCallback<ClientUserThumb>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ClientUserThumb result) {
                            ScheduleData.get().chairmans().put(oldChairUuid, result.getName());
                            chairPanel.setChair(oldChairUuid);
                        }
                    }, this));
        } else {
            chairPanel.setChair(oldChairUuid);
        }
    }

    @Override
    protected Widget createContent() {
        ActionLabel setChair = new ActionLabel(ESMessageUtils.c.scheduleEditBlockDialog_action_setChair(),
                StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        setChair.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog sd = new UserSearchDialog(false);
                sd.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        ScheduleData.get().chairmans().put(item.getUuid(), item.getName());
                        chairPanel.setChair(item.getUuid());
                    }
                });
                sd.showCentered();
            }
        });

        final SelectBox<Long> sectionBox = new SelectBox<Long>();
        sectionBox.selectFirst(true);
        ArrayList<SelectBoxItem<Long>> items = new ArrayList<SelectBoxItem<Long>>();
        items.add(new SelectBoxItem<Long>(0l, ESMessageUtils.c.scheduleEditBlockDialog_text_none()));
        for (Entry<Long, ClientSection> e : ScheduleData.get().sections().entrySet()) {
            items.add(new SelectBoxItem<Long>(e.getKey(), e.getValue().getName()));
        }
        sectionBox.setItems(items);
        sectionBox.selectItem(oldSectionId == null ? 0l : oldSectionId);

        ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_update());
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (oldSectionId == null) {
                    oldSectionId = 0l;
                }
                if (!oldSectionId.equals(sectionBox.getSelectedItem().getItem()) && content.getWidgetCount() > 0) {
                    getFailMessage().setText(ESMessageUtils.c.scheduleEditBlockDialog_error_changetSectionInNonEmptyBlock());
                    return;
                }
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateBlockAction(blockId, sectionBox.getSelectedItem().getItem(), chairPanel.getChairUuid()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                doSuccessAction(sectionBox.getSelectedItem().getItem(),
                                        chairPanel.getChairUuid(), chairPanel.getChairName());
                            }
                        }, ScheduleEditBlockDialog.this));
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        BaseForm form = new BaseForm("180px");
        form.addOptional(0, ESMessageUtils.c.scheduleEditBlockDialog_form_chair(), chairPanel);
        form.addOptional(1, ESMessageUtils.c.scheduleEditBlockDialog_form_section(), sectionBox);
        form.addActionMembers(2, button, cancel);

        panel().clear();
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(StyleUtils.getStyledLabel(ESMessageUtils.c.scheduleEditBlockDialog_title(),
                StyleUtils.FONT_17PX, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_HOR_10, StyleUtils.FLOAT_LEFT));
        panel().add(setChair);
        panel().add(StyleUtils.clearDiv());
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(form);
        panel().add(ESResourceUtils.dialogSeparator10px());
        return mainPanel();
    }

    /** akce provedena po uspesne aktualizaci na serveru */
    protected abstract void doSuccessAction(Long sectionId, String chairUuid, String chairName);

    /** vyber chaira nebo zruseni chaira */
    private class ChairPanel extends FlowPanel {

        // komponenty
        private Label label;
        private Image close;
        // data
        private String uuid;
        private String name;

        public ChairPanel() {
            label = StyleUtils.getStyledLabel(UIMessageUtils.c.const_loadingDots(),
                    StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_10);

            close = new Image(ResourceUtils.system().removeCross());
            close.setStyleName(StyleUtils.HAND);
            close.addStyleName(StyleUtils.FLOAT_LEFT);
            close.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    setChair(null);
                }
            });
            close.setVisible(false);

            add(label);
            add(close);
            add(StyleUtils.clearDiv());
        }

        /** nastaveni chaira */
        public void setChair(String uuid) {
            this.uuid = uuid;
            if (uuid == null) {
                name = "";
                label.setText(ESMessageUtils.c.scheduleEditBlockDialog_text_none());
                close.setVisible(false);
            } else {
                name = ScheduleData.get().chairmans().get(uuid);
                label.setText(name);
                close.setVisible(true);
            }
        }

        /** vraci jmeno chaira */
        public String getChairName() {
            return name;
        }

        /** vraci uuid chaira */
        public String getChairUuid() {
            return uuid;
        }
    }
}
