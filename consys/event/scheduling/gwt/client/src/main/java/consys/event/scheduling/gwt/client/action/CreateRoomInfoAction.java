package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;

/**
 * Akce pro vytvoreni informace o mistnosti
 * @author pepa
 */
public class CreateRoomInfoAction extends EventAction<LongResult> {

    private static final long serialVersionUID = 3475315360994092601L;
    // data
    private ClientRoomInfo room;

    public CreateRoomInfoAction() {
    }

    public CreateRoomInfoAction(ClientRoomInfo room) {
        this.room = room;
    }

    public ClientRoomInfo getRoom() {
        return room;
    }
}
