package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.scheduling.gwt.client.action.DeleteDaySettingsAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.Collections;

/**
 * Polozka zalozkoveho panelu nastaveni planovani
 * @author pepa
 */
public class SchedulingSettingsItem extends FlowPanel implements ConsysTabPanelItem, ActionExecutionDelegate {

    // komponenty
    private ConsysTabPanelLabel tabLabel;
    // data
    private ConsysMessage helpMessage;
    private ConsysTabPanel tabPanel;

    public SchedulingSettingsItem(ConsysTabPanelLabel tabLabel, ClientTabItem item, final ConsysMessage helpMessage) {
        super();
        this.tabLabel = tabLabel;
        this.helpMessage = helpMessage;
    }

    /** nastavi nova data a aktualizuje obsah */
    public void setData(final ClientDay data) {
        clear();
        tabLabel.setText(DateTimeUtils.getDate(data.getDate()));

        ActionLabel edit = new ActionLabel(UIMessageUtils.c.const_edit(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_RIGHT_10);
        edit.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EditFormDaySettings w = new EditFormDaySettings(data, SchedulingSettingsItem.this);
                FormUtils.fireNextBreadcrumb(ESMessageUtils.c.editFormDaySettings_title(), w);
            }
        });

        Remover remover = Remover.commonRemover(new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteDaySettingsAction(data.getId()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // odstraneni v ui
                                tabPanel.removeTab(SchedulingSettingsItem.this);
                                if (tabPanel.getTabCount() == 0) {
                                    tabPanel.setVisible(false);
                                    helpMessage.setVisible(true);
                                }
                            }
                        }, SchedulingSettingsItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        });
        remover.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        controlPanel.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        controlPanel.addStyleName(StyleUtils.MARGIN_TOP_10);
        controlPanel.add(edit);
        controlPanel.add(remover);

        add(controlPanel);
        add(StyleUtils.clearDiv());

        init(data);
    }

    /** inicializace obsahu */
    private void init(ClientDay data) {
        FlowPanel content = new FlowPanel();
        content.addStyleName(StyleUtils.MARGIN_LEFT_10);
        content.addStyleName(StyleUtils.MARGIN_BOT_10);

        Label fromLabel = StyleUtils.getStyledLabel(ECoMessageUtils.c.const_date_from() + ":", StyleUtils.FONT_BOLD,
                StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
        fromLabel.setWidth("80px");
        content.add(fromLabel);
        Label fromData = StyleUtils.getStyledLabel(data.getFrom().toString(), StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
        fromData.setWidth("50px");
        content.add(fromData);
        content.add(StyleUtils.clearDiv("10px"));

        Label toLabel = StyleUtils.getStyledLabel(ECoMessageUtils.c.const_date_to() + ":", StyleUtils.FONT_BOLD,
                StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
        toLabel.setWidth("80px");
        content.add(toLabel);
        Label toData = StyleUtils.getStyledLabel(data.getTo().toString(), StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
        toData.setWidth("50px");
        content.add(toData);
        content.add(StyleUtils.clearDiv("10px"));

        Label messagesLabel = StyleUtils.getStyledLabel(ESMessageUtils.c.editFormDaySettings_form_rooms() + ":",
                StyleUtils.FONT_BOLD, StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT, StyleUtils.MARGIN_RIGHT_20);
        messagesLabel.setWidth("80px");
        content.add(messagesLabel);
        String rooms = "";
        for (ClientRoomInfo cri : data.getRooms()) {
            rooms += cri.getName() + ", ";
        }
        if (rooms.equals("")) {
            rooms = FormUtils.MDASH;
        } else {
            rooms = rooms.substring(0, rooms.length() - 2);
        }
        Label messagesData = StyleUtils.getStyledLabel(rooms, StyleUtils.FLOAT_LEFT);
        messagesData.setWidth("500px");
        content.add(messagesData);
        content.add(StyleUtils.clearDiv("10px"));

        Label splitsLabel = StyleUtils.getStyledLabel(ESMessageUtils.c.editFormDaySettings_form_splits() + ":",
                StyleUtils.FONT_BOLD, StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT, StyleUtils.MARGIN_RIGHT_20);
        splitsLabel.setWidth("80px");
        content.add(splitsLabel);
        String splits = "";
        Collections.sort(data.getSplits(), TimeUtils.ascendingComparator());
        for (ClientTime ct : data.getSplits()) {
            splits += ct.toString() + "<br/>";
        }
        if (splits.equals("")) {
            splits = FormUtils.MDASH;
        } else {
            splits = splits.substring(0, splits.length() - 5);
        }
        HTML splitsData = new HTML(splits);
        splitsData.addStyleName(StyleUtils.FLOAT_LEFT);
        splitsData.setWidth("500px");
        content.add(splitsData);
        content.add(StyleUtils.clearDiv());

        add(content);
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel tabPanel) {
        this.tabPanel = tabPanel;
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
    }
}
