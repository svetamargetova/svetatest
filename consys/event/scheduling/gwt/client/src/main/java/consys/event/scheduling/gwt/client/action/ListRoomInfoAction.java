package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;

/**
 * Akce pro nacteni seznamu mistnosti s jejich nazvem (pozdeji dalsim infem)
 * @author pepa
 */
public class ListRoomInfoAction extends EventAction<ArrayListResult<ClientRoomInfo>> {

    private static final long serialVersionUID = 1031694165976109692L;
}
