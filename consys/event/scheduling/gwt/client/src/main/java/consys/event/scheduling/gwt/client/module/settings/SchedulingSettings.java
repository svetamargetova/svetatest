package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysDateBox.ShowDateAs;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.event.scheduling.gwt.client.action.CreateDaySettingsAction;
import consys.event.scheduling.gwt.client.action.LoadDaySettingsAction;
import consys.event.scheduling.gwt.client.action.LoadSchedulingSettingsAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Nastaveni planovani pro dny
 * @author pepa
 */
public class SchedulingSettings extends SimpleFormPanel {

    // komponenty
    private ActionLabel addTab;
    private ConsysTabPanel<String> tabs;
    private ConsysMessage helpMessage;
    // data
    private boolean firstLoad = true;

    public SchedulingSettings() {
        addTab = new ActionLabel(ESMessageUtils.c.schedulingSettings_action_addDay(), TEXT_NOWRAP);
        addTab.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewDayUI> cd = new NewItemDialog<NewDayUI>(ESMessageUtils.c.schedulingSettings_text_newDay(), helpMessage) {
                    @Override
                    public void doCreateAction(ArrayList<NewDayUI> uis, final ConsysMessage fail) {
                        for (NewDayUI ui : uis) {
                            if (ui.getDate() == null) {
                                fail.addOrSetText("Missing day date");
                                return;
                            }
                        }

                        final CreateDaySettingsAction createDayAction = new CreateDaySettingsAction();
                        final ArrayList<Date> days = createDayAction.getDays();

                        for (NewDayUI ui : uis) {
                            days.add(ui.getDate());
                        }

                        EventBus.get().fireEvent(new EventDispatchEvent(
                                createDayAction,
                                new AsyncCallback<ArrayListResult<Long>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava actionexecuter
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<Long> result) {
                                        ArrayList<Long> ids = result.getArrayListResult();
                                        // vlozeni zalozek
                                        for (int i = 0; i < days.size(); i++) {
                                            ClientTabItem cti = new ClientTabItem();
                                            cti.setTitle(DateTimeUtils.getDate(days.get(i)));
                                            cti.setUuid(ids.get(i).toString());
                                            addTab(tabs, cti, helpMessage);
                                            if (!tabs.isVisible()) {
                                                tabs.setVisible(true);
                                            }
                                            tabs.selectTab(tabs.getTabCount() - 1);
                                        }
                                        successCreateAction();
                                    }
                                },
                                this));
                    }

                    @Override
                    public NewDayUI newItem() {
                        NewDayUI dayUI = new NewDayUI();
                        dayUI.showDateAs(ShowDateAs.EVENT);
                        return dayUI;
                    }
                };
                cd.showCentered();
            }
        });
        addWidget(addTab);

        FlowPanel sep = new FlowPanel();
        sep.setSize("10px", "10px");
        addWidget(sep);

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.hideClose(true);
        helpMessage.setText(ESMessageUtils.c.schedulingSettings_text_beginByAddingDay());
        addWidget(helpMessage);

        tabs = new ConsysTabPanel<String>(SchedulingSettings.class.getName());
        tabs.setVisible(false);
        addWidget(tabs);
    }

    @Override
    protected void onLoad() {
        if (firstLoad) {
            EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(new LoadSchedulingSettingsAction(),
                    new AsyncCallback<ArrayListResult<ClientDayThumb>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava ActionExcecutor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientDayThumb> result) {
                            firstLoad = false;
                            setDays(result.getArrayListResult());
                        }
                    }, SchedulingSettings.this);
            EventBus.get().fireEvent(loadSettingsEvent);
        }
    }

    /** nastavi nazvy zalozek */
    private void setDays(ArrayList<ClientDayThumb> thumbs) {
        tabs.clear();
        // Usporiadame thumby podla datumu
        Collections.sort(thumbs);

        if (thumbs != null && thumbs.size() > 0) {
            helpMessage.setVisible(false);
            tabs.setVisible(true);
            for (ClientDayThumb thumb : thumbs) {
                ClientTabItem cti = new ClientTabItem();
                cti.setTitle(DateTimeUtils.getDate(thumb.getDate(), TimeZone.createTimeZone(0)));
                cti.setUuid(thumb.getId().toString());
                addTab(tabs, cti, helpMessage);
            }
        }
    }

    /** prida zalozku s typem prispevku */
    private void addTab(ConsysTabPanel<String> tabs, final ClientTabItem item, ConsysMessage helpMessage) {
        ConsysTabPanelLabel<String> tl = new ConsysTabPanelLabel<String>(item.getUuid(), item.getTitle());
        final SchedulingSettingsItem ssItem = new SchedulingSettingsItem(tl, item, helpMessage);
        ConsysAction action = new ConsysAction() {
            @Override
            public void run() {
                EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(
                        new LoadDaySettingsAction(Long.parseLong(item.getUuid())),
                        new AsyncCallback<ClientDay>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                            }

                            @Override
                            public void onSuccess(ClientDay result) {
                                ssItem.setData(result);
                            }
                        }, ssItem);
                EventBus.get().fireEvent(loadSettingsEvent);
            }
        };
        tabs.addTabItem(tl, ssItem, action);
    }
}
