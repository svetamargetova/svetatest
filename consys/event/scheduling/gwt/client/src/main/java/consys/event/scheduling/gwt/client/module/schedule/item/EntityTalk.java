package consys.event.scheduling.gwt.client.module.schedule.item;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.dialog.ScheduleEditTalkDialog;
import java.util.ArrayList;

/**
 * Prednaska
 * @author pepa
 */
public class EntityTalk extends EntityMain<ClientEntityTalk> {

    // konstanty
    public static final int REQUIRED_MAX_PUP_ROW_CHARS = 60;
    public static final int MAX_PUP_ROW_CHARS = REQUIRED_MAX_PUP_ROW_CHARS + 10;
    // komponenty
    private PopupHelp pup;

    public EntityTalk() {
        super();

        final FlowPanel helpPanel = new FlowPanel();
        helpPanel.addStyleName(StyleUtils.MARGIN_HOR_10);
        helpPanel.addStyleName(StyleUtils.MARGIN_VER_10);
        pup = new PopupHelp(helpPanel, EntityTalk.this);

        getHandle().addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                helpPanel.clear();
                if (getBlock() != null) {
                    helpPanel.add(StyleUtils.getStyledLabel(getTimeLabel().getText(), StyleUtils.FONT_BOLD));
                }

                String name = getData().getName();
                name = name.replaceAll("\\s+", " ");

                if (name.length() < MAX_PUP_ROW_CHARS) {
                    helpPanel.add(StyleUtils.getStyledLabel(name, StyleUtils.FONT_16PX, StyleUtils.MARGIN_VER_5));
                } else {
                    String[] rows = nameRows(name);
                    helpPanel.add(StyleUtils.getStyledLabel(rows[0], StyleUtils.FONT_16PX, StyleUtils.MARGIN_TOP_5));
                    for (int i = 1; i < rows.length; i++) {
                        if (i != rows.length - 1) {
                            helpPanel.add(StyleUtils.getStyledLabel(rows[i], StyleUtils.FONT_16PX));
                        } else {
                            helpPanel.add(StyleUtils.getStyledLabel(rows[i], StyleUtils.FONT_16PX, StyleUtils.MARGIN_BOT_5));
                        }
                    }
                }

                helpPanel.add(StyleUtils.getStyledLabel(getData().getAuthor(), StyleUtils.FONT_BOLD));
                pup.show();
            }
        });
        getHandle().addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                pup.hide();
            }
        });
    }

    /** rozdeli text na radky podle konstanty pozadovane delky na radek */
    public static String[] nameRows(String text) {
        int rows = text.length() / REQUIRED_MAX_PUP_ROW_CHARS;
        rows += text.length() % REQUIRED_MAX_PUP_ROW_CHARS != 0 ? 1 : 0;
        String[] result = new String[rows];
        for (int i = 0; i < rows; i++) {
            int index = text.indexOf(" ", REQUIRED_MAX_PUP_ROW_CHARS);
            if (index == -1) {
                result[i] = text;
                break;
            } else {
                result[i] = text.substring(0, index);
                text = text.substring(index).trim();
            }
        }
        return result;
    }

    @Override
    protected void showEditDialog() {
        ArrayList<Long> rooms = new ArrayList<Long>();
        for (Entity e : getSlaves()) {
            rooms.add(e.getRoomId());
        }
        new ScheduleEditTalkDialog(this, rooms, getBlock()) {

            @Override
            protected void doSuccessAction(ClientTime from, int length, ArrayList<Long> slaveRooms) {
                setFrom(from);
                setLength(length);
                refreshSlaves(slaveRooms);
                DrawProvider.get().updateEntityPosition(EntityTalk.this, getBlock());
                hide();
            }
        }.showCentered();
    }

    @Override
    public void createSelf(ClientSection section) {
        super.createSelf(section);

        Label nameLabel = StyleUtils.getStyledLabel(getData().getName(), StyleUtils.FONT_11PX, StyleUtils.MARGIN_HOR_5);
        Label authorLabel = StyleUtils.getStyledLabel(getData().getAuthor(), StyleUtils.FONT_11PX, StyleUtils.MARGIN_HOR_5);

        setWidgetTextColor(getTimeLabel());
        setWidgetTextColor(nameLabel);
        setWidgetTextColor(authorLabel);

        mainPanel().clear();
        mainPanel().add(nameLabel);
        mainPanel().add(authorLabel);
    }

    /** prepocita se vyska pupu */
    public void recalculatePup() {
        pup.recalculate();
    }
}
