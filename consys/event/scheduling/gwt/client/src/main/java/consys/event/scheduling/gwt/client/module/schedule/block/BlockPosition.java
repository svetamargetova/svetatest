package consys.event.scheduling.gwt.client.module.schedule.block;

/**
 * Pozice bloku v rozvrhu
 * @author pepa
 */
public class BlockPosition {

    private int x;
    private int y;

    /**
     * @param x je poradi mistnosti nebo horizontalni pozice (left)
     * @param y je poradi bloku v mistnosti nebo vertikalni pozice (top)
     */
    public BlockPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /** vraci poradi mistnosti (sloupec) nebo horizontalni pozici (left) */
    public int getX() {
        return x;
    }

    /** vraci poradi bloku v mistnosti (radek) nebo vertikalni pozice (top) */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return x + ":" + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlockPosition other = (BlockPosition) obj;
        if (this.x != other.x || this.y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + this.x;
        hash = 11 * hash + this.y;
        return hash;
    }
}
