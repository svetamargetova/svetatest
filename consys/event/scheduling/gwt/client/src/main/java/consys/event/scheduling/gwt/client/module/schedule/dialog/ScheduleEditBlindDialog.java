package consys.event.scheduling.gwt.client.module.schedule.dialog;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.scheduling.gwt.client.action.UpdateBlindAction;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.ScheduleValidator;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.module.settings.EditFormDaySettings.ConsysTimeTextBox;
import consys.event.scheduling.gwt.client.module.settings.RoomCheckBox;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import java.util.ArrayList;

/**
 * Dialog pro editaci blindu
 * @author pepa
 */
public abstract class ScheduleEditBlindDialog extends AbstractScheduleEditDialog {

    // komponenty
    private ConsysIntTextBox lengthBox;
    private ConsysStringTextBox nameBox;
    private ConsysTimeTextBox startBox;
    private Label toLabel;
    private ConsysFlowPanel startPanel;
    private FlowPanel roomsPanel;
    // data
    private EntityBlind blind;
    private ClientTime blockFrom;
    private ClientTime blockTo;
    private ClientTime from;
    private Integer length;

    public ScheduleEditBlindDialog(EntityBlind blind, ArrayList<Long> rooms, ScheduleBlock block) {
        this.blind = blind;
        this.blockFrom = block.getFrom();
        this.blockTo = block.getTo();
        from = new ClientTime(blind.getFrom().getTime());
        length = blind.getLength();

        lengthBox = new ConsysIntTextBox(0, 1440, ESMessageUtils.c.scheduleEditBlindDialog_form_blindLenght());
        lengthBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                try {
                    length = lengthBox.getTextInt();
                } catch (Exception ex) {
                    length = null;
                }
                refreshToLabel();
            }
        });
        lengthBox.setText(blind.getLength());

        startBox = new ConsysTimeTextBox(ESMessageUtils.c.scheduleEditTalkDialog_form_start());
        startBox.addStyleName(StyleUtils.FLOAT_LEFT);
        startBox.setText(from.toString());
        startBox.setWidth("50px");
        startBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                try {
                    from = startBox.getTime();
                } catch (Exception ex) {
                    from = null;
                }
                refreshToLabel();
            }
        });
        toLabel = StyleUtils.getStyledLabel("", StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_TOP_3, StyleUtils.FONT_BOLD);
        refreshToLabel();

        startPanel = new ConsysFlowPanel();
        startPanel.add(startBox);
        startPanel.add(StyleUtils.getStyledLabel(FormUtils.NDASH, StyleUtils.MARGIN_HOR_10,
                StyleUtils.MARGIN_TOP_3, StyleUtils.FLOAT_LEFT, StyleUtils.FONT_BOLD));
        startPanel.add(toLabel);
        startPanel.add(StyleUtils.clearDiv());

        nameBox = new ConsysStringTextBox(1, 255, ESMessageUtils.c.scheduleEditBlindDialog_form_name());
        nameBox.setText(blind.getData().getName());

        roomsPanel = new FlowPanel();
        for (ClientRoom cr : ScheduleData.get().getRooms()) {
            if (cr.getId().equals(blind.getRoomId())) {
                RoomCheckBox<ClientRoom> check = new RoomCheckBox<ClientRoom>(cr, cr.getName(), true);
                check.setEnabled(false);
                roomsPanel.add(check);
            } else {
                roomsPanel.add(new RoomCheckBox<ClientRoom>(cr, cr.getName(), rooms.contains(cr.getId())));
            }
        }
    }

    @Override
    protected Widget createContent() {
        final BaseForm form = new BaseForm("180px");
        ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_update());
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final int length = lengthBox.getTextInt();
                final String name = nameBox.getText().trim();

                final ArrayList<Long> rooms = new ArrayList<Long>();
                for (int i = 0; i < roomsPanel.getWidgetCount(); i++) {
                    RoomCheckBox<ClientRoom> box = (RoomCheckBox<ClientRoom>) roomsPanel.getWidget(i);
                    if (box.isChecked() && !box.getRoom().getId().equals(blind.getRoomId())) {
                        rooms.add(box.getRoom().getId());
                    }
                }

                if (blockFrom.getTime() > from.getTime()
                        || blockTo.getTime() <= from.getTime()
                        || blockTo.getTime() < from.getTime() + length) {
                    getFailMessage().setText(ESMessageUtils.c.scheduleEditTalkDialog_error_outOfBlock());
                    return;
                }

                // validace jestli lze takovou delku a a mistnosti nastavit
                if (!ScheduleValidator.checkEditedEntity(blind, from, length, rooms, getFailMessage())) {
                    return;
                }

                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateBlindAction(blind.getData().getId(), from, length, name, rooms),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                doSuccessAction(from, length, name, rooms);
                            }
                        }, ScheduleEditBlindDialog.this));
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        form.addRequired(0, ESMessageUtils.c.scheduleEditBlindDialog_form_name(), nameBox);
        form.addRequired(1, ESMessageUtils.c.scheduleEditTalkDialog_form_start(), startPanel);
        form.addRequired(2, ESMessageUtils.c.scheduleEditBlindDialog_form_blindLenght(), lengthBox);
        form.addOptional(3, ESMessageUtils.c.scheduleEditTalkDialog_form_nextRooms(), roomsPanel);
        form.addActionMembers(4, button, cancel);

        panel().clear();
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(StyleUtils.getStyledLabel(ESMessageUtils.c.scheduleEditBlindDialog_title(),
                StyleUtils.FONT_17PX, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_LEFT_10));
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(form);
        panel().add(ESResourceUtils.dialogSeparator10px());
        return mainPanel();
    }

    /** akce provedena po uspesne aktualizaci na serveru */
    protected abstract void doSuccessAction(ClientTime from, int length, String name, ArrayList<Long> slaveRooms);

    /** aktualizuje hodnotu konce prednasky */
    private void refreshToLabel() {
        String time = from == null || length == null ? "?" : new ClientTime(from.getTime() + length).toString();
        toLabel.setText(time);
    }
}
