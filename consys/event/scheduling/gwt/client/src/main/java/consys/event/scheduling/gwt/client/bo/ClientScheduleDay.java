package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientTime;
import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Denni rozvrh
 * @author pepa
 */
public class ClientScheduleDay implements IsSerializable, Result {

    private static final long serialVersionUID = 2390396936491733827L;
    // data
    private ArrayList<ClientRoom> rooms;
    private ClientTime from;
    private ClientTime to;

    public ClientScheduleDay() {
        rooms = new ArrayList<ClientRoom>();
    }

    public ClientTime getFrom() {
        return from;
    }

    public void setFrom(ClientTime from) {
        this.from = from;
    }

    public ArrayList<ClientRoom> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<ClientRoom> rooms) {
        this.rooms = rooms;
    }

    public ClientTime getTo() {
        return to;
    }

    public void setTo(ClientTime to) {
        this.to = to;
    }
}
