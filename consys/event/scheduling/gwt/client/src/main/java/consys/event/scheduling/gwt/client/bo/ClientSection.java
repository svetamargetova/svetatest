package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Sekce
 * @author pepa
 */
public class ClientSection implements IsSerializable, Result {

    private static final long serialVersionUID = -8363450643277286183L;
    // konstanty
    public static final String DEFAULT_BG_COLOR = "eafdd5";
    public static final String DEFAULT_FG_COLOR = "caf49b";
    // data
    private Long id;
    private String name;
    private String bgColor;
    private String fgColor;
    private String fontColor;
    private ArrayList<String> authorizedUsers;

    public ClientSection() {
        authorizedUsers = new ArrayList<String>();
    }

    public ClientSection(Long id, String name, String bgColor, String fgColor, ArrayList<String> authorizedUsers) {
        this.id = id;
        this.name = name;
        this.bgColor = bgColor;
        this.fgColor = fgColor;
        this.authorizedUsers = authorizedUsers == null ? new ArrayList<String>() : authorizedUsers;
    }

    public ArrayList<String> getAuthorizedUsers() {
        return authorizedUsers;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFgColor() {
        return fgColor;
    }

    public void setFgColor(String fgColor) {
        this.fgColor = fgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientSection other = (ClientSection) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
