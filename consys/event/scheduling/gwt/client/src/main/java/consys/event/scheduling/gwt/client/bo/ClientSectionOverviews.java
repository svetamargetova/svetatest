package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.CommonLongThumb;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientSectionOverviews implements Result {

    private static final long serialVersionUID = 541325130741764842L;
    // data
    private ArrayList<CommonLongThumb> sectionThumbs;

    public ClientSectionOverviews() {
        sectionThumbs = new ArrayList<CommonLongThumb>();
    }

    public ArrayList<CommonLongThumb> getSectionThumbs() {
        return sectionThumbs;
    }

    public void setSectionThumbs(ArrayList<CommonLongThumb> sectionThumbs) {
        this.sectionThumbs = sectionThumbs;
    }
}
