package consys.event.scheduling.gwt.client.bo;

/**
 * Data prednasky
 * @author pepa
 */
public class ClientEntityTalk extends ClientEntity {

    // data
    private String author;
    private String name;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
