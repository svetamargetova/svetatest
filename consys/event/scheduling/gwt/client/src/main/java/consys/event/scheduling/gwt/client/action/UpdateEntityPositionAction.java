package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientTime;

/**
 * Akce oznamujici aktualizaci pozice entity, vraci id entity
 * @author pepa
 */
public class UpdateEntityPositionAction extends EventAction<LongResult> {

    private static final long serialVersionUID = -6529981802638320861L;
    // data
    private Long entityId;
    private Long blockId;
    private ClientTime time;
    private boolean buffer;
    private String blindName;

    public UpdateEntityPositionAction() {
    }

    public UpdateEntityPositionAction(boolean buffer, Long entityId) {
        this.buffer = true;
        this.entityId = entityId;
    }

    public UpdateEntityPositionAction(Long entityId, Long blockId, ClientTime time) {
        this.buffer = false;
        this.entityId = entityId;
        this.blockId = blockId;
        this.time = time;
    }

    public String getBlindName() {
        return blindName;
    }

    public void setBlindName(String blindName) {
        this.blindName = blindName;
    }

    public Long getBlockId() {
        return blockId;
    }

    public boolean isBuffer() {
        return buffer;
    }

    public Long getEntityId() {
        return entityId;
    }

    public ClientTime getTime() {
        return time;
    }
}
