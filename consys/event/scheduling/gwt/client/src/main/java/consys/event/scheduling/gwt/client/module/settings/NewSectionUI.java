package consys.event.scheduling.gwt.client.module.settings;

import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;

/**
 *
 * @author pepa
 */
public class NewSectionUI extends NewItemUI {

    // komponenty
    private ConsysStringTextBox nameBox;

    public NewSectionUI() {
        super();
    }

    @Override
    public void focus() {
    }

    @Override
    public void createForm(BaseForm form) {
        nameBox = new ConsysStringTextBox(1, 256, ESMessageUtils.c.editFormSectionsSetting_form_name());
        form.reset();
        form.addRequired(0, ESMessageUtils.c.editFormSectionsSetting_form_name(), nameBox);
    }

    /** vraci zadany nazev sekce */
    public String getSectionName() {
        return nameBox.getText();
    }
}
