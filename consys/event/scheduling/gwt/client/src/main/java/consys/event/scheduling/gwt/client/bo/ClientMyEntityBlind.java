package consys.event.scheduling.gwt.client.bo;

/**
 *
 * @author pepa
 */
public class ClientMyEntityBlind extends ClientMyEntity {

    // data
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
