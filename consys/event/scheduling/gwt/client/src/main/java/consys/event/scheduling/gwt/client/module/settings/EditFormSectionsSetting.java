package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.event.scheduling.gwt.client.action.LoadSectionAction;
import consys.event.scheduling.gwt.client.action.UpdateSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class EditFormSectionsSetting extends RootPanel {

    // konstanty
    private static final String HEXNUMBER = "[0-9a-fA-F]{6}";
    // komponenty
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysStringTextBox bgBox;
    private ConsysStringTextBox fgBox;
    private ConsysStringTextBox fontBox;
    private FlowPanel authorizedUsers;
    // data
    private ClientSection originalSection;
    private SectionsSettingItem source;
    private HashMap<Integer, Schema> schemas;

    public EditFormSectionsSetting(ClientSection originalSection, SectionsSettingItem source) {
        super(ESMessageUtils.c.editFormSectionsSetting_title() + " " + originalSection.getName());
        this.originalSection = originalSection;
        this.source = source;

        schemas = new HashMap<Integer, Schema>();
        schemas.put(1, new Schema("eafdd5", "caf49b", "555555"));
        schemas.put(2, new Schema("dcf0fe", "b4ddfa", "555555"));
        schemas.put(3, new Schema("feeadc", "f9acd5", "555555"));
        schemas.put(4, new Schema("ffffcc", "fefe83", "555555"));
    }

    @Override
    protected void onLoad() {
        EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(
                new LoadSectionAction(originalSection.getId()),
                new AsyncCallback<ClientSection>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExcecutor
                    }

                    @Override
                    public void onSuccess(ClientSection result) {
                        init(result);
                    }
                }, this);
        EventBus.get().fireEvent(loadSettingsEvent);
    }

    /** vygeneruje formular */
    private void init(ClientSection data) {
        clear();

        nameBox = new ConsysStringTextBox(1, 256, ESMessageUtils.c.editFormSectionsSetting_form_name());
        nameBox.setText(data.getName());

        bgBox = new ConsysStringTextBox(6, 6, ESMessageUtils.c.editFormSectionsSetting_form_background()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result && !isHexNum(getText())) {
                    fail.addOrSetText(ESMessageUtils.m.editFormSectionsSetting_error_noHex(getIdentifier()));
                    return false;
                }
                return result;
            }
        };
        bgBox.setText(data.getBgColor());

        fgBox = new ConsysStringTextBox(6, 6, ESMessageUtils.c.editFormSectionsSetting_form_foreground()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result && !isHexNum(getText())) {
                    fail.addOrSetText(ESMessageUtils.m.editFormSectionsSetting_error_noHex(getIdentifier()));
                    return false;
                }
                return result;
            }
        };
        fgBox.setText(data.getFgColor());

        fontBox = new ConsysStringTextBox(6, 6, ESMessageUtils.c.editFormSectionsSetting_form_text()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result && !isHexNum(getText())) {
                    fail.addOrSetText(ESMessageUtils.m.editFormSectionsSetting_error_noHex(getIdentifier()));
                    return false;
                }
                return result;
            }
        };
        fontBox.setText(data.getFontColor() == null ? "555555" : data.getFontColor());

        authorizedUsers = new FlowPanel();
        ActionLabel addUser = new ActionLabel(ESMessageUtils.c.editFormSectionsSetting_form_addUser());
        addUser.setClickHandler(selectUserHandler());
        authorizedUsers.add(addUser);
        if (!data.getAuthorizedUsers().isEmpty()) {
            EventBus.get().fireEvent(new DispatchEvent(new ListUserThumbsAction(data.getAuthorizedUsers()),
                    new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                            for (ClientUserThumb cut : result.getArrayListResult()) {
                                authorizedUsers.add(new EditFormSectionsSettingUserUI(cut.getUuid(), cut.getName(), true));
                            }
                            authorizedUsers.add(StyleUtils.clearDiv());
                        }
                    }, this));
        } else {
            authorizedUsers.add(StyleUtils.clearDiv());
        }

        final SimplePanel previewPanel = new SimplePanel();
        previewPanel.addStyleName(FLOAT_LEFT);
        previewPanel.addStyleName(MARGIN_LEFT_10);
        previewPanel.setWidget(previewObject(bgBox.getText(), fgBox.getText(), fontBox.getText()));

        FlowPanel preview = new FlowPanel();
        preview.addStyleName(FLOAT_LEFT);
        preview.addStyleName(MARGIN_LEFT_20);
        preview.add(StyleUtils.getStyledLabel(ESMessageUtils.c.editFormSectionsSetting_text_preview() + ":", FONT_BOLD, FLOAT_LEFT));
        preview.add(previewPanel);
        preview.add(StyleUtils.clearDiv());

        ArrayList<SelectBoxItem<Integer>> schemaBoxItems = new ArrayList<SelectBoxItem<Integer>>();
        schemaBoxItems.add(new SelectBoxItem<Integer>(1, ESMessageUtils.c.editFormSectionsSetting_text_green()));
        schemaBoxItems.add(new SelectBoxItem<Integer>(2, ESMessageUtils.c.editFormSectionsSetting_text_blue()));
        schemaBoxItems.add(new SelectBoxItem<Integer>(3, ESMessageUtils.c.editFormSectionsSetting_text_violet()));
        schemaBoxItems.add(new SelectBoxItem<Integer>(4, ESMessageUtils.c.editFormSectionsSetting_text_yellow()));

        SelectBox<Integer> schemaBox = new SelectBox<Integer>();
        schemaBox.setItems(schemaBoxItems);
        schemaBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                Schema s = schemas.get(event.getValue().getItem());
                if (s != null) {
                    previewPanel.setWidget(previewObject(s.getBg(), s.getFg(), s.getFnt()));
                    bgBox.setText(s.getBg());
                    fgBox.setText(s.getFg());
                    fontBox.setText(s.getFnt());
                }
            }
        });

        addHandlers(schemaBox, previewPanel);

        ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_save());
        button.addClickHandler(buttonClickHandler());

        form = new BaseForm();
        form.addStyleName(FLOAT_LEFT);
        form.addOptional(0, ESMessageUtils.c.editFormSectionsSetting_form_schema(), schemaBox);
        form.addRequired(1, ESMessageUtils.c.editFormSectionsSetting_form_name(), nameBox);
        form.addRequired(2, ESMessageUtils.c.editFormSectionsSetting_form_background(), bgBox);
        form.addRequired(3, ESMessageUtils.c.editFormSectionsSetting_form_foreground(), fgBox);
        form.addRequired(4, ESMessageUtils.c.editFormSectionsSetting_form_text(), fontBox);
        form.addOptional(5, ESMessageUtils.c.editFormSectionsSetting_form_authorizedUsers(), authorizedUsers);
        form.addActionMembers(6, button, ActionLabel.breadcrumbBackCancel());

        addWidget(form);
        addWidget(preview);
        addWidget(StyleUtils.clearDiv());
    }

    /** vraci true pokud se jedna o hexadecimalni cislo o sesti znacich */
    private boolean isHexNum(String text) {
        if (text == null) {
            return false;
        }
        return text.trim().matches(HEXNUMBER);
    }

    /** pokud jsou vsechny tri barvy platne, prekresli nahled */
    private void actualizePreview(SimplePanel previewPanel) {
        if (isHexNum(bgBox.getText()) && isHexNum(fgBox.getText()) && isHexNum(fontBox.getText())) {
            previewPanel.setWidget(previewObject(bgBox.getText(), fgBox.getText(), fontBox.getText()));
        }
    }

    /** nastavi handlery pro aktualizaci nahledu */
    private void addHandlers(final SelectBox<Integer> schemaBox, final SimplePanel previewPanel) {
        bgBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                Schema s = schemaBox.getSelectedItem() == null ? null : schemas.get(schemaBox.getSelectedItem().getItem());
                if (s != null && !bgBox.getText().equals(s.getBg())) {
                    schemaBox.setSelectedItem(null);
                }
                actualizePreview(previewPanel);
            }
        });
        fgBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                Schema s = schemaBox.getSelectedItem() == null ? null : schemas.get(schemaBox.getSelectedItem().getItem());
                if (s != null && !fgBox.getText().equals(s.getBg())) {
                    schemaBox.setSelectedItem(null);
                }
                actualizePreview(previewPanel);
            }
        });
        fontBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                Schema s = schemaBox.getSelectedItem() == null ? null : schemas.get(schemaBox.getSelectedItem().getItem());
                if (s != null && !fontBox.getText().equals(s.getBg())) {
                    schemaBox.setSelectedItem(null);
                }
                actualizePreview(previewPanel);
            }
        });
    }

    /** vraci clickHandler pro potvrzeni editace */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final ClientSection section = new ClientSection();
                section.setId(originalSection.getId());
                section.setName(nameBox.getText());
                section.setBgColor(bgBox.getText());
                section.setFgColor(fgBox.getText());
                section.setFontColor(fontBox.getText());
                for (int i = 0; i < authorizedUsers.getWidgetCount(); i++) {
                    Widget w = authorizedUsers.getWidget(i);
                    if (w instanceof EditFormSectionsSettingUserUI) {
                        EditFormSectionsSettingUserUI ui = (EditFormSectionsSettingUserUI) w;
                        String uuid = ui.getUUID();
                        if (!section.getAuthorizedUsers().contains(uuid)) {
                            section.getAuthorizedUsers().add(uuid);
                        }
                    }
                }

                EventDispatchEvent updateEvent = new EventDispatchEvent(
                        new UpdateSectionAction(section),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                source.setData(section);
                                FormUtils.fireBreadcrumbBack();
                            }
                        },
                        EditFormSectionsSetting.this);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }

    /** clickHandler pro zobrazeni pridavaciho handleru */
    private ClickHandler selectUserHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog sd = new UserSearchDialog(true);
                sd.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        authorizedUsers.insert(new EditFormSectionsSettingUserUI(item.getUuid(), item.getName(), true),
                                authorizedUsers.getWidgetCount() - 1);
                    }
                });
                sd.showCentered();
            }
        };
    }

    /** vytvori objekt se znazornenim nahledu a vrati ho */
    private SimplePanel previewObject(String bg, String fg, String fnt) {
        SimplePanel p1 = new SimplePanel();
        p1.setSize("50px", "50px");
        p1.addStyleName(StyleUtils.FLOAT_LEFT);
        DOM.setStyleAttribute(p1.getElement(), "backgroundColor", "#" + bg);
        SimplePanel p2 = new SimplePanel();
        p2.setSize("40px", "40px");
        p2.addStyleName(StyleUtils.MARGIN_TOP_3);
        p2.addStyleName(StyleUtils.MARGIN_LEFT_3);
        DOM.setStyleAttribute(p2.getElement(), "backgroundColor", "#" + fg);
        HTML p3 = new HTML("123");
        p3.addStyleName(StyleUtils.MARGIN_TOP_3);
        p3.addStyleName(StyleUtils.MARGIN_LEFT_3);
        DOM.setStyleAttribute(p3.getElement(), "color", "#" + fnt);
        p2.setWidget(p3);
        p1.setWidget(p2);
        return p1;
    }

    /** pomocna trida pro vybirani barevnych schemat */
    private class Schema {

        private String bg;
        private String fg;
        private String fnt;

        public Schema(String bg, String fg, String fnt) {
            this.bg = bg;
            this.fg = fg;
            this.fnt = fnt;
        }

        public String getBg() {
            return bg;
        }

        public String getFg() {
            return fg;
        }

        public String getFnt() {
            return fnt;
        }
    }
}
