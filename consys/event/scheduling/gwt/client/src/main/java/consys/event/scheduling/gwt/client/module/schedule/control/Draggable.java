package consys.event.scheduling.gwt.client.module.schedule.control;

import consys.event.scheduling.gwt.client.module.schedule.item.Entity;

/**
 *
 * @author pepa
 */
public class Draggable {

    public int desiredX;
    public int desiredY;
    public int relativeX;
    public int relativeY;
    final int offsetHeight;
    final int offsetWidth;
    public TimeLabel timeLabel = null;
    public Positioner positioner = null;
    final Entity entity;

    public Draggable(Entity entity) {
        this.entity = entity;
        offsetWidth = entity.getOffsetWidth();
        offsetHeight = entity.getOffsetHeight();
    }
}
