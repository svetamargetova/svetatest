package consys.event.scheduling.gwt.client.module.schedule.control;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Pozicovaci ramecek
 * @author pepa
 */
public class Positioner extends AbsolutePanel {

    public Positioner(int width, int height) {
        super();
        setPixelSize(width, height);
        setStyleName(ESResourceUtils.bundle().css().positioner());
    }
}
