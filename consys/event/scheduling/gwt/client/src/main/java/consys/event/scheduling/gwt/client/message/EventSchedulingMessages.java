package consys.event.scheduling.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface EventSchedulingMessages extends Messages {

    String blockDropController_error_blockCannotContainThisEntity(String room, String from, String to);

    String blockDropController_error_invalidEntityPosition(String room, String from, String to);

    String blockSplitter_error_alreadyInserted(String value);

    String editFormSectionsSetting_error_noHex(String value);
}
