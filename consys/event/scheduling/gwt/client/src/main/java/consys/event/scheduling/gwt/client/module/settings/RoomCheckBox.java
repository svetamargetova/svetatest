package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Jedno zaskrtavatko mistnosti
 * @author pepa
 */
public class RoomCheckBox<T> extends FlowPanel {

    // komponenty
    private ConsysCheckBox check;
    // data
    private T room;

    public RoomCheckBox(T room, String name, boolean checked) {
        this.room = room;

        addStyleName(StyleUtils.MARGIN_TOP_3);

        check = new ConsysCheckBox();
        check.setValue(checked);
        check.addStyleName(StyleUtils.FLOAT_LEFT);
        check.addStyleName(StyleUtils.MARGIN_RIGHT_5);
        add(check);

        Label roomName = StyleUtils.getStyledLabel(name, StyleUtils.FLOAT_LEFT);
        add(roomName);

        add(StyleUtils.clearDiv());
    }

    public boolean isChecked() {
        return check.getValue();
    }

    public void setChecked(boolean value) {
        check.setValue(value);
    }

    public T getRoom() {
        return room;
    }

    public void setEnabled(boolean enabled) {
        check.setEnabled(enabled);
    }

    public void addChangeValueHandler(ChangeValueEvent.Handler<Boolean> handler) {
        check.addChangeValueHandler(handler);
    }
}
