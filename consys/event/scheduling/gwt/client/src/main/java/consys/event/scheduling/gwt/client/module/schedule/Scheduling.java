package consys.event.scheduling.gwt.client.module.schedule;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.HiddableLabel;
import consys.common.gwt.client.ui.comp.Reverser;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.LabelColorEnum;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import consys.event.scheduling.gwt.client.action.LoadScheduleAction;
import consys.event.scheduling.gwt.client.action.LoadScheduleDayAction;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import consys.event.scheduling.gwt.client.bo.ClientSchedule;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleDnDContentImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleDragController;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Vstupni formular planovani
 * @author pepa
 */
public class Scheduling extends RootPanel {

    // komponenty
    private ScheduleDnDContentImpl content;
    public static HiddableLabel hiddableLabel = new HiddableLabel(LabelColorEnum.FAIL_COLOR, "");
    // data
    private ArrayList<ClientDayThumb> frames;
    private boolean showBuffer = false;

    public Scheduling() {
        super(ESMessageUtils.c.scheduling_title());
        hiddableLabel.setStyleName(MARGIN_BOT_10);

        ActionImage mySchedule = ActionImage.getPersonButton(ESMessageUtils.c.myScheduling_title(), true);
        mySchedule.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireNextBreadcrumb(ESMessageUtils.c.myScheduling_title(), new MyScheduling());
            }
        });
        addLeftControll(mySchedule);
    }

    @Override
    protected void onLoad() {
        EventDispatchEvent loadInsert = new EventDispatchEvent(
                new CurrentEventUserRightAction(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN),
                new AsyncCallback<BooleanResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        loadSchedule(false);
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        loadSchedule(result.isBool());
                    }
                }, this);
        EventBus.get().fireEvent(loadInsert);
    }

    /** nacte data rozvrhu */
    private void loadSchedule(final boolean isAdmin) {
        EventDispatchEvent loadScheduleAction = new EventDispatchEvent(new LoadScheduleAction(),
                new AsyncCallback<ClientSchedule>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientSchedule schedule) {
                        frames = schedule.getFrames();

                        // vynulovani obecnych dat z predchoziho planovani
                        ScheduleData.get().reset();
                        // nastaveni sekci a prav
                        ScheduleData.get().setSections(schedule.getSections());
                        ScheduleData.get().setSectionIdRights(schedule.getSectionIdRights());
                        ScheduleData.get().setAdmin(isAdmin);

                        init();
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadScheduleAction);
    }

    /** inicializace komponent */
    private void init() {
        clear();
        Reverser reverser = new Reverser();
        Long firstDay = null;
        int counter = 1;
        Collections.sort(frames, new Comparator<ClientDayThumb>() {

            @Override
            public int compare(ClientDayThumb o1, ClientDayThumb o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        for (final ClientDayThumb thumb : frames) {
            if (firstDay == null) {
                firstDay = thumb.getId();
            }
            String text = "<div class=\"" + FONT_BOLD + " " + FONT_14PX + " " + MARGIN_LEFT_5 + "\">";
            text += ESMessageUtils.c.scheduling_text_day() + " " + counter + "</div>";
            text += "<div class=\"" + FONT_BOLD + " " + FONT_10PX + " " + MARGIN_TOP_3 + " " + MARGIN_LEFT_5 + "\">";
            text += DateTimeUtils.getDate(thumb.getDate()) + "</div>";
            reverser.add(text, new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    loadDay(thumb.getId());
                }
            }, 100, true);
            counter++;
        }
        if (frames.size() == 1) {
            reverser.onlyOneItem();
        }
        addWidget(reverser);
        reverser.setVisible(!frames.isEmpty());

        addWidget(StyleUtils.clearDiv());

        content = GWT.create(ScheduleDnDContentImpl.class);
        content.setParent(this);
        ScheduleData.get().setDndContent(content);

        // zruseni pripadneho predchoziho drag controlleru
        ScheduleDragController.get().destroy();
        // inicializace drag controlleru
        ScheduleDragController.get().init(content);

        final ActionImage buffer = new ActionImage(null, ESMessageUtils.c.scheduling_button_buffer_off());
        buffer.addStyleName(FLOAT_RIGHT);
        buffer.setVisible(ScheduleData.get().isAdmin() || !ScheduleData.get().sectionIdRights().keySet().isEmpty());
        buffer.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showBuffer = !showBuffer;
                content.showBuffer(showBuffer);
                buffer.setText(showBuffer ? ESMessageUtils.c.scheduling_button_buffer_on() : ESMessageUtils.c.scheduling_button_buffer_off());
            }
        });

        if (!frames.isEmpty()) {
            FlowPanel scalePanel = new FlowPanel();
            scalePanel.addStyleName(MARGIN_VER_10);
            scalePanel.add(hiddableLabel);
            scalePanel.add(buffer);
            scalePanel.add(StyleUtils.clearDiv());
            addWidget(scalePanel);
            addWidget(content);

            loadDay(firstDay);
        } else {
            addWidget(StyleUtils.getStyledLabel(ESMessageUtils.c.scheduling_text_noneDaySet(), MARGIN_TOP_10, FONT_BOLD));
        }
    }

    /** spusti nacteni dne podle id */
    private void loadDay(final Long id) {
        if (id == null) {
            getFailMessage().setText("Bad day id");
            return;
        }
        EventDispatchEvent loadScheduleAction = new EventDispatchEvent(new LoadScheduleDayAction(id),
                new AsyncCallback<ClientScheduleDay>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientScheduleDay scheduleDay) {
                        GWT.log("--- loadScheduleDayAction ---");
                        content.setDay(scheduleDay, id);
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadScheduleAction);
    }
}
