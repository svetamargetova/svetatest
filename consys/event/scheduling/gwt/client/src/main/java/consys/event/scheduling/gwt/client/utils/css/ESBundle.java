package consys.event.scheduling.gwt.client.utils.css;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 *
 * @author pepa
 */
public interface ESBundle extends ClientBundle {

    /** ikonka pera */
    @Source("pen.gif")
    public ImageResource pen();

    @Source("es.css")
    public ESCssResources css();
}
