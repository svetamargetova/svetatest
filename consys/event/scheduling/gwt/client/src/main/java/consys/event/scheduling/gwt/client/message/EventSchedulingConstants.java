package consys.event.scheduling.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * Abecedne serazene konstanty
 * @author pepa
 */
public interface EventSchedulingConstants extends Constants {

    String blockDropController_error_updatePositionFail();

    String blockSplitter_action_addSplit();

    String blockSplitter_text_splitInput();

    String drawProvider_text_newBlind();

    String editFormDaySettings_error_divisibleByFive();

    String editFormDaySettings_form_end();

    String editFormDaySettings_form_rooms();

    String editFormDaySettings_form_start();

    String editFormDaySettings_form_splits();

    String editFormDaySettings_text_allRooms();

    String editFormDaySettings_title();

    String editFormSectionsSetting_form_addUser();

    String editFormSectionsSetting_form_authorizedUsers();

    String editFormSectionsSetting_form_background();

    String editFormSectionsSetting_form_foreground();

    String editFormSectionsSetting_form_name();

    String editFormSectionsSetting_form_schema();

    String editFormSectionsSetting_form_text();

    String editFormSectionsSetting_text_blue();

    String editFormSectionsSetting_text_green();

    String editFormSectionsSetting_text_preview();

    String editFormSectionsSetting_text_violet();

    String editFormSectionsSetting_text_yellow();

    String editFormSectionsSetting_title();

    String editFormTalkOverview_form_unassignedTalks();

    String editFormTalkOverview_title();

    String entity_text_pending();

    String entityNoteDialog_form_note();

    String entityNoteDialog_title();

    String myScheduling_title();

    String roomsSetting_error_enteredEmptyValue();

    String roomsSetting_title();

    String scheduleEditBlindDialog_form_name();

    String scheduleEditBlindDialog_form_blindLenght();

    String scheduleEditBlindDialog_title();

    String scheduleEditBlockDialog_action_setChair();

    String scheduleEditBlockDialog_error_changetSectionInNonEmptyBlock();

    String scheduleEditBlockDialog_form_chair();

    String scheduleEditBlockDialog_form_section();

    String scheduleEditBlockDialog_text_none();

    String scheduleEditBlockDialog_title();

    String scheduleEditTalkDialog_error_outOfBlock();

    String scheduleEditTalkDialog_form_author();

    String scheduleEditTalkDialog_form_name();

    String scheduleEditTalkDialog_form_nextRooms();

    String scheduleEditTalkDialog_form_talkLenght();

    String scheduleEditTalkDialog_form_start();

    String scheduleEditTalkDialog_title();

    String scheduleValidator_error_badTimeValue();

    String scheduleValidator_error_entityCollision();

    String scheduleValidator_error_sectionCollision();

    String scheduling_button_buffer_off();

    String scheduling_button_buffer_on();

    String scheduling_text_day();

    String scheduling_text_noneDaySet();

    String scheduling_title();

    String schedulingModule_text_moduleName();

    String schedulingModule_text_roleAdministratorName();

    String schedulingModule_text_roleAdministratorDescription();

    String schedulingModule_text_schedule();

    String schedulingSettings_action_addDay();

    String schedulingSettings_text_beginByAddingDay();

    String schedulingSettings_text_newDay();

    String schedulingSettings_title();

    String sectionsSetting_action_addSection();

    String sectionsSetting_text_beginByAddingSection();

    String sectionsSetting_text_newSection();

    String sectionsSetting_title();

    String talkOverview_text_noneSectionYet();

    String talkOverview_text_sectionIsEmpty();

    String talkOverview_title();
}
