package consys.event.scheduling.gwt.client.module.schedule.item.behaviour;

import com.google.gwt.core.client.GWT;
import consys.event.scheduling.gwt.client.bo.ClientEntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;
import consys.event.scheduling.gwt.client.module.schedule.item.UIEntityFactory.PresenterFactory;

/**
 *
 * @author pepa
 */
public class BlindBehaviour implements PresenterFactory<ClientEntityBlind> {

    @Override
    public EntityMain create(ClientEntityBlind blind) {
        EntityBlind ib = GWT.create(EntityBlind.class);
        ib.setData(blind);
        ib.setName(blind.getName());
        return ib;
    }
}
