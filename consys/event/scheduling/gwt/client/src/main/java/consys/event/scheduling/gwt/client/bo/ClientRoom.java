package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;

/**
 * Mistnost v rozvrhu
 * @author pepa
 */
public class ClientRoom implements IsSerializable {

    private Long id;
    private String name;
    private ArrayList<ClientBlock> blocks;

    public ClientRoom() {
        blocks = new ArrayList<ClientBlock>();
    }

    public ClientRoom(Long id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public ArrayList<ClientBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(ArrayList<ClientBlock> blocks) {
        this.blocks = blocks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
