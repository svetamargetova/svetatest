package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.CommonLongThumb;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientEditableSectionOverview implements Result {

    private static final long serialVersionUID = 5421641615276017890L;
    // data
    private ArrayList<CommonLongThumb> talks;
    private ArrayList<CommonLongThumb> unassignedTalks;

    public ClientEditableSectionOverview() {
        talks = new ArrayList<CommonLongThumb>();
        unassignedTalks = new ArrayList<CommonLongThumb>();
    }

    public ArrayList<CommonLongThumb> getTalks() {
        return talks;
    }

    public void setTalks(ArrayList<CommonLongThumb> talks) {
        this.talks = talks;
    }

    public ArrayList<CommonLongThumb> getUnassignedTalks() {
        return unassignedTalks;
    }

    public void setUnassignedTalks(ArrayList<CommonLongThumb> unassignedTalks) {
        this.unassignedTalks = unassignedTalks;
    }
}
