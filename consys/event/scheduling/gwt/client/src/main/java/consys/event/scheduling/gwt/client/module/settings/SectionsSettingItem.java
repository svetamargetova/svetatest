package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.event.scheduling.gwt.client.action.DeleteSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;

/**
 *
 * @author pepa
 */
public class SectionsSettingItem extends FlowPanel implements ConsysTabPanelItem, ActionExecutionDelegate {

    // komponenty
    private ConsysTabPanelLabel tabLabel;
    // data
    private ConsysMessage helpMessage;
    private ConsysTabPanel tabPanel;

    public SectionsSettingItem(ConsysTabPanelLabel tabLabel, ClientTabItem item, final ConsysMessage helpMessage) {
        super();
        this.tabLabel = tabLabel;
        this.helpMessage = helpMessage;
    }

    /** nastavi data do panelu */
    public void setData(final ClientSection data) {
        clear();
        tabLabel.setText(data.getName());

        ConsysAction editAction = new ConsysAction() {

            @Override
            public void run() {
                EditFormSectionsSetting w = new EditFormSectionsSetting(data, SectionsSettingItem.this);
                String title = ESMessageUtils.c.editFormSectionsSetting_title() + " " + data.getName();
                FormUtils.fireNextBreadcrumb(title, w);
            }
        };

        ConsysAction removeAction = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteSectionAction(data.getId()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // odstraneni v ui
                                tabPanel.removeTab(SectionsSettingItem.this);
                                if (tabPanel.getTabCount() == 0) {
                                    tabPanel.setVisible(false);
                                    helpMessage.setVisible(true);
                                }
                            }
                        }, SectionsSettingItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        };

        EditWithRemover ewr = new EditWithRemover(editAction, removeAction);

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        controlPanel.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        controlPanel.addStyleName(StyleUtils.MARGIN_TOP_10);
        controlPanel.add(ewr);

        add(controlPanel);
        add(StyleUtils.clearDiv());

        init(data);
    }

    /** inicializace obsahu */
    private void init(ClientSection data) {
        HTML permissionHTML = new HTML("<b>" + ESMessageUtils.c.editFormSectionsSetting_form_authorizedUsers() + "</b>:");
        permissionHTML.setSize("100px", "60px");
        permissionHTML.addStyleName(StyleUtils.FLOAT_LEFT);
        permissionHTML.addStyleName(StyleUtils.MARGIN_TOP_10);
        final FlowPanel permissionItem = new FlowPanel();
        permissionItem.addStyleName(StyleUtils.FLOAT_LEFT);
        permissionItem.addStyleName(StyleUtils.MARGIN_TOP_10);
        permissionItem.setWidth("500px");

        if (data.getAuthorizedUsers().isEmpty()) {
            permissionItem.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_noneLower(), StyleUtils.MARGIN_LEFT_10));
        } else {
            EventBus.get().fireEvent(new DispatchEvent(new ListUserThumbsAction(data.getAuthorizedUsers()),
                    new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                            for (ClientUserThumb cut : result.getArrayListResult()) {
                                EditFormSectionsSettingUserUI ui = new EditFormSectionsSettingUserUI(cut.getUuid(),
                                        cut.getName(), false);
                                ui.addStyleName(StyleUtils.MARGIN_LEFT_10);
                                ui.addStyleName(StyleUtils.MARGIN_BOT_10);
                                permissionItem.add(ui);
                            }
                            permissionItem.add(StyleUtils.clearDiv());
                        }
                    }, this));
        }

        HTML bgHTML = new HTML("<b>" + ESMessageUtils.c.editFormSectionsSetting_form_background() + "</b>:");
        bgHTML.setSize("100px", "60px");
        bgHTML.setStyleName(StyleUtils.FLOAT_LEFT);
        bgHTML.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        bgHTML.addStyleName(StyleUtils.ALIGN_RIGHT);
        SimplePanel bgItem = new SimplePanel();
        bgItem.setSize("50px", "50px");
        bgItem.addStyleName(StyleUtils.FLOAT_LEFT);
        DOM.setStyleAttribute(bgItem.getElement(), "backgroundColor", "#" + data.getBgColor());

        HTML fgHTML = new HTML("<b>" + ESMessageUtils.c.editFormSectionsSetting_form_foreground() + "</b>:");
        fgHTML.setSize("100px", "60px");
        fgHTML.setStyleName(StyleUtils.FLOAT_LEFT);
        fgHTML.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        fgHTML.addStyleName(StyleUtils.ALIGN_RIGHT);
        SimplePanel fgItem1 = new SimplePanel();
        fgItem1.setSize("50px", "50px");
        fgItem1.addStyleName(StyleUtils.FLOAT_LEFT);
        DOM.setStyleAttribute(fgItem1.getElement(), "backgroundColor", "#" + data.getBgColor());
        SimplePanel fgItem2 = new SimplePanel();
        fgItem2.setSize("40px", "40px");
        fgItem2.addStyleName(StyleUtils.MARGIN_TOP_3);
        fgItem2.addStyleName(StyleUtils.MARGIN_LEFT_3);
        DOM.setStyleAttribute(fgItem2.getElement(), "backgroundColor", "#" + data.getFgColor());
        fgItem1.setWidget(fgItem2);

        HTML fontHTML = new HTML("<b>" + ESMessageUtils.c.editFormSectionsSetting_form_text() + "</b>:");
        fontHTML.setSize("100px", "60px");
        fontHTML.setStyleName(StyleUtils.FLOAT_LEFT);
        fontHTML.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        fontHTML.addStyleName(StyleUtils.ALIGN_RIGHT);
        SimplePanel fontItem1 = new SimplePanel();
        fontItem1.setSize("50px", "50px");
        fontItem1.addStyleName(StyleUtils.FLOAT_LEFT);
        DOM.setStyleAttribute(fontItem1.getElement(), "backgroundColor", "#" + data.getBgColor());
        SimplePanel fontItem2 = new SimplePanel();
        fontItem2.setSize("40px", "40px");
        fontItem2.addStyleName(StyleUtils.MARGIN_TOP_3);
        fontItem2.addStyleName(StyleUtils.MARGIN_LEFT_3);
        DOM.setStyleAttribute(fontItem2.getElement(), "backgroundColor", "#" + data.getFgColor());
        HTML fontItem3 = new HTML("123");
        fontItem3.addStyleName(StyleUtils.MARGIN_TOP_3);
        fontItem3.addStyleName(StyleUtils.MARGIN_LEFT_3);
        DOM.setStyleAttribute(fontItem3.getElement(), "color", "#" + (data.getFontColor() == null ? "555555" : data.getFontColor()));
        fontItem2.setWidget(fontItem3);
        fontItem1.setWidget(fontItem2);

        FlowPanel content = new FlowPanel();
        content.addStyleName(StyleUtils.MARGIN_LEFT_10);
        content.addStyleName(StyleUtils.MARGIN_BOT_10);
        content.add(permissionHTML);
        content.add(permissionItem);
        content.add(StyleUtils.clearDiv());
        content.add(bgHTML);
        content.add(bgItem);
        content.add(StyleUtils.clearDiv());
        content.add(fgHTML);
        content.add(fgItem1);
        content.add(StyleUtils.clearDiv());
        content.add(fontHTML);
        content.add(fontItem1);
        content.add(StyleUtils.clearDiv());

        add(content);
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel tabPanel) {
        this.tabPanel = tabPanel;
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
    }
}
