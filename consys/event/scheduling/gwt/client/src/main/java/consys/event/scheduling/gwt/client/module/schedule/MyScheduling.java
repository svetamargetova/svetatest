package consys.event.scheduling.gwt.client.module.schedule;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Reverser;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.action.LoadMyScheduleDayAction;
import consys.event.scheduling.gwt.client.action.LoadScheduleAction;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import consys.event.scheduling.gwt.client.bo.ClientSchedule;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleMyContentImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author pepa
 */
public class MyScheduling extends RootPanel {

    // komponenty
    private ScheduleMyContentImpl content;
    // data
    private ArrayList<ClientDayThumb> frames;

    public MyScheduling() {
        super(ESMessageUtils.c.myScheduling_title());
        addLeftControll(ActionImage.breadcrumbBackBigBack());
    }

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadScheduleAction(),
                new AsyncCallback<ClientSchedule>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientSchedule schedule) {
                        // vynulovani obecnych dat z predchoziho planovani
                        ScheduleData.get().reset();
                        // nastaveni sekci
                        ScheduleData.get().setSections(schedule.getSections());

                        frames = schedule.getFrames();
                        init();
                    }
                }, this));
    }

    /** inicializace komponent */
    private void init() {
        clear();
        Reverser reverser = new Reverser();
        reverser.addStyleName(MARGIN_BOT_20);
        Long firstDay = null;
        int counter = 1;
        Collections.sort(frames, new Comparator<ClientDayThumb>() {

            @Override
            public int compare(ClientDayThumb o1, ClientDayThumb o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        for (final ClientDayThumb thumb : frames) {
            if (firstDay == null) {
                firstDay = thumb.getId();
            }
            String text = "<div class=\"" + FONT_BOLD + " " + FONT_14PX + " " + MARGIN_LEFT_5 + "\">";
            text += ESMessageUtils.c.scheduling_text_day() + " " + counter + "</div>";
            text += "<div class=\"" + FONT_BOLD + " " + FONT_10PX + " " + MARGIN_TOP_3 + " " + MARGIN_LEFT_5 + "\">";
            text += DateTimeUtils.getDate(thumb.getDate()) + "</div>";
            reverser.add(text, new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    loadDay(thumb.getId());
                }
            }, 100, true);
            counter++;
        }
        if (frames.size() == 1) {
            reverser.onlyOneItem();
        }
        addWidget(reverser);
        reverser.setVisible(!frames.isEmpty());

        content = GWT.create(ScheduleMyContentImpl.class);

        if (!frames.isEmpty()) {
            addWidget(content);
            loadDay(firstDay);
        } else {
            addWidget(StyleUtils.getStyledLabel(ESMessageUtils.c.scheduling_text_noneDaySet(), MARGIN_TOP_10, FONT_BOLD));
        }
    }

    /** spusti nacteni dne podle id */
    private void loadDay(final Long id) {
        if (id == null) {
            getFailMessage().setText("Bad day id");
            return;
        }
        EventDispatchEvent loadScheduleAction = new EventDispatchEvent(new LoadMyScheduleDayAction(id),
                new AsyncCallback<ClientScheduleDay>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientScheduleDay scheduleDay) {
                        GWT.log("--- loadMyScheduleDayAction ---");
                        content.setDay(scheduleDay, id);
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadScheduleAction);
    }
}
