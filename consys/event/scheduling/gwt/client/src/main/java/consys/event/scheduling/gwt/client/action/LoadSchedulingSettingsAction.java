package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;

/**
 * Akce pro nacteni nastaveni jednotlivych dni
 * @author pepa
 */
public class LoadSchedulingSettingsAction extends EventAction<ArrayListResult<ClientDayThumb>> {

    private static final long serialVersionUID = 7562328293241636822L;
}
