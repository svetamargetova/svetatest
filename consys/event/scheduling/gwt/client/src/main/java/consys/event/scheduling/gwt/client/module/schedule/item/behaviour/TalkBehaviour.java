package consys.event.scheduling.gwt.client.module.schedule.item.behaviour;

import com.google.gwt.core.client.GWT;
import consys.event.scheduling.gwt.client.bo.ClientEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.item.UIEntityFactory.PresenterBufferFactory;
import consys.event.scheduling.gwt.client.module.schedule.item.UIEntityFactory.PresenterFactory;

/**
 *
 * @author pepa
 */
public class TalkBehaviour implements PresenterFactory<ClientEntityTalk>, PresenterBufferFactory<ClientUnassignedEntityTalk> {

    @Override
    public EntityMain create(ClientEntityTalk talk) {
        EntityTalk it = GWT.create(EntityTalk.class);
        it.setData(talk);
        return it;
    }

    @Override
    public EntityMain bufferCreate(ClientUnassignedEntityTalk talk) {
        EntityTalk it = GWT.create(EntityTalk.class);
        it.setData(talk.getClientEntityTalkForBuffer());
        it.createSelf(ScheduleData.get().sections().get(talk.getSectionId()));
        it.showBufferLook(true);
        return it;
    }
}
