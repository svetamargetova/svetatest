package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro oznaceni entity za oblibenou
 * @author pepa
 */
public class UpdateFavouriteEntityAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 904493072918410763L;
    // data
    private Long entityId;
    private boolean favourite;

    public UpdateFavouriteEntityAction() {
    }

    public UpdateFavouriteEntityAction(Long entityId, boolean favourite) {
        this.entityId = entityId;
        this.favourite = favourite;
    }

    public Long getEntityId() {
        return entityId;
    }

    public boolean isFavourite() {
        return favourite;
    }
}
