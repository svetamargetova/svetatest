package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntity;
import java.util.ArrayList;

/**
 * Nacte seznam neprirazenych entit
 * @author mishak
 */
public class LoadBufferAction extends EventAction<ArrayListResult<ClientUnassignedEntity>> {

    private static final long serialVersionUID = -3948777149738001904L;
    // data
    private ArrayList<Long> sectionIds;

    public LoadBufferAction() {
        sectionIds = new ArrayList<Long>();
    }

    public LoadBufferAction(ArrayList<Long> sectionIds) {
        this.sectionIds = sectionIds;
    }

    public ArrayList<Long> getSectionIds() {
        return sectionIds;
    }
}
