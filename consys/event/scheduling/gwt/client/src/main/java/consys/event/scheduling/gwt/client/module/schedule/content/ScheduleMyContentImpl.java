package consys.event.scheduling.gwt.client.module.schedule.content;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleMyBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Uzivateluv rozvrh
 * @author pepa
 */
public class ScheduleMyContentImpl extends AbsolutePanel {

    // komponenty
    private ScheduleGridImpl grid;
    private ScheduleBackgrounds backgrounds;
    private ScheduleBlockPanel blocks;
    private AbsolutePanel scroller;
    // data
    private Long dayId;

    public ScheduleMyContentImpl() {
        setSize(ScheduleDnDContentImpl.WIDTH + "px", 10 + "px");

        scroller = new AbsolutePanel();
        scroller.setSize(ScheduleDnDContentImpl.WIDTH + "px", (DOMX.getScreenHeight() - 450) + "px");
        DOM.setStyleAttribute(scroller.getElement(), "overflow", "auto");
        add(scroller, 0, 0);

        backgrounds = new ScheduleBackgrounds(10, 10);
        ScheduleData.get().setBackgrounds(backgrounds);
        scroller.add(backgrounds, 0, 0);

        grid = GWT.create(ScheduleGridImpl.class);
        scroller.add(grid, 0, 0);

        blocks = new ScheduleBlockPanel(10, 10);
        scroller.add(blocks, 0, 0);
    }

    /** injectovaci metoda pro aktualni den */
    public void setDay(final ClientScheduleDay scheduleDay, Long dayId) {
        this.dayId = dayId;
        backgrounds.clear();
        blocks.clear();
        ScheduleData.get().blocksUI().clear();
        ScheduleData.get().bgPosition().clear();

        ScheduleData.get().setRooms(scheduleDay.getRooms());
        Collections.sort(scheduleDay.getRooms(), new Comparator<ClientRoom>() {

            @Override
            public int compare(ClientRoom o1, ClientRoom o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        grid.setDay(scheduleDay);

        ArrayList<String> uuids = new ArrayList<String>();
        for (ClientRoom room : scheduleDay.getRooms()) {
            for (ClientBlock cb : room.getBlocks()) {
                if (cb.getChairUuid() != null) {
                    uuids.add(cb.getChairUuid());
                }
            }
        }

        if (!uuids.isEmpty()) {
            // je potreba natahnout jmena predsedu
            EventBus.get().fireEvent(new DispatchEvent(new ListUserThumbsAction(uuids),
                    new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                            for (ClientUserThumb cut : result.getArrayListResult()) {
                                ScheduleData.get().chairmans().put(cut.getUuid(), cut.getName());
                            }
                            generateBlocks(scheduleDay, getConst());
                        }
                    }, null));
        } else {
            generateBlocks(scheduleDay, getConst());
        }
    }

    /** vygeneruje bloky */
    private void generateBlocks(ClientScheduleDay scheduleDay, ScheduleDnDContentConst cc) {
        final int scale = ScheduleData.get().getScale();
        // pozice odkud se vyskladava rozvrh
        final int x = (ScheduleGridImpl.CORNER_W * scale) + cc.getDiffStartX();
        final int y = (ScheduleGridImpl.CORNER_H * scale) + cc.getDiffStartY();
        // sirka mistnosti
        final int roomWidth = (ScheduleGridImpl.ROOM_W * scale) + cc.getDiffRoomWidth();

        int xT = x;
        int yT = y;

        // vygenerovani bloku
        int roomCounter = 1;
        for (ClientRoom room : scheduleDay.getRooms()) {
            int blockCounter = 1;
            for (ClientBlock cb : room.getBlocks()) {
                BlockPosition bp = new BlockPosition(roomCounter, blockCounter);
                ScheduleData.get().bgPosition().put(bp, new BlockPosition(xT, yT));

                ScheduleMyBlockImpl sb = GWT.create(ScheduleMyBlockImpl.class);
                sb.setScale(scale);
                ScheduleData.get().blocksUI().put(bp, sb);

                sb.setBlockOnPosition(cb, bp, room.getId());
                blocks.add(sb, xT, yT);

                yT += sb.getTotalHeight() + cc.getDiffTotalHeight();
                blockCounter++;
            }
            xT += roomWidth;
            yT = y;
            roomCounter++;
        }

        // nastavit vysku panelu pozadi a bloku podle aktualni mrizky
        int gridWidth = grid.getOffsetWidth();
        int gridHeight = grid.getOffsetHeight();
        blocks.setSize(gridWidth, gridHeight);
        backgrounds.setSize(gridWidth, gridHeight);
        scroller.setHeight((gridHeight + 65) + "px");
        setHeight((gridHeight + 65) + "px");

        // vygenerovani obsahu bloku
        for (int i = 0; i < blocks.getWidgetCount(); i++) {
            Widget w = blocks.getWidget(i);
            if (w instanceof ScheduleMyBlockImpl) {
                ScheduleMyBlockImpl impl = (ScheduleMyBlockImpl) w;
                impl.generateItems();
            }
        }
    }

    /** vraci objekt s rozdily pozic pro vypocet pozice bloku nad mrizkou */
    protected ScheduleDnDContentConst getConst() {
        return new ScheduleDnDContentConst(2, 3, 2, 1);
    }
}
