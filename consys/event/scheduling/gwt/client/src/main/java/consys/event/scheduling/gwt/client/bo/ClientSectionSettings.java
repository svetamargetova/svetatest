package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.CommonLongThumb;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientSectionSettings implements Result {

    private static final long serialVersionUID = -3721640040060072416L;
    // data
    private ClientSection firstSection;
    private ArrayList<CommonLongThumb> sectionThumbs;

    public ClientSectionSettings() {
        sectionThumbs = new ArrayList<CommonLongThumb>();
    }

    public ClientSection getFirstSection() {
        return firstSection;
    }

    public void setFirstSection(ClientSection firstSection) {
        this.firstSection = firstSection;
    }

    public ArrayList<CommonLongThumb> getSectionThumbs() {
        return sectionThumbs;
    }

    public void setSectionThumbs(ArrayList<CommonLongThumb> sectionThumbs) {
        this.sectionThumbs = sectionThumbs;
    }
}
