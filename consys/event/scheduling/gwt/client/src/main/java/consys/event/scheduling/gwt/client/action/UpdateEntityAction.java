package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;

/**
 * Akce pro aktualizaci obecne entity z dialogu
 * @author pepa
 */
public class UpdateEntityAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7645059879692719913L;
    // data
    private Long entityId;
    private ClientTime from;
    private int length;
    private ArrayList<Long> roomsId;

    public UpdateEntityAction() {
    }

    public UpdateEntityAction(Long entityId, ClientTime from, int length) {
        this.entityId = entityId;
        this.from = from;
        this.length = length;
        this.roomsId = new ArrayList<Long>();
    }

    public UpdateEntityAction(Long entityId, ClientTime from, int length, ArrayList<Long> roomsId) {
        this.entityId = entityId;
        this.from = from;
        this.length = length;
        this.roomsId = roomsId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public ClientTime getFrom() {
        return from;
    }

    public int getLength() {
        return length;
    }

    public ArrayList<Long> getRoomsId() {
        return roomsId;
    }
}
