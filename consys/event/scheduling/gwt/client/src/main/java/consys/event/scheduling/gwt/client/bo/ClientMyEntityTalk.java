package consys.event.scheduling.gwt.client.bo;

/**
 *
 * @author pepa
 */
public class ClientMyEntityTalk extends ClientMyEntity {

    // data
    private String author;
    private String name;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
