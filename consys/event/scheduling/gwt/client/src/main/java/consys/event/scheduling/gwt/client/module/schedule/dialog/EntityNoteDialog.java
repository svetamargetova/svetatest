package consys.event.scheduling.gwt.client.module.schedule.dialog;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.scheduling.gwt.client.action.UpdateEntityNoteAction;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Dialog pro editovani poznamek k entitam
 * @author pepa
 */
public abstract class EntityNoteDialog extends AbstractScheduleEditDialog {

    // komponenty
    private ConsysTextArea noteBox;
    // data
    private Long entityId;

    public EntityNoteDialog(Long entityId, String note) {
        this.entityId = entityId;
        noteBox = new ConsysTextArea(0, 5000, ESMessageUtils.c.entityNoteDialog_form_note());
        noteBox.setText(note != null ? note : "");
    }

    @Override
    protected Widget createContent() {
        final BaseForm form = new BaseForm("180px");
        ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_update());
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }
                final String newNote = noteBox.getText().trim();
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateEntityNoteAction(entityId, newNote),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                successUpdate(newNote);
                                hide();
                            }
                        }, EntityNoteDialog.this));
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        form.addRequired(0, ESMessageUtils.c.entityNoteDialog_form_note(), noteBox);
        form.addActionMembers(1, button, cancel);

        panel().clear();
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(StyleUtils.getStyledLabel(ESMessageUtils.c.entityNoteDialog_title(),
                StyleUtils.FONT_17PX, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_LEFT_10));
        panel().add(ESResourceUtils.dialogSeparator10px());
        panel().add(form);
        panel().add(ESResourceUtils.dialogSeparator10px());
        return mainPanel();
    }

    /** vraci string nove poznamky */
    public abstract void successUpdate(String note);
}
