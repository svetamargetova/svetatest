package consys.event.scheduling.gwt.client.module.schedule.content;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.event.scheduling.gwt.client.action.LoadBufferAction;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntity;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.control.BufferDropController;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleDragController;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.module.schedule.item.DrawProvider;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import java.util.ArrayList;

/**
 * Zasobnik neprirazenych prednasek
 * @author pepa
 */
public class ScheduleBufferImpl extends AbsolutePanel {

    // konstanty
    public static final int HEIGHT = 100;
    // data
    private boolean loaded;
    private Scheduling parent;
    private BufferDropController dropController;
    private ScheduleDnDContentImpl content;
    // data - matice
    private int matrixItems = 0;
    private ArrayList<EntityPosition> matrixFree;

    public ScheduleBufferImpl() {
        loaded = false;
        setSize();
        DOM.setStyleAttribute(getElement(), "overflow", "auto");
        addStyleName(ESResourceUtils.bundle().css().bufferBody());

        matrixFree = new ArrayList<EntityPosition>();
    }

    @Override
    protected void onLoad() {
        ScheduleData.get().setBuffer(this);
        dropController = new BufferDropController();
        ScheduleDragController.get().registerDropController(dropController);
    }

    @Override
    protected void onUnload() {
        ScheduleDragController.get().unregisterDropController(dropController);
    }

    public void setDNDContent(ScheduleDnDContentImpl content) {
        this.content = content;
    }

    /** pro zobrazeni waitingu */
    public void setParent(Scheduling parent) {
        this.parent = parent;
    }

    /** nastaveni rozmeru v zavislosti na prohlizeci */
    protected void setSize() {
        setSize(ScheduleDnDContentImpl.WIDTH + "px", HEIGHT + "px");
    }

    @Override
    public void setVisible(boolean visible) {
        content.bufferShow(visible);
        if (visible && !loaded) {
            ArrayList<Long> list;
            if (ScheduleData.get().isAdmin()) {
                list = new ArrayList<Long>(ScheduleData.get().sections().keySet());
            } else {
                list = new ArrayList<Long>(ScheduleData.get().sectionIdRights().keySet());
            }
            EventBus.get().fireEvent(new EventDispatchEvent(
                    new LoadBufferAction(list),
                    new AsyncCallback<ArrayListResult<ClientUnassignedEntity>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUnassignedEntity> result) {
                            DrawProvider.get().initBuffer(result.getArrayListResult(), ScheduleBufferImpl.this);
                            ScheduleBufferImpl.super.setVisible(true);
                        }
                    }, parent));
            loaded = true;
        } else {
            super.setVisible(visible);
        }
    }

    @Override
    public boolean remove(Widget w) {
        int index = getWidgetIndex(w);
        if (index != -1 && w instanceof EntityBlind && ((EntityBlind) w).isNew()) {
            EntityBlind eb = (EntityBlind) w;
            EntityBlind child = DrawProvider.get().bufferEntityBlind();
            eb.setNew(false);
            child.setNew(true);
            insert(child, index);
        } else {
            matrixItems--;
            matrixFree.add(new EntityPosition(getWidgetLeft(w), getWidgetTop(w)));
        }
        return super.remove(w);
    }

    /** vlozi entity do razene matice bufferu */
    public void addToMatrix(Entity entity) {
        int row = 0;
        int col = 0;
        boolean found = false;

        if (!matrixFree.isEmpty()) {
            EntityPosition ep = matrixFree.get(0);
            matrixFree.remove(ep);
            col = ep.getX();
            row = ep.getY();
            found = true;
        }

        if (!found) {
            // posledni pozice
            row = matrixItems / 5;
            col = matrixItems % 5;
            // sirka + 3a3 margin a vyska 3a3 margin
            add(entity, col * ((ScheduleGridImpl.ROOM_W * ScheduleData.get().getScale()) + 6), row * (36));
        } else {
            add(entity, col, row);
        }
        matrixItems++;
    }

    /** pomocny objekt pro urcovani pozice */
    private class EntityPosition {

        // data
        private int x;
        private int y;

        public EntityPosition(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final EntityPosition other = (EntityPosition) obj;
            if (this.x != other.x) {
                return false;
            }
            if (this.y != other.y) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 71 * hash + this.x;
            hash = 71 * hash + this.y;
            return hash;
        }
    }
}
