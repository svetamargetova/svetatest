package consys.event.scheduling.gwt.client.module.schedule.content;

/**
 *
 * @author pepa
 */
public class ScheduleMyContentImplIE extends ScheduleMyContentImpl {

    @Override
    protected ScheduleDnDContentConst getConst() {
        return new ScheduleDnDContentConst(0, 0, 0, 0);
    }
}
