package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Aktualizace poznamky k entite
 * @author pepa
 */
public class UpdateEntityNoteAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 2470080125868790832L;
    // data
    private Long entityId;
    private String note;

    public UpdateEntityNoteAction() {
    }

    public UpdateEntityNoteAction(Long entityId, String note) {
        this.entityId = entityId;
        this.note = note;
    }

    public Long getEntityId() {
        return entityId;
    }

    public String getNote() {
        return note;
    }
}
