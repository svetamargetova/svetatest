package consys.event.scheduling.gwt.client.module.schedule.control;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Cas pozicovaciho ramecku
 * @author pepa
 */
public class TimeLabel extends AbsolutePanel {

    // data
    private Label timeLabel;
    private int entityLength;

    public TimeLabel(int entityLength) {
        this.entityLength = entityLength;

        setStyleName(ESResourceUtils.bundle().css().timeLabel());

        timeLabel = StyleUtils.getStyledLabel("", ESResourceUtils.bundle().css().label());
        add(timeLabel);
    }

    /** aktualizuje hodnotu casu */
    public void update(int from) {
        timeLabel.setText(new ClientTime(from) + " " + FormUtils.NDASH + " " + new ClientTime(from + entityLength));
    }
}
