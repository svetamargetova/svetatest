package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani mistnosti
 * @author pepa
 */
public class DeleteRoomInfoAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 6676078135817865465L;
    // data
    private Long id;

    public DeleteRoomInfoAction() {
    }

    public DeleteRoomInfoAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
