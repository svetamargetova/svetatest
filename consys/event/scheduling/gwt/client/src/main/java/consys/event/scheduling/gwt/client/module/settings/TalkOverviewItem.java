package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverview;
import consys.event.scheduling.gwt.client.module.schedule.TalkOverview;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;

/**
 *
 * @author pepa
 */
public class TalkOverviewItem extends FlowPanel implements ConsysTabPanelItem {

    // komponenty
    private ActionLabel edit;
    // data
    private TalkOverview parent;
    private Long sectionId;

    public TalkOverviewItem(TalkOverview parent, final Long sectionId, final String sectionName) {
        this.parent = parent;
        this.sectionId = sectionId;

        edit = new ActionLabel(UIMessageUtils.c.const_edit(), StyleUtils.FLOAT_RIGHT);
        edit.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EditFormTalkOverview w = new EditFormTalkOverview(sectionId, sectionName);
                String title = ESMessageUtils.c.editFormTalkOverview_title() + " " + sectionName;
                FormUtils.fireNextBreadcrumb(title, w);
            }
        });
        edit.setVisible(false);

        EventBus.get().fireEvent(new EventDispatchEvent(
                new CurrentEventUserRightAction(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN),
                new AsyncCallback<BooleanResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        if (result.isBool()) {
                            edit.setVisible(true);
                        }
                    }
                }, null));
    }

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new EventDispatchEvent(
                new LoadTalkOverviewAction(sectionId),
                new AsyncCallback<ClientSectionOverview>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientSectionOverview result) {
                        setData(result);
                    }
                }, parent));
    }

    /** nastavi data zalozky */
    public void setData(ClientSectionOverview overview) {
        clear();

        FlowPanel wrapper = new FlowPanel();
        wrapper.addStyleName(StyleUtils.MARGIN_TOP_20);
        wrapper.addStyleName(StyleUtils.MARGIN_BOT_10);
        wrapper.addStyleName(StyleUtils.MARGIN_HOR_10);

        wrapper.add(edit);
        wrapper.add(StyleUtils.clearDiv());

        int counter = 1;
        for (String talk : overview.getTalks()) {
            Label counterLabel = StyleUtils.getStyledLabel(counter + ".", StyleUtils.FONT_BOLD, StyleUtils.MARGIN_BOT_10,
                    StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT, StyleUtils.MARGIN_RIGHT_5);
            counterLabel.setWidth("50px");
            Label titleLabel = StyleUtils.getStyledLabel(talk, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_BOT_10,
                    StyleUtils.FLOAT_LEFT);
            titleLabel.setWidth("600px");
            wrapper.add(counterLabel);
            wrapper.add(titleLabel);
            wrapper.add(StyleUtils.clearDiv());
            counter++;
        }
        if (overview.getTalks().isEmpty()) {
            wrapper.add(StyleUtils.getStyledLabel(ESMessageUtils.c.talkOverview_text_sectionIsEmpty(), StyleUtils.FONT_BOLD));
        }
        add(wrapper);
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel tabPanel) {
        // neni tu potreba
    }
}
