package consys.event.scheduling.gwt.client.module.schedule.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.action.UpdateFavouriteEntityAction;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.dialog.EntityNoteDialog;

/**
 *
 * @author pepa
 */
public class MyEntityTalk extends EntityMain<ClientMyEntityTalk> {

    // konstanty
    public static final double OPACITY_PART = 0.2d;
    public static final double OPACITY_FULL = 1d;
    // komponenty
    private PopupHelp pup;
    // data
    private HandlerRegistration overReg;
    private HandlerRegistration outReg;

    public MyEntityTalk() {
        getHandle().addStyleName(StyleUtils.HAND);
        getHandle().addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                getHandle().setFocus(false);
            }
        });
        getHandle().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final boolean isFavourite = getData().isFavourite();
                EventBus.get().fireEvent(new EventDispatchEvent(new UpdateFavouriteEntityAction(getData().getId(), !isFavourite),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // TODO: chyby si musim zpracovat sam!
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                getData().setFavourite(!isFavourite);
                                refreshOpacity();
                                if (!isFavourite) {
                                    registerOverOutHandler();
                                } else {
                                    if (pup != null) {
                                        pup.hide();
                                    }
                                    if (overReg != null) {
                                        overReg.removeHandler();
                                        overReg = null;
                                        outReg.removeHandler();
                                        outReg = null;
                                    }
                                }
                            }
                        }, null));
            }
        });
    }

    /** zaregistruje handlery po prejeti a odjeti */
    private void registerOverOutHandler() {
        final FlowPanel helpPanel = new FlowPanel();
        helpPanel.addStyleName(StyleUtils.MARGIN_HOR_10);
        helpPanel.addStyleName(StyleUtils.MARGIN_VER_10);
        pup = new PopupHelp(helpPanel, MyEntityTalk.this);

        overReg = getHandle().addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                helpPanel.clear();
                if (getBlock() != null) {
                    helpPanel.add(StyleUtils.getStyledLabel(getTimeLabel().getText(), StyleUtils.FONT_BOLD));
                }

                String name = getData().getName();
                name = name.replaceAll("\\s+", " ");

                if (name.length() < EntityTalk.MAX_PUP_ROW_CHARS) {
                    helpPanel.add(StyleUtils.getStyledLabel(name, StyleUtils.FONT_16PX, StyleUtils.MARGIN_VER_5));
                } else {
                    String[] rows = EntityTalk.nameRows(name);
                    helpPanel.add(StyleUtils.getStyledLabel(rows[0], StyleUtils.FONT_16PX, StyleUtils.MARGIN_TOP_5));
                    for (int i = 1; i < rows.length; i++) {
                        if (i != rows.length - 1) {
                            helpPanel.add(StyleUtils.getStyledLabel(rows[i], StyleUtils.FONT_16PX));
                        } else {
                            helpPanel.add(StyleUtils.getStyledLabel(rows[i], StyleUtils.FONT_16PX, StyleUtils.MARGIN_BOT_5));
                        }
                    }
                }

                helpPanel.add(StyleUtils.getStyledLabel(getData().getAuthor(), StyleUtils.FONT_BOLD));
                pup.show();
            }
        });
        outReg = getHandle().addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                pup.hide();
            }
        });
    }

    @Override
    protected void showEditDialog() {
        new EntityNoteDialog(getData().getId(), getData().getNote()) {

            @Override
            public void successUpdate(String note) {
                getData().setNote(note);
            }
        }.showCentered();
    }

    @Override
    public void createSelf(ClientSection section) {
        super.createSelf(section);

        Label nameLabel = StyleUtils.getStyledLabel(getData().getName(), StyleUtils.FONT_11PX, StyleUtils.MARGIN_HOR_5);
        Label authorLabel = StyleUtils.getStyledLabel(getData().getAuthor(), StyleUtils.FONT_11PX, StyleUtils.MARGIN_HOR_5);

        setWidgetTextColor(getTimeLabel());
        setWidgetTextColor(nameLabel);
        setWidgetTextColor(authorLabel);

        mainPanel().clear();
        mainPanel().add(nameLabel);
        mainPanel().add(authorLabel);

        if (getData().isFavourite()) {
            registerOverOutHandler();
        }
    }

    @Override
    public void setBlock(ScheduleBlock block) {
        super.setBlock(block);
        refreshOpacity();
    }

    /** aktualizuje pruhlednost */
    private void refreshOpacity() {
        boolean isFavourite = getData().isFavourite();
        DOMX.setOpacity(getElement(), isFavourite ? OPACITY_FULL : OPACITY_PART);
        for (Entity e : getSlaves()) {
            DOMX.setOpacity(e.getElement(), isFavourite ? OPACITY_FULL : OPACITY_PART);
        }
    }
}
