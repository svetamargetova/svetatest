package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Inicialni klientska data planovani
 * @author pepa
 */
public class ClientSchedule implements IsSerializable, Result {

    private static final long serialVersionUID = 6462900546717093355L;
    // data
    private ArrayList<ClientDayThumb> frames;
    private ArrayList<ClientSection> sections;
    private ArrayList<Long> sectionIdRights;

    public ClientSchedule() {
        frames = new ArrayList<ClientDayThumb>();
        sections = new ArrayList<ClientSection>();
        sectionIdRights = new ArrayList<Long>();
    }

    public ArrayList<ClientDayThumb> getFrames() {
        return frames;
    }

    public void setFrames(ArrayList<ClientDayThumb> frames) {
        this.frames = frames;
    }

    public ArrayList<Long> getSectionIdRights() {
        return sectionIdRights;
    }

    public void setSectionIdRights(ArrayList<Long> sectionIdRights) {
        this.sectionIdRights = sectionIdRights;
    }

    public ArrayList<ClientSection> getSections() {
        return sections;
    }

    public void setSections(ArrayList<ClientSection> sections) {
        this.sections = sections;
    }
}
