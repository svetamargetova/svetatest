package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.ArrayList;
import java.util.Date;

/**
 * Vytvori dny pro planovani
 * @author pepa
 */
public class CreateDaySettingsAction extends EventAction<ArrayListResult<Long>> {

    private static final long serialVersionUID = -8664658828707839438L;
    // data
    private ArrayList<Date> days;

    public CreateDaySettingsAction() {
        days = new ArrayList<Date>();
    }

    public ArrayList<Date> getDays() {
        return days;
    }
}
