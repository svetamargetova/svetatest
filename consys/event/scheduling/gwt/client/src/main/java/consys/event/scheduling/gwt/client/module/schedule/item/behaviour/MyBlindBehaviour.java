package consys.event.scheduling.gwt.client.module.schedule.item.behaviour;

import com.google.gwt.core.client.GWT;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;
import consys.event.scheduling.gwt.client.module.schedule.item.MyEntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.UIEntityFactory.PresenterFactory;

/**
 *
 * @author pepa
 */
public class MyBlindBehaviour implements PresenterFactory<ClientMyEntityBlind> {

    @Override
    public EntityMain create(ClientMyEntityBlind blind) {
        MyEntityBlind ib = GWT.create(MyEntityBlind.class);
        ib.setData(blind);
        ib.setName(blind.getName());
        return ib;
    }
}
