package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientTime;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;

/**
 * Blok v rozvrhu
 * @author pepa
 */
public class ClientBlock<T extends ClientEntity> implements IsSerializable {

    // data
    private Long blockId;
    private ClientTime from;
    private ClientTime to;
    private String chairUuid;
    private Long sectionId;
    private ArrayList<T> submissions;

    public ClientBlock() {
        submissions = new ArrayList<T>();
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }

    public String getChairUuid() {
        return chairUuid;
    }

    public void setChairUuid(String chairUuid) {
        this.chairUuid = chairUuid;
    }

    public ClientTime getFrom() {
        return from;
    }

    public void setFrom(ClientTime from) {
        this.from = from;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public ArrayList<T> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(ArrayList<T> submissions) {
        this.submissions = submissions;
    }

    public ClientTime getTo() {
        return to;
    }

    public void setTo(ClientTime to) {
        this.to = to;
    }
}
