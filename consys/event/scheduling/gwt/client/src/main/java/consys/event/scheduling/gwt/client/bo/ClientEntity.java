package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientTime;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;

/**
 * Predek jakekoliv polozky rozvrhu
 * @author pepa
 */
public abstract class ClientEntity implements IsSerializable {

    private Long id;
    private ClientTime from;
    private ClientTime to;
    /** pokud vice id, jedna se o plenarku */
    private ArrayList<Long> roomIds;

    public ClientEntity() {
        roomIds = new ArrayList<Long>();
    }

    public ClientTime getFrom() {
        return from;
    }

    public void setFrom(ClientTime from) {
        this.from = from;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<Long> getRoomIds() {
        return roomIds;
    }

    public void setRoomIds(ArrayList<Long> roomIds) {
        this.roomIds = roomIds;
    }

    public ClientTime getTo() {
        return to;
    }

    public void setTo(ClientTime to) {
        this.to = to;
    }
}
