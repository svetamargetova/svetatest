package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.scheduling.gwt.client.action.CreateSectionAction;
import consys.event.scheduling.gwt.client.action.ListSectionsAction;
import consys.event.scheduling.gwt.client.action.LoadSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.bo.ClientSectionSettings;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;

/**
 * Nastaveni sekci
 * @author pepa
 */
public class SectionsSetting extends RootPanel {

    // komponenty
    private ConsysTabPanel<String> tabs;
    private ConsysMessage helpMessage;
    // data
    private boolean firstLoad = true;

    public SectionsSetting() {
        super(ESMessageUtils.c.sectionsSetting_title());

        addLeftControll(ActionImage.breadcrumbBackBigBack());

        ActionLabel addSection = new ActionLabel(ESMessageUtils.c.sectionsSetting_action_addSection());
        addSection.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewSectionUI> cd = new NewItemDialog<NewSectionUI>(ESMessageUtils.c.sectionsSetting_text_newSection(), helpMessage) {

                    @Override
                    public NewSectionUI newItem() {
                        return new NewSectionUI();
                    }

                    @Override
                    public void doCreateAction(ArrayList<NewSectionUI> uis, ConsysMessage fail) {
                        for (NewSectionUI ui : uis) {
                            if (ui.getSectionName().isEmpty()) {
                                fail.addOrSetText("Missing day date");
                                return;
                            }
                        }

                        CreateSectionAction createSectionAction = new CreateSectionAction();
                        final ArrayList<String> sections = createSectionAction.getNames();

                        for (NewSectionUI ui : uis) {
                            sections.add(ui.getSectionName());
                        }

                        EventBus.get().fireEvent(new EventDispatchEvent(
                                createSectionAction,
                                new AsyncCallback<ArrayListResult<Long>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava event action executor
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<Long> result) {
                                        ArrayList<Long> ids = result.getArrayListResult();
                                        // vlozeni zalozek
                                        for (int i = 0; i < sections.size(); i++) {
                                            ClientTabItem cti = new ClientTabItem();
                                            cti.setTitle(sections.get(i));
                                            cti.setUuid(ids.get(i).toString());
                                            addTab(cti, helpMessage);
                                            if (!tabs.isVisible()) {
                                                tabs.setVisible(true);
                                            }
                                            tabs.selectTab(tabs.getTabCount() - 1);
                                        }
                                        successCreateAction();
                                    }
                                }, SectionsSetting.this));
                    }
                };
                cd.showCentered();
            }
        });

        FlowPanel sep = new FlowPanel();
        sep.setSize("10px", "10px");

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.hideClose(true);
        helpMessage.setText(ESMessageUtils.c.sectionsSetting_text_beginByAddingSection());

        tabs = new ConsysTabPanel<String>(SectionsSetting.class.getName());
        tabs.setVisible(false);

        addWidget(addSection);
        addWidget(sep);
        addWidget(helpMessage);
        addWidget(tabs);
    }

    @Override
    protected void onLoad() {
        if (firstLoad) {
            EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(new ListSectionsAction(),
                    new AsyncCallback<ClientSectionSettings>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava ActionExcecutor
                        }

                        @Override
                        public void onSuccess(ClientSectionSettings result) {
                            firstLoad = false;
                            setSections(result);
                        }
                    }, this);
            EventBus.get().fireEvent(loadSettingsEvent);
        }
    }

    /** nastavi zalozky sekci */
    private void setSections(ClientSectionSettings sectionSetting) {
        tabs.clear();
        if (sectionSetting.getFirstSection() != null) {
            helpMessage.setVisible(false);
            tabs.setVisible(true);

            ClientSection cs = sectionSetting.getFirstSection();
            ClientTabItem first = new ClientTabItem();
            first.setTitle(cs.getName());
            first.setUuid(cs.getId().toString());
            addTab(cs, first, helpMessage);

            for (CommonLongThumb t : sectionSetting.getSectionThumbs()) {
                ClientTabItem cti = new ClientTabItem();
                cti.setTitle(t.getName());
                cti.setUuid(t.getId().toString());
                addTab(cti, helpMessage);
            }
        } else {
            helpMessage.setVisible(true);
            tabs.setVisible(false);
        }
    }

    /** prida zalozku s nastavenou akci */
    private void addTab(final ClientTabItem item, ConsysMessage helpMessage) {
        addTab(null, item, helpMessage);
    }

    /** prida zalozku s nastavenou akci */
    private void addTab(ClientSection data, final ClientTabItem item, ConsysMessage helpMessage) {
        ConsysTabPanelLabel<String> tl = new ConsysTabPanelLabel<String>(item.getUuid(), item.getTitle());
        final SectionsSettingItem ssItem = new SectionsSettingItem(tl, item, helpMessage);
        if (data != null) {
            ssItem.setData(data);
        }
        ConsysAction action = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(
                        new LoadSectionAction(Long.parseLong(item.getUuid())),
                        new AsyncCallback<ClientSection>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                            }

                            @Override
                            public void onSuccess(ClientSection result) {
                                ssItem.setData(result);
                            }
                        }, ssItem);
                EventBus.get().fireEvent(loadSettingsEvent);
            }
        };
        tabs.addTabItem(tl, ssItem, action);
    }
}
