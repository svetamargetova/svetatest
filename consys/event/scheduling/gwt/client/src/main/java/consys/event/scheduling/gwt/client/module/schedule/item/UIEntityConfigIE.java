package consys.event.scheduling.gwt.client.module.schedule.item;

import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;

/**
 *
 * @author pepa
 */
public class UIEntityConfigIE extends UIEntityConfig {

    @Override
    public void setEntitySize(Entity entity) {
        int scale = ScheduleData.get().getScale();
        int length = entity.getLength() * scale;
        entity.setSize(((ScheduleGridImpl.ROOM_W * scale) - 1) + "px", length + "px");
    }

    @Override
    public void setBufferEntitySize(Entity entity) {
        int scale = ScheduleData.get().getScale();
        int length = BUFFER_ENTITY_LENGTH * scale;
        entity.setSize(((ScheduleGridImpl.ROOM_W * scale) - 1) + "px", length + "px");
    }
}
