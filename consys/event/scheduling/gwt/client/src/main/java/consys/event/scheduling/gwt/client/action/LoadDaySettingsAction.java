package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;

/**
 * Akce pro nacteni nastaveni dne
 * @author pepa
 */
public class LoadDaySettingsAction extends EventAction<ClientDay> {

    private static final long serialVersionUID = 8255700403356779341L;
    // data
    private Long id;

    public LoadDaySettingsAction() {
    }

    public LoadDaySettingsAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
