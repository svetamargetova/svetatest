package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani dne z planovani
 * @author pepa
 */
public class DeleteDaySettingsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 5313213143432239437L;
    // data
    private Long id;

    public DeleteDaySettingsAction() {
    }

    public DeleteDaySettingsAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
