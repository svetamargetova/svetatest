package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.HashSet;

/**
 * Aktualizuje prirazene prednasky k sekci
 * @author pepa
 */
public class UpdateTalkOverviewAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 5741243918114664062L;
    // data
    private Long sectionId;
    private HashSet<Long> talkIds;

    public UpdateTalkOverviewAction() {
    }

    public UpdateTalkOverviewAction(Long sectionId) {
        this.sectionId = sectionId;
        talkIds = new HashSet<Long>();
    }

    public Long getSectionId() {
        return sectionId;
    }

    public HashSet<Long> getTalkIds() {
        return talkIds;
    }
}
