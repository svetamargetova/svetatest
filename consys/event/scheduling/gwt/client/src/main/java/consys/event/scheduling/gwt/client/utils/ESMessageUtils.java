package consys.event.scheduling.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.event.scheduling.gwt.client.message.EventSchedulingConstants;
import consys.event.scheduling.gwt.client.message.EventSchedulingMessages;

/**
 *
 * @author pepa
 */
public class ESMessageUtils {

    /** konstanty modulu event conference */
    public static final EventSchedulingConstants c = (EventSchedulingConstants) GWT.create(EventSchedulingConstants.class);
    public static final EventSchedulingMessages m = (EventSchedulingMessages) GWT.create(EventSchedulingMessages.class);
}
