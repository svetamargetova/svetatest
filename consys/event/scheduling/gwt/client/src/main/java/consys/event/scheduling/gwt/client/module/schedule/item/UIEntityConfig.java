package consys.event.scheduling.gwt.client.module.schedule.item;

import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;

/**
 * Nastavovali entity pro zavisle vlastnosti vzhledem k prohlizeci
 * @author pepa
 */
public class UIEntityConfig {

    public static final int BUFFER_ENTITY_LENGTH = 30;

    /** nastavi velikost entity podle dat */
    public void setEntitySize(Entity entity) {
        int scale = ScheduleData.get().getScale();
        int length = entity.getLength() * scale;
        entity.setSize((ScheduleGridImpl.ROOM_W * scale) + "px", (length - 1) + "px");
    }

    /** nastavi velikost entity podle rozmeru do bufferu */
    public void setBufferEntitySize(Entity entity) {
        int scale = ScheduleData.get().getScale();
        int length = BUFFER_ENTITY_LENGTH * scale;
        entity.setSize((ScheduleGridImpl.ROOM_W * scale) + "px", (length - 1) + "px");
    }
}
