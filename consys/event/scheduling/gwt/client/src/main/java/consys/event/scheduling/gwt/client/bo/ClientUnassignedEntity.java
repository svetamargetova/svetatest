package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Obecny predek neprirazene entity
 * @author pepa
 */
public abstract class ClientUnassignedEntity implements IsSerializable, Result {

    private static final long serialVersionUID = -4593406877861954985L;
    // data
    private Long id;
    private Long sectionId;

    public ClientUnassignedEntity() {
    }

    public ClientUnassignedEntity(Long id, Long sectionId) {
        this.id = id;
        this.sectionId = sectionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }
}
