package consys.event.scheduling.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface ESCssResources extends CssResource {

    String gridContent();

    String gridHeaderLeft();

    String gridHeader();

    String gridBlockHeaderLeft();

    String gridBlockHeader();

    String gridHeaderTitle();

    String gridBlockLeft();

    String gridBlockLeftTitle();

    String gridBlockFullHour();

    String gridBlockHalfHour();

    String gridFoot();

    String scheduleBackgrounds();

    String scheduleBlocks();

    String blockContent();

    String chairmanLabel();

    String pen();

    String scheduleItem();

    String scheduleItemTime();

    String bufferBody();

    String frameSelPan();

    String frameSelBtn();

    String activeFrameSelBtn();

    String day();

    String noRoom();

    String room();

    String buffer();

    String bufferSectionsBox();

    String chckbox();

    String schedWrapper();

    String schedScroller();

    String tlChair();

    String chairPanel();

    String schedTableCell();

    String schedTableLabel();

    String tlEmpty();

    String schedCell();

    String schedTLLabel();

    String schedTLEmpty();

    String schedCellHalfHour();

    String schedCellHour();

    String schedTLhour();

    String blockBox();

    String abstractBlockBox();

    String submissionBox();

    String submissionLabel();

    String submissionBlind();

    String chairLine();

    String chairTLCell();

    String chairLabel();

    String chairEdit();

    String controlPanel();

    String bufferBtn();

    String bufferItem();

    String bufferTalksBox();

    String bufferSpace();

    String label();

    String boundryPanel();

    String positioner();

    String timeLabel();
}
