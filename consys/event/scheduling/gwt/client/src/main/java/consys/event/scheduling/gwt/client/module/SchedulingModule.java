package consys.event.scheduling.gwt.client.module;

import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.ArrayUtils;
import consys.event.common.api.right.Role;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.SchedulingSettings;
import consys.event.scheduling.gwt.client.module.schedule.TalkOverview;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class SchedulingModule implements EventModule {

    @Override
    public String getLocalizedModuleName() {
        return ESMessageUtils.c.schedulingModule_text_moduleName();
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
        EventScheduleModuleRoleModel roles = new EventScheduleModuleRoleModel();
        Map<String, Role> rightMap = roles.getModelInMap();
        // EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN
        ModuleRole administrator = new ModuleRole();
        administrator.setId(rightMap.get(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN).getIdentifier());
        administrator.setSystem(rightMap.get(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN).isSystem());
        administrator.setShortcut(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN);
        administrator.setLocalizedName(ESMessageUtils.c.schedulingModule_text_roleAdministratorName());
        administrator.setLocalizedDescription(ESMessageUtils.c.schedulingModule_text_roleAdministratorDescription());
        moduleRoles.add(administrator);
    }

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
        EventNavigationItem scheduling = new EventNavigationItem(getLocalizedModuleName(), 70, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ESMessageUtils.c.schedulingSettings_title(), new SchedulingSettings());
            }
        }, true, EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN);

        model.insertFirstLevelAddon(scheduling, UIMessageUtils.c.const_settings(), 5, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ESMessageUtils.c.schedulingSettings_title(), new SchedulingSettings());
            }
        }, EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN);

        model.insertFirstLevelAddon(scheduling, ESMessageUtils.c.talkOverview_title(), 10, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ESMessageUtils.c.talkOverview_title(), new TalkOverview());
            }
        }, ArrayUtils.EMPTY_STRING_ARRAY);

        model.insertFirstLevelAddon(scheduling, ESMessageUtils.c.schedulingModule_text_schedule(), 20, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ESMessageUtils.c.schedulingModule_text_schedule(), new Scheduling());
            }
        }, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
    }

    @Override
    public void registerModule() {
    }

    @Override
    public void unregisterModule() {
    }
}
