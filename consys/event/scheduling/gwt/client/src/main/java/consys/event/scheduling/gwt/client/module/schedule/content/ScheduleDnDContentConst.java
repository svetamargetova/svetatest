package consys.event.scheduling.gwt.client.module.schedule.content;

/**
 * Prepravni objekt pro konstanty zajistujici spravne umistovani bloku nad mrizku
 * @author pepa
 */
public class ScheduleDnDContentConst {

    private final int diffStartX;
    private final int diffStartY;
    private final int diffTotalHeight;
    private final int diffRoomWidth;

    public ScheduleDnDContentConst(final int diffStartX, final int diffStartY, final int diffTotalHeight, final int diffRoomWidth) {
        this.diffStartX = diffStartX;
        this.diffStartY = diffStartY;
        this.diffTotalHeight = diffTotalHeight;
        this.diffRoomWidth = diffRoomWidth;
    }

    /** rozdil od souradnice x odkud se vkladaji bloky */
    public int getDiffStartX() {
        return diffStartX;
    }

    /** rozdil souradnice y odkud se vkladaji bloky */
    public int getDiffStartY() {
        return diffStartY;
    }

    /** rozdil celkove vysky oproti standardne v prohlizeci */
    public int getDiffTotalHeight() {
        return diffTotalHeight;
    }

    /** rozdil celkove sirky mistnosti oproti standardne v prohlizeci */
    public int getDiffRoomWidth() {
        return diffRoomWidth;
    }
}
