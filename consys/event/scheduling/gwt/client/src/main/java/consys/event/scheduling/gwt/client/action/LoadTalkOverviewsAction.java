package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverviews;

/**
 * Akce pro nacteni prehledu sekci a prirazenach prednasek
 * @author pepa
 */
public class LoadTalkOverviewsAction extends EventAction<ClientSectionOverviews> {

    private static final long serialVersionUID = -1459199895072351841L;
}
