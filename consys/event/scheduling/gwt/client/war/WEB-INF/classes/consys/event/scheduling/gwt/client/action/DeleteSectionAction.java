package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani sekce
 * @author pepa
 */
public class DeleteSectionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -6423189241795657128L;
    // data
    private Long id;

    public DeleteSectionAction() {
    }

    public DeleteSectionAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
