package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;

/**
 * Neprirazena prednaska v bufferu
 * @author mishak
 */
public class ClientUnassignedEntityTalk extends ClientUnassignedEntity {

    private static final long serialVersionUID = -7449271111861054487L;
    // data
    private String name;
    private String author;

    public ClientUnassignedEntityTalk() {
    }

    public ClientUnassignedEntityTalk(Long id, Long sectionId, String name, String author) {
        super(id, sectionId);
        this.name = name;
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /** vygeneruje entity talk pro objekt v bufferu */
    public ClientEntityTalk getClientEntityTalkForBuffer() {
        ClientEntityTalk cet = new ClientEntityTalk();
        cet.setAuthor(author);
        cet.setFrom(new ClientTime());
        cet.setId(getId());
        cet.setName(name);
        cet.setRoomIds(new ArrayList<Long>());
        cet.setTo(new ClientTime(30));
        return cet;
    }
}
