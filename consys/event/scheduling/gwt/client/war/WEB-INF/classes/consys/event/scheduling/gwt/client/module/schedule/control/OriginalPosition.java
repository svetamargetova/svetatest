package consys.event.scheduling.gwt.client.module.schedule.control;

import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;

/**
 * Uchovává původní umístění entity
 * @author pepa
 */
public class OriginalPosition {

    private Entity entity;
    private BlockPosition blockPosition;
    private ClientTime timeFrom;

    public OriginalPosition(Entity entity, BlockPosition blockPosition, ClientTime timeFrom) {
        this.entity = entity;
        this.blockPosition = blockPosition;
        this.timeFrom = timeFrom;
    }

    public BlockPosition getBlockPosition() {
        return blockPosition;
    }

    public Entity getEntity() {
        return entity;
    }

    public ClientTime getTimeFrom() {
        return timeFrom;
    }
}
