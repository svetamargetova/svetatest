package consys.event.scheduling.gwt.client.module.schedule.utils;

/**
 *
 * @author pepa
 */
public class PositionUtils {

    /** pozice entity v mrizce */
    public static int inGrid(int yEntity, int grid) {
        return Math.round((float) yEntity / grid) * grid;
    }
}
