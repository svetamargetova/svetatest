package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class ClientRoomInfo implements IsSerializable {

    private Long id;
    private String name;

    public ClientRoomInfo() {
    }

    public ClientRoomInfo(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientRoomInfo other = (ClientRoomInfo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
