package consys.event.scheduling.gwt.client.module.schedule.block;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.utils.TimeUtils;

/**
 * Implementace bloku pro IE prohlizece
 * @author pepa
 */
public class ScheduleBlockImplIE extends ScheduleBlockImpl {

    @Override
    protected AbsolutePanel getContentPanel() {
        AbsolutePanel panel = new AbsolutePanel();
        final int scale = ScheduleData.get().getScale();
        panel.setSize(((ScheduleGridImpl.ROOM_W * scale) - 1) + "px", (TimeUtils.diff(getFrom(), getTo()) * scale) + "px");
        return panel;
    }
}
