package consys.event.scheduling.gwt.client.bo;

/**
 * Data neprednasky
 * @author pepa
 */
public class ClientEntityBlind extends ClientEntity {

    // data
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
