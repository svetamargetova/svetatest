package consys.event.scheduling.gwt.client.module.schedule.block;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;

/**
 * Pozadi bloku
 * @author pepa
 */
public class BlockBackground extends SimplePanel {

    // data
    private BlockPosition position;

    public BlockBackground(BlockPosition position, int height) {
        this.position = position;
        final int scale = ScheduleData.get().getScale();
        setSize((ScheduleGridImpl.ROOM_W * scale) + "px", height + "px");
        DOM.setStyleAttribute(getElement(), "backgroundColor", getColor());
    }

    /** aktualizuje pozadi podle zadane pozice bloku */
    public void refreshByPosition(BlockPosition position, int height) {
        this.position = position;
        final int scale = ScheduleData.get().getScale();
        setSize((ScheduleGridImpl.ROOM_W * scale) + "px", height + "px");
        DOM.setStyleAttribute(getElement(), "backgroundColor", getColor());
    }

    /** vraci barvu pozadi bloku */
    private String getColor() {
        ScheduleBlock sbi = ScheduleData.get().blocksUI().get(position);
        ClientSection section = sbi.getSection();
        if (section != null) {
            if (section.getBgColor() == null) {
                return "transparent";
            } else {
                return "#" + section.getBgColor();
            }
        }
        return "transparent";
    }
}
