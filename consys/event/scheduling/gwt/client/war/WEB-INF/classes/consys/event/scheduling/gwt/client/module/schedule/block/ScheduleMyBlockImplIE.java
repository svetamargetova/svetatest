package consys.event.scheduling.gwt.client.module.schedule.block;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.utils.TimeUtils;

/**
 *
 * @author pepa
 */
public class ScheduleMyBlockImplIE extends ScheduleMyBlockImpl {

    @Override
    protected AbsolutePanel getContentPanel() {
        AbsolutePanel panel = new AbsolutePanel();
        final int scale = ScheduleData.get().getScale();
        panel.setSize(((ScheduleGridImpl.ROOM_W * scale) - 1) + "px", (TimeUtils.diff(getFrom(), getTo()) * scale) + "px");
        return panel;
    }
}
