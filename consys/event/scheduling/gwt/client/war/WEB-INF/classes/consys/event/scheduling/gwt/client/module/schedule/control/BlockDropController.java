package consys.event.scheduling.gwt.client.module.schedule.control;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.allen_sauer.gwt.dnd.client.drop.AbstractPositioningDropController;
import com.allen_sauer.gwt.dnd.client.util.DOMUtil;
import com.allen_sauer.gwt.dnd.client.util.WidgetLocation;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.action.UpdateEntityPositionAction;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.ScheduleValidator;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.module.schedule.item.DrawProvider;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.utils.PositionUtils;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.HashMap;

/**
 * DropController pro bloky
 * @author pepa
 */
public class BlockDropController extends AbstractPositioningDropController {

    // data
    final AbsolutePanel dropTarget;
    int dropTargetClientHeight;
    int dropTargetClientWidth;
    int dropTargetOffsetX;
    int dropTargetOffsetY;
    // data - vlastni
    private ScheduleBlockImpl block;
    private int grid;
    private Draggable draggable;
    private WidgetLocation location;
    private HashMap<String, BlockBehavior> blockItemsBehavior;
    private int actualScale;

    public BlockDropController(AbsolutePanel blockDropArea, ScheduleBlockImpl block, int grid) {
        super(blockDropArea);
        this.dropTarget = blockDropArea;
        this.block = block;
        this.grid = grid;

        blockItemsBehavior = new HashMap<String, BlockBehavior>();
        blockItemsBehavior.put(EntityTalk.class.getName(), new TalkBlockBehavior());
        blockItemsBehavior.put(EntityBlind.class.getName(), new BlindBlockBehavior());
    }

    @Override
    public void onDrop(DragContext context) {
        draggable.positioner.removeFromParent();
        draggable.timeLabel.removeFromParent();
        dropTarget.add(draggable.entity, draggable.desiredX, draggable.desiredY);

        final Entity entity = (Entity) context.selectedWidgets.get(0);
        BlockBehavior bb = blockItemsBehavior.get(entity.getClass().getName());
        if (bb != null) {
            ClientTime from = new ClientTime(block.getBlock().getFrom().getTime() + (draggable.desiredY / actualScale));
            bb.onDropAction(entity, from);
        } else {
            GWT.log("Unknown behavior for entity " + entity.getClass().getName());
        }

        super.onDrop(context);
    }

    @Override
    public void onEnter(DragContext context) {
        super.onEnter(context);

        dropTargetClientWidth = DOMUtil.getClientWidth(dropTarget.getElement());
        dropTargetClientHeight = DOMUtil.getClientHeight(dropTarget.getElement());
        calcDropTargetOffset();

        int draggableAbsoluteLeft = context.draggable.getAbsoluteLeft();
        int draggableAbsoluteTop = context.draggable.getAbsoluteTop();
        Entity entity = (Entity) context.selectedWidgets.get(0);

        BlockBehavior bb = blockItemsBehavior.get(entity.getClass().getName());
        if (bb != null) {
            bb.onEnterAction(entity);
        } else {
            GWT.log("Unknown behavior for entity " + entity.getClass().getName());
        }

        draggable = new Draggable(entity);
        draggable.positioner = makePositioner(entity);
        draggable.relativeX = entity.getAbsoluteLeft() - draggableAbsoluteLeft;
        draggable.relativeY = entity.getAbsoluteTop() - draggableAbsoluteTop;

        location = new WidgetLocation(dropTarget, ScheduleData.get().getDndContent());
        actualScale = ScheduleData.get().getScale();
    }

    @Override
    public void onLeave(DragContext context) {
        Entity entity = (Entity) context.selectedWidgets.get(0);
        BlockBehavior bb = blockItemsBehavior.get(entity.getClass().getName());
        if (bb != null) {
            bb.onLeaveAction(entity);
        } else {
            GWT.log("Unknown behavior for entity " + entity.getClass().getName());
        }

        draggable.positioner.removeFromParent();
        draggable.timeLabel.removeFromParent();
        draggable.positioner = null;
        draggable.timeLabel = null;
        super.onLeave(context);
    }

    @Override
    public void onMove(DragContext context) {
        super.onMove(context);

        draggable.desiredY = context.desiredDraggableY - dropTargetOffsetY + draggable.relativeY;
        draggable.desiredY = Math.max(0, Math.min(draggable.desiredY, dropTargetClientHeight - draggable.offsetHeight));
        draggable.desiredY = PositionUtils.inGrid(draggable.desiredY, grid);

        // update casu v timelabelu
        draggable.timeLabel.update(block.getBlock().getFrom().getTime() + (draggable.desiredY / actualScale));

        // nastaveni pozice positioneru a timelabelu
        dropTarget.add(draggable.positioner, draggable.desiredX, draggable.desiredY);
        ScheduleData.get().getDndContent().add(draggable.timeLabel, location.getLeft(), location.getTop() + draggable.desiredY - 15);

        calcDropTargetOffset();
    }

    @Override
    public void onPreviewDrop(DragContext context) throws VetoDragException {
        BlockPosition bp = block.getBlockPosition();
        GWT.log("Block: (" + bp.getX() + "," + bp.getY() + ")");

        ClientTime desiredTime = new ClientTime(block.getFrom().getTime() + draggable.desiredY);
        String roomName = ScheduleData.get().clientRooms().get(block.getRoomId()).getName();

        Entity entity = (Entity) context.selectedWidgets.get(0);

        if (!ScheduleValidator.canContainEntity(block, entity, null, block)) {
            Scheduling.hiddableLabel.show(ESMessageUtils.m.blockDropController_error_blockCannotContainThisEntity(
                    roomName, block.getFrom().toString(), block.getTo().toString()));
            // pokud taham z bufferu odstranim buffer styly
            removeBufferStyle(entity);
            throw new VetoDragException();
        }

        if (!ScheduleValidator.canContainEntityOnPosition(block, entity, desiredTime)) {
            Scheduling.hiddableLabel.show(ESMessageUtils.m.blockDropController_error_invalidEntityPosition(
                    roomName, block.getFrom().toString(), block.getTo().toString()));
            // pokud taham z bufferu odstranim buffer styly
            removeBufferStyle(entity);
            throw new VetoDragException();
        }
    }

    /** vytvori pozicioner */
    private Positioner makePositioner(Entity entity) {
        draggable.timeLabel = new TimeLabel(entity.getLength());
        final int scale = ScheduleData.get().getScale();
        return new Positioner(ScheduleGridImpl.ROOM_W * scale, entity.getLength() * scale);
    }

    /** nastavi offset dropovaci oblasti */
    private void calcDropTargetOffset() {
        WidgetLocation dropTargetLocation = new WidgetLocation(dropTarget, null);
        dropTargetOffsetX = dropTargetLocation.getLeft() + DOMUtil.getBorderLeft(dropTarget.getElement());
        dropTargetOffsetY = dropTargetLocation.getTop() + DOMUtil.getBorderTop(dropTarget.getElement());
    }

    /** odstrani styly pridavane v bufferu */
    private void removeBufferStyle(Entity entity) {
        entity.removeStyleName(StyleUtils.MARGIN_HOR_3);
        entity.removeStyleName(StyleUtils.MARGIN_VER_3);
    }

    /** interface pro akce chovani entit v bloku */
    private interface BlockBehavior<W extends Entity> {

        public void onDropAction(W entity, ClientTime from);

        public void onEnterAction(W entity);

        public void onLeaveAction(W entity);
    }

    /** talk */
    private class TalkBlockBehavior implements BlockBehavior<EntityTalk> {

        @Override
        public void onDropAction(final EntityTalk entity, ClientTime from) {
            GWT.log("Behavior for entity EntityTalk: onDrop");
            removeBufferStyle(entity);
            entity.showBufferLook(false);
            entity.setFrom(from);
            DrawProvider.get().updatePlenaries(entity, from, block);
            entity.setBlock(block);
            entity.showPendingState(true);

            EventBus.get().fireEvent(new EventDispatchEvent(
                    new UpdateEntityPositionAction(entity.getData().getId(),
                    entity.getBlock().getBlock().getBlockId(), entity.getFrom()),
                    new AsyncCallback<LongResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            Scheduling.hiddableLabel.show(ESMessageUtils.c.blockDropController_error_updatePositionFail());
                            DrawProvider.get().onOriginalPosition(entity);
                            entity.showPendingState(false);
                            entity.recalculatePup();
                        }

                        @Override
                        public void onSuccess(LongResult result) {
                            entity.showPendingState(false);
                            entity.recalculatePup();
                        }
                    }, null));
        }

        @Override
        public void onEnterAction(EntityTalk entity) {
            GWT.log("Behavior for entity EntityTalk: onEnter");
            entity.showContent(false);
        }

        @Override
        public void onLeaveAction(EntityTalk entity) {
            GWT.log("Behavior for entity EntityTalk: onLeave");
            entity.showContent(true);
        }
    }

    /** blind */
    private class BlindBlockBehavior implements BlockBehavior<EntityBlind> {

        @Override
        public void onDropAction(final EntityBlind entity, ClientTime from) {
            GWT.log("Behavior for entity EntityBlind: onDrop");
            removeBufferStyle(entity);
            entity.showBufferLook(false);
            entity.setFrom(from);
            DrawProvider.get().updatePlenaries(entity, from, block);
            entity.setBlock(block);
            entity.showPendingState(true);

            UpdateEntityPositionAction updateAction = new UpdateEntityPositionAction(entity.getData().getId(),
                    entity.getBlock().getBlock().getBlockId(), entity.getFrom());
            updateAction.setBlindName(ESMessageUtils.c.drawProvider_text_newBlind());

            EventBus.get().fireEvent(new EventDispatchEvent(updateAction,
                    new AsyncCallback<LongResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            Scheduling.hiddableLabel.show(ESMessageUtils.c.blockDropController_error_updatePositionFail());
                            DrawProvider.get().onOriginalPosition(entity);
                            entity.showPendingState(false);
                            entity.recalculatePup();
                        }

                        @Override
                        public void onSuccess(LongResult result) {
                            entity.getData().setId(result.getValue());
                            entity.showPendingState(false);
                            entity.recalculatePup();
                        }
                    }, null));
        }

        @Override
        public void onEnterAction(EntityBlind entity) {
            GWT.log("Behavior for entity EntityBlind: onEnter");
            entity.showContent(false);
        }

        @Override
        public void onLeaveAction(EntityBlind entity) {
            GWT.log("Behavior for entity EntityBlind: onLeave");
            entity.showContent(true);
        }
    }
}
