package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;

/**
 * Akce pro aktualizaci blindu
 * @author pepa
 */
public class UpdateBlindAction extends UpdateEntityAction {

    private static final long serialVersionUID = -5614030077092032153L;
    // data
    private String name;

    public UpdateBlindAction() {
    }

    public UpdateBlindAction(Long entityId, ClientTime from, int length, String name) {
        super(entityId, from, length, new ArrayList<Long>());
        this.name = name;
    }

    public UpdateBlindAction(Long entityId, ClientTime from, int length, String name, ArrayList<Long> roomsId) {
        super(entityId, from, length, roomsId);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
