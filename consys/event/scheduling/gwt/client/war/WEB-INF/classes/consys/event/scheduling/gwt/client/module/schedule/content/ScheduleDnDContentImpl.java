package consys.event.scheduling.gwt.client.module.schedule.content;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.event.scheduling.gwt.client.action.LoadScheduleDayAction;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Oblast ve ktere probiha dnd
 * @author pepa
 */
public abstract class ScheduleDnDContentImpl extends AbsolutePanel {

    // konstanty
    public static final int WIDTH = LayoutManager.LAYOUT_CONTENT_WIDTH_INT;
    // komponenty
    private ScheduleBackgrounds backgrounds;
    private ScheduleGridImpl grid;
    private ScheduleBlockPanel blocks;
    private ScheduleBufferImpl buffer;
    private AbsolutePanel scroller;
    // data
    private Long dayId;
    private Scheduling parent;

    public ScheduleDnDContentImpl() {
        super();
        setSize(WIDTH + "px", 10 + "px");

        buffer = GWT.create(ScheduleBufferImpl.class);
        buffer.setDNDContent(this);
        buffer.setVisible(false);
        add(buffer, 0, 0);

        scroller = new AbsolutePanel();
        scroller.setSize(WIDTH + "px", (DOMX.getScreenHeight() - 450) + "px");
        DOM.setStyleAttribute(scroller.getElement(), "overflow", "auto");
        add(scroller, 0, 0);

        backgrounds = new ScheduleBackgrounds(10, 10);
        ScheduleData.get().setBackgrounds(backgrounds);
        scroller.add(backgrounds, 0, 0);

        grid = GWT.create(ScheduleGridImpl.class);
        scroller.add(grid, 0, 0);

        blocks = new ScheduleBlockPanel(10, 10);
        scroller.add(blocks, 0, 0);
    }

    /** podle zobrazeni bufferu nastavuje vysku panelu obsahu */
    public void bufferShow(boolean show) {
        if (grid != null) {
            int height = grid.getOffsetHeight();
            if (show) {
                setHeight((height + ScheduleBufferImpl.HEIGHT + 20) + "px");
            } else {
                setHeight((height + 20) + "px");
            }
        }
    }

    /** nastaveni rodice pro zobrazovani waitingu a chyb */
    public void setParent(Scheduling parent) {
        this.parent = parent;
        buffer.setParent(parent);
    }

    /** injectovaci metoda pro aktualni den */
    public void setDay(final ClientScheduleDay scheduleDay, Long dayId) {
        this.dayId = dayId;
        backgrounds.clear();
        blocks.clear();
        ScheduleData.get().blocksUI().clear();
        ScheduleData.get().bgPosition().clear();

        ScheduleData.get().setRooms(scheduleDay.getRooms());
        Collections.sort(scheduleDay.getRooms(), new Comparator<ClientRoom>() {

            @Override
            public int compare(ClientRoom o1, ClientRoom o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        grid.setDay(scheduleDay);

        ArrayList<String> uuids = new ArrayList<String>();
        for (ClientRoom room : scheduleDay.getRooms()) {
            for (ClientBlock cb : room.getBlocks()) {
                if (cb.getChairUuid() != null) {
                    uuids.add(cb.getChairUuid());
                }
            }
        }

        if (!uuids.isEmpty()) {
            // je potreba natahnout jmena predsedu
            EventBus.get().fireEvent(new DispatchEvent(new ListUserThumbsAction(uuids),
                    new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                            for (ClientUserThumb cut : result.getArrayListResult()) {
                                ScheduleData.get().chairmans().put(cut.getUuid(), cut.getName());
                            }
                            generateBlocks(scheduleDay, getConst());
                        }
                    }, null));
        } else {
            generateBlocks(scheduleDay, getConst());
        }
    }

    /** aktualizace pro nastavene meritko */
    public void refreshScale() {
        grid.refreshScale();
        blocks.refreshScale();
        backgrounds.refreshScale();

        if (dayId == null) {
            parent.getFailMessage().setText("Bad day id");
            return;
        }
        EventDispatchEvent loadScheduleAction = new EventDispatchEvent(new LoadScheduleDayAction(dayId),
                new AsyncCallback<ClientScheduleDay>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientScheduleDay scheduleDay) {
                        GWT.log("--- loadScheduleDayAction (change scale) ---");
                        setDay(scheduleDay, dayId);
                    }
                }, parent);
        EventBus.get().fireEvent(loadScheduleAction);
    }

    /** zobrazi skryje buffer */
    public void showBuffer(boolean show) {
        if (show) {
            setWidgetPosition(scroller, 0, ScheduleBufferImpl.HEIGHT);
            buffer.setVisible(show);
        } else {
            buffer.setVisible(show);
            setWidgetPosition(scroller, 0, 0);
        }
    }

    /** vraci vrstvu pro pozadi */
    protected ScheduleBackgrounds getBackgrounds() {
        return backgrounds;
    }

    /** vraci vrstvu s bloky */
    protected ScheduleBlockPanel getBlockPanel() {
        return blocks;
    }

    /** vygeneruje bloky */
    private void generateBlocks(ClientScheduleDay scheduleDay, ScheduleDnDContentConst cc) {
        final int scale = ScheduleData.get().getScale();
        // pozice odkud se vyskladava rozvrh
        final int x = (ScheduleGridImpl.CORNER_W * scale) + cc.getDiffStartX();
        final int y = (ScheduleGridImpl.CORNER_H * scale) + cc.getDiffStartY();
        // sirka mistnosti
        final int roomWidth = (ScheduleGridImpl.ROOM_W * scale) + cc.getDiffRoomWidth();

        int xT = x;
        int yT = y;

        // vygenerovani bloku
        int roomCounter = 1;
        for (ClientRoom room : scheduleDay.getRooms()) {
            int blockCounter = 1;
            for (ClientBlock cb : room.getBlocks()) {
                BlockPosition bp = new BlockPosition(roomCounter, blockCounter);
                ScheduleData.get().bgPosition().put(bp, new BlockPosition(xT, yT));

                ScheduleBlockImpl sb = GWT.create(ScheduleBlockImpl.class);
                sb.setScale(scale);
                ScheduleData.get().blocksUI().put(bp, sb);

                if (!ScheduleData.get().isAdmin()) {
                    sb.showPen(false);
                }

                sb.setBlockOnPosition(cb, bp, room.getId());
                blocks.add(sb, xT, yT);

                yT += sb.getTotalHeight() + cc.getDiffTotalHeight();
                blockCounter++;
            }
            xT += roomWidth;
            yT = y;
            roomCounter++;
        }

        // nastavit vysku panelu pozadi a bloku podle aktualni mrizky
        int gridWidth = grid.getOffsetWidth();
        int gridHeight = grid.getOffsetHeight();
        blocks.setSize(gridWidth, gridHeight);
        backgrounds.setSize(gridWidth, gridHeight);
        if (!ScheduleData.get().isAdmin() || ScheduleData.get().sectionIdRights().isEmpty()) {
            scroller.setHeight((gridHeight + 15) + "px");
        }
        setHeight((gridHeight + 65) + "px");

        // vygenerovani obsahu bloku
        for (int i = 0; i < blocks.getWidgetCount(); i++) {
            Widget w = blocks.getWidget(i);
            if (w instanceof ScheduleBlockImpl) {
                ScheduleBlockImpl impl = (ScheduleBlockImpl) w;
                impl.generateItems();
            }
        }
    }

    /** vraci objekt s rozdily pozic pro vypocet pozice bloku nad mrizkou */
    protected ScheduleDnDContentConst getConst() {
        return new ScheduleDnDContentConst(2, 3, 2, 1);
    }
}
