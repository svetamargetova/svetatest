package consys.event.scheduling.gwt.client.bo;

/**
 *
 * @author pepa
 */
public class ClientMyEntity extends ClientEntity {

    // data
    private String note;
    private boolean favourite;

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
