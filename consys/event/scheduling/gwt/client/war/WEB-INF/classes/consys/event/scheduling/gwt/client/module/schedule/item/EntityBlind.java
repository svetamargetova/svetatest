package consys.event.scheduling.gwt.client.module.schedule.item;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.dialog.ScheduleEditBlindDialog;
import java.util.ArrayList;

/**
 * Neprednaska - obcerstveni, vylet, ...
 * @author pepa
 */
public class EntityBlind extends EntityMain<ClientEntityBlind> {

    // komponenty
    private Label nameLabel;
    private PopupHelp pup;
    // data
    private boolean isNew = false;

    public EntityBlind() {
        nameLabel = StyleUtils.getStyledLabel("", StyleUtils.FONT_11PX, StyleUtils.MARGIN_HOR_5);

        final FlowPanel helpPanel = new FlowPanel();
        helpPanel.addStyleName(StyleUtils.MARGIN_HOR_10);
        helpPanel.addStyleName(StyleUtils.MARGIN_VER_10);
        pup = new PopupHelp(helpPanel, EntityBlind.this);

        getHandle().addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                pup.recalculate();
                helpPanel.clear();
                if (getBlock() != null) {
                    helpPanel.add(StyleUtils.getStyledLabel(getTimeLabel().getText(), StyleUtils.FONT_BOLD));
                }
                helpPanel.add(StyleUtils.getStyledLabel(nameLabel.getText(), StyleUtils.FONT_16PX, StyleUtils.MARGIN_VER_5));
                pup.show();
            }
        });
        getHandle().addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                pup.hide();
            }
        });
    }

    @Override
    protected void showEditDialog() {
        ArrayList<Long> rooms = new ArrayList<Long>();
        for (Entity e : getSlaves()) {
            rooms.add(e.getRoomId());
        }
        new ScheduleEditBlindDialog(this, rooms, getBlock()) {

            @Override
            protected void doSuccessAction(ClientTime from, int lenght, String name, ArrayList<Long> slaveRooms) {
                setFrom(from);
                setName(name);
                setLength(lenght);
                refreshSlaves(slaveRooms);
                DrawProvider.get().updateEntityPosition(EntityBlind.this, getBlock());
                hide();
            }
        }.showCentered();
    }

    @Override
    public void createSelf(ClientSection section) {
        super.createSelf(null);
        mainPanel().clear();
        mainPanel().add(nameLabel);
    }

    /** nastavi nazev blindu */
    public void setName(String name) {
        nameLabel.setText(name);
    }

    /** vraci jestli se jedna o jeste nepouzity blind */
    public boolean isNew() {
        return isNew;
    }

    /** nastavuje zda se jedna o uz pouzity blind */
    public void setNew(boolean value) {
        isNew = value;
    }

    /** prepocita se vyska pupu */
    public void recalculatePup() {
        pup.recalculate();
    }
}
