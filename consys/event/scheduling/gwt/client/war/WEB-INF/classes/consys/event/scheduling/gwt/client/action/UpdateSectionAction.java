package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;

/**
 * Akce pro aktualizaci sekce
 * @author pepa
 */
public class UpdateSectionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 142981127733721762L;
    // data
    private ClientSection section;

    public UpdateSectionAction() {
    }

    public UpdateSectionAction(ClientSection section) {
        this.section = section;
    }

    public ClientSection getSection() {
        return section;
    }
}
