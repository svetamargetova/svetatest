package consys.event.scheduling.gwt.client.module.schedule.item.behaviour;

import com.google.gwt.core.client.GWT;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;
import consys.event.scheduling.gwt.client.module.schedule.item.MyEntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.item.UIEntityFactory.PresenterFactory;

/**
 *
 * @author pepa
 */
public class MyTalkBehaviour implements PresenterFactory<ClientMyEntityTalk> {

    @Override
    public EntityMain create(ClientMyEntityTalk talk) {
        MyEntityTalk it = GWT.create(MyEntityTalk.class);
        it.setData(talk);
        return it;
    }
}
