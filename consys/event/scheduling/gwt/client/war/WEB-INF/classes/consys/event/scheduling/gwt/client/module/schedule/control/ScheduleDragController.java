package consys.event.scheduling.gwt.client.module.schedule.control;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.drop.DropController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleDnDContentImpl;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;

/**
 * Rizeni uchopovani predmetu
 * @author pepa
 */
public class ScheduleDragController {

    // instance
    private static ScheduleDragController instance = new ScheduleDragController();
    // data
    private SDController dragController;

    /** konstruktor pro nezinicializovanou instanci */
    private ScheduleDragController() {
    }

    /** konstruktor pro zinicializovanou instanci */
    private ScheduleDragController(ScheduleDnDContentImpl content) {
        dragController = new SDController(content);
    }

    /** zinicializuje potrebne komponenty */
    public void init(ScheduleDnDContentImpl content) {
        instance = new ScheduleDragController(content);
    }

    /** zrusi potrebne komponenty, pred dalsim pouzitim je potrebna nova inicializace */
    public void destroy() {
        if (dragController != null) {
            dragController.unregisterDropControllers();
            dragController = null;
        }
    }

    /** instance ScheduleDragControlleru */
    public static ScheduleDragController get() {
        return instance;
    }

    /** kontrola zda je zinicializovano */
    private void checkInitState() {
        if (dragController == null) {
            throw new IllegalStateException("ScheduleDragController is not initialized");
        }
    }

    /** oznaci widget za premistovatelny */
    public void makeDraggable(Widget draggable, Widget dragHandle) {
        checkInitState();
        dragController.makeDraggable(draggable, dragHandle);
    }

    /** oznaci widget ze nepremistovatelny */
    public void makeNotDraggable(Widget draggable) {
        checkInitState();
        dragController.makeNotDraggable(draggable);
    }

    /** registrace DropControlleru */
    public void registerDropController(DropController dropController) {
        checkInitState();
        dragController.registerDropController(dropController);
    }

    /** odregistrace DropControlleru */
    public void unregisterDropController(DropController dropController) {
        if (dragController == null) {
            return;
        }
        dragController.unregisterDropController(dropController);
    }

    /** rozsireni PickupDragController pro vlastni potreby */
    private class SDController extends PickupDragController {

        public SDController(AbsolutePanel boundaryPanel) {
            super(boundaryPanel, false);
            setBehaviorBoundaryPanelDrop(false);
            setBehaviorMultipleSelection(false);
        }

        @Override
        public void dragStart() {
            if (context.draggable instanceof EntityMain) {
                GWT.log("dragStart EntityMain instance");
                EntityMain em = (EntityMain) context.draggable;
                BlockPosition bp = em.getBlock() == null ? null : em.getBlock().getBlockPosition();
                em.setOriginalPosition(new OriginalPosition(em, bp, em.getFrom()));
            }
            super.dragStart();
        }
    }
}
