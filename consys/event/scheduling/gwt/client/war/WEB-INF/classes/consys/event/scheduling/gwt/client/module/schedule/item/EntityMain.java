package consys.event.scheduling.gwt.client.module.schedule.item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.control.OriginalPosition;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;

/**
 * Hlavni polozka (pokud neni plenarni, je jen tato, pokud je plenarni, je tato hlavni)
 * @author pepa
 */
public abstract class EntityMain<T extends ClientEntity> extends Entity<T> {

    // komponenty
    private FocusPanel handle;
    private FlowPanel mainPanel;
    private Label timeLabel;
    private FocusPanel pen;
    // data
    private ArrayList<Entity> slaves;
    private OriginalPosition position;

    public EntityMain() {
        slaves = new ArrayList<Entity>();
        handle = generateHandle();
    }

    @Override
    public void createSelf(ClientSection section) {
        super.createSelf(section);
        setSection(section);

        pen = new FocusPanel();
        pen.addStyleName(ESResourceUtils.bundle().css().pen());
        pen.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showEditDialog();
            }
        });

        String time = TimeUtils.fromTo(getData().getFrom(), getData().getTo());
        timeLabel = StyleUtils.getStyledLabel(time, ESResourceUtils.bundle().css().scheduleItemTime());
        timeLabel.setSize((ScheduleGridImpl.ROOM_W - 40) + "px", "14px");
        mainPanel = new FlowPanel();

        FlowPanel dataContent = new FlowPanel();
        dataContent.add(timeLabel);
        dataContent.add(mainPanel);

        handle.setHeight((getLength() * ScheduleData.get().getScale()) + "px");
        handle.setWidget(dataContent);

        FlowPanel content = new FlowPanel();
        content.add(handle);
        content.add(pen);
        content.add(StyleUtils.clearDiv());

        setWidget(content);
    }

    @Override
    public void showPendingState(boolean pending) {
        super.showPendingState(pending);
        for (Entity e : slaves) {
            e.showPendingState(pending);
        }
        showContent(true);
    }

    /** zobrazi editacni dialog */
    protected abstract void showEditDialog();

    /** zapnuti / zruseni bufferoveho vzhledu */
    public void showBufferLook(boolean show) {
        setItemSize(show);
        showTime(!show);
        showPen(!show);
    }

    /** vraci objekt za ktery se uchopuje entita */
    public FocusPanel getHandle() {
        return handle;
    }

    /** vraci label s hodnotou casu */
    protected Label getTimeLabel() {
        return timeLabel;
    }

    /** vygeneruje objekt za ktery se uchopuje entita */
    protected FocusPanel generateHandle() {
        FocusPanel panel = new FocusPanel();
        panel.setSize(((ScheduleGridImpl.ROOM_W * ScheduleData.get().getScale()) - 16) + "px", "4px");
        panel.addStyleName(StyleUtils.FLOAT_LEFT);
        return panel;
    }

    @Override
    public void setBlock(ScheduleBlock block) {
        super.setBlock(block);

        if (getWidget() == null) {
            createSelf(block.getSection());
            if (getData().getRoomIds().size() > 0) {
                generateSlaves(getData(), block);
            }
        }
    }

    /** vygeneruje vedlejsi polozky podle dat hlavni polozky */
    private void generateSlaves(T data, ScheduleBlock block) {
        slaves.clear();

        for (Long id : getData().getRoomIds()) {
            if (block.getRoomId() != id) {
                Entity entity = GWT.create(Entity.class);
                entity.setData(data);
                entity.setRoomId(id);
                entity.setSection(getSection());
                slaves.add(entity);
            }
        }
    }

    /** vraci falesne polozky pro plenarky */
    public ArrayList<Entity> getSlaves() {
        return slaves;
    }

    /** prida / odebere slavy z pozadovanych mistnosti */
    public void refreshSlaves(ArrayList<Long> slaveRooms) {
        ArrayList<Long> oldSlaves = new ArrayList<Long>();
        ArrayList<Entity> toRemove = new ArrayList<Entity>();
        // nalezeni vedlejsich entit k zruseni
        for (Entity e : slaves) {
            if (slaveRooms.contains(e.getRoomId())) {
                oldSlaves.add(new Long(e.getRoomId()));
            } else {
                toRemove.add(e);
                getData().getRoomIds().remove(e.getRoomId());
            }
        }
        // odstraneni nepotrebnych entit
        for (Entity e : toRemove) {
            e.removeFromParent();
            slaves.remove(e);
        }

        final int y = getBlock().getBlockPosition().getY();
        // pridani novych
        for (Long l : slaveRooms) {
            if (!oldSlaves.contains(l)) {
                getData().getRoomIds().add(l);
                Entity entity = GWT.create(Entity.class);
                entity.setData(getData());
                slaves.add(entity);
                DrawProvider.get().drawInPosition(entity, l, y);
                entity.setSection(getSection());
            }
        }
    }

    /** aktualizuje cas */
    private void updateTime() {
        timeLabel.setText(TimeUtils.fromTo(getData().getFrom(), getData().getTo()));
    }

    /** vraci panel s vlastnim obsahem */
    protected FlowPanel mainPanel() {
        return mainPanel;
    }

    /** zobrazí / skryje obsah */
    public void showContent(boolean value) {
        getWidget().setVisible(value);
    }

    /** zobrazí / skryje cas */
    public void showTime(boolean value) {
        timeLabel.setVisible(value);
    }

    /** zobrazí / skryje editacni pero */
    public void showPen(boolean value) {
        pen.setVisible(value);
    }

    @Override
    protected void dataChanged(int state) {
        switch (state) {
            case CHANGED_FROM:
                updateTime();
                break;
            case CHANGED_LENGTH:
                updateTime();
                for (Entity e : slaves) {
                    e.setLength(getLength());
                }
                break;
        }
    }

    /** vraci originalni pozici entity (pred tazenim) */
    public OriginalPosition getOriginalPosition() {
        return position;
    }

    /** nastavuje originalni pozici entity (pred tazenim) */
    public void setOriginalPosition(OriginalPosition position) {
        this.position = position;
    }
}
