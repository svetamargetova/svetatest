package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.scheduling.gwt.client.action.CreateRoomInfoAction;
import consys.event.scheduling.gwt.client.action.DeleteRoomInfoAction;
import consys.event.scheduling.gwt.client.action.ListRoomInfoAction;
import consys.event.scheduling.gwt.client.action.UpdateRoomInfoAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;

/**
 * Nastaveni mistnosti planovani
 * @author pepa
 */
public class RoomsSetting extends RootPanel {

    // komponenty
    private FlowPanel panel;

    public RoomsSetting() {
        super(ESMessageUtils.c.roomsSetting_title());

        addLeftControll(ActionImage.breadcrumbBackBigBack());

        panel = new FlowPanel();
        addWidget(panel);
        addWidget(StyleUtils.clearDiv());
    }

    @Override
    protected void onLoad() {
        panel.clear();
        EventDispatchEvent loadEvent = new EventDispatchEvent(new ListRoomInfoAction(),
                new AsyncCallback<ArrayListResult<ClientRoomInfo>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientRoomInfo> result) {
                        ArrayList<ClientRoomInfo> list = result.getArrayListResult();
                        int i = 0;
                        for (i = 0; i < list.size(); i++) {
                            panel.add(new RoomItem(list.get(i)));
                        }
                        panel.add(new RoomItem());
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }

    /** room item */
    private class RoomItem extends FlowPanel {

        // komponenty
        private TextBox textBox;
        private ActionLabel save;
        private Remover remover;
        // data
        private ClientRoomInfo room;

        public RoomItem() {
            this(null);
        }

        public RoomItem(ClientRoomInfo room) {
            super();
            setStyleName(MARGIN_TOP_5);
            this.room = room;

            final Timer t = new Timer() {

                @Override
                public void run() {
                    hideControl();
                }
            };

            textBox = StyleUtils.getFormInput();
            textBox.setText(room != null ? room.getName() : "");
            textBox.addStyleName(FLOAT_LEFT);
            textBox.addFocusHandler(new FocusHandler() {

                @Override
                public void onFocus(FocusEvent event) {
                    t.cancel();
                    showControl(RoomItem.this.room != null);
                }
            });
            textBox.addBlurHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    if (RoomItem.this.room == null && textBox.getText().trim().equals("")) {
                        t.schedule(500);
                    } else if (RoomItem.this.room != null && textBox.getText().trim().equals(RoomItem.this.room.getName())) {
                        t.schedule(500);
                    }
                }
            });
            textBox.addKeyUpHandler(new KeyUpHandler() {

                @Override
                public void onKeyUp(KeyUpEvent event) {
                    if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                        if (RoomItem.this.room == null) {
                            saveRoom();
                        } else {
                            updateRoom();
                        }
                    }
                }
            });
            add(textBox);

            save = new ActionLabel(UIMessageUtils.c.const_save(), MARGIN_LEFT_10 + " " + FLOAT_LEFT + " " + MARGIN_TOP_3);
            save.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    saveRoom();
                }
            });
            add(save);

            ActionLabel update = new ActionLabel(UIMessageUtils.c.const_update(), MARGIN_LEFT_10 + " " + FLOAT_LEFT + " " + MARGIN_TOP_3);
            update.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    updateRoom();
                }
            });

            Image remove = new Image(ResourceUtils.system().removeCross());
            remove.setStyleName(StyleUtils.HAND);
            remove.addStyleName(FLOAT_LEFT);
            remove.addStyleName(MARGIN_LEFT_10);
            remove.addStyleName(MARGIN_TOP_3);
            ConsysAction removeAction = new ConsysAction() {

                @Override
                public void run() {
                    EventDispatchEvent deleteEvent = new EventDispatchEvent(new DeleteRoomInfoAction(RoomItem.this.room.getId()),
                            new AsyncCallback<VoidResult>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava event action executor
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    removeFromParent();
                                }
                            }, RoomsSetting.this);
                    EventBus.get().fireEvent(deleteEvent);
                }
            };

            FlowPanel controlPanel = new FlowPanel();
            controlPanel.addStyleName(FLOAT_LEFT);
            controlPanel.add(update);
            controlPanel.add(remove);

            remover = new Remover(controlPanel, remove, removeAction) {

                @Override
                protected void onQuestionSwap() {
                    t.cancel();
                }

                @Override
                protected void onDefaultSwap() {
                    focus();
                }
            };
            remover.addStyleName(FLOAT_LEFT);
            remover.addQuestionStyleName(MARGIN_TOP_3);
            remover.addQuestionStyleName(MARGIN_LEFT_10);
            add(remover);
            hideControl();

            add(StyleUtils.clearDiv());
        }

        /** zobrazi prislusne ovladace */
        private void showControl(boolean doUpdate) {
            if (!doUpdate) {
                save.setVisible(true);
                remover.setVisible(false);
            } else {
                save.setVisible(false);
                remover.setVisible(true);
            }
        }

        /** skryje vsechny ovladace */
        private void hideControl() {
            save.setVisible(false);
            remover.setVisible(false);
        }

        public void focus() {
            textBox.setFocus(true);
        }

        /** spusti ulozeni roomu */
        private void saveRoom() {
            String name = textBox.getText().trim();
            if (name.isEmpty()) {
                getFailMessage().setText(ESMessageUtils.c.roomsSetting_error_enteredEmptyValue());
                return;
            }
            final ClientRoomInfo cri = new ClientRoomInfo();
            cri.setName(name);
            EventDispatchEvent createEvent = new EventDispatchEvent(new CreateRoomInfoAction(cri),
                    new AsyncCallback<LongResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(LongResult result) {
                            cri.setId(result.getValue());
                            RoomItem.this.room = cri;

                            hideControl();

                            RoomItem ti = new RoomItem();
                            panel.add(ti);
                            ti.focus();
                        }
                    }, RoomsSetting.this);
            EventBus.get().fireEvent(createEvent);
        }

        /** spusti aktualizaci roomu */
        private void updateRoom() {
            final String name = textBox.getText().trim();
            if (name.isEmpty()) {
                getFailMessage().setText(ESMessageUtils.c.roomsSetting_error_enteredEmptyValue());
                return;
            }

            EventDispatchEvent updateEvent = new EventDispatchEvent(new UpdateRoomInfoAction(new ClientRoomInfo(room.getId(), name)),
                    new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            RoomItem.this.room.setName(name);
                        }
                    }, RoomsSetting.this);
            EventBus.get().fireEvent(updateEvent);
        }
    }
}
