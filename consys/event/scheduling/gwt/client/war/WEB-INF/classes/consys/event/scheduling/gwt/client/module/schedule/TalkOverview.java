package consys.event.scheduling.gwt.client.module.schedule;

import consys.event.scheduling.gwt.client.module.settings.TalkOverviewItem;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewsAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverviews;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;

/**
 * Pro obyc uzivatele zobrazi prehled sekci, pro admina umozni sekce prirazovat
 * @author pepa
 */
public class TalkOverview extends RootPanel {

    // komponenty
    private ConsysTabPanel<Long> tabs;
    private ConsysMessage infoMessage;
    // data
    private boolean loaded;

    public TalkOverview() {
        super(ESMessageUtils.c.talkOverview_title());
        loaded = false;

        infoMessage = ConsysMessage.getSuccess();
        infoMessage.hideClose(true);
        infoMessage.setText(ESMessageUtils.c.talkOverview_text_noneSectionYet());
        infoMessage.setVisible(false);
        addWidget(infoMessage);

        tabs = new ConsysTabPanel<Long>(TalkOverview.class.getName());
        tabs.setVisible(false);
        addWidget(tabs);
    }

    @Override
    protected void onLoad() {
        if (!loaded) {
            EventDispatchEvent loadSectionsEvent = new EventDispatchEvent(new LoadTalkOverviewsAction(),
                    new AsyncCallback<ClientSectionOverviews>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava ActionExcecutor
                        }

                        @Override
                        public void onSuccess(ClientSectionOverviews result) {
                            loaded = true;
                            generateTabs(result);
                        }
                    }, this);
            EventBus.get().fireEvent(loadSectionsEvent);
        }
    }

    /** vygeneruje zalozky */
    private void generateTabs(ClientSectionOverviews data) {
        tabs.clear();

        boolean someSections = !data.getSectionThumbs().isEmpty();
        infoMessage.setVisible(!someSections);
        tabs.setVisible(someSections);

        for (CommonLongThumb clt : data.getSectionThumbs()) {
            ConsysTabPanelLabel<Long> lab = new ConsysTabPanelLabel<Long>(clt.getId(), clt.getName());
            tabs.addTabItem(lab, new TalkOverviewItem(this, clt.getId(), clt.getName()));
        }
    }
}
