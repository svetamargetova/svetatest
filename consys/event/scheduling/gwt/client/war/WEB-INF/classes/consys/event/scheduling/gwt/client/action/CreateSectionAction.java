package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni sekce
 * @author pepa
 */
public class CreateSectionAction extends EventAction<ArrayListResult<Long>> {

    private static final long serialVersionUID = 1116462560262243L;
    // data
    private ArrayList<String> names;

    public CreateSectionAction() {
        names = new ArrayList<String>();
    }

    public ArrayList<String> getNames() {
        return names;
    }
}
