package consys.event.scheduling.gwt.client.module.schedule.block;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.common.gwt.shared.bo.ClientTime;

/**
 * Blok v mistnosti
 * @author pepa
 */
public interface ScheduleBlock {

    /** id mistnosti ve ktere je blok umisteny */
    public Long getRoomId();

    /** sekce bloku */
    public ClientSection getSection();

    /** obsah pro vkladani polozek */
    public AbsolutePanel getContent();

    /** cas zacatku bloku */
    public ClientTime getFrom();

    /** cas konce bloku */
    public ClientTime getTo();

    /** vraci data bloku */
    public ClientBlock getBlock();

    /** vraci pozici bloku */
    public BlockPosition getBlockPosition();
}

