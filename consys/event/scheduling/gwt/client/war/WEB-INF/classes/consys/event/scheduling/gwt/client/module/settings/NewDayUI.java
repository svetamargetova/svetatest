package consys.event.scheduling.gwt.client.module.settings;

import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysDateBox.ShowDateAs;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class NewDayUI extends NewItemUI {

    // komponenty
    private ConsysDateBox dateBox;
    // data
    private ShowDateAs as;

    public NewDayUI() {
        super();
    }

    @Override
    public void focus() {
    }

    @Override
    public void createForm(BaseForm form) {
        dateBox = ConsysDateBox.getHalfBox();
        if (as != null) {
            dateBox.setShowDateAs(as);
        }

        form.reset();
        form.addRequired(0, ECoMessageUtils.c.const_date(), dateBox);
    }

    public ConsysDateBox getDateBox() {
        return dateBox;
    }

    public Date getDate() {
        return dateBox.getValue();
    }

    public void showDateAs(ShowDateAs as) {
        this.as = as;
    }
}