package consys.event.scheduling.gwt.client.module.schedule.content;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Panel pro pozadi bloku
 * @author pepa
 */
public class ScheduleBackgrounds extends AbsolutePanel {

    // data
    private int defaultWidth;
    private int defaultHeight;

    public ScheduleBackgrounds(int width, int height) {
        addStyleName(ESResourceUtils.bundle().css().scheduleBackgrounds());
        setSize(width + "px", height + "px");
        defaultWidth = width;
        defaultHeight = height;
    }

    public void refreshScale() {
        final int scale = ScheduleData.get().getScale();
        setSize((defaultWidth * scale) + "px", (defaultHeight * scale) + "px");
    }

    public void setSize(int width, int height) {
        defaultWidth = width;
        defaultHeight = height;
        super.setSize(defaultWidth + "px", defaultHeight + "px");
    }
}
