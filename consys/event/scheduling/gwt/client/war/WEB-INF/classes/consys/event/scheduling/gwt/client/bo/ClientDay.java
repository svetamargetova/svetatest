package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientTime;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Nastaveni dne
 * @author pepa
 */
public class ClientDay extends ClientDayThumb implements Result {

    private static final long serialVersionUID = -2759492146544403820L;
    // data
    private ClientTime from;
    private ClientTime to;
    private ArrayList<ClientRoomInfo> rooms;
    private ArrayList<ClientTime> splits;

    public ClientDay() {
        rooms = new ArrayList<ClientRoomInfo>();
        splits = new ArrayList<ClientTime>();
    }

    public ClientTime getFrom() {
        return from;
    }

    public void setFrom(ClientTime from) {
        this.from = from;
    }

    public ArrayList<ClientRoomInfo> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<ClientRoomInfo> rooms) {
        this.rooms = rooms;
    }

    public ArrayList<ClientTime> getSplits() {
        return splits;
    }

    public void setSplits(ArrayList<ClientTime> splits) {
        this.splits = splits;
    }

    public ClientTime getTo() {
        return to;
    }

    public void setTo(ClientTime to) {
        this.to = to;
    }
}
