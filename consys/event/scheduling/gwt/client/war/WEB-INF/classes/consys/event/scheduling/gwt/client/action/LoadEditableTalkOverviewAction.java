package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientEditableSectionOverview;

/**
 *
 * @author pepa
 */
public class LoadEditableTalkOverviewAction extends EventAction<ClientEditableSectionOverview> {

    private static final long serialVersionUID = 8538545631035121828L;
    // data
    private Long sectionId;

    public LoadEditableTalkOverviewAction() {
    }

    public LoadEditableTalkOverviewAction(Long sectionId) {
        this.sectionId = sectionId;
    }

    public Long getSectionId() {
        return sectionId;
    }
}
