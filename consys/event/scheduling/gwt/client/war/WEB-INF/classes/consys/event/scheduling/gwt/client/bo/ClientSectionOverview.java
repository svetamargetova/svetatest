package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientSectionOverview implements Result {

    private static final long serialVersionUID = -6823752888344649157L;
    // data
    private ArrayList<String> talks;

    public ClientSectionOverview() {
        talks = new ArrayList<String>();
    }

    public ArrayList<String> getTalks() {
        return talks;
    }

    public void setTalks(ArrayList<String> talks) {
        this.talks = talks;
    }
}
