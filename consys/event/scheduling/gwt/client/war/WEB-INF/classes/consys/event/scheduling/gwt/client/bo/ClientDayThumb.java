package consys.event.scheduling.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.Date;

/**
 * Nahladovy objekt konfigurace dne planovani
 * @author pepa
 */
public class ClientDayThumb implements IsSerializable, Comparable<ClientDayThumb> {

    // data
    private Long id;
    private Date date;

    public ClientDayThumb() {
    }

    public ClientDayThumb(Long id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int compareTo(ClientDayThumb o) {
        return date.compareTo(o.getDate());
    }
}
