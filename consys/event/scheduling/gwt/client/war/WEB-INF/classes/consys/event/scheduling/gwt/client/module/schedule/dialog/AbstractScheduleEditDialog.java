package consys.event.scheduling.gwt.client.module.schedule.dialog;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Abstraktni predek editacnich dialogu planovani
 * @author pepa
 */
public abstract class AbstractScheduleEditDialog extends Dialog implements ActionExecutionDelegate {

    // komponenty
    private FlowPanel mainPanel;
    private FlowPanel failPanel;
    private FlowPanel panel;
    // waiting
    private Waiting waiting;

    public AbstractScheduleEditDialog() {
        mainPanel = new FlowPanel();
        failPanel = new FlowPanel();
        panel = new FlowPanel();
        waiting = new Waiting(mainPanel);

        mainPanel.setWidth(DEFAULT_WIDTH_PX);
        mainPanel.add(waiting);
        mainPanel.add(failPanel);
        mainPanel.add(panel);
    }

    /** hlavni obalovy panel, vracet z vytvareni contentu */
    protected FlowPanel mainPanel() {
        return mainPanel;
    }

    /** pouzivat pro vkladani vlastniho obsahu */
    protected FlowPanel panel() {
        return panel;
    }

    /** zobrazuje waiting */
    private void showWaiting(boolean value) {
        if (value) {
            waiting.show(mainPanel);
        } else {
            waiting.hide();
        }
    }

    @Override
    public void actionStarted() {
        showWaiting(true);
    }

    @Override
    public void actionEnds() {
        showWaiting(false);
    }

    /** vraci chybovou zpravu */
    protected ConsysMessage getFailMessage() {
        ConsysMessage fail = ConsysMessage.getFail();
        failPanel.add(fail);
        return fail;
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }
}
