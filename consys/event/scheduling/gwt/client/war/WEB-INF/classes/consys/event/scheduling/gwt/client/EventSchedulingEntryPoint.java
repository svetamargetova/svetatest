package consys.event.scheduling.gwt.client;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventUserRightsCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.bo.CommonLongThumb;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.common.gwt.client.cache.handler.CurrentEventUserRightCacheHandler;
import consys.event.common.gwt.client.module.OverseerModule;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import consys.event.scheduling.gwt.client.action.CreateDaySettingsAction;
import consys.event.scheduling.gwt.client.action.CreateRoomInfoAction;
import consys.event.scheduling.gwt.client.action.CreateSectionAction;
import consys.event.scheduling.gwt.client.action.DeleteDaySettingsAction;
import consys.event.scheduling.gwt.client.action.DeleteRoomInfoAction;
import consys.event.scheduling.gwt.client.action.DeleteSectionAction;
import consys.event.scheduling.gwt.client.action.ListRoomInfoAction;
import consys.event.scheduling.gwt.client.action.ListSectionsAction;
import consys.event.scheduling.gwt.client.action.LoadBufferAction;
import consys.event.scheduling.gwt.client.action.LoadDaySettingsAction;
import consys.event.scheduling.gwt.client.action.LoadEditableTalkOverviewAction;
import consys.event.scheduling.gwt.client.action.LoadMyScheduleDayAction;
import consys.event.scheduling.gwt.client.action.LoadScheduleAction;
import consys.event.scheduling.gwt.client.action.LoadScheduleDayAction;
import consys.event.scheduling.gwt.client.action.LoadSchedulingSettingsAction;
import consys.event.scheduling.gwt.client.action.LoadSectionAction;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewAction;
import consys.event.scheduling.gwt.client.action.LoadTalkOverviewsAction;
import consys.event.scheduling.gwt.client.action.UpdateBlindAction;
import consys.event.scheduling.gwt.client.action.UpdateBlockAction;
import consys.event.scheduling.gwt.client.action.UpdateDaySettingsAction;
import consys.event.scheduling.gwt.client.action.UpdateEntityAction;
import consys.event.scheduling.gwt.client.action.UpdateEntityNoteAction;
import consys.event.scheduling.gwt.client.action.UpdateEntityPositionAction;
import consys.event.scheduling.gwt.client.action.UpdateFavouriteEntityAction;
import consys.event.scheduling.gwt.client.action.UpdateSectionAction;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientDayThumb;
import consys.event.scheduling.gwt.client.bo.ClientEditableSectionOverview;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.event.scheduling.gwt.client.bo.ClientSchedule;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.bo.ClientEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientMyEntity;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverview;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverviews;
import consys.event.scheduling.gwt.client.bo.ClientSectionSettings;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntityTalk;
import consys.event.scheduling.gwt.client.module.SchedulingModule;
import consys.event.scheduling.gwt.client.module.schedule.MyScheduling;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.TalkOverview;
import consys.event.scheduling.gwt.client.module.settings.BlockSplitter;
import consys.event.scheduling.gwt.client.module.settings.SchedulingSettings;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventSchedulingEntryPoint extends DebugModuleEntryPoint {

    @Override
    public void initMocks() {
        ClientUser cu = new ClientUser();
        cu.setUuid("mojeuuid");
        Cache.get().register(UserCacheAction.USER, cu);

        List<String> rightList = new ArrayList<String>();
        rightList.add(EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN);
        Cache.get().register(CurrentEventUserRightsCacheAction.CURRENT_USER_EVENT_RIGHTS, rightList);

        CacheRegistrator.register(CurrentEventUserRightAction.class, new CurrentEventUserRightCacheHandler());

        final long day = 86400000l;
        Date now = new Date();
        // --------------
        ClientDayThumb frame1 = new ClientDayThumb();
        frame1.setId(1l);
        frame1.setDate(now);
        ClientDayThumb frame2 = new ClientDayThumb();
        frame2.setId(2l);
        frame2.setDate(new Date(now.getTime() + day));
        ClientDayThumb frame3 = new ClientDayThumb();
        frame3.setId(3l);
        frame3.setDate(new Date(now.getTime() + day + day));

        ClientSchedule clientSchedule = new ClientSchedule();

        clientSchedule.getFrames().add(frame3);
        clientSchedule.getFrames().add(frame1);
        clientSchedule.getFrames().add(frame2);


        //clientSchedule.getSectionIdRights().add(1l);
        clientSchedule.getSectionIdRights().add(2l);
        //clientSchedule.getSectionIdRights().add(3l);

        clientSchedule.getSections().add(new ClientSection(1l, "Sekce jedna", "ffffcc", "fefe83", null));
        clientSchedule.getSections().add(new ClientSection(2l, "Sekce dva", "dcf0fe", "b4ddfa", null));
        clientSchedule.getSections().add(new ClientSection(3l, "Sekce tři", "eafdd5", "caf49b", null));
        clientSchedule.getSections().add(new ClientSection(4l, "Sekce čtyři", null, "F5F5F5", null));

        MockFactory.addActionMock(LoadScheduleAction.class, new ActionMock<ClientSchedule>(clientSchedule));
        // --------------
        ClientBlock cb1 = new ClientBlock();
        cb1.setFrom(new ClientTime(480));
        cb1.setTo(new ClientTime(540));
        cb1.setSectionId(-1l);
        cb1.setBlockId(1l);

        ClientBlock cb2 = new ClientBlock();
        cb2.setFrom(new ClientTime(540));
        cb2.setTo(new ClientTime(685));
        cb2.setSectionId(1l);
        cb2.setChairUuid("123");
        cb2.setBlockId(2l);

        ClientBlock cb3 = new ClientBlock();
        cb3.setFrom(new ClientTime(685));
        cb3.setTo(new ClientTime(810));
        cb3.setSectionId(-1l);
        cb3.setBlockId(3l);

        ClientBlock cb4 = new ClientBlock();
        cb4.setFrom(new ClientTime(810));
        cb4.setTo(new ClientTime(960));
        cb4.setSectionId(2l);
        cb4.setBlockId(4l);

        ClientBlock cb5 = new ClientBlock();
        cb5.setFrom(new ClientTime(960));
        cb5.setTo(new ClientTime(1020));
        cb5.setSectionId(-1l);
        cb5.setBlockId(5l);
        cb5.setChairUuid("mojeuuid");

        ClientBlock cb6 = new ClientBlock();
        cb6.setFrom(new ClientTime(480));
        cb6.setTo(new ClientTime(540));
        cb6.setSectionId(-1l);
        cb6.setBlockId(6l);

        ClientBlock cb7 = new ClientBlock();
        cb7.setFrom(new ClientTime(540));
        cb7.setTo(new ClientTime(685));
        cb7.setSectionId(1l);
        cb7.setChairUuid("123");
        cb7.setBlockId(7l);

        ClientBlock cb8 = new ClientBlock();
        cb8.setFrom(new ClientTime(685));
        cb8.setTo(new ClientTime(810));
        cb8.setSectionId(-1l);
        cb8.setBlockId(8l);

        ClientBlock cb9 = new ClientBlock();
        cb9.setFrom(new ClientTime(810));
        cb9.setTo(new ClientTime(960));
        cb9.setSectionId(3l);
        cb9.setBlockId(9l);

        ClientBlock cb10 = new ClientBlock();
        cb10.setFrom(new ClientTime(960));
        cb10.setTo(new ClientTime(1020));
        cb10.setSectionId(-1l);
        cb10.setBlockId(10l);

        ClientBlock cb11 = new ClientBlock();
        cb11.setFrom(new ClientTime(480));
        cb11.setTo(new ClientTime(540));
        cb11.setSectionId(-1l);
        cb11.setBlockId(11l);

        ClientBlock cb12 = new ClientBlock();
        cb12.setFrom(new ClientTime(540));
        cb12.setTo(new ClientTime(685));
        cb12.setSectionId(2l);
        cb12.setChairUuid("1234");
        cb12.setBlockId(12l);

        ClientBlock cb13 = new ClientBlock();
        cb13.setFrom(new ClientTime(685));
        cb13.setTo(new ClientTime(810));
        cb13.setSectionId(-1l);
        cb13.setBlockId(13l);

        ClientBlock cb14 = new ClientBlock();
        cb14.setFrom(new ClientTime(810));
        cb14.setTo(new ClientTime(960));
        cb14.setSectionId(3l);
        cb14.setBlockId(14l);

        ClientBlock cb15 = new ClientBlock();
        cb15.setFrom(new ClientTime(960));
        cb15.setTo(new ClientTime(1020));
        cb15.setSectionId(-1l);
        cb15.setBlockId(15l);

        ClientScheduleDay csd = new ClientScheduleDay();
        csd.setFrom(new ClientTime(480));
        csd.setTo(new ClientTime(1020));

        // Roomy
        ClientRoom room1 = new ClientRoom(1l, "Room 1");
        room1.getBlocks().add(cb1);
        room1.getBlocks().add(cb2);
        room1.getBlocks().add(cb3);
        room1.getBlocks().add(cb4);
        room1.getBlocks().add(cb5);

        ClientRoom room2 = new ClientRoom(2l, "Room 2");
        room2.getBlocks().add(cb6);
        room2.getBlocks().add(cb7);
        room2.getBlocks().add(cb8);
        room2.getBlocks().add(cb9);
        room2.getBlocks().add(cb10);

        ClientRoom room3 = new ClientRoom(3l, "Room 3");
        room3.getBlocks().add(cb11);
        room3.getBlocks().add(cb12);
        room3.getBlocks().add(cb13);
        room3.getBlocks().add(cb14);
        room3.getBlocks().add(cb15);

        // submissions
        ClientEntityTalk talk1 = new ClientEntityTalk();
        talk1.setFrom(new ClientTime(550));
        talk1.setTo(new ClientTime(595));
        talk1.setId(1l);
        talk1.setName("Závěrečná zpráva z výzkumného projektu zaměřeného na psychodiagnostické metody „Substance Use Risk Profile Scale“ a „High School Personality Questionnaire“");
        talk1.setAuthor("Michal Kolínek");
        ArrayList<Long> rooms = new ArrayList();
        rooms.add(room1.getId());
        rooms.add(room3.getId());
        talk1.setRoomIds(rooms);

        cb7.getSubmissions().add(talk1);

        ClientEntityTalk talk3 = new ClientEntityTalk();
        talk3.setFrom(new ClientTime(810));
        talk3.setTo(new ClientTime(920));
        talk3.setId(5l);
        talk3.setName("Math Engineering");
        talk3.setAuthor("Steve Nowak");

        cb4.getSubmissions().add(talk3);


        // planarky
        ClientEntityTalk talk2 = new ClientEntityTalk();
        talk2.setFrom(new ClientTime(600));
        talk2.setTo(new ClientTime(630));
        talk2.setId(2l);
        talk2.setAuthor("Max Planck");
        talk2.setName("Math Engineering");
        rooms = new ArrayList();
        rooms.add(room1.getId());
        rooms.add(room2.getId());
        talk2.setRoomIds(rooms);

        cb2.getSubmissions().add(talk2);

        // blindy
        ClientEntityBlind blind1 = new ClientEntityBlind();
        blind1.setFrom(new ClientTime(480));
        blind1.setTo(new ClientTime(520));
        blind1.setId(3l);
        blind1.setName("COnference Opening");
        rooms = new ArrayList();
        rooms.add(room1.getId());
        rooms.add(room2.getId());
        rooms.add(room3.getId());
        blind1.setRoomIds(rooms);

        cb1.getSubmissions().add(blind1);

//        ClientSubmissionBlind blind2 = new ClientSubmissionBlind();
//        blind2.setFrom(new ClientTime(705));
//        blind2.setTo(new ClientTime(800));
//        blind2.setId(4l);
//        blind2.setName("Time for lunch :)");
//        rooms = new ArrayList();
//        rooms.add(room1.getId());
//        rooms.add(room2.getId());
//        rooms.add(room3.getId());
//        blind2.setRoomIds(rooms);
//
//        cb8.getSubmissions().add(blind2);




        csd.getRooms().add(room1);
        csd.getRooms().add(room2);
        csd.getRooms().add(room3);

        MockFactory.addActionMock(LoadScheduleDayAction.class, new ActionMock<ClientScheduleDay>(csd));
        // --------------

        //sznam talku do bufferu

        ArrayList<ClientUnassignedEntityTalk> bufferList = new ArrayList<ClientUnassignedEntityTalk>();

        ClientUnassignedEntityTalk cut1 = new ClientUnassignedEntityTalk(1l, 1l, "Math engineering", "Jan Zakopcanik");
        bufferList.add(cut1);

        ClientUnassignedEntityTalk cut2 = new ClientUnassignedEntityTalk(2l, 1l, "Linear colider simulation", "Matt O'neal");
        bufferList.add(cut2);

        ClientUnassignedEntityTalk cut3 = new ClientUnassignedEntityTalk(3l, 2l, "Linear colider simulation", "Matt O'neal");
        bufferList.add(cut3);

        ClientUnassignedEntityTalk cut4 = new ClientUnassignedEntityTalk(4l, 3l, "Linear colider simulation", "Matt O'neal");
        bufferList.add(cut4);

        ClientUnassignedEntityTalk cut5 = new ClientUnassignedEntityTalk(5l, 1l, "Solving diff. equations", "Matt O'neal");
        bufferList.add(cut5);

        ArrayListResult<ClientUnassignedEntityTalk> buffer = new ArrayListResult<ClientUnassignedEntityTalk>(bufferList);



        MockFactory.addActionMock(LoadBufferAction.class, new ActionMock<ArrayListResult<ClientUnassignedEntityTalk>>(buffer));

        //--------------------------
        ArrayList<ClientRoomInfo> acri = new ArrayList<ClientRoomInfo>();
        acri.add(new ClientRoomInfo(1l, "Místnost A4"));
        acri.add(new ClientRoomInfo(2l, "Místnost C1"));
        acri.add(new ClientRoomInfo(3l, "Místnost E6"));
        acri.add(new ClientRoomInfo(4l, "Místnost K9"));
        ArrayListResult<ClientRoomInfo> arcri = new ArrayListResult<ClientRoomInfo>(acri);
        MockFactory.addActionMock(ListRoomInfoAction.class, new ActionMock<ArrayListResult<ClientRoomInfo>>(arcri));
        //--------------------------
        MockFactory.addActionMock(CreateRoomInfoAction.class, new ActionMock<LongResult>(new LongResult(2l)));
        //--------------------------
        MockFactory.addActionMock(DeleteRoomInfoAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        ArrayList<Long> longArray = new ArrayList<Long>();
        longArray.add(1l);
        ArrayListResult<Long> longResult = new ArrayListResult<Long>(longArray);
        MockFactory.addActionMock(CreateDaySettingsAction.class, new ActionMock<ArrayListResult<Long>>(longResult));
        //--------------------------
        ArrayList<ClientDayThumb> dayThumbs = new ArrayList<ClientDayThumb>();
        dayThumbs.add(new ClientDayThumb(2l, new Date(now.getTime() + day)));
        dayThumbs.add(new ClientDayThumb(1l, new Date()));
        ArrayListResult<ClientDayThumb> dayThumbResult = new ArrayListResult<ClientDayThumb>(dayThumbs);
        MockFactory.addActionMock(LoadSchedulingSettingsAction.class, new ActionMock<ArrayListResult<ClientDayThumb>>(dayThumbResult));
        //--------------------------
        ClientDay cday = new ClientDay();
        cday.setDate(new Date());
        cday.setId(5l);
        cday.setFrom(new ClientTime(500));
        cday.setTo(new ClientTime(1000));
        cday.getSplits().add(new ClientTime(800));
        cday.getSplits().add(new ClientTime(900));
        cday.getRooms().add(new ClientRoomInfo(1l, "Místnost A4"));
        cday.getRooms().add(new ClientRoomInfo(2l, "Místnost C1"));
        MockFactory.addActionMock(LoadDaySettingsAction.class, new ActionMock<ClientDay>(cday));
        //--------------------------
        MockFactory.addActionMock(DeleteDaySettingsAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        MockFactory.addActionMock(UpdateDaySettingsAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        ClientUserThumb userThumb = new ClientUserThumb();
        userThumb.setUuid("123");
        userThumb.setName("User Thumb 123");
        MockFactory.addActionMock(LoadUserThumbAction.class, new ActionMock<ClientUserThumb>(userThumb));
        //--------------------------
        MockFactory.addActionMock(UpdateBlockAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        MockFactory.addActionMock(UpdateEntityAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        MockFactory.addActionMock(UpdateBlindAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        MockFactory.addActionMock(UpdateEntityPositionAction.class, new ActionMock<LongResult>(new LongResult(5)));
        //MockFactory.addActionMock(UpdateEntityPositionAction.class, new ActionMock<LongResult>(new BadInputException()));
        //--------------------------
        ArrayListResult<ClientUserThumb> cuts = new ArrayListResult<ClientUserThumb>();
        ClientUserThumb cutik1 = new ClientUserThumb();
        cutik1.setUuid("123");
        cutik1.setName("Pepa Vokurka");
        cuts.getArrayListResult().add(cutik1);
        ClientUserThumb cutik2 = new ClientUserThumb();
        cutik2.setUuid("1234");
        cutik2.setName("John Vogelmut");
        cuts.getArrayListResult().add(cutik2);
        MockFactory.addActionMock(ListUserThumbsAction.class, new ActionMock<ArrayListResult<ClientUserThumb>>(cuts));
        //--------------------------
        ClientSectionSettings css = new ClientSectionSettings();
        ClientSection cs = new ClientSection();
        cs.setId(4l);
        cs.setName("Sekce jedna");
        cs.setBgColor("213456");
        cs.setFgColor("ff0000");
        css.setFirstSection(cs);
        MockFactory.addActionMock(ListSectionsAction.class, new ActionMock<ClientSectionSettings>(css));
        //--------------------------
        MockFactory.addActionMock(DeleteSectionAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        ArrayList<Long> createSectionList = new ArrayList<Long>();
        createSectionList.add(6l);
        ArrayListResult<Long> createSectionResult = new ArrayListResult<Long>(createSectionList);
        MockFactory.addActionMock(CreateSectionAction.class, new ActionMock<ArrayListResult<Long>>(createSectionResult));
        //--------------------------
        ClientSection clientSection = new ClientSection();
        clientSection.setId(3l);
        clientSection.setName("Nactena sekce s trochu delším názvem");
        clientSection.setBgColor("FF0000");
        clientSection.setFgColor("00FF33");
        clientSection.getAuthorizedUsers().add("mojeuuid");
        MockFactory.addActionMock(LoadSectionAction.class, new ActionMock<ClientSection>(clientSection));
        //--------------------------
        MockFactory.addActionMock(UpdateSectionAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        ClientSectionOverviews csos = new ClientSectionOverviews();
        csos.getSectionThumbs().add(new CommonLongThumb(1l, "Název sekce 1"));
        csos.getSectionThumbs().add(new CommonLongThumb(2l, "Název sekce 2"));
        MockFactory.addActionMock(LoadTalkOverviewsAction.class, new ActionMock<ClientSectionOverviews>(csos));
        //--------------------------
        ClientSectionOverview cso = new ClientSectionOverview();
        cso.getTalks().add("Drawing in assessment of children in hospital");
        cso.getTalks().add("Evaluace – základ kvalitního programu z pohledu  teorie i praxe");
        cso.getTalks().add("Závěrečná zpráva z výzkumného projektu zaměřeného na psychodiagnostické metody „Substance Use Risk Profile Scale“ a „High School Personality Questionnaire“");
        cso.getTalks().add("Drawing in assessment of children in hospital");
        cso.getTalks().add("Evaluace – základ kvalitního programu z pohledu  teorie i praxe");
        cso.getTalks().add("Závěrečná zpráva z výzkumného projektu zaměřeného na psychodiagnostické metody „Substance Use Risk Profile Scale“ a „High School Personality Questionnaire“");
        MockFactory.addActionMock(LoadTalkOverviewAction.class, new ActionMock<ClientSectionOverview>(cso));
        //--------------------------
        ClientEditableSectionOverview ceso = new ClientEditableSectionOverview();
        //ceso.getTalks().add(new CommonLongThumb(6l, "Drawing in assessment of children in hospital"));
        ceso.getTalks().add(new CommonLongThumb(5l, "Přednáška pět a sedmdesát pět osmdesátin"));
        ceso.getTalks().add(new CommonLongThumb(4l, "Přednáška čtyři"));
        ceso.getUnassignedTalks().add(new CommonLongThumb(1l, "Drawing in assessment of children in hospital"));
        ceso.getUnassignedTalks().add(new CommonLongThumb(2l, "Evaluace – základ kvalitního programu z pohledu  teorie i praxe"));
        ceso.getUnassignedTalks().add(new CommonLongThumb(3l, "Závěrečná zpráva z výzkumného projektu zaměřeného na psychodiagnostické metody „Substance Use Risk Profile Scale“ a „High School Personality Questionnaire“"));
        MockFactory.addActionMock(LoadEditableTalkOverviewAction.class, new ActionMock<ClientEditableSectionOverview>(ceso));
        //--------------------------
        ClientSchedule mySchedule = new ClientSchedule();
        mySchedule.setFrames(dayThumbs);
        mySchedule.getSections().add(new ClientSection(1l, "Sekce jedna", "ffffcc", "fefe83", null));
        mySchedule.getSections().add(new ClientSection(2l, "Sekce dva", "dcf0fe", "b4ddfa", null));
        mySchedule.getSections().add(new ClientSection(3l, "Sekce tři", "eafdd5", "caf49b", null));
        mySchedule.getSections().add(new ClientSection(4l, "Sekce čtyři", null, "F5F5F5", null));
        MockFactory.addActionMock(LoadScheduleAction.class, new ActionMock<ClientSchedule>(mySchedule));
        //--------------------------
        ClientScheduleDay myDay = new ClientScheduleDay();
        myDay.setFrom(new ClientTime(480));
        myDay.setTo(new ClientTime(1020));

        ClientBlock<ClientMyEntity> myCb1 = new ClientBlock<ClientMyEntity>();
        myCb1.setFrom(new ClientTime(480));
        myCb1.setTo(new ClientTime(540));
        myCb1.setSectionId(-1l);
        myCb1.setBlockId(1l);

        ClientBlock myCb2 = new ClientBlock();
        myCb2.setFrom(new ClientTime(540));
        myCb2.setTo(new ClientTime(685));
        myCb2.setSectionId(1l);
        myCb2.setChairUuid("123");
        myCb2.setBlockId(2l);

        ClientBlock myCb3 = new ClientBlock();
        myCb3.setFrom(new ClientTime(685));
        myCb3.setTo(new ClientTime(810));
        myCb3.setSectionId(-1l);
        myCb3.setBlockId(3l);

        ClientBlock myCb4 = new ClientBlock();
        myCb4.setFrom(new ClientTime(810));
        myCb4.setTo(new ClientTime(960));
        myCb4.setSectionId(2l);
        myCb4.setBlockId(4l);

        ClientBlock myCb5 = new ClientBlock();
        myCb5.setFrom(new ClientTime(960));
        myCb5.setTo(new ClientTime(1020));
        myCb5.setSectionId(-1l);
        myCb5.setBlockId(5l);
        myCb5.setChairUuid("mojeuuid");

        ClientBlock myCb6 = new ClientBlock();
        myCb6.setFrom(new ClientTime(480));
        myCb6.setTo(new ClientTime(540));
        myCb6.setSectionId(-1l);
        myCb6.setBlockId(6l);

        ClientBlock myCb7 = new ClientBlock();
        myCb7.setFrom(new ClientTime(540));
        myCb7.setTo(new ClientTime(685));
        myCb7.setSectionId(1l);
        myCb7.setChairUuid("123");
        myCb7.setBlockId(7l);

        ClientBlock myCb8 = new ClientBlock();
        myCb8.setFrom(new ClientTime(685));
        myCb8.setTo(new ClientTime(810));
        myCb8.setSectionId(-1l);
        myCb8.setBlockId(8l);

        ClientBlock myCb9 = new ClientBlock();
        myCb9.setFrom(new ClientTime(810));
        myCb9.setTo(new ClientTime(960));
        myCb9.setSectionId(3l);
        myCb9.setBlockId(9l);

        ClientBlock myCb10 = new ClientBlock();
        myCb10.setFrom(new ClientTime(960));
        myCb10.setTo(new ClientTime(1020));
        myCb10.setSectionId(-1l);
        myCb10.setBlockId(10l);

        ClientBlock myCb11 = new ClientBlock();
        myCb11.setFrom(new ClientTime(480));
        myCb11.setTo(new ClientTime(540));
        myCb11.setSectionId(-1l);
        myCb11.setBlockId(11l);

        ClientBlock myCb12 = new ClientBlock();
        myCb12.setFrom(new ClientTime(540));
        myCb12.setTo(new ClientTime(685));
        myCb12.setSectionId(2l);
        myCb12.setChairUuid("1234");
        myCb12.setBlockId(12l);

        ClientBlock myCb13 = new ClientBlock();
        myCb13.setFrom(new ClientTime(685));
        myCb13.setTo(new ClientTime(810));
        myCb13.setSectionId(-1l);
        myCb13.setBlockId(13l);

        ClientBlock myCb14 = new ClientBlock();
        myCb14.setFrom(new ClientTime(810));
        myCb14.setTo(new ClientTime(960));
        myCb14.setSectionId(3l);
        myCb14.setBlockId(14l);

        ClientBlock myCb15 = new ClientBlock();
        myCb15.setFrom(new ClientTime(960));
        myCb15.setTo(new ClientTime(1020));
        myCb15.setSectionId(-1l);
        myCb15.setBlockId(15l);

        ClientRoom myRoom1 = new ClientRoom(1l, "myRoom d1");
        myRoom1.getBlocks().add(myCb1);
        myRoom1.getBlocks().add(myCb2);
        myRoom1.getBlocks().add(myCb3);
        myRoom1.getBlocks().add(myCb4);
        myRoom1.getBlocks().add(myCb5);

        ClientRoom myRoom2 = new ClientRoom(2l, "myRoom a2");
        myRoom2.getBlocks().add(myCb6);
        myRoom2.getBlocks().add(myCb7);
        myRoom2.getBlocks().add(myCb8);
        myRoom2.getBlocks().add(myCb9);
        myRoom2.getBlocks().add(myCb10);

        ClientRoom myRoom3 = new ClientRoom(3l, "myRoom c3");
        myRoom3.getBlocks().add(myCb11);
        myRoom3.getBlocks().add(myCb12);
        myRoom3.getBlocks().add(myCb13);
        myRoom3.getBlocks().add(myCb14);
        myRoom3.getBlocks().add(myCb15);

        myDay.getRooms().add(myRoom1);
        myDay.getRooms().add(myRoom2);
        myDay.getRooms().add(myRoom3);

        ClientMyEntityBlind myBlind1 = new ClientMyEntityBlind();
        myBlind1.setFrom(new ClientTime(480));
        myBlind1.setTo(new ClientTime(520));
        myBlind1.setId(3l);
        myBlind1.setName("Conference Opening");
        myBlind1.setFavourite(false);
        myBlind1.setNote("Moje poznámka k blindu");
        rooms = new ArrayList();
        rooms.add(room1.getId());
        rooms.add(room2.getId());
        rooms.add(room3.getId());
        myBlind1.setRoomIds(rooms);
        myCb1.getSubmissions().add(myBlind1);

        ClientMyEntityTalk myTalk1 = new ClientMyEntityTalk();
        myTalk1.setFrom(new ClientTime(550));
        myTalk1.setTo(new ClientTime(595));
        myTalk1.setId(1l);
        myTalk1.setName("Závěrečná zpráva z výzkumného projektu zaměřeného na psychodiagnostické metody „Substance Use Risk Profile Scale“ a „High School Personality Questionnaire“");
        myTalk1.setAuthor("Michal Kolínek");
        myTalk1.setNote("Moje poznámka k talku");
        myTalk1.setFavourite(true);
        rooms = new ArrayList();
        rooms.add(room1.getId());
        rooms.add(room3.getId());
        myTalk1.setRoomIds(rooms);
        myCb7.getSubmissions().add(myTalk1);

        MockFactory.addActionMock(LoadMyScheduleDayAction.class, new ActionMock<ClientScheduleDay>(myDay));
        //--------------------------
        MockFactory.addActionMock(UpdateFavouriteEntityAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //--------------------------
        MockFactory.addActionMock(UpdateEntityNoteAction.class, new ActionMock<VoidResult>(new VoidResult()));
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new EventCoreModule());
        modules.add(new SchedulingModule());
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        ArrayList<ClientTime> splits = new ArrayList<ClientTime>();
        splits.add(new ClientTime(800));
        splits.add(new ClientTime(950));

        ClientTime from = new ClientTime(480);
        ClientTime to = new ClientTime(1200);

        formMap.put("My Schedule", new MyScheduling());
        formMap.put("Scheduling", new Scheduling());
        formMap.put("Sections", new TalkOverview());
        formMap.put("Block splitter", new BlockSplitter(from, to, splits));
        formMap.put("Settings", new SchedulingSettings());
    }
}


