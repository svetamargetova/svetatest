package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSchedule;

/**
 * Akce pro inicialni nacteni planovani
 * @author pepa
 */
public class LoadScheduleAction extends EventAction<ClientSchedule> {

    private static final long serialVersionUID = -7857755162869667947L;
}
