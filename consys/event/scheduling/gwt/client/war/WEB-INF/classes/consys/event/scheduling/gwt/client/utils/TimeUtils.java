package consys.event.scheduling.gwt.client.utils;

import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.Comparator;

/**
 *
 * @author pepa
 */
public class TimeUtils {

    /** vraci komparator, ktery necha radit casy vzestupne */
    public static Comparator<ClientTime> ascendingComparator() {
        return new Comparator<ClientTime>() {

            @Override
            public int compare(ClientTime o1, ClientTime o2) {
                return o1.getTime() - o2.getTime();
            }
        };
    }

    /**
     * Pokusi se vytvorit ze zadaneho textu objekt ClientTime
     * @throws IllegalArgumentException pokud je chyba pri parsovani textu
     */
    public static ClientTime parser(String text) {
        text = text.trim();
        text = text.replace(" ", "");
        String[] parts = text.split(":");

        if (parts.length > 2 || parts.length < 1) {
            throw new IllegalArgumentException();
        }

        int time = 0;

        try {
            if (parts.length == 1) {
                int hours = Integer.parseInt(parts[0]);

                if (hours >= 0 && hours <= 24) {
                    time = hours * 60;
                } else {
                    throw new IllegalArgumentException();
                }
            } else {
                int hours = Integer.parseInt(parts[0]);
                int minutes = Integer.parseInt(parts[1]);

                if (hours == 24 && minutes != 0) {
                    throw new IllegalArgumentException();
                }
                if (hours >= 0 && hours <= 24) {
                    time = hours * 60;
                } else {
                    throw new IllegalArgumentException();
                }
                if (minutes >= 0 && minutes <= 59) {
                    time += minutes;
                } else {
                    throw new IllegalArgumentException();
                }
            }
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException();
        }

        return new ClientTime(time);
    }

    /** vraci absolutni hodnotu rozdilu mezi casy v minutach*/
    public static int diff(ClientTime t1, ClientTime t2) {
        if (t1.getTime() > t2.getTime()) {
            return t1.getTime() - t2.getTime();
        } else {
            return t2.getTime() - t1.getTime();
        }
    }

    public static String fromTo(ClientTime from, ClientTime to) {
        return from.toString() + FormUtils.NDASH + to.toString();
    }
}
