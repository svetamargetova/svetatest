package consys.event.scheduling.gwt.client.module.schedule.item;

import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.event.scheduling.gwt.client.bo.ClientEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientMyEntityTalk;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntity;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntityTalk;
import consys.event.scheduling.gwt.client.module.schedule.item.behaviour.BlindBehaviour;
import consys.event.scheduling.gwt.client.module.schedule.item.behaviour.MyBlindBehaviour;
import consys.event.scheduling.gwt.client.module.schedule.item.behaviour.MyTalkBehaviour;
import consys.event.scheduling.gwt.client.module.schedule.item.behaviour.TalkBehaviour;
import java.util.HashMap;

/**
 * Podle zadanych dat vytvari polozky pro planovani
 * @author pepa
 */
public class UIEntityFactory {

    // data
    private HashMap<String, PresenterFactory> factoryItems;
    private HashMap<String, PresenterBufferFactory> factoryBufferItems;

    public UIEntityFactory() {
        TalkBehaviour talkB = new TalkBehaviour();

        factoryItems = new HashMap<String, PresenterFactory>();
        factoryItems.put(ClientEntityTalk.class.getName(), talkB);
        factoryItems.put(ClientEntityBlind.class.getName(), new BlindBehaviour());
        factoryItems.put(ClientMyEntityTalk.class.getName(), new MyTalkBehaviour());
        factoryItems.put(ClientMyEntityBlind.class.getName(), new MyBlindBehaviour());

        factoryBufferItems = new HashMap<String, PresenterBufferFactory>();
        factoryBufferItems.put(ClientUnassignedEntityTalk.class.getName(), talkB);
    }

    /**
     * vraci objekt vytvoreny na zaklade zadanych dat
     * @return EntityMain objekt pokud zna typ atributu jinak null
     */
    public <S extends ClientEntity> EntityMain item(S s) {
        PresenterFactory pf = factoryItems.get(s.getClass().getName());
        if (pf != null) {
            return pf.create(s);
        }
        return null;
    }

    /**
     * vraci objekt vytvoreny na zaklade zadanych dat
     * @return Entity objekt pokud zna typ atributu jinak null
     */
    public <B extends ClientUnassignedEntity> EntityMain bufferItem(B b) {
        PresenterBufferFactory pbf = factoryBufferItems.get(b.getClass().getName());
        if (pbf != null) {
            return pbf.bufferCreate(b);
        }
        return null;
    }

    /** interface pro generovani polozek rozvrhu z dodanych dat */
    public interface PresenterFactory<S extends ClientEntity> {

        public EntityMain create(S s);
    }

    /** interface pro generovani polozek bufferu z dodanych dat */
    public interface PresenterBufferFactory<S extends ClientUnassignedEntity> {

        public EntityMain bufferCreate(S s);
    }
}
