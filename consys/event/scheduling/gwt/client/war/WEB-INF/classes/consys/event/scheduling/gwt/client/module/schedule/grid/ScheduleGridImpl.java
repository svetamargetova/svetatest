package consys.event.scheduling.gwt.client.module.schedule.grid;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import java.util.ArrayList;

/**
 * Obecna implementace mrizky rozvrhu
 * @author pepa
 */
public abstract class ScheduleGridImpl extends FlowPanel {

    // konstanty
    public static final int CORNER_W = 47;
    public static final int CORNER_H = 41;
    public static final int ROOM_W = 129;
    public static final int BLOCK_HEAD_H = 20;
    // data
    private int scale;

    public ScheduleGridImpl() {
        scale = ScheduleData.get().getScale();
        addStyleName(ESResourceUtils.bundle().css().gridContent());
    }

    /** aktualizuje rozmery podle meritka */
    public void refreshScale() {
        scale = ScheduleData.get().getScale();
    }

    /** pregeneruje mrizku podle zadaneho planu dne */
    public void setDay(ClientScheduleDay scheduleDay) {
        clear();

        int rooms = scheduleDay.getRooms().size();
        setWidth((getScheduleWidth(ROOM_W, rooms, scale) + (CORNER_W * scale) + 3) + "px");

        // zahlavi
        add(getHeaderLeft());
        for (ClientRoom cr : scheduleDay.getRooms()) {
            add(getHeader(cr.getName()));
        }

        // pod bloky
        if (!scheduleDay.getRooms().isEmpty()) {
            ClientRoom cr = scheduleDay.getRooms().get(0);
            for (ClientBlock cb : cr.getBlocks()) {
                add(getBlockHeaderLeft());
                add(getBlockHeader(rooms));
                drawBlockTimeline(cb.getFrom(), cb.getTo(), rooms);
            }
        }

        add(getFoot(rooms));
    }

    /** vraci zahlavi rozvrhu v casovem sloupci */
    protected SimplePanel getHeaderLeft() {
        SimplePanel panel = new SimplePanel();
        panel.setSize((CORNER_W * scale) + "px", (CORNER_H * scale) + "px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridHeaderLeft());
        return panel;
    }

    /** vraci zahlavi rozvrhu */
    protected SimplePanel getHeader(String name) {
        Label label = new Label(name);
        label.addStyleName(ESResourceUtils.bundle().css().gridHeaderTitle());

        SimplePanel panel = new SimplePanel();
        panel.setSize((ROOM_W * scale) + "px", (CORNER_H * scale) + "px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridHeader());
        panel.setWidget(label);
        return panel;
    }

    /** vraci zahlavi bloku v casovem sloupci */
    protected SimplePanel getBlockHeaderLeft() {
        SimplePanel panel = new SimplePanel();
        panel.setSize((CORNER_W * scale) + "px", (BLOCK_HEAD_H * scale) + "px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridBlockHeaderLeft());
        return panel;
    }

    /** vraci zahlavi bloku v rozvrhu */
    protected SimplePanel getBlockHeader(int rooms) {
        SimplePanel panel = new SimplePanel();
        panel.setSize(getScheduleWidth(ROOM_W, rooms, scale) + "px", (BLOCK_HEAD_H * scale) + "px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridBlockHeader());
        return panel;
    }

    /** vraci casovy usek bloku v casovem sloupci */
    protected FlowPanel getBlockLeft(int height, ClientTime from, ClientTime to) {
        FlowPanel panel = new FlowPanel();
        panel.setSize((CORNER_W * scale) + "px", (height * scale) + "px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridBlockLeft());

        ArrayList<ClientTime> times = new ArrayList<ClientTime>();
        times.add(from);

        int diff = from.getTime() % 30;
        int start = from.getTime() - diff + (diff > 10 ? 60 : 30);

        for (int i = start; i < to.getTime(); i += 30) {
            times.add(new ClientTime(i));
        }

        for (ClientTime f : times) {
            Label label = new Label(f.toString());
            label.addStyleName(ESResourceUtils.bundle().css().gridBlockLeftTitle());

            int top = f.getTime() - from.getTime();
            DOM.setStyleAttribute(label.getElement(), "top", (3 + top) + "px");

            panel.add(label);
        }
        return panel;
    }

    /** vraci casovy usek bloku v rozvrhu */
    protected SimplePanel getBlock(int rooms, int height, boolean halfHour) {
        SimplePanel panel = new SimplePanel();
        panel.setSize(getScheduleWidth(ROOM_W, rooms, scale) + "px", (height * scale) + "px");
        if (halfHour) {
            panel.addStyleName(ESResourceUtils.bundle().css().gridBlockHalfHour());
        } else {
            panel.addStyleName(ESResourceUtils.bundle().css().gridBlockFullHour());
        }
        return panel;
    }

    /** vraci zakonceni celeho rozvrhu */
    protected SimplePanel getFoot(int rooms) {
        SimplePanel panel = new SimplePanel();
        panel.setSize((getScheduleWidth(ROOM_W, rooms, scale) + (CORNER_W * scale) + 2) + "px", "2px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridFoot());
        return panel;
    }

    /** vypocita sirku pres vsechny mistnosti */
    protected int getScheduleWidth(int roomWidth, int rooms, int scale) {
        int width = roomWidth * rooms * scale;
        width += rooms == 0 ? 0 : rooms;
        return width;
    }

    /** vykresli casovou osu bloku pro zadane casy */
    protected void drawBlockTimeline(ClientTime from, ClientTime to, int rooms) {
        int length = to.getTime() - from.getTime();
        add(getBlockLeft(length - 1, from, to));

        int time = from.getTime();
        boolean halfHour = false;
        for (Integer i : halfHourParts(from, to)) {
            add(getBlock(rooms, i - 1, halfHour));
            time += i;
            halfHour = time % 60 != 0;
        }
    }

    /** vrati delky rozdeleni po pulhodinach ze zadaneho casoveho intervalu */
    protected ArrayList<Integer> halfHourParts(ClientTime from, ClientTime to) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        int time = from.getTime();

        if (time % 30 == 0) {
            // zacina 0 nebo 30 minut
            if (time + 30 < to.getTime()) {
                // interval delsi nez pul hodiny
                result.add(30);
                time += 30;
            } else {
                // interval kratsi nez pul hodiny
                result.add(to.getTime() - time);
                return result;
            }
        } else {
            // zacina cokliv nez 0 a 30 minut
            if (time + 30 < to.getTime()) {
                // interval delsi nez pul hodiny
                if (from.getMinutes() < 30) {
                    // do pul hodiny
                    int minutes = time % 30;
                    result.add(30 - minutes);
                    time += 30 - minutes;
                } else {
                    // od pul hodiny
                    int minutes = from.getMinutes();
                    result.add(60 - minutes);
                    time += 60 - minutes;
                }
            } else {
                // interval kratsi nez pul hodiny
                if (from.getHours() == to.getHours()) {
                    // v jedne hodine
                    int minutes = to.getMinutes() - from.getMinutes();
                    int fMinutes = from.getMinutes();
                    if (fMinutes >= 30 || fMinutes + minutes <= 30) {
                        result.add(minutes);
                    } else {
                        // predel pul hodiny
                        result.add(30 - from.getMinutes());
                        result.add(to.getMinutes() - 30);
                    }
                } else {
                    // v predelu hodin
                    result.add(60 - from.getMinutes());
                    int minutes = to.getMinutes();
                    if (minutes != 0) {
                        result.add(minutes);
                    }
                }
                return result;
            }
        }

        while (time + 30 < to.getTime()) {
            time += 30;
            result.add(30);
        }
        result.add(to.getTime() - time);

        return result;
    }
}
