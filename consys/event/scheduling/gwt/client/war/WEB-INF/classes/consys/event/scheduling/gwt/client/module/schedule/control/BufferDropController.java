package consys.event.scheduling.gwt.client.module.schedule.control;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.drop.AbstractPositioningDropController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.action.UpdateEntityPositionAction;
import consys.event.scheduling.gwt.client.module.schedule.Scheduling;
import consys.event.scheduling.gwt.client.module.schedule.item.DrawProvider;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityTalk;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class BufferDropController extends AbstractPositioningDropController {

    // data
    private HashMap<String, BufferBehavior> bufferItemsBehavior;
    private Draggable draggable;

    public BufferDropController() {
        super(ScheduleData.get().buffer());

        bufferItemsBehavior = new HashMap<String, BufferBehavior>();
        bufferItemsBehavior.put(EntityTalk.class.getName(), new TalkBufferBehavior());
        bufferItemsBehavior.put(EntityBlind.class.getName(), new BlindBufferBehavior());
    }

    @Override
    public void onDrop(DragContext context) {
        BufferBehavior bb = bufferItemsBehavior.get(draggable.entity.getClass().getName());
        if (bb != null) {
            bb.bufferAction(draggable.entity);
        } else {
            GWT.log("Unknown behavior for entity " + draggable.entity.getClass().getName());
        }
        super.onDrop(context);
    }

    @Override
    public void onEnter(DragContext context) {
        super.onEnter(context);

        int draggableAbsoluteLeft = context.draggable.getAbsoluteLeft();
        int draggableAbsoluteTop = context.draggable.getAbsoluteTop();
        Entity entity = (Entity) context.selectedWidgets.get(0);

        draggable = new Draggable(entity);
        draggable.relativeX = entity.getAbsoluteLeft() - draggableAbsoluteLeft;
        draggable.relativeY = entity.getAbsoluteTop() - draggableAbsoluteTop;
    }

    /** interface pro akce chovani entit v bufferu */
    private interface BufferBehavior<W extends Entity> {

        public void bufferAction(W entity);
    }

    /** talk */
    private class TalkBufferBehavior implements BufferBehavior<EntityTalk> {

        @Override
        public void bufferAction(final EntityTalk entity) {
            GWT.log("Behavior for entity EntityTalk");
            entity.addStyleName(StyleUtils.MARGIN_HOR_3);
            entity.addStyleName(StyleUtils.MARGIN_VER_3);
            ScheduleData.get().buffer().addToMatrix(entity);

            entity.showBufferLook(true);

            ScheduleDragController.get().makeNotDraggable(entity);
            EventBus.get().fireEvent(new EventDispatchEvent(
                    new UpdateEntityPositionAction(true, entity.getData().getId()),
                    new AsyncCallback<LongResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            Scheduling.hiddableLabel.show(ESMessageUtils.c.blockDropController_error_updatePositionFail());
                            DrawProvider.get().onOriginalPosition(entity);
                            entity.removeStyleName(StyleUtils.MARGIN_HOR_3);
                            entity.removeStyleName(StyleUtils.MARGIN_VER_3);
                            entity.showBufferLook(false);
                            ScheduleDragController.get().makeDraggable(entity, entity.getHandle());
                            entity.recalculatePup();
                        }

                        @Override
                        public void onSuccess(LongResult result) {
                            for (Entity s : (ArrayList<Entity>) entity.getSlaves()) {
                                s.removeFromParent();
                            }
                            entity.getSlaves().clear();
                            entity.getData().getRoomIds().clear();
                            ScheduleDragController.get().makeDraggable(entity, entity.getHandle());
                            entity.recalculatePup();
                            entity.setBlock(null);
                        }
                    }, null));
        }
    }

    /** blind */
    private class BlindBufferBehavior implements BufferBehavior<EntityBlind> {

        @Override
        public void bufferAction(final EntityBlind entity) {
            GWT.log("Behavior for entity EntityBlind");
            entity.addStyleName(StyleUtils.MARGIN_HOR_3);
            entity.addStyleName(StyleUtils.MARGIN_VER_3);

            entity.showBufferLook(true);

            if (entity.getData().getId() != null) {
                ScheduleDragController.get().makeNotDraggable(entity);
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateEntityPositionAction(true, entity.getData().getId()),
                        new AsyncCallback<LongResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                Scheduling.hiddableLabel.show(ESMessageUtils.c.blockDropController_error_updatePositionFail());
                                DrawProvider.get().onOriginalPosition(entity);
                                entity.removeStyleName(StyleUtils.MARGIN_HOR_3);
                                entity.removeStyleName(StyleUtils.MARGIN_VER_3);
                                entity.showBufferLook(false);
                                ScheduleDragController.get().makeDraggable(entity, entity.getHandle());
                                entity.recalculatePup();
                            }

                            @Override
                            public void onSuccess(LongResult result) {
                                for (Entity s : (ArrayList<Entity>) entity.getSlaves()) {
                                    s.removeFromParent();
                                }
                                entity.getSlaves().clear();
                                entity.getData().getRoomIds().clear();
                                entity.recalculatePup();
                                entity.setBlock(null);
                            }
                        }, null));
            }
        }
    }
}
