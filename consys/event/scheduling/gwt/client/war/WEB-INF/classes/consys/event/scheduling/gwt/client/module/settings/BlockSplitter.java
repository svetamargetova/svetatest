package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.settings.EditFormDaySettings.ConsysTimeTextBox;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Rozdelovani dne do bloku
 * @author pepa
 */
public class BlockSplitter extends SimpleFormPanel {

    // komponenty
    private FlowPanel graphicsPanel;
    private AbsolutePanel prezenter;
    private FlowPanel itemsPanel;
    // data
    private ClientTime from;
    private ClientTime to;
    private ArrayList<ClientTime> splits;

    public BlockSplitter(ClientTime from, ClientTime to, ArrayList<ClientTime> splits) {
        super("500px");
        this.from = from;
        this.to = to;

        Collections.sort(splits, TimeUtils.ascendingComparator());
        this.splits = new ArrayList<ClientTime>(splits);

        graphicsPanel = new FlowPanel();
        graphicsPanel.setWidth("52px");
        graphicsPanel.addStyleName(FLOAT_LEFT);
        graphicsPanel.addStyleName(MARGIN_RIGHT_20);

        prezenter = new AbsolutePanel();
        prezenter.addStyleName(StyleUtils.BORDER_GRAY);
        prezenter.addStyleName(StyleUtils.OVERFLOW_HIDDEN);

        itemsPanel = new FlowPanel();
        itemsPanel.setWidth("200px");
        itemsPanel.addStyleName(FLOAT_LEFT);
        DOM.setStyleAttribute(itemsPanel.getElement(), "marginTop", "28px");
    }

    @Override
    protected void onLoad() {
        clear();
        refresh();

        final ConsysTimeTextBox tb = new ConsysTimeTextBox(ESMessageUtils.c.blockSplitter_text_splitInput(), "52px");
        tb.addStyleName(FLOAT_LEFT);
        tb.addStyleName(MARGIN_RIGHT_10);
        tb.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    if (!tb.doValidate(getFailMessage())) {
                        return;
                    }
                    addSplitItem(tb.getTime(), false);
                    tb.setText("");
                }
            }
        });

        ActionLabel addSplit = new ActionLabel(ESMessageUtils.c.blockSplitter_action_addSplit(), FLOAT_LEFT);
        addSplit.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!tb.doValidate(getFailMessage())) {
                    return;
                }
                addSplitItem(tb.getTime(), false);
                tb.setText("");
            }
        });

        FlowPanel addPanel = new FlowPanel();
        addPanel.add(tb);
        addPanel.add(addSplit);
        addPanel.add(StyleUtils.clearDiv());

        FlowPanel content = new FlowPanel();
        content.add(addPanel);
        content.add(graphicsPanel);
        content.add(itemsPanel);
        content.add(StyleUtils.clearDiv());
        addWidget(content);
    }

    /** cas zacatku dne */
    public void setFromTime(ClientTime from) {
        this.from = from;
        refresh();
    }

    /** cas konce dne */
    public void setToTime(ClientTime to) {
        this.to = to;
        refresh();
    }

    /** vraci nastavene oddelovace */
    public ArrayList<ClientTime> getSplits() {
        return splits;
    }

    /** prekresli znazornovaci panel */
    private void refresh() {
        prezenter.clear();
        prezenter.setSize("50px", ((to.getTime() - from.getTime()) / 2) + "px");
        itemsPanel.clear();

        ArrayList<ClientTime> toRemove = new ArrayList<ClientTime>();
        for (ClientTime split : splits) {
            int time = split.getTime();
            if (time <= from.getTime() || time >= to.getTime()) {
                // mimo rozsah - bude se mazat
                toRemove.add(split);
            } else {
                addSplitItem(split, true);
            }
        }
        for (ClientTime split : toRemove) {
            splits.remove(split);
        }

        Label topLabel = StyleUtils.getStyledLabel(from.toString(), MARGIN_VER_5);
        topLabel.setHeight("18px");
        Label bottomLabel = StyleUtils.getStyledLabel(to.toString(), MARGIN_VER_5);
        bottomLabel.setHeight("18px");

        DOM.setStyleAttribute(topLabel.getElement(), "textAlign", "center");
        DOM.setStyleAttribute(bottomLabel.getElement(), "textAlign", "center");

        graphicsPanel.clear();
        graphicsPanel.add(topLabel);
        graphicsPanel.add(prezenter);
        graphicsPanel.add(bottomLabel);
    }

    /** prida polozku do znazorneni */
    private void addSplitItem(ClientTime time, boolean init) {
        if (!init) {
            if (splits.contains(time)) {
                getFailMessage().setText(ESMessageUtils.m.blockSplitter_error_alreadyInserted(time.toString()));
                return;
            } else {
                int split = time.getTime();
                if (split <= from.getTime() || split >= to.getTime()) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInRange(ESMessageUtils.c.blockSplitter_text_splitInput()));
                    return;
                }
                splits.add(time);
            }
        }

        int top = time.getTime() - from.getTime();
        BlockSplitterItem item = new BlockSplitterItem();
        prezenter.add(item, 0, (top) / 2);

        BlockSplitterControlItem citem = new BlockSplitterControlItem(time, item);
        if (itemsPanel.getWidgetCount() > 0) {
            // razeni podle casu
            boolean between = false;
            for (int i = 0; i < itemsPanel.getWidgetCount(); i++) {
                Widget w = itemsPanel.getWidget(i);
                if (w instanceof BlockSplitterControlItem) {
                    BlockSplitterControlItem bsci = (BlockSplitterControlItem) w;
                    if (bsci.getTime().getTime() > time.getTime()) {
                        itemsPanel.insert(citem, i);
                        between = true;
                        break;
                    }
                }
            }
            if (!between) {
                itemsPanel.add(citem);
            }
        } else {
            itemsPanel.add(citem);
        }
    }

    /** jedna graficka polozka BlockSplitteru */
    private class BlockSplitterItem extends SimplePanel {

        public BlockSplitterItem() {
            addStyleName(StyleUtils.OVERFLOW_HIDDEN);
            setSize("50px", "1px");
            DOM.setStyleAttribute(getElement(), "background", "red");
        }
    }

    /** jedna ovladaci polozka BlockSplitteru */
    private class BlockSplitterControlItem extends FlowPanel {

        // data
        private ClientTime time;

        public BlockSplitterControlItem(final ClientTime time, final BlockSplitterItem item) {
            this.time = time;
            addStyleName(MARGIN_BOT_5);

            Label timeLabel = StyleUtils.getStyledLabel(time.toString(), FLOAT_LEFT, ALIGN_RIGHT, MARGIN_RIGHT_10);
            timeLabel.setWidth("50px");
            add(timeLabel);

            Remover remover = Remover.commonRemover(new ConsysAction() {

                @Override
                public void run() {
                    splits.remove(time);
                    item.removeFromParent();
                    BlockSplitterControlItem.this.removeFromParent();
                }
            });
            remover.addStyleName(StyleUtils.FLOAT_LEFT);
            add(remover);

            add(StyleUtils.clearDiv());
        }

        /** vraci cas polozky */
        public ClientTime getTime() {
            return time;
        }
    }
}
