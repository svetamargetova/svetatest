package consys.event.scheduling.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientMySchedule implements Result {

    private static final long serialVersionUID = 6982111055438369376L;
    // data
    private ArrayList<ClientDayThumb> frames;
    private ArrayList<ClientSection> sections;

    public ClientMySchedule() {
        frames = new ArrayList<ClientDayThumb>();
        sections = new ArrayList<ClientSection>();
    }

    public ArrayList<ClientDayThumb> getFrames() {
        return frames;
    }

    public void setFrames(ArrayList<ClientDayThumb> frames) {
        this.frames = frames;
    }

    public ArrayList<ClientSection> getSections() {
        return sections;
    }

    public void setSections(ArrayList<ClientSection> sections) {
        this.sections = sections;
    }
}
