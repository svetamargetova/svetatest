package consys.event.scheduling.gwt.client.module.schedule.content;

/**
 *
 * @author pepa
 */
public class ScheduleBufferImplStd extends ScheduleBufferImpl {

    @Override
    protected void setSize() {
        setSize((ScheduleDnDContentImpl.WIDTH - 2) + "px", (HEIGHT - 2) + "px");
    }
}
