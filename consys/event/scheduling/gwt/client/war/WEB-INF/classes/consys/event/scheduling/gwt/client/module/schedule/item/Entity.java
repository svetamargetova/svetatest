package consys.event.scheduling.gwt.client.module.schedule.item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;

/**
 * Zaklad vsech vkladatelnych polozek planovani
 * @author pepa
 */
public class Entity<T extends ClientEntity> extends FocusPanel {

    // konstanty
    protected final static int CHANGED_FROM = 1;
    protected final static int CHANGED_LENGTH = 2;
    private final static UIEntityConfig config = GWT.create(UIEntityConfig.class);
    // komponenty
    protected Widget clipBoard;
    // data
    private T data;
    /** globalni cas zacatku polozky */
    private ClientTime from;
    /** delka v minutach */
    private int length;
    /** rodicovsky blok, pokud je null neni umisteny v rozvrhu */
    private ScheduleBlock block;
    /** id mistnosti */
    private Long roomId;
    /** sekce ke ktere entita patri */
    private ClientSection section;

    public Entity() {
        addStyleName(ESResourceUtils.bundle().css().scheduleItem());
    }

    /** vytvori vlastni obsah polozky, barva pozadi se nastavuje podle sekce, pokud je null, nastavi se bila */
    public void createSelf(ClientSection section) {
        setSectionColor(section);
    }

    /** cekani na odpoved ze serveru */
    public void showPendingState(boolean pending) {
        if (pending) {
            if (clipBoard == null) {
                clipBoard = getWidget();
            }
            setWidget(StyleUtils.getStyledLabel(ESMessageUtils.c.entity_text_pending(),
                    StyleUtils.MARGIN_LEFT_3, StyleUtils.MARGIN_TOP_3));
        } else {
            setWidget(clipBoard);
            clipBoard = null;
        }
    }

    /** vraci data polozky */
    public T getData() {
        return data;
    }

    /** nastavi data */
    public void setData(T data) {
        this.data = data;
        from = data.getFrom();
        length = TimeUtils.diff(data.getFrom(), data.getTo());
        setItemSize();
    }

    /** globalni cas zacatku polozky */
    public ClientTime getFrom() {
        return from;
    }

    /** nastavuje globalni cas zacatku polozky */
    public void setFrom(ClientTime from) {
        this.from = from;
        data.setFrom(from);
        data.setTo(new ClientTime(from.getTime() + length));
        dataChanged(CHANGED_FROM);
    }

    /** delka v minutach */
    public int getLength() {
        return length;
    }

    /** nastavuje delku v minutach */
    public void setLength(int length) {
        this.length = length;
        data.setTo(new ClientTime(from.getTime() + length));
        config.setEntitySize(this);
        dataChanged(CHANGED_LENGTH);
    }

    /** rodicovsky blok, pokud je null neni umisteny v rozvrhu */
    public ScheduleBlock getBlock() {
        return block;
    }

    /** rodicovsky blok, pokud je null neni umisteny v rozvrhu */
    public void setBlock(ScheduleBlock block) {
        this.block = block;
        if (block != null) {
            roomId = block.getRoomId();
        } else {
            roomId = null;
        }
    }

    /** vraci id mistnosti ve ktere je / ma byt polozka umistena */
    public Long getRoomId() {
        return roomId;
    }

    /** nastavuje id mistnosti ve ktere je polozka umistena */
    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    /** vraci sekci prirazenou entite */
    public ClientSection getSection() {
        return section;
    }

    /** nastavuje sekci prirazenou entite */
    public void setSection(ClientSection section) {
        this.section = section;
        setSectionColor(section);
    }

    /** nastavi barvu entity podle zadane sekce */
    private void setSectionColor(ClientSection section) {
        DOM.setStyleAttribute(getElement(), "backgroundColor", getColor(section));
    }

    /** nastavi velikost polozky */
    protected void setItemSize() {
        setItemSize(false);
    }

    /** nastavi velikost polozky, pokud buffer true nastavi se velikost pro buffer */
    protected void setItemSize(boolean buffer) {
        if (buffer) {
            config.setBufferEntitySize(this);
        } else {
            config.setEntitySize(this);
        }
    }

    /** vraci barvu pozadi polozky */
    protected String getColor(ClientSection section) {
        if (section != null) {
            if (section.getFgColor() == null) {
                return "#eeeeee";
            } else {
                return "#" + section.getFgColor();
            }
        }
        return "#eeeeee";
    }

    /** vraci barvu textu polozky */
    protected String getFontColor(ClientSection section) {
        if (section != null) {
            if (section.getFontColor() == null) {
                return "#555555";
            } else {
                return "#" + section.getFontColor();
            }
        }
        return "#555555";
    }

    /** nastavi barvu textu podle nastavené sekce v zadanem widgetu */
    protected void setWidgetTextColor(Widget w) {
        DOM.setStyleAttribute(w.getElement(), "color", getFontColor(section));
    }

    /** interni oznameni o zmene dat (state je hodnota z konstant Entity), urceno k pretizeni */
    protected void dataChanged(int state) {
    }
}
