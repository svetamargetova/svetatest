package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSection;

/**
 * Akce pro nacteni sekce
 * @author pepa
 */
public class LoadSectionAction extends EventAction<ClientSection> {

    private static final long serialVersionUID = -5493063099679680834L;
    // data
    private Long id;

    public LoadSectionAction() {
    }

    public LoadSectionAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
