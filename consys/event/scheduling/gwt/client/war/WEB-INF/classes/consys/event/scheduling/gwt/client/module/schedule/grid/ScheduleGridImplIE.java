package consys.event.scheduling.gwt.client.module.schedule.grid;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Implementace mrizky rozvrhu pro IE prohlizece
 * @author pepa
 */
public class ScheduleGridImplIE extends ScheduleGridImpl {

    @Override
    protected SimplePanel getFoot(int rooms) {
        SimplePanel panel = new SimplePanel();
        int scale = ScheduleData.get().getScale();
        panel.setSize((getScheduleWidth(ROOM_W, rooms, scale) + (CORNER_W * scale)) + "px", "2px");
        panel.addStyleName(ESResourceUtils.bundle().css().gridFoot());
        return panel;
    }

    @Override
    protected int getScheduleWidth(int roomWidth, int rooms, int scale) {
        return roomWidth * rooms * scale;
    }

    @Override
    protected void drawBlockTimeline(ClientTime from, ClientTime to, int rooms) {
        int length = to.getTime() - from.getTime();
        add(getBlockLeft(length, from, to));

        int time = from.getTime();
        boolean halfHour = false;
        for (Integer i : halfHourParts(from, to)) {
            add(getBlock(rooms, i, halfHour));
            time += i;
            halfHour = time % 60 != 0;
        }
    }
}
