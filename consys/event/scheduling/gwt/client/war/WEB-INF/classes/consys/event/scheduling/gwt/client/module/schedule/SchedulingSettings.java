package consys.event.scheduling.gwt.client.module.schedule;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.scheduling.gwt.client.module.settings.RoomsSetting;
import consys.event.scheduling.gwt.client.module.settings.SectionsSetting;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;

/**
 *
 * @author pepa
 */
public class SchedulingSettings extends RootPanel {

    public SchedulingSettings() {
        super(ESMessageUtils.c.schedulingSettings_title());

        ActionImage sections = ActionImage.getPlusButton(ESMessageUtils.c.sectionsSetting_title(), true);
        sections.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireNextBreadcrumb(ESMessageUtils.c.sectionsSetting_title(), new SectionsSetting());
            }
        });
        addLeftControll(sections);

        ActionImage rooms = ActionImage.getPlusButton(ESMessageUtils.c.roomsSetting_title(), true);
        rooms.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireNextBreadcrumb(ESMessageUtils.c.roomsSetting_title(), new RoomsSetting());
            }
        });
        addLeftControll(rooms);

        addWidget(new consys.event.scheduling.gwt.client.module.settings.SchedulingSettings());
    }
}
