package consys.event.scheduling.gwt.client.module.schedule.content;

/**
 * Implementace dnd obsahu pro IE prohlizece
 * @author pepa
 */
public class ScheduleDnDContentImplIE extends ScheduleDnDContentImpl {

    @Override
    protected ScheduleDnDContentConst getConst() {
        return new ScheduleDnDContentConst(0, 0, 0, 0);
    }
}
