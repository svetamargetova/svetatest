package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientSectionOverview;

/**
 * Nacte prehled pro jednu sekci
 * @author pepa
 */
public class LoadTalkOverviewAction extends EventAction<ClientSectionOverview> {

    private static final long serialVersionUID = 4416456388352934697L;
    // data
    private Long sectionId;

    public LoadTalkOverviewAction() {
    }

    public LoadTalkOverviewAction(Long sectionId) {
        this.sectionId = sectionId;
    }

    /** id sekce k nacteni nahledu */
    public Long getSectionId() {
        return sectionId;
    }
}
