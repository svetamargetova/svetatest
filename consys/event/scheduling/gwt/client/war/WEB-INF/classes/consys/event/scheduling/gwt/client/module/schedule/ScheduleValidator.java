package consys.event.scheduling.gwt.client.module.schedule;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityBlind;
import consys.event.scheduling.gwt.client.module.schedule.item.EntityMain;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;

/**
 * Zakladni validatory
 * @author pepa
 */
public class ScheduleValidator {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(ScheduleValidator.class);

    /**
     * overi jestli muze blok obsahovat entitu
     * @return true pokud muze
     */
    public static boolean canContainEntity(ScheduleBlock block, Entity masterEntity, Entity entity, ScheduleBlock dropBlock) {
        boolean result = true;

        // blind jde natáhnout do jakekoliv sekce i nesekce
        if (masterEntity instanceof EntityBlind) {
            if (entity == null) {
                // jde o hlavni cast
                return result;
            } else {
                // jde o plenarku
                if (block.getSection() == null && dropBlock.getSection() == null) {
                    // v poradku
                } else if (block.getSection() == null && dropBlock.getSection() != null) {
                    logger.debug("block section is null and entity section not");
                    result = false;
                } else if (block.getSection() == null || !block.getSection().equals(dropBlock.getSection())) {
                    logger.debug("block section is null or entity section not equal block section");
                    result = false;
                }
                return result;
            }
        }

        // jedna se o cokoliv co neni blindove a tady se kontroluji jak hlavni casti tak plenarky stejne
        if (entity == null) {
            entity = masterEntity;
        }

        if (block.getSection() == null && entity.getSection() == null) {
            // v poradku
        } else if (block.getSection() == null && entity.getSection() != null) {
            logger.debug("block section is null and entity section not");
            result = false;
        } else if (block.getSection() == null || !block.getSection().equals(entity.getSection())) {
            logger.debug("block section is null or entity section not equal block section");
            result = false;
        }
        return result;
    }

    /** overi jestli muze blok obsahovat zadanou entitu na pozici pozadovane pozici */
    public static boolean canContainEntityOnPosition(ScheduleBlockImpl block, Entity entity, ClientTime desiredTime) {
        int entityLength = entity.getLength();
        int blockLength = TimeUtils.diff(block.getFrom(), block.getTo());
        logger.debug("entity length: " + entityLength + ", block length: " + blockLength);

        if (entityLength > blockLength) {
            logger.debug("entity is bigger than block");
            return false;
        }

        for (int i = 0; i < block.getContent().getWidgetCount(); i++) {
            Widget w = block.getContent().getWidget(i);
            if (w instanceof Entity) {
                Entity item = (Entity) w;
                logger.debug("other entity from " + item.getFrom() + " with length " + item.getLength());
                // vyloucit z kontroly vlastni vedlejsi polozky
                if (isOwnSlave(entity, item)) {
                    logger.debug("--- own slave");
                    continue;
                }
                // kontrola na presah
                if (checkOverlap(desiredTime.getTime(), entityLength, item)) {
                    logger.debug("entity overlap");
                    return false;
                }
            }
        }

        // kontrola vedlejsich bloku pokud ma vedlejsi entity
        if (checkSlaveOverlap(desiredTime.getTime(), entity, block)) {
            logger.debug("entity slave overlap or bad block section");
            return false;
        }

        return true;
    }

    /**
     * zjisti jestli je entita main vlastnikem entity slave (proste jestli je to jeji vlastni vycpavka)<br>
     * pokud neni prvni entita instanci EntityMain, vraci automaticky false
     */
    public static boolean isOwnSlave(Entity main, Entity slave) {
        if (main instanceof EntityMain) {
            EntityMain em = (EntityMain) main;
            for (int i = 0; i < em.getSlaves().size(); i++) {
                Entity s = (Entity) em.getSlaves().get(i);
                if (slave.getData().getId().equals(s.getData().getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /** zkontroluje jestli se na požadované pozici entity nepřesahují, pokud přesahuji vraci true */
    public static boolean checkOverlap(int startOne, int length, Entity entity) {
        int startTwo = entity.getFrom().getTime();
        int endOne = startOne + length;
        int endTwo = startTwo + entity.getLength();

        if (startOne == startTwo && endOne == endTwo) {
            // stejne pres sebe
            return true;
        }
        if (startOne >= startTwo && startOne < endTwo) {
            // prvni prekryva druhy
            return true;
        }
        if (startTwo >= startOne && startTwo < endOne) {
            // druhy prekryva prvni
            return true;
        }

        return false;
    }

    /** zkontroluje jestli se na požadované pozici vedlejší entity nepřesahují, pokud přesahuji vraci true */
    public static boolean checkSlaveOverlap(int desired, Entity entity, ScheduleBlockImpl dropBlock) {
        if (entity instanceof EntityMain) {
            EntityMain em = (EntityMain) entity;

            if (em.getSlaves().isEmpty()) {
                return false;
            }

            BlockPosition dropBlockPosition = dropBlock.getBlockPosition();
            // kontrola pokud je prohozena hlavni a vedlejsi entita
            if (entity.getBlock().getBlockPosition().getX() != dropBlockPosition.getX()) {
                logger.debug("--- swap main and slave");
                BlockPosition pos = new BlockPosition(entity.getBlock().getBlockPosition().getX(), dropBlockPosition.getY());
                ScheduleBlock sbi = ScheduleData.get().blocksUI().get(pos);
                if (!canContainEntity(sbi, entity, null, dropBlock)) {
                    return true;
                }
                if (blockItemsCollision(desired, entity, sbi)) {
                    return true;
                }
                if (em instanceof EntityBlind) {
                    // kontrola na shodu sekci v pripadu blindu
                    for (int ii = 0; ii < em.getSlaves().size(); ii++) {
                        Entity s = (Entity) em.getSlaves().get(ii);
                        BlockPosition posB = new BlockPosition(s.getBlock().getBlockPosition().getX(), dropBlockPosition.getY());
                        ScheduleBlock sbiB = ScheduleData.get().blocksUI().get(posB);
                        if (!canContainEntity(sbiB, em, s, sbi)) {
                            return true;
                        }
                    }
                }
            }
            // kontrola slavu
            for (int ii = 0; ii < em.getSlaves().size(); ii++) {
                Entity s = (Entity) em.getSlaves().get(ii);
                BlockPosition pos = new BlockPosition(s.getBlock().getBlockPosition().getX(), dropBlockPosition.getY());
                ScheduleBlock sbi = ScheduleData.get().blocksUI().get(pos);
                if (!canContainEntity(sbi, em, s, dropBlock)) {
                    return true;
                }
                if (blockItemsCollision(desired, s, sbi)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * kontroluje zda nekoliduji polozky v bloku s nove vkladanou
     * @param desired je cas v minutach na ktery se chce vlozit entita
     * @param entity vkladana entita
     * @param block blok, ktery se ma zkontrolovat
     */
    private static boolean blockItemsCollision(int desired, Entity entity, ScheduleBlock block) {
        for (int i = 0; i < block.getContent().getWidgetCount(); i++) {
            Widget w = block.getContent().getWidget(i);
            if (w instanceof Entity) {
                Entity item = (Entity) w;
                logger.debug("check entity from " + item.getFrom() + " with length " + item.getLength());
                // sama sebe ignorovat
                if (item.getData().getId().equals(entity.getData().getId())) {
                    logger.debug("--- self ignore");
                    continue;
                }
                // kontrola na presah
                if (checkOverlap(desired, entity.getLength(), item)) {
                    logger.debug("entity overlap collision");
                    return true;
                }
            }
        }
        return false;
    }

    /** overi jestli lze umistit entitu a jeji vedlejsi casti na zadane umisteni */
    public static boolean checkEditedEntity(Entity entity, ClientTime from, int newLength,
            ArrayList<Long> slaveRoomsId, ConsysMessage fail) {
        final boolean isBlind = entity instanceof EntityBlind;

        ClientEntity testData = new ClientEntity() {};
        testData.setFrom(from);
        testData.setTo(new ClientTime(from.getTime() + newLength));
        testData.setId(entity.getData().getId());
        testData.setRoomIds(entity.getData().getRoomIds());

        Entity test = new Entity();
        test.setData(testData);
        test.setSection(entity.getSection());
        test.setLength(newLength);

        // kontrola na delku
        if (newLength < 5 || newLength % 5 != 0 || from.getTime() % 5 != 0) {
            fail.setText(ESMessageUtils.c.scheduleValidator_error_badTimeValue());
            return false;
        }

        // kontrola na hlavni entitu
        if (blockItemsCollision(test.getFrom().getTime(), test, entity.getBlock())) {
            fail.setText(ESMessageUtils.c.scheduleValidator_error_entityCollision());
            return false;
        }

        final int y = entity.getBlock().getBlockPosition().getY();

        // kontrola na vedlejsi entity
        for (int i = 0; i < ScheduleData.get().getRoomsCount(); i++) {
            BlockPosition bp = new BlockPosition(i + 1, y);
            ScheduleBlock sbi = ScheduleData.get().blocksUI().get(bp);
            if (slaveRoomsId.contains(((ScheduleBlockImpl) sbi).getRoomId())) {
                if (blockItemsCollision(test.getFrom().getTime(), test, sbi)) {
                    fail.setText(ESMessageUtils.c.scheduleValidator_error_entityCollision());
                    return false;
                }
            }
        }

        // kontrola na sekce vedlejsich entit
        Long sectionId;
        if (isBlind) {
            sectionId = entity.getBlock().getSection() == null ? null : entity.getBlock().getSection().getId();
        } else {
            sectionId = entity.getSection() == null ? null : entity.getSection().getId();
        }
        for (int i = 0; i < ScheduleData.get().getRoomsCount(); i++) {
            BlockPosition bp = new BlockPosition(i + 1, y);
            ScheduleBlock sbi = ScheduleData.get().blocksUI().get(bp);
            if (sectionId == null) {
                if (slaveRoomsId.contains(sbi.getRoomId()) && sbi.getSection() != null) {
                    fail.setText(ESMessageUtils.c.scheduleValidator_error_sectionCollision());
                    return false;
                }
            } else {
                Long slaveSectionId = sbi.getSection() == null ? -1 : sbi.getSection().getId();
                if (slaveRoomsId.contains(sbi.getRoomId()) && !slaveSectionId.equals(sectionId)) {
                    fail.setText(ESMessageUtils.c.scheduleValidator_error_sectionCollision());
                    return false;
                }
            }
        }

        test = null;
        return true;
    }
}
