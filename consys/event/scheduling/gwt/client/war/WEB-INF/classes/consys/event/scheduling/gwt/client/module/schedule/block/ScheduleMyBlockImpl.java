package consys.event.scheduling.gwt.client.module.schedule.block;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientBlock;
import consys.event.scheduling.gwt.client.bo.ClientMyEntity;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.grid.ScheduleGridImpl;
import consys.event.scheduling.gwt.client.module.schedule.item.DrawProvider;
import consys.event.scheduling.gwt.client.module.schedule.item.Entity;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ScheduleMyBlockImpl extends FlowPanel implements ScheduleBlock {

    // komponenty
    private AbsolutePanel content;
    private Label chairmanLabel;
    private BlockBackground background;
    // data
    private ClientBlock block;
    private BlockPosition position;
    private ClientSection section;
    private int totalHeight;
    private Long roomId;
    private int scale;

    public ScheduleMyBlockImpl() {
        addStyleName(ESResourceUtils.bundle().css().blockContent());
    }

    /** nastavi meritko podle ktereho se vypocitava */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /** vytvori ui bloku */
    private void createBlock() {
        clear();

        FlowPanel headPanel = new FlowPanel();
        headPanel.setHeight(getHeadHeight());
        headPanel.add(chairmanLabel);
        headPanel.add(StyleUtils.clearDiv());
        add(headPanel);

        content = getContentPanel();
        add(content);

        refreshBackground();
    }

    /** vraci vysku zahlavi */
    protected String getHeadHeight() {
        return (ScheduleGridImpl.BLOCK_HEAD_H * scale) + "px";
    }

    /** vraci novy panel obsahu s nastavenou velikosti */
    protected AbsolutePanel getContentPanel() {
        AbsolutePanel panel = new AbsolutePanel();
        panel.setSize((ScheduleGridImpl.ROOM_W * scale) + "px", (TimeUtils.diff(getFrom(), getTo()) * scale) + "px");
        return panel;
    }

    @Override
    public ClientBlock getBlock() {
        return block;
    }

    @Override
    public BlockPosition getBlockPosition() {
        return position;
    }

    /** nastavi blok */
    public void setBlockOnPosition(ClientBlock block, BlockPosition position, Long roomId) {
        this.block = block;
        this.position = position;
        this.roomId = roomId;

        section = ScheduleData.get().sections().get(block.getSectionId());

        totalHeight = (TimeUtils.diff(block.getFrom(), block.getTo()) * scale) + (ScheduleGridImpl.BLOCK_HEAD_H * scale);
        setSize((ScheduleGridImpl.ROOM_W * scale) + "px", totalHeight + "px");

        chairmanLabel = StyleUtils.getStyledLabel(ScheduleData.get().chairmans().get(block.getChairUuid()),
                ESResourceUtils.bundle().css().chairmanLabel());
        chairmanLabel.setSize(((ScheduleGridImpl.ROOM_W * scale) - 24) + "px", ((ScheduleGridImpl.BLOCK_HEAD_H * scale) - 5) + "px");

        createBlock();
    }

    /** nastavi entitu na pozici v bloku */
    public void setEntityOnPosition(Entity entity, ClientTime entityTime) {
        content.add(entity, 0, (entityTime.getTime() - block.getFrom().getTime()) * scale);
        entity.setFrom(entityTime);
    }

    /** aktualizuje pozadi, velikost, barvu a pozici */
    private void refreshBackground() {
        BlockPosition bgPosition = ScheduleData.get().bgPosition().get(position);
        if (background == null) {
            background = new BlockBackground(position, totalHeight);
            ScheduleData.get().getBackgrounds().add(background, bgPosition.getX(), bgPosition.getY());
        } else {
            background.refreshByPosition(position, totalHeight);
            ScheduleData.get().getBackgrounds().setWidgetPosition(background, bgPosition.getX(), bgPosition.getY());
        }
    }

    @Override
    public AbsolutePanel getContent() {
        return content;
    }

    @Override
    public ClientTime getFrom() {
        return block.getFrom();
    }

    @Override
    public ClientTime getTo() {
        return block.getTo();
    }

    @Override
    public Long getRoomId() {
        return roomId;
    }

    @Override
    public ClientSection getSection() {
        return section;
    }

    /** vraci celkovou vysku bloku, ale nejdriv musi byt nastaveny data bloku, jinak nebude odpovidat spravne */
    public int getTotalHeight() {
        return totalHeight;
    }

    /** vykresli polozky bloku */
    public void generateItems() {
        for (ClientMyEntity s : (ArrayList<ClientMyEntity>) block.getSubmissions()) {
            DrawProvider.get().drawMyInContent(s, this);
        }
    }
}
