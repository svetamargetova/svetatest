package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * UI jednoho autorizovaneho uzivatele pri nastavovani nebo prehledu sekce
 * @author pepa
 */
public class EditFormSectionsSettingUserUI extends FlowPanel {

    // data
    private String uuid;

    public EditFormSectionsSettingUserUI(String uuid, String name, boolean removable) {
        this.uuid = uuid;

        if (removable) {
            setStyleName(StyleUtils.MARGIN_TOP_5);

            Image remove = new Image(ResourceUtils.system().removeCross());
            remove.setStyleName(StyleUtils.HAND);
            remove.addStyleName(StyleUtils.MARGIN_LEFT_10);
            remove.addStyleName(StyleUtils.FLOAT_RIGHT);
            remove.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    removeFromParent();
                }
            });
            add(remove);
        }

        Label nameLabel = StyleUtils.getStyledLabel(name, StyleUtils.FONT_BOLD);
        nameLabel.setSize("240px", "14px");
        add(nameLabel);
    }

    public String getUUID() {
        return uuid;
    }
}
