package consys.event.scheduling.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.event.scheduling.gwt.client.utils.css.ESBundle;

/**
 *
 * @author pepa
 */
public class ESResourceUtils {

    private static ESBundle bundle;

    static {
        bundle = (ESBundle) GWT.create(ESBundle.class);
        bundle.css().ensureInjected();
    }

    public static ESBundle bundle() {
        return bundle;
    }

    /** separator do dialogu, aby nebyly presahy do cerneho okraje */
    public static SimplePanel dialogSeparator10px() {
        SimplePanel panel = new SimplePanel();
        panel.setSize("10px", "10px");
        return panel;
    }
}
