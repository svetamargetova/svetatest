package consys.event.scheduling.gwt.client.module.schedule.content;

import com.google.gwt.user.client.ui.AbsolutePanel;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.utils.ESResourceUtils;

/**
 * Vrstva pro bloky
 * @author pepa
 */
public class ScheduleBlockPanel extends AbsolutePanel {

    // data
    private int defaultWidth;
    private int defaultHeight;

    public ScheduleBlockPanel(int width, int height) {
        addStyleName(ESResourceUtils.bundle().css().scheduleBlocks());
        setSize(width + "px", height + "px");
        defaultWidth = width;
        defaultHeight = height;
    }

    public void refreshScale() {
        final int scale = ScheduleData.get().getScale();
        setSize(((defaultWidth * scale) + scale) + "px", (defaultHeight * scale) + "px");
    }

    public void setSize(int width, int height) {
        defaultWidth = width;
        defaultHeight = height;
        super.setSize(defaultWidth + "px", defaultHeight + "px");
    }
}
