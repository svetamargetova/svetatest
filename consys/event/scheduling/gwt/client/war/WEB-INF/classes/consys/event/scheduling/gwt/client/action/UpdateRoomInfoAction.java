package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;

/**
 * Aktualizace infa o mistnosti
 * @author pepa
 */
public class UpdateRoomInfoAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 4119385911101135798L;
    // data
    private ClientRoomInfo room;

    public UpdateRoomInfoAction() {
    }

    public UpdateRoomInfoAction(ClientRoomInfo room) {
        this.room = room;
    }

    public ClientRoomInfo getRoom() {
        return room;
    }
}
