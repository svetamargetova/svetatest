package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;

/**
 * Nacte rozvrh pro den
 * @author pepa
 */
public class LoadScheduleDayAction extends EventAction<ClientScheduleDay> {

    private static final long serialVersionUID = -3948777149738001904L;
    // data
    private Long id;

    public LoadScheduleDayAction() {
    }

    public LoadScheduleDayAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
