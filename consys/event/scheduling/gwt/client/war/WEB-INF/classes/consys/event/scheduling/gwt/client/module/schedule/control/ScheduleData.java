package consys.event.scheduling.gwt.client.module.schedule.control;

import consys.event.scheduling.gwt.client.bo.ClientRoom;
import consys.event.scheduling.gwt.client.bo.ClientSection;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleBackgrounds;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleBufferImpl;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleDnDContentImpl;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Aktualni data pro rozvrh
 * @author pepa
 */
public class ScheduleData {

    // instance
    private static ScheduleData instance = new ScheduleData();
    // data
    private HashMap<Long, ClientSection> sections;
    private HashMap<Long, String> sectionIdRights;
    private HashMap<BlockPosition, ScheduleBlock> blocksUI;
    private HashMap<BlockPosition, BlockPosition> bgPosition;
    private HashMap<String, String> chairmans;
    private HashMap<Long, ClientRoom> clientRooms;
    private ArrayList<ClientRoom> rooms;
    private int scale;
    private ScheduleBufferImpl buffer;
    private boolean admin;
    // data - komponenty
    private ScheduleBackgrounds backgrounds;
    private ScheduleDnDContentImpl dndContent;

    private ScheduleData() {
        sections = new HashMap<Long, ClientSection>();
        sectionIdRights = new HashMap<Long, String>();
        blocksUI = new HashMap<BlockPosition, ScheduleBlock>();
        bgPosition = new HashMap<BlockPosition, BlockPosition>();
        clientRooms = new HashMap<Long, ClientRoom>();

        chairmans = new HashMap<String, String>();
        chairmans.put(null, "");

        scale = 1;
        admin = false;
    }

    /** vraci instanci ScheduleData */
    public static ScheduleData get() {
        return instance;
    }

    /** vynuluje vsechna data */
    public void reset() {
        sections.clear();
        sectionIdRights.clear();
        blocksUI.clear();
        bgPosition.clear();
        chairmans.clear();
        clientRooms.clear();
        if (rooms != null) {
            rooms.clear();
        }
        buffer = null;
        backgrounds = null;
        dndContent = null;
        admin = false;
        scale = 1;
    }

    /** vraci objekt bufferu */
    public ScheduleBufferImpl buffer() {
        return buffer;
    }

    /** nastavi objekt bufferu pro zpristupneni z ostatnich komponent */
    public void setBuffer(ScheduleBufferImpl buffer) {
        this.buffer = buffer;
    }

    public void setSectionIdRights(ArrayList<Long> data) {
        sectionIdRights.clear();
        for (Long id : data) {
            sectionIdRights.put(id, "yes");
        }
    }

    public void setSections(ArrayList<ClientSection> data) {
        sections.clear();
        for (ClientSection section : data) {
            sections.put(section.getId(), section);
        }
    }

    /** mapa sekci podle jejich id */
    public HashMap<Long, ClientSection> sections() {
        return sections;
    }

    /** mapa opravneni k sekcim */
    public HashMap<Long, String> sectionIdRights() {
        return sectionIdRights;
    }

    /**
     * mapa ui bloku podle zadane pozice<br>
     * (prvni blok, v prvni mistnosti ma pozici x=1, y=1; druhy blok v prvni mistnosti ma pozici x=1, y=2)
     */
    public HashMap<BlockPosition, ScheduleBlock> blocksUI() {
        return blocksUI;
    }

    /**
     * mapa pozic pozadi podle pozice bloku<br>
     * (prvni blok, v prvni mistnosti ma pozici x=1, y=1; druhy blok v prvni mistnosti ma pozici x=1, y=2)
     */
    public HashMap<BlockPosition, BlockPosition> bgPosition() {
        return bgPosition;
    }

    /** mapa uuid, jmen predsedu bloku  */
    public HashMap<String, String> chairmans() {
        return chairmans;
    }

    public HashMap<Long, ClientRoom> clientRooms() {
        return clientRooms;
    }

    /** vrstva pozadi bloku */
    public ScheduleBackgrounds getBackgrounds() {
        return backgrounds;
    }

    /** vrstva pozadi bloku */
    public void setBackgrounds(ScheduleBackgrounds backgrounds) {
        this.backgrounds = backgrounds;
    }

    /** vraci pocet mistnosti v rozvrhu */
    public int getRoomsCount() {
        return rooms.size();
    }

    /** seznam mistnoosti */
    public ArrayList<ClientRoom> getRooms() {
        return rooms;
    }

    /** seznam mistnoosti */
    public void setRooms(ArrayList<ClientRoom> rooms) {
        this.rooms = rooms;
        for (ClientRoom cr : rooms) {
            clientRooms.put(cr.getId(), cr);
        }
    }

    public ScheduleDnDContentImpl getDndContent() {
        return dndContent;
    }

    public void setDndContent(ScheduleDnDContentImpl dndContent) {
        this.dndContent = dndContent;
    }

    /** meritko planovani - vychozi hodnota je 1 */
    public int getScale() {
        return scale;
    }

    /** meritko planovani */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /** vraci zda je aktualni uzivatel admin */
    public boolean isAdmin() {
        return admin;
    }

    /** nastavi ze aktualni uzivatel je/neni admin */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
