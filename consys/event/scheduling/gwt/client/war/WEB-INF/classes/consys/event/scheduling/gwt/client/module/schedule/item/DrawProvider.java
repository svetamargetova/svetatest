package consys.event.scheduling.gwt.client.module.schedule.item;

import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.scheduling.gwt.client.bo.ClientEntity;
import consys.event.scheduling.gwt.client.bo.ClientEntityBlind;
import consys.event.scheduling.gwt.client.bo.ClientMyEntity;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.bo.ClientUnassignedEntity;
import consys.event.scheduling.gwt.client.module.schedule.block.BlockPosition;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlock;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.block.ScheduleMyBlockImpl;
import consys.event.scheduling.gwt.client.module.schedule.content.ScheduleBufferImpl;
import consys.event.scheduling.gwt.client.module.schedule.control.OriginalPosition;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleData;
import consys.event.scheduling.gwt.client.module.schedule.control.ScheduleDragController;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;

/**
 * Zajistuje vykreslovani polozek
 * @author pepa
 */
public class DrawProvider {

    // instance
    private static DrawProvider instance = new DrawProvider();
    // data
    private UIEntityFactory itemFactory;

    private DrawProvider() {
        itemFactory = new UIEntityFactory();
    }

    /** vraci instanci DrawProvidera */
    public static DrawProvider get() {
        return instance;
    }

    /** vytvori zakladni entity blind v bufferu pro tahani do rozvrhu */
    public EntityBlind bufferEntityBlind() {
        ClientEntityBlind d = new ClientEntityBlind();
        d.setFrom(new ClientTime(0));
        d.setTo(new ClientTime(UIEntityConfig.BUFFER_ENTITY_LENGTH));
        d.setName(ESMessageUtils.c.drawProvider_text_newBlind());

        EntityBlind eb = (EntityBlind) itemFactory.item(d);
        eb.createSelf(null);
        eb.showBufferLook(true);
        eb.setNew(true);
        eb.addStyleName(StyleUtils.MARGIN_HOR_3);
        eb.addStyleName(StyleUtils.MARGIN_VER_3);
        ScheduleDragController.get().makeDraggable(eb, eb.getHandle());

        return eb;
    }

    /** zinicializuje buffer */
    public boolean initBuffer(ArrayList<ClientUnassignedEntity> data, ScheduleBufferImpl buffer) {
        if (data == null) {
            return false;
        }

        buffer.addToMatrix(bufferEntityBlind());

        for (ClientUnassignedEntity ue : data) {
            EntityMain item = itemFactory.bufferItem(ue);
            if (item == null) {
                return false;
            }
            item.addStyleName(StyleUtils.MARGIN_HOR_3);
            item.addStyleName(StyleUtils.MARGIN_VER_3);

            ScheduleDragController.get().makeDraggable(item, item.getHandle());

            buffer.addToMatrix(item);
        }

        return true;
    }

    /**
     * vykresli polozku do zadaneho panelu
     * @return true pokud vse probehlo v poradku, jinak false
     */
    public <S extends ClientEntity> boolean drawInContent(S s, ScheduleBlockImpl block) {
        EntityMain item = itemFactory.item(s);
        if (item == null) {
            return false;
        }
        item.setBlock(block);

        // moznost editace polozky
        Long sectionId = item.getBlock().getSection() == null ? -1l : item.getBlock().getSection().getId();
        if (ScheduleData.get().sectionIdRights().get(sectionId) != null || ScheduleData.get().isAdmin()) {
            ScheduleDragController.get().makeDraggable(item, item.getHandle());
        } else {
            item.showPen(false);
        }

        // vykresleni
        int top = TimeUtils.diff(block.getFrom(), item.getFrom()) * ScheduleData.get().getScale();
        if (item.getData().getRoomIds().size() > 0) {
            // plenarni prednaska
            ArrayList<Entity> slaves = new ArrayList<Entity>(item.getSlaves());
            BlockPosition bp = block.getBlockPosition();

            for (int i = 1; i <= ScheduleData.get().getRoomsCount(); i++) {
                if (i != bp.getX()) {
                    ScheduleBlockImpl b = (ScheduleBlockImpl) ScheduleData.get().blocksUI().get(new BlockPosition(i, bp.getY()));
                    if (b == null) {
                        // nemelo by nikdy nastat
                        return false;
                    }
                    Entity found = null;
                    for (Entity slave : slaves) {
                        if (b.getRoomId() == slave.getRoomId()) {
                            found = slave;
                            break;
                        }
                    }
                    if (found != null) {
                        if (item instanceof EntityBlind) {
                            // blind prednasky nemaji zadne sekce
                            found.createSelf(null);
                        } else {
                            found.createSelf(block.getSection());
                        }
                        found.setBlock(b);
                        b.getContent().add(found, 0, top);
                        slaves.remove(found);
                    }
                }
            }
        } else {
            // obycejna neplenarni prednaska, staci jen pridat do bloku
        }

        block.getContent().add(item, 0, top);

        return true;
    }

    /**
     * vykresli polozku do zadaneho panelu
     * @return true pokud vse probehlo v poradku, jinak false
     */
    public <S extends ClientMyEntity> boolean drawMyInContent(S s, ScheduleMyBlockImpl block) {
        EntityMain item = itemFactory.item(s);
        if (item == null) {
            return false;
        }
        item.setBlock(block);

        // vykresleni
        int top = TimeUtils.diff(block.getFrom(), item.getFrom()) * ScheduleData.get().getScale();
        if (item.getData().getRoomIds().size() > 0) {
            // plenarni prednaska
            ArrayList<Entity> slaves = new ArrayList<Entity>(item.getSlaves());
            BlockPosition bp = block.getBlockPosition();

            for (int i = 1; i <= ScheduleData.get().getRoomsCount(); i++) {
                if (i != bp.getX()) {
                    ScheduleMyBlockImpl b = (ScheduleMyBlockImpl) ScheduleData.get().blocksUI().get(new BlockPosition(i, bp.getY()));
                    if (b == null) {
                        // nemelo by nikdy nastat
                        return false;
                    }
                    Entity found = null;
                    for (Entity slave : slaves) {
                        if (b.getRoomId() == slave.getRoomId()) {
                            found = slave;
                            break;
                        }
                    }
                    if (found != null) {
                        if (item instanceof EntityBlind) {
                            // blind prednasky nemaji zadne sekce
                            found.createSelf(null);
                        } else {
                            found.createSelf(block.getSection());
                        }
                        found.setBlock(b);
                        b.getContent().add(found, 0, top);
                        slaves.remove(found);
                    }
                }
            }
        } else {
            // obycejna neplenarni prednaska, staci jen pridat do bloku
        }

        block.getContent().add(item, 0, top);

        return true;
    }

    /** vykresli entitu do bloku na zadane pozici */
    public void drawInPosition(Entity entity, BlockPosition bp) {
        ScheduleBlock block = ScheduleData.get().blocksUI().get(bp);
        int top = TimeUtils.diff(block.getFrom(), entity.getFrom());
        entity.setBlock(block);
        block.getContent().add(entity, 0, top);
    }

    /** vykresli entitu do bloku na zadane pozici */
    public void drawInPosition(Entity entity, Long roomId, int blockPositionY) {
        for (int i = 0; i < ScheduleData.get().getRoomsCount(); i++) {
            BlockPosition bp = new BlockPosition(i + 1, blockPositionY);
            ScheduleBlock sbi = ScheduleData.get().blocksUI().get(bp);
            if (sbi.getRoomId().equals(roomId)) {
                int top = TimeUtils.diff(sbi.getFrom(), entity.getFrom());
                entity.setBlock(sbi);
                sbi.getContent().add(entity, 0, top);
                break;
            }
        }
    }

    /** vykresli entitu na originalni pozici */
    public void onOriginalPosition(EntityMain main) {
        OriginalPosition op = main.getOriginalPosition();
        if (op != null) {
            if (op.getBlockPosition() == null) {
                // hodi zpatky do bufferu
                main.setBlock(null);
                main.showBufferLook(true);
                ScheduleData.get().buffer().addToMatrix(main);
            } else {
                op.getEntity().setFrom(op.getTimeFrom());
                drawInPosition(op.getEntity(), op.getBlockPosition());
                for (Entity s : (ArrayList<Entity>) main.getSlaves()) {
                    s.setFrom(op.getTimeFrom());
                    drawInPosition(s, s.getBlock().getBlockPosition());
                }
                // TODO: osefovat pripad swapu
            }
        } else {
            // toto by se nemělo nikdy stát
        }
    }

    /** aktualizuje pripadne plenarky */
    public void updatePlenaries(EntityMain em, ClientTime from, ScheduleBlock block) {
        if (!em.getSlaves().isEmpty()) {
            // prekresleni plenarek
            BlockPosition mbp = block.getBlockPosition();
            // projiti pres vedlejsi entity
            for (int i = 0; i < em.getSlaves().size(); i++) {
                Entity e = (Entity) em.getSlaves().get(i);
                BlockPosition bp = e.getBlock().getBlockPosition();

                if (mbp.getX() == bp.getX()) {
                    // prohozeni mistnosti vedlejsi a hlavni casti
                    bp = em.getBlock().getBlockPosition();
                }

                if (mbp.getY() != bp.getY()) {
                    // umisteni v jinem bloku
                    ScheduleBlockImpl b = (ScheduleBlockImpl) ScheduleData.get().blocksUI().get(new BlockPosition(bp.getX(), mbp.getY()));
                    e.setBlock(b);
                    b.setEntityOnPosition(e, from);
                } else {
                    // umisteni ve stejnem bloku
                    ScheduleBlockImpl b = (ScheduleBlockImpl) ScheduleData.get().blocksUI().get(bp);
                    e.setBlock(b);
                    b.setEntityOnPosition(e, from);
                }
            }
        }
    }

    /** aktualizuje pozici entity v bloku + jeji plenarky */
    public void updateEntityPosition(EntityMain em, ScheduleBlock block) {
        block.getContent().add(em, 0, TimeUtils.diff(em.getFrom(), block.getFrom()));
        updatePlenaries(em, em.getFrom(), block);
    }
}
