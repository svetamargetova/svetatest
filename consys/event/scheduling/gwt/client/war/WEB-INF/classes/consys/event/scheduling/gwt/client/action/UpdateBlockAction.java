package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro aktualizaci bloku
 * @author pepa
 */
public class UpdateBlockAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7873727010263257253L;
    // data
    private Long blockId;
    private Long sectionId;
    private String chairUuid;

    public UpdateBlockAction() {
    }

    /**
     * @param sectionId id sekce nebo null, kdyz zadna
     * @param chairUuid uuid chaira nebo null, kdyz zadny
     */
    public UpdateBlockAction(Long blockId, Long sectionId, String chairUuid) {
        this.blockId = blockId;
        this.sectionId = sectionId;
        this.chairUuid = chairUuid;
    }

    /** id bloku */
    public Long getBlockId() {
        return blockId;
    }

    /** uuid chaira nebo null, kdyz zadny */
    public String getChairUuid() {
        return chairUuid;
    }

    /** id sekce nebo null, pokud bez sekce */
    public Long getSectionId() {
        return sectionId;
    }
}
