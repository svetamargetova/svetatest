package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientScheduleDay;

/**
 *
 * @author pepa
 */
public class LoadMyScheduleDayAction extends EventAction<ClientScheduleDay> {

    private static final long serialVersionUID = 6172745571435720013L;
    // data
    private Long id;

    public LoadMyScheduleDayAction() {
    }

    public LoadMyScheduleDayAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
