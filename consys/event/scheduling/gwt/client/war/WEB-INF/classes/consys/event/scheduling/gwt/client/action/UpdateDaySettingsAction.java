package consys.event.scheduling.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.ArrayList;
import java.util.Date;

/**
 * Akce pro aktualizaci nastaveni dne v planovani
 * @author pepa
 */
public class UpdateDaySettingsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -5951795581832321479L;
    // data
    private Long id;
    private Date date;
    private ClientTime from;
    private ClientTime to;
    private ArrayList<Long> rooms;
    private ArrayList<ClientTime> splits;

    public UpdateDaySettingsAction() {
    }

    public UpdateDaySettingsAction(ClientDay day) {
        id = day.getId();
        date = day.getDate();
        from = day.getFrom();
        to = day.getTo();

        rooms = new ArrayList<Long>();
        for (ClientRoomInfo cri : day.getRooms()) {
            rooms.add(cri.getId());
        }

        splits = day.getSplits();
    }

    public Date getDate() {
        return date;
    }

    public ClientTime getFrom() {
        return from;
    }

    public Long getId() {
        return id;
    }

    public ArrayList<Long> getRooms() {
        return rooms;
    }

    public ArrayList<ClientTime> getSplits() {
        return splits;
    }

    public ClientTime getTo() {
        return to;
    }
}
