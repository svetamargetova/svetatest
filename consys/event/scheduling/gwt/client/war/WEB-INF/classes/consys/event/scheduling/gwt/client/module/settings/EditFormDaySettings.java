package consys.event.scheduling.gwt.client.module.settings;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.scheduling.gwt.client.action.ListRoomInfoAction;
import consys.event.scheduling.gwt.client.action.UpdateDaySettingsAction;
import consys.event.scheduling.gwt.client.bo.ClientDay;
import consys.event.scheduling.gwt.client.bo.ClientRoomInfo;
import consys.common.gwt.shared.bo.ClientTime;
import consys.event.scheduling.gwt.client.utils.ESMessageUtils;
import consys.event.scheduling.gwt.client.utils.TimeUtils;
import java.util.ArrayList;

/**
 * Editace nastaveni dne
 * @author pepa
 */
public class EditFormDaySettings extends RootPanel {

    // konstanty
    private static final long ALL_ROOMS = -1l;
    // komponenty
    private BaseForm form;
    private ConsysDateBox dateBox;
    private ConsysTimeTextBox fromBox;
    private ConsysTimeTextBox toBox;
    private FlowPanel roomsPanel;
    private BlockSplitter blockSplitter;
    // data
    private ClientDay originalDay;
    private SchedulingSettingsItem source;

    public EditFormDaySettings(ClientDay originalDay, SchedulingSettingsItem source) {
        super(ESMessageUtils.c.editFormDaySettings_title());
        this.originalDay = originalDay;
        this.source = source;
    }

    @Override
    protected void onLoad() {
        EventDispatchEvent loadEvent = new EventDispatchEvent(new ListRoomInfoAction(),
                new AsyncCallback<ArrayListResult<ClientRoomInfo>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientRoomInfo> result) {
                        init(result.getArrayListResult());
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }

    private void init(ArrayList<ClientRoomInfo> rooms) {
        clear();

        dateBox = ConsysDateBox.getHalfBox();
        dateBox.setShowDateAs(ConsysDateBox.ShowDateAs.ONLY_DATE);
        dateBox.setValue(originalDay.getDate());

        blockSplitter = new BlockSplitter(originalDay.getFrom(), originalDay.getTo(), originalDay.getSplits());

        fromBox = new ConsysTimeTextBox(ECoMessageUtils.c.const_date_from());
        fromBox.setText(originalDay.getFrom().toString());
        fromBox.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (fromBox.doValidate(getFailMessage())) {
                    ClientTime time = fromBox.getTime();
                    blockSplitter.setFromTime(time);
                    fromBox.setText(time.toString());
                }
            }
        });

        toBox = new ConsysTimeTextBox(ECoMessageUtils.c.const_date_to());
        toBox.setText(originalDay.getTo().toString());
        toBox.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (toBox.doValidate(getFailMessage())) {
                    ClientTime time = toBox.getTime();
                    blockSplitter.setToTime(time);
                    toBox.setText(time.toString());
                }
            }
        });

        ArrayList<ClientRoomInfo> origRooms = originalDay.getRooms();

        RoomCheckBox<ClientRoomInfo> allRoomsCheckBox = new RoomCheckBox<ClientRoomInfo>(
                new ClientRoomInfo(ALL_ROOMS, ESMessageUtils.c.editFormDaySettings_text_allRooms()),
                ESMessageUtils.c.editFormDaySettings_text_allRooms(), false);
        allRoomsCheckBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                for (int i = 0; i < roomsPanel.getWidgetCount(); i++) {
                    RoomCheckBox<ClientRoomInfo> cb = (RoomCheckBox<ClientRoomInfo>) roomsPanel.getWidget(i);
                    if (cb.getRoom().getId() != ALL_ROOMS) {
                        cb.setVisible(!event.getValue());
                        if (event.getValue()) {
                            cb.setChecked(true);
                        }
                    }
                }
            }
        });

        roomsPanel = new FlowPanel();
        roomsPanel.add(allRoomsCheckBox);

        boolean allChecked = true;
        for (ClientRoomInfo cri : rooms) {
            boolean checked = origRooms.contains(cri);
            RoomCheckBox<ClientRoomInfo> checkBox = new RoomCheckBox<ClientRoomInfo>(cri, cri.getName(), checked);
            if (!checked) {
                allChecked = false;
            }
            roomsPanel.add(checkBox);
        }
        if (allChecked) {
            allRoomsCheckBox.setChecked(true);
            for (int i = 0; i < roomsPanel.getWidgetCount(); i++) {
                RoomCheckBox<ClientRoomInfo> cb = (RoomCheckBox<ClientRoomInfo>) roomsPanel.getWidget(i);
                if (cb.getRoom().getId() != ALL_ROOMS) {
                    cb.setVisible(false);
                }
            }
        }

        ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_save());
        button.addClickHandler(buttonClickHandler());

        form = new BaseForm();
        form.addRequired(0, ECoMessageUtils.c.const_date(), dateBox);
        form.addRequired(1, ECoMessageUtils.c.const_date_from(), fromBox);
        form.addRequired(2, ECoMessageUtils.c.const_date_to(), toBox);
        form.addOptional(3, ESMessageUtils.c.editFormDaySettings_form_rooms(), roomsPanel);
        form.addOptional(4, ESMessageUtils.c.editFormDaySettings_form_splits(), blockSplitter);
        form.addActionMembers(5, button, ActionLabel.breadcrumbBackCancel());

        addWidget(form);
    }

    /** vraci clickHandler pro potvrzeni editace */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                if (dateBox.getValue() == null) {
                    getFailMessage().setText("Date must be set.");
                    return;
                }

                ArrayList<ClientRoomInfo> rooms = new ArrayList<ClientRoomInfo>();
                for (int i = 0; i < roomsPanel.getWidgetCount(); i++) {
                    Widget w = roomsPanel.getWidget(i);
                    if (w instanceof RoomCheckBox) {
                        RoomCheckBox<ClientRoomInfo> room = (RoomCheckBox<ClientRoomInfo>) w;
                        if (room.getRoom().getId() == ALL_ROOMS) {
                            continue;
                        }
                        if (room.isChecked()) {
                            rooms.add(room.getRoom());
                        }
                    }
                }

                final ClientDay day = new ClientDay();
                day.setId(originalDay.getId());
                day.setDate(dateBox.getValue());
                day.setFrom(fromBox.getTime());
                day.setTo(toBox.getTime());
                day.setRooms(rooms);
                day.setSplits(blockSplitter.getSplits());

                EventDispatchEvent updateEvent = new EventDispatchEvent(
                        new UpdateDaySettingsAction(day),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                source.setData(day);
                                FormUtils.fireBreadcrumbBack();
                            }
                        },
                        EditFormDaySettings.this);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }

    /** textove pole pro zadavani casu planovani */
    public static class ConsysTimeTextBox extends ConsysStringTextBox {

        public ConsysTimeTextBox(String fieldName) {
            super(1, 7, fieldName);
        }

        public ConsysTimeTextBox(String fieldName, String width) {
            super(width, 1, 7, fieldName);
        }

        @Override
        public boolean doValidate(ConsysMessage fail) {
            boolean result = super.doValidate(fail);
            try {
                ClientTime time = TimeUtils.parser(getText());

                if (time.getTime() % 5 != 0) {
                    fail.addOrSetText(ESMessageUtils.c.editFormDaySettings_error_divisibleByFive());
                    result = false;
                }
            } catch (IllegalArgumentException ex) {
                fail.addOrSetText(UIMessageUtils.m.const_invalidValueInField(getIdentifier()));
                result = false;
            }
            return result;
        }

        /**
         * pred pouzitim zavolat validate a overit vysledek, jinak muze vyhodit IllegalArgumentException
         * @throws IllegalArgumentException
         */
        public ClientTime getTime() {
            return TimeUtils.parser(getText());
        }
    }
}
