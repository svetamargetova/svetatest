package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.TimeFrameBlockDao;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pepa
 */
public class TimeFrameBlockDaoImpl extends GenericDaoImpl<TimeFrameBlock> implements TimeFrameBlockDao {

    @Override
    public void delete(List<TimeFrameBlock> blocks) {
        List<Long> blockIds = new ArrayList<Long>();
        for (TimeFrameBlock tfb : blocks) {
            blockIds.add(tfb.getId());
        }
        final String query = "delete from TimeFrameBlock tfb where tfb.id in (:IDS)";
        session().createQuery(query).setParameterList("IDS", blockIds).executeUpdate();
    }

    @Override
    public TimeFrameBlock load(Long l) throws NoRecordException {
        return load(l, TimeFrameBlock.class);
    }

    @Override
    public List<TimeFrameBlock> listBlockByRoom(Room room) {
        final String query = "from TimeFrameBlock tfb where tfb.room.id = (:RID)";
        return session().createQuery(query).setLong("RID", room.getId()).list();
    }

    @Override
    public List<TimeFrameBlock> listBlockByRoomIds(Long timeFrameId, Set<Long> roomIds, int fromTime) {
        if (roomIds.isEmpty()) {
            return new ArrayList<TimeFrameBlock>();
        }
        final String query = "from TimeFrameBlock tfb where tfb.room.id in (:IDS) and tfb.timeFrom = :fromTime and tfb.timeFrame = :TFID";
        return session().createQuery(query).setParameterList("IDS", roomIds).setInteger("fromTime", fromTime).setLong("TFID", timeFrameId).list();
    }

    @Override
    public List<TimeFrameBlock> listBlockByTimeFrameId(Long timeFrameId) throws NoRecordException {
        final String query = "from TimeFrameBlock tfb left join fetch tfb.chair where tfb.timeFrame.id=:TFID order by tfb.timeFrom";
        List<TimeFrameBlock> blocks = session().createQuery(query).setLong("TFID", timeFrameId).list();
        if (blocks == null || blocks.isEmpty()) {
            throw new NoRecordException("No block for time frame " + timeFrameId);
        }
        return blocks;
    }

    @Override
    public void updateTime(List<TimeFrameBlock> blocks, int timeFrom, int timeTo) {
        List<Long> blockIds = new ArrayList<Long>();
        for (TimeFrameBlock tfb : blocks) {
            blockIds.add(tfb.getId());
        }

        //final String query = "update TimeFrameBlock b set b.timeFrom = :timeFrom, b.timeTo = :timeTo where b.id in (:IDS)";
        //session().createQuery(query).setInteger("timeFrom", timeFrom).setInteger("timeTo", timeTo).setParameterList("IDS", blockIds).executeUpdate();

        for (TimeFrameBlock block : blocks) {
            block.setTimeFrom(timeFrom);
            block.setTimeTo(timeTo);
            update(block);
        }
    }

    @Override
    public List<TimeFrameBlock> listAllBlocksDetailed() throws NoRecordException {
        List<TimeFrameBlock> out = session().
                createQuery("select tfb from TimeFrameBlock tfb left join fetch tfb.chair left join fetch tfb.section left join fetch tfb.timeFrame").
                list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }
        return out;
    }

    @Override
    public void deleteAllBlocks() {
        session().createQuery("delete from TimeFrameBlock").executeUpdate();
    }
}
