package consys.event.scheduling.core.service.impl;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.service.SubmissionService;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityBlindDao;
import consys.event.scheduling.core.dao.EntityDao;
import consys.event.scheduling.core.dao.EntityPlenaryBlockDao;
import consys.event.scheduling.core.dao.EntitySubmissionDao;
import consys.event.scheduling.core.dao.RoomDao;
import consys.event.scheduling.core.dao.SectionDao;
import consys.event.scheduling.core.dao.TimeFrameBlockDao;
import consys.event.scheduling.core.dao.TimeFrameDao;
import consys.event.scheduling.core.service.SchedulingSettingsService;
import consys.event.scheduling.core.utils.ValidateHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author pepa
 */
public class SchedulingSettingsServiceImpl extends AbstractService implements SchedulingSettingsService {

    // dao
    private EntityDao entityDao;
    private EntityBlindDao entityBlindDao;
    private EntitySubmissionDao entitySubmissionDao;
    private EntityPlenaryBlockDao entityPlenaryBlockDao;
    private RoomDao roomDao;
    private SectionDao sectionDao;
    private TimeFrameDao timeFrameDao;
    private TimeFrameBlockDao timeFrameBlockDao;
    private UserEventService userEventService;
    private SubmissionService submissionService;

    

    @Override
    public void deleteRoom(Room room) throws NoRecordException, RequiredPropertyNullException {
        if (room == null || room.getId() == null) {
            throw new RequiredPropertyNullException();
        }
        List<TimeFrameBlock> blocks = timeFrameBlockDao.listBlockByRoom(room);
        resetBlockEntities(blocks);
        timeFrameBlockDao.delete(blocks);
        roomDao.delete(room);
    }

    @Override
    public Long createRoom(Room room) throws RequiredPropertyNullException {
        if (room == null || StringUtils.isBlank(room.getName())) {
            throw new RequiredPropertyNullException();
        }
        roomDao.create(room);
        return room.getId();
    }

    @Override
    public void updateRoom(Room room) throws RequiredPropertyNullException {
        if (room == null || room.getId() == null || StringUtils.isBlank(room.getName())) {
            throw new RequiredPropertyNullException();
        }
        roomDao.update(room);
    }

    @Override
    public Room loadRoom(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return roomDao.load(id);
    }

    @Override
    public List<Room> listRooms() {
        return roomDao.listAll(Room.class);
    }

    @Override
    public List<Room> listRoomsForTimeFrame(Long timeFrameId) throws RequiredPropertyNullException {
        if (timeFrameId == null) {
            throw new RequiredPropertyNullException();
        }
        return roomDao.listRoomsByTimeFrameId(timeFrameId);
    }

    @Override
    public void deleteSection(Section section) throws NoRecordException, RequiredPropertyNullException {
        if (section == null || section.getId() == null) {
            throw new RequiredPropertyNullException();
        }
        // TODO: vynulovat prednasky prirazene v sekci a resetovat bloky
        sectionDao.delete(section);
    }

    @Override
    public Long createSection(Section section) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(section.getName())) {
            throw new RequiredPropertyNullException();
        }
        sectionDao.create(section);
        return section.getId();
    }

    @Override
    public void updateSection(Section section) throws RequiredPropertyNullException {
        if (section.getId() == null || StringUtils.isBlank(section.getName())) {
            throw new RequiredPropertyNullException();
        }
        sectionDao.update(section);
    }

    @Override
    public Section loadSection(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return sectionDao.loadWithPermittedUsers(id);
    }

    @Override
    public List<Section> listSections() {
        return sectionDao.listAll(Section.class);
    }

    @Override
    public List<String> listSectionRights(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return sectionDao.listUserUuidWithRights(id);
    }

    @Override
    public List<Long> listUserSectionRights(String uuid) throws RequiredPropertyNullException {
        if (uuid == null) {
            throw new RequiredPropertyNullException();
        }
        return sectionDao.listUserSectionRights(uuid);
    }

    @Override
    public void updateSubmissionAssign(Long sectionId, Set<Long> assignedSubmissionIds) throws RequiredPropertyNullException, NoRecordException {
        if (sectionId == null || assignedSubmissionIds == null) {
            throw new RequiredPropertyNullException();
        }
        Section section = sectionDao.load(sectionId);
        List<EntitySubmission> entitySubmissions = entitySubmissionDao.listSubmissionsBySectionId(sectionId);

        List<Long> newIds = new ArrayList<Long>(assignedSubmissionIds);
        // odstraneni odebranych
        for (EntitySubmission es : entitySubmissions) {
            if (!assignedSubmissionIds.contains(es.getId())) {
                // nactem a smazem pripadne plenarky
                List<EntityPlenaryBlock> plenaries = entityPlenaryBlockDao.listByEntity(es.getId());
                for (EntityPlenaryBlock epb : plenaries) {
                    entityPlenaryBlockDao.delete(epb);
                }
                // smazeme vlastni entity submission
                entitySubmissionDao.delete(es);
            }
            // odebrani ze seznamu pro nove
            newIds.remove(es.getId());
        }

        // nacteni nove prirazenych submissionu
        List<Submission> submissions = submissionService.listSubmissionsByIds(newIds);

        // vytvoreni novych
        for (Submission s : submissions) {
            EntitySubmission es = new EntitySubmission();
            es.setFromMinutesInBlock(0);
            es.setLengthMinutes(30);
            es.setSection(section);
            es.setSubmission(s);
            es.setUuid(UuidProvider.getUuid());

            entitySubmissionDao.create(es);
        }
    }

    @Override
    public void deleteTimeFrame(TimeFrame timeFrame) throws NoRecordException, RequiredPropertyNullException {
        if (timeFrame == null || timeFrame.getId() == null) {
            throw new RequiredPropertyNullException();
        }
        // reset entity v blocich ke smazani
        List<TimeFrameBlock> blocks = timeFrameBlockDao.listBlockByTimeFrameId(timeFrame.getId());
        resetBlockEntities(blocks);
        // odstranit bloky patrici do ramce
        timeFrameBlockDao.delete(blocks);
        // odstranit ramec
        timeFrameDao.delete(timeFrame);
    }

    @Override
    public Long createTimeFrame(TimeFrame timeFrame) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (timeFrame.getDay() == null || timeFrame.getUuid() == null) {
            throw new RequiredPropertyNullException();
        }
        ValidateHelper.timeRange(timeFrame.getTimeFrom(), timeFrame.getTimeTo());
        timeFrameDao.create(timeFrame);
        return timeFrame.getId();
    }

    @Override
    public void updateTimeFrame(TimeFrame timeFrame) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (timeFrame == null || timeFrame.getId() == null || timeFrame.getDay() == null || timeFrame.getUuid() == null) {
            throw new RequiredPropertyNullException();
        }
        ValidateHelper.timeRange(timeFrame.getTimeFrom(), timeFrame.getTimeTo());
        timeFrameDao.update(timeFrame);
    }

    @Override
    public TimeFrame loadTimeFrame(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return timeFrameDao.load(id);
    }

    @Override
    public List<TimeFrame> listTimeFrames() {
        return timeFrameDao.listAll(TimeFrame.class);
    }

    @Override
    public void deleteTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws NoRecordException, RequiredPropertyNullException {
        if (timeFrameBlock == null || timeFrameBlock.getId() == null) {
            throw new RequiredPropertyNullException();
        }
        List<TimeFrameBlock> blocks = new ArrayList<TimeFrameBlock>();
        blocks.add(timeFrameBlock);
        resetBlockEntities(blocks);
        timeFrameBlockDao.delete(timeFrameBlock);
    }

    @Override
    public void deleteTimeFrameBlocks(List<TimeFrameBlock> blocks) throws NoRecordException, RequiredPropertyNullException {
        if (blocks == null) {
            throw new RequiredPropertyNullException();
        }
        if (blocks.isEmpty()) {
            return;
        }
        timeFrameBlockDao.delete(blocks);
    }

    @Override
    public Long createTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (timeFrameBlock == null || timeFrameBlock.getRoom() == null || timeFrameBlock.getTimeFrame() == null
                || timeFrameBlock.getTimeFrame().getId() == null) {
            throw new RequiredPropertyNullException();
        }
        timeFrameBlockDao.create(timeFrameBlock);
        return timeFrameBlock.getId();
    }

    @Override
    public void updateTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (timeFrameBlock == null || timeFrameBlock.getId() == null || timeFrameBlock.getRoom() == null
                || timeFrameBlock.getTimeFrame() == null || timeFrameBlock.getTimeFrame().getId() == null) {
            throw new RequiredPropertyNullException();
        }
        TimeFrame timeFrame = timeFrameBlock.getTimeFrame();
        if (timeFrame.getTimeFrom() > timeFrameBlock.getTimeFrom() || timeFrame.getTimeTo() < timeFrameBlock.getTimeTo()) {
            throw new IllegalPropertyValue("Time of block is not in time of frame");
        }
        // TODO: aktualizovat plenarni bloky
        timeFrameBlockDao.update(timeFrameBlock);
    }

    @Override
    public void updateTimeFrameBlocksTimes(List<TimeFrameBlock> blocks, int timeFrom, int timeTo) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (blocks == null) {
            throw new RequiredPropertyNullException();
        }
        if (blocks.isEmpty()) {
            return;
        }
        ValidateHelper.timeRange(timeFrom, timeTo);
        timeFrameBlockDao.updateTime(blocks, timeFrom, timeTo);
    }

    @Override
    public List<TimeFrameBlock> listTimeFrameBlocks(Long timeFrameId) throws NoRecordException {
        if (timeFrameId == null) {
            throw new NoRecordException();
        }
        return timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);
    }

    @Override
    public void resetEntityForBlocks(List<TimeFrameBlock> blocks) throws RequiredPropertyNullException {
        if (blocks == null) {
            throw new RequiredPropertyNullException();
        }
        if (blocks.isEmpty()) {
            return;
        }
        resetBlockEntities(blocks);
    }

    /** vyresetuje submission, smaze plenarky, smaze blindy v zadanych blocich */
    private void resetBlockEntities(List<TimeFrameBlock> blocks) {
        // nejdriv zrusit plenarky
        entityPlenaryBlockDao.resetInBlocks(blocks);
        // smazat blindy
        entityBlindDao.resetInBlocks(blocks);
        // zrusit prirazeni blokum
        entityDao.resetInBlocks(blocks);
    }

    @Override
    public List<UserEvent> listUserEventByUuids(List<String> uuids) throws RequiredPropertyNullException {
        if (uuids == null) {
            throw new RequiredPropertyNullException();
        }
        if (uuids.isEmpty()) {
            return new ArrayList<UserEvent>();
        }
        return userEventService.listUserEventsByUuids(uuids);
    }

    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public void setEntityBlindDao(EntityBlindDao entityBlindDao) {
        this.entityBlindDao = entityBlindDao;
    }

    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    public void setEntityPlenaryBlockDao(EntityPlenaryBlockDao entityPlenaryBlockDao) {
        this.entityPlenaryBlockDao = entityPlenaryBlockDao;
    }

    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    public void setSectionDao(SectionDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    public void setSubmissionService(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }
}
