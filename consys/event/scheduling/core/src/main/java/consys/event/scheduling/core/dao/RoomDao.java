package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.scheduling.core.bo.Room;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface RoomDao extends GenericDao<Room> {

    /** nacteni seznamu mistnosti pro casovy ramec */
    public List<Room> listRoomsByTimeFrameId(Long timeFrameId);
}
