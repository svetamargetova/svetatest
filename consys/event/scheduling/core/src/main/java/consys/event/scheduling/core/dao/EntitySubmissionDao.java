package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.conference.core.bo.Submission;
import consys.event.scheduling.core.bo.EntitySubmission;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface EntitySubmissionDao extends GenericDao<EntitySubmission> {

    /** nacte submission pro buffer podle seznamu id povolenych */
    public List<EntitySubmission> listBufferSubmission(List<Long> sectionIds);

    /** nacte submissiony pro zadany casovy ramec */
    public List<EntitySubmission> listByTimeFrame(Long timeFrameId);

    public List<EntitySubmission> listAllWithSubmission();

    /** nacte vsechny submission prirazene do zadane sekce */
    public List<EntitySubmission> listSubmissionsBySectionId(Long sectionId);

    /** nacte seznam vsech prijatych, ale neprirazenych submission */
    public List<Submission> listUnassignedSubmissions();
}
