package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.dao.RoomDao;
import java.util.List;

/**
 *
 * @author pepa
 */
public class RoomDaoImpl extends GenericDaoImpl<Room> implements RoomDao {

    @Override
    public List<Room> listRoomsByTimeFrameId(Long timeFrameId) {
        final String query = "select distinct r from TimeFrameBlock as b left join b.room as r where b.timeFrame.id = :TFID";
        return session().createQuery(query).setLong("TFID", timeFrameId).list();
    }

    @Override
    public Room load(Long l) throws NoRecordException {
        return load(l, Room.class);
    }
}
