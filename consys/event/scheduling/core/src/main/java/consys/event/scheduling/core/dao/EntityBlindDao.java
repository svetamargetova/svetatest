package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface EntityBlindDao extends GenericDao<EntityBlind> {

    /** pokud je o id blindu tak ho smaze */
    public void deleteIfBlind(Long entityId);

    /** pokud je o id blindu tak ho smaze */
    public void deleteIfBlind(List<Long> entityIds);

    /** nacte blindy pro zadany casovy ramec */
    public List<EntityBlind> listByTimeFrame(Long timeFrameId);

    public List<EntityBlind> listAll();

    /** vynuluje blindy v zadanych blocich */
    public void resetInBlocks(List<TimeFrameBlock> blocks);
}
