package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.dao.SectionDao;
import java.util.List;

/**
 *
 * @author pepa
 */
public class SectionDaoImpl extends GenericDaoImpl<Section> implements SectionDao {

    @Override
    public List<Long> listUserSectionRights(String uuid) {
        final String query = "select s.id from Section s join s.permittedUsers as ue where ue.uuid = :UUID";
        return session().createQuery(query).setString("UUID", uuid).list();
    }

    @Override
    public List<String> listUserUuidWithRights(Long id) throws NoRecordException {
        final String query = "select ue.uuid from Section s join s.permittedUsers as ue where s.id = :SID";
        return session().createQuery(query).setLong("SID", id).list();
    }

    @Override
    public Section load(Long l) throws NoRecordException {
        return load(l, Section.class);
    }

    @Override
    public Section loadWithPermittedUsers(Long id) throws NoRecordException {
        final String query = "select s from Section s left join fetch s.permittedUsers as pu where s.id = :ID";
        Section section = (Section) session().createQuery(query).setLong("ID", id).uniqueResult();
        if (section == null) {
            throw new NoRecordException();
        }
        return section;
    }

    @Override
    public void deleteAllSections() {
        session().createSQLQuery("delete from scheduling.section_right_rel").executeUpdate();
        session().createQuery("delete from Section").executeUpdate();
    }
}
