package consys.event.scheduling.core.service;

/**
 * Service pro vycisteni naklonovanych dat
 * @author pepa
 */
public interface SchedulingCloneClearService {

    /** Vycisti naklonovana data */
    void clearClone();
}
