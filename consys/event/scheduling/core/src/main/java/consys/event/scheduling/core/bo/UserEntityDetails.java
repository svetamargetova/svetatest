package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.UserEvent;

/**
 *
 * @author pepa
 */
public class UserEntityDetails implements ConsysObject {

    private static final long serialVersionUID = 6879713427022537076L;
    // data
    private boolean favourite;
    private String note;
    private UserEvent userEvent;
    private Entity entity;

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public UserEvent getUserEvent() {
        return userEvent;
    }

    public void setUserEvent(UserEvent userEvent) {
        this.userEvent = userEvent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        UserEntityDetails e = (UserEntityDetails) obj;
        return entity.getId().equals(e.getEntity().getId()) && userEvent.getId().equals(e.userEvent.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.userEvent != null ? this.userEvent.getId().hashCode() : 0);
        hash = 97 * hash + (this.entity != null ? this.entity.getId().hashCode() : 0);
        return hash;
    }

    
}
