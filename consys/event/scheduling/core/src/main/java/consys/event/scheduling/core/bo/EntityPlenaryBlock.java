package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author pepa
 */
public class EntityPlenaryBlock implements ConsysObject {

    private static final long serialVersionUID = 1311738054198832300L;
    // data
    private Entity entity;
    private TimeFrameBlock timeFrameBlock;

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public TimeFrameBlock getTimeFrameBlock() {
        return timeFrameBlock;
    }

    public void setTimeFrameBlock(TimeFrameBlock timeFrameBlock) {
        this.timeFrameBlock = timeFrameBlock;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntityPlenaryBlock other = (EntityPlenaryBlock) obj;
        if (this.entity != other.entity && (this.entity == null || !this.entity.equals(other.entity))) {
            return false;
        }
        if (this.timeFrameBlock != other.timeFrameBlock && (this.timeFrameBlock == null || !this.timeFrameBlock.equals(other.timeFrameBlock))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.entity != null ? this.entity.hashCode() : 0);
        hash = 89 * hash + (this.timeFrameBlock != null ? this.timeFrameBlock.hashCode() : 0);
        return hash;
    }
}
