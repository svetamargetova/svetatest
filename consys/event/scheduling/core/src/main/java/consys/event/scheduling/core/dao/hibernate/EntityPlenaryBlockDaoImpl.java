package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityPlenaryBlockDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class EntityPlenaryBlockDaoImpl extends GenericDaoImpl<EntityPlenaryBlock> implements EntityPlenaryBlockDao {

    @Override
    public List<EntityPlenaryBlock> listByEntity(Long entityId) {
        final String query = "from EntityPlenaryBlock epb where epb.entity.id = :EID";
        return session().createQuery(query).setLong("EID", entityId).list();
    }

    @Override
    public List<EntityPlenaryBlock> listByTimeFrame(Long timeFrameId) {
        final String query = "from EntityPlenaryBlock epb join fetch epb.timeFrameBlock where epb.timeFrameBlock.timeFrame.id = :TFID";
        return session().createQuery(query).setLong("TFID", timeFrameId).list();
    }

    @Override
    public EntityPlenaryBlock load(Long l) throws NoRecordException {
        return load(l, EntityPlenaryBlock.class);
    }

    @Override
    public void resetInBlocks(List<TimeFrameBlock> blocks) {
        List<Long> blockIds = new ArrayList<Long>();
        for (TimeFrameBlock tfb : blocks) {
            blockIds.add(tfb.getId());
        }

        final String query = "from EntityPlenaryBlock p where p.timeFrameBlock.id in (:IDS)";
        List<EntityPlenaryBlock> plenaries = session().createQuery(query).setParameterList("IDS", blockIds).list();
        for (EntityPlenaryBlock p : plenaries) {
            delete(p);
        }
    }
}
