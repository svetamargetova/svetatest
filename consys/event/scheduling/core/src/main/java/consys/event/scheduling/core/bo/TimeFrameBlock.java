package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.UserEvent;

/**
 *
 * @author pepa
 */
public class TimeFrameBlock implements ConsysObject {

    private static final long serialVersionUID = -4458794644765533001L;
    // data
    private Long id;
    private Room room;
    private TimeFrame timeFrame;
    private Section section;
    private UserEvent chair;
    private int timeFrom;
    private int timeTo;

    public UserEvent getChair() {
        return chair;
    }

    public void setChair(UserEvent chair) {
        this.chair = chair;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public TimeFrame getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(TimeFrame timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(int timeFrom) {
        this.timeFrom = timeFrom;
    }

    public int getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(int timeTo) {
        this.timeTo = timeTo;
    }
}
