package consys.event.scheduling.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.dao.ModuleRightDao;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityDao;
import consys.event.scheduling.core.dao.EntitySubmissionDao;
import consys.event.scheduling.core.dao.SectionDao;
import consys.event.scheduling.core.dao.TimeFrameBlockDao;
import consys.event.scheduling.core.service.SchedulingActionPermissionService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class SchedulingActionPermissionServiceImpl extends AbstractService implements SchedulingActionPermissionService {

    // dao
    private EntityDao entityDao;
    private EntitySubmissionDao entitySubmissionDao;
    private SectionDao sectionDao;
    private TimeFrameBlockDao timeFrameBlockDao;
    private ModuleRightDao moduleRightDao;

    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    public void setSectionDao(SectionDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    public void setModuleRightDao(ModuleRightDao moduleRightDao) {
        this.moduleRightDao = moduleRightDao;
    }

    @Override
    public boolean isAdmin(String userEventUuid) throws RequiredPropertyNullException {
        if (userEventUuid == null) {
            throw new RequiredPropertyNullException();
        }
        return moduleRightDao.hasUserRight(userEventUuid, EventScheduleModuleRoleModel.RIGHT_SCHEDULING_ADMIN);
    }

    @Override
    public boolean isInUserSection(Long entityId, String userEventUuid) throws RequiredPropertyNullException, NoRecordException {
        if (isAdmin(userEventUuid)) {
            // pokud je uzivatel admin, neni potreba nic dalsiho resit
            return true;
        }
        if (entityId == null || userEventUuid == null) {
            throw new RequiredPropertyNullException();
        }

        Entity e = entityDao.load(entityId);
        Section s = e.getTimeFrameBlock().getSection();
        if (s == null) {
            return false;
        }

        List<Long> userSections = sectionDao.listUserSectionRights(userEventUuid);
        return userSections.contains(entityId);
    }

    @Override
    public boolean isInUserSection(Long blockId, Long entityId, String userEventUuid) throws RequiredPropertyNullException, NoRecordException {
        if (isAdmin(userEventUuid)) {
            // pokud je uzivatel admin, neni potreba nic dalsiho resit
            return true;
        }
        if (blockId == null) {
            throw new RequiredPropertyNullException();
        }

        TimeFrameBlock block = timeFrameBlockDao.load(blockId);
        Section section = block.getSection();
        if (section == null) {
            // chair uzivatel muze pracovat jen nad bloky se sekcema
            return false;
        }

        // kontrola bloku jestli jeho sekce patri do uzivatelovych
        List<Long> userSections = sectionDao.listUserSectionRights(userEventUuid);
        if (!userSections.contains(section.getId())) {
            return false;
        }

        if (entityId != null) {
            // existujici entita
            try {
                // je to submission
                EntitySubmission es = entitySubmissionDao.load(entityId);
                return userSections.contains(es.getSection().getId());
            } catch (NoRecordException ex) {
                // TODO: je to blind
            }
        } else {
            // jedna se o novy blind
        }

        return true;
    }

    @Override
    public boolean isUserSection(List<Long> sectionIds, String userEventUuid) throws RequiredPropertyNullException {
        if (isAdmin(userEventUuid)) {
            // pokud je uzivatel admin, neni potreba nic dalsiho resit
            return true;
        }
        if (sectionIds == null) {
            throw new RequiredPropertyNullException();
        }

        List<Long> userSectionRights = sectionDao.listUserSectionRights(userEventUuid);

        List<Long> removeList = new ArrayList<Long>();
        for (Long id : sectionIds) {
            if (!userSectionRights.contains(id)) {
                removeList.add(id);
            }
        }
        sectionIds.removeAll(removeList);

        return !sectionIds.isEmpty();
    }
}
