package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.bo.Section;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface SectionDao extends GenericDao<Section> {

    /** nacte seznam id sekci pro ktere ma uzivatel opravneni */
    public List<Long> listUserSectionRights(String uuid);

    /** nacte uuid uzivatelu s opravnenim k zadane sekci */
    public List<String> listUserUuidWithRights(Long id) throws NoRecordException;

    /** nacte sekci s dotazenym seznamem opravnenych uzivatelu */
    public Section loadWithPermittedUsers(Long id) throws NoRecordException;
    
    void deleteAllSections();
}
