package consys.event.scheduling.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.service.UserEventService;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.UserEntityDetails;
import consys.event.scheduling.core.bo.thumb.EntityThumb;
import consys.event.scheduling.core.dao.EntityBlindDao;
import consys.event.scheduling.core.dao.EntityDao;
import consys.event.scheduling.core.dao.EntitySubmissionDao;
import consys.event.scheduling.core.dao.UserEntityDetailsDao;
import consys.event.scheduling.core.service.EntityService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EntityServiceImpl extends AbstractService implements EntityService {

    private EntitySubmissionDao entitySubmissionDao;
    private EntityBlindDao entityBlindDao;
    private EntityDao entityDao;
    private UserEntityDetailsDao userEntityDetailsDao;
    private UserEventService userEventService;

    @Override
    public List<EntityThumb> listAllEntitiesWithDetail() throws NoRecordException {

        List<EntitySubmission> listEntitySubmission = getEntitySubmissionDao().listAllWithSubmission();
        List<EntityBlind> listEntityBlind = getEntityBlindDao().listAll();

        List<EntityThumb> entities = new ArrayList<EntityThumb>(listEntityBlind.size() + listEntitySubmission.size());
        for (EntitySubmission s : listEntitySubmission) {
            EntityThumb thumb = new EntityThumb();
            thumb.setUuid(s.getUuid());
            thumb.setType(EntityThumb.TYPE.TALK);
            thumb.setTags(s.getSubmission().getAgregatedTopics());
            thumb.setBlockId(s.getTimeFrameBlock().getId());
            thumb.setBlockOffset(s.getFromMinutesInBlock());
            thumb.setEntityLength(s.getLengthMinutes());
            thumb.setContributors(s.getSubmission().getContributors());
            thumb.setTitle(s.getSubmission().getTitle());
            thumb.setSubtitle(s.getSubmission().getSubTitle());
            entities.add(thumb);
        }

        for (EntityBlind blind : listEntityBlind) {
            EntityThumb thumb = new EntityThumb();
            thumb.setUuid(blind.getUuid());
            thumb.setBlockId(blind.getTimeFrameBlock().getId());
            thumb.setType(EntityThumb.TYPE.BLIND);
            thumb.setBlockOffset(blind.getFromMinutesInBlock());
            thumb.setEntityLength(blind.getLengthMinutes());
            thumb.setTitle(blind.getTitle());
            thumb.setDescription(blind.getDescription());
            entities.add(thumb);
        }

        return entities;
    }

    @Override
    public List<UserEntityDetails> listUserEntityDetails(String userUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return userEntityDetailsDao.listForUserWithEntity(userUuid);
    }

    @Override
    public void updateUserEntityDetails(String userUuid, String entityUuid, boolean favourite, String note) throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(entityUuid)) {
            throw new RequiredPropertyNullException();
        }
        UserEntityDetails ued = loadUserEntityDetails(userUuid, entityUuid);
        ued.setFavourite(favourite);
        ued.setNote(note);
        userEntityDetailsDao.update(ued);
    }

    @Override
    public void updateUserEntityDetails(String userUuid, String entityUuid, boolean favourite)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(entityUuid)) {
            throw new RequiredPropertyNullException();
        }
        UserEntityDetails ued = loadUserEntityDetails(userUuid, entityUuid);
        ued.setFavourite(favourite);
        userEntityDetailsDao.update(ued);
    }

    @Override
    public void updateUserEntityDetails(String userUuid, String entityUuid, String note)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(entityUuid)) {
            throw new RequiredPropertyNullException();
        }
        UserEntityDetails ued = loadUserEntityDetails(userUuid, entityUuid);
        ued.setNote(note);
        userEntityDetailsDao.update(ued);
    }

    @Override
    public void updateUserEntityDetails(String userUuid, Long entityId, boolean favourite)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(userUuid) || entityId == null) {
            throw new RequiredPropertyNullException();
        }
        UserEntityDetails ued = loadUserEntityDetails(userUuid, entityId);
        ued.setFavourite(favourite);
        userEntityDetailsDao.update(ued);
    }

    @Override
    public void updateUserEntityDetails(String userUuid, Long entityId, String note)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(userUuid) || entityId == null) {
            throw new RequiredPropertyNullException();
        }
        UserEntityDetails ued = loadUserEntityDetails(userUuid, entityId);
        ued.setNote(note);
        userEntityDetailsDao.update(ued);
    }

    /*------------------------------------------------------------------------*/
    /*----------  H E L P   M E T H O D S  ------------*/
    /*------------------------------------------------------------------------*/
    private UserEntityDetails loadUserEntityDetails(String userUuid, String entityUuid) throws ServiceExecutionFailed, RequiredPropertyNullException, NoRecordException {
        UserEntityDetails ued = null;
        try {
            ued = userEntityDetailsDao.loadForUser(userUuid, entityUuid);
        } catch (NoRecordException e) {
            Entity entity = getEntityDao().loadEntityByUuid(entityUuid);
            ued = createEntityDetailFor(userUuid, entity);
        }
        return ued;
    }

    private UserEntityDetails loadUserEntityDetails(String userUuid, Long entityId) throws ServiceExecutionFailed, RequiredPropertyNullException, NoRecordException {
        UserEntityDetails ued = null;
        try {
            ued = userEntityDetailsDao.loadForUser(userUuid, entityId);
        } catch (NoRecordException e) {
            Entity entity = getEntityDao().load(entityId);
            ued = createEntityDetailFor(userUuid, entity);
        }
        return ued;
    }

    private UserEntityDetails createEntityDetailFor(String userUuid, Entity entity) throws ServiceExecutionFailed, RequiredPropertyNullException {
        UserEntityDetails ued = new UserEntityDetails();
        Invitation user = new ExistingUserInvitation(userUuid);
        UserEvent userEvent = getUserEventService().createUserOrLoad(user);
        ued.setEntity(entity);
        ued.setUserEvent(userEvent);
        userEntityDetailsDao.create(ued);
        return ued;
    }

    /**
     * @return the entitySubmissionDao
     */
    public EntitySubmissionDao getEntitySubmissionDao() {
        return entitySubmissionDao;
    }

    /**
     * @param entitySubmissionDao the entitySubmissionDao to set
     */
    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    /**
     * @return the entityBlindDao
     */
    public EntityBlindDao getEntityBlindDao() {
        return entityBlindDao;
    }

    /**
     * @param entityBlindDao the entityBlindDao to set
     */
    public void setEntityBlindDao(EntityBlindDao entityBlindDao) {
        this.entityBlindDao = entityBlindDao;
    }

    /**
     * @return the userEntityDetailsDao
     */
    public UserEntityDetailsDao getUserEntityDetailsDao() {
        return userEntityDetailsDao;
    }

    /**
     * @param userEntityDetailsDao the userEntityDetailsDao to set
     */
    public void setUserEntityDetailsDao(UserEntityDetailsDao userEntityDetailsDao) {
        this.userEntityDetailsDao = userEntityDetailsDao;
    }

    /**
     * @return the userEventService
     */
    public UserEventService getUserEventService() {
        return userEventService;
    }

    /**
     * @param userEventService the userEventService to set
     */
    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    /**
     * @return the entityDao
     */
    public EntityDao getEntityDao() {
        return entityDao;
    }

    /**
     * @param entityDao the entityDao to set
     */
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }
}
