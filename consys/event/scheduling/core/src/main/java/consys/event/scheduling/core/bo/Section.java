package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.UserEvent;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author pepa
 */
public class Section implements ConsysObject {

    private static final long serialVersionUID = -8980778039762695721L;
    // data
    private Long id;
    private String name;
    private String bgColor;
    private String fgColor;
    private String fontColor;
    private Set<UserEvent> permittedUsers;

    public Section() {
        permittedUsers = new HashSet<UserEvent>();
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getFgColor() {
        return fgColor;
    }

    public void setFgColor(String fgColor) {
        this.fgColor = fgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserEvent> getPermittedUsers() {
        return permittedUsers;
    }

    public void setPermittedUsers(Set<UserEvent> permittedUsers) {
        this.permittedUsers = permittedUsers;
    }
}
