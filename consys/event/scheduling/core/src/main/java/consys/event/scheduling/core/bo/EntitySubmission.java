package consys.event.scheduling.core.bo;

import consys.event.conference.core.bo.Submission;

/**
 *
 * @author pepa
 */
public class EntitySubmission extends Entity {

    private static final long serialVersionUID = -9151016154489024895L;
    // data
    private Submission submission;
    private Section section;

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
    }
}
