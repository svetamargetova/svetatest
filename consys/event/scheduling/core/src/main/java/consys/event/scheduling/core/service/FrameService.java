package consys.event.scheduling.core.service;

import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;

/**
 * Rozhranie pre sluzby ktore pracuju s business objektami:
 * <ol>
 *  <li> <code>TimeFrame</code> - casovy ramec, teda den ktory je na rozvrhu.
 *  <li> <code>TimeFrameBlock</code> - blok v konrektnom dni a miestnosti
 *  <li> <code>Room</code> - miestnost
 * </o>
 * <p>
 * Jedna sa len o sluzby ktore su urcene na nacitavanie. K nastavovaniu je urcena
 * slubza {@link SchedulingSettingsService}.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface FrameService {

   /**
    * Nacita vsetky bloky s casovym ramcom a chairom a sekciou
    * @return zoznam vsetkych blokov s dotiahnutymi miestnostami, ramcom a chairom
    * @throws NoRecordException ak niesu ziadne zatial
    */
   public List<TimeFrameBlock> listTimeFrameBlocksDetailed() throws NoRecordException;

   /**
    * Nacita vsetky miestnosti
    * @return zoznam miestnosti
    * @throws NoRecordException
    */
   public List<Room> listRooms() throws NoRecordException;

}
