package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.UserEntityDetails;
import consys.event.scheduling.core.dao.UserEntityDetailsDao;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserEntityDetailsDaoImpl extends GenericDaoImpl<UserEntityDetails> implements UserEntityDetailsDao {

    @Override
    public UserEntityDetails load(Long id) throws NoRecordException {
        throw new UnsupportedOperationException("Entity has no ID column.");
    }

    @Override
    public UserEntityDetails loadForUser(String userUuid, String entityUuid) throws NoRecordException {
        UserEntityDetails ued = (UserEntityDetails) session().
                createQuery("select ued from UserEntityDetails ued join fetch ued.entity where ued.userEvent.uuid=:USER_UUID and ued.entity.uuid=:ENTITY_UUID").
                setString("USER_UUID", userUuid).
                setString("ENTITY_UUID", entityUuid).
                uniqueResult();
        if (ued == null) {
            throw new NoRecordException("There is no UserEntityDetails for user: " + userUuid + " entity: " + entityUuid);
        }
        return ued;
    }

    @Override
    public UserEntityDetails loadForUser(String userUuid, Long entityId) throws NoRecordException {

        UserEntityDetails ued = (UserEntityDetails) session().
                createQuery("select ued from UserEntityDetails ued join fetch ued.entity where ued.userEvent.uuid=:USER_UUID and ued.entity.id=:ENTITY_ID").
                setString("USER_UUID", userUuid).
                setLong("ENTITY_ID", entityId).
                uniqueResult();
        if (ued == null) {
            throw new NoRecordException("There is no UserEntityDetails for user: " + userUuid + " entity: " + entityId);
        }
        return ued;
    }

    @Override
    public List<UserEntityDetails> listForUserWithEntity(String userUuid) {
        List<UserEntityDetails> out = session().
                createQuery("select ued from UserEntityDetails ued join fetch ued.entity where ued.userEvent.uuid=:USER_UUID").
                setString("USER_UUID", userUuid).
                list();
        if (out == null) {
            out = Collections.EMPTY_LIST;
        }
        return out;

    }

    @Override
    public void deleteAllDetails() {
        session().createQuery("delete from UserEntityDetails").executeUpdate();
    }
}
