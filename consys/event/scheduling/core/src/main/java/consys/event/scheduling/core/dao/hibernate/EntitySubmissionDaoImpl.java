package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.Submission;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.dao.EntitySubmissionDao;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author pepa
 */
public class EntitySubmissionDaoImpl extends GenericDaoImpl<EntitySubmission> implements EntitySubmissionDao {

    @Override
    public List<EntitySubmission> listBufferSubmission(List<Long> sectionIds) {
        if (sectionIds == null || sectionIds.isEmpty()) {
            sectionIds = new ArrayList<Long>();
            sectionIds.add(-1L);
        }

        final String query = "from EntitySubmission es join fetch es.submission where es.timeFrameBlock is null and es.section.id in (:IDS)";
        List<EntitySubmission> list = session().createQuery(query).setParameterList("IDS", sectionIds).list();
        if (list == null) {
            list = new ArrayList<EntitySubmission>();
        }
        return list;
    }

    @Override
    public List<EntitySubmission> listByTimeFrame(Long timeFrameId) {
        final String query = "from EntitySubmission e join fetch e.timeFrameBlock join fetch e.submission where e.timeFrameBlock.timeFrame.id = :TFID";
        return session().createQuery(query).setLong("TFID", timeFrameId).list();
    }

    @Override
    public List<EntitySubmission> listSubmissionsBySectionId(Long sectionId) {
        final String query = "from EntitySubmission es join fetch es.submission where es.section.id = :SID";
        return session().createQuery(query).setLong("SID", sectionId).list();
    }

    @Override
    public List<Submission> listUnassignedSubmissions() {
        final String query = "from Submission s where s.state = 1 and s.id not in (select es.submission from EntitySubmission es)";
        return session().createQuery(query).list();
    }

    @Override
    public EntitySubmission load(Long l) throws NoRecordException {
        return load(l, EntitySubmission.class);
    }

    @Override
    public List<EntitySubmission> listAllWithSubmission() {
        List<EntitySubmission> out = session().
                createQuery("select e from EntitySubmission e join fetch e.submission as s join fetch s.contributors").
                list();

        return out == null ? Collections.EMPTY_LIST : out;
    }
}
