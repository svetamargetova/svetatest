package consys.event.scheduling.core.utils;

import consys.common.core.exception.IllegalPropertyValue;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrameBlock;

/**
 *
 * @author pepa
 */
public class ValidateHelper {

    /** provede kontrolu jestli se rozmezi od do vleze do zadaneho bloku */
    public static void blockTimeRange(TimeFrameBlock block, int timeFrom, int timeTo) throws IllegalPropertyValue {
        if (block.getTimeFrom() > timeFrom || block.getTimeTo() < timeTo) {
            throw new IllegalPropertyValue("timeFrom-timeTo is not in block");
        }
    }

    /** provede kontrolu na rozsah a prekrizeni casu */
    public static void timeRange(int from, int to) throws IllegalPropertyValue {
        if (from < 0 || from > 1440) {
            throw new IllegalPropertyValue("Time from is not in range <0,1440>");
        }
        if (to < 0 || to > 1440) {
            throw new IllegalPropertyValue("Time to is not in range <0,1440>");
        }
        if (from >= to) {
            throw new IllegalPropertyValue("Time from is after or equal time to");
        }
    }

    /** kontrola jestli muze byt entita v bloku podle sekce */
    public static void sectionCheck(TimeFrameBlock block, Entity entity) throws IllegalPropertyValue {
        // TODO: blindy
        Section eSection = entity.getTimeFrameBlock() == null ? null : entity.getTimeFrameBlock().getSection();
        if (block.getSection() == null && eSection != null) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        } else if (block.getSection() != null && eSection == null) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        } else if (block.getSection() != null && eSection != null && !block.getSection().getId().equals(eSection.getId())) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        }
    }

    /** kontrola jestli muze byt entita v bloku podle sekce */
    public static void sectionCheck(TimeFrameBlock block, Section eSection) throws IllegalPropertyValue {
        if (block.getSection() == null && eSection != null) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        } else if (block.getSection() != null && eSection == null) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        } else if (block.getSection() != null && eSection != null && !block.getSection().getId().equals(eSection.getId())) {
            throw new IllegalPropertyValue("Illegal block section for entity");
        }
    }
}
