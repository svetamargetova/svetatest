package consys.event.scheduling.core.service.impl;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.core.bo.Submission;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityBlindDao;
import consys.event.scheduling.core.dao.EntityDao;
import consys.event.scheduling.core.dao.EntityPlenaryBlockDao;
import consys.event.scheduling.core.dao.EntitySubmissionDao;
import consys.event.scheduling.core.dao.RoomDao;
import consys.event.scheduling.core.dao.SectionDao;
import consys.event.scheduling.core.dao.TimeFrameBlockDao;
import consys.event.scheduling.core.dao.TimeFrameDao;
import consys.event.scheduling.core.dao.UserEntityDetailsDao;
import consys.event.scheduling.core.service.SchedulingService;
import consys.event.scheduling.core.utils.ValidateHelper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pepa
 */
public class SchedulingServiceImpl extends AbstractService implements SchedulingService {

    // dao
    private EntityDao entityDao;
    private EntityBlindDao entityBlindDao;
    private EntitySubmissionDao entitySubmissionDao;
    private EntityPlenaryBlockDao entityPlenaryBlockDao;
    private RoomDao roomDao;
    private SectionDao sectionDao;
    private TimeFrameDao timeFrameDao;
    private TimeFrameBlockDao timeFrameBlockDao;
    private UserEventService userEventService;
    

  

    @Override
    public Long createEntityBlind(EntityBlind eb) throws RequiredPropertyNullException, IllegalPropertyValue {
        if (eb == null || eb.getTimeFrameBlock() == null || eb.getTitle() == null) {
            throw new RequiredPropertyNullException();
        }

        int timeFrom = eb.getTimeFrameBlock().getTimeFrom() + eb.getFromMinutesInBlock();
        ValidateHelper.blockTimeRange(eb.getTimeFrameBlock(), timeFrom, timeFrom + eb.getLengthMinutes());
        ValidateHelper.sectionCheck(eb.getTimeFrameBlock(), eb);

        entityBlindDao.create(eb);
        return eb.getId();
    }

    @Override
    public List<Entity> listEntities(Long timeFrameId) throws RequiredPropertyNullException {
        if (timeFrameId == null) {
            throw new RequiredPropertyNullException();
        }
        List<EntitySubmission> listEntitySubmission = entitySubmissionDao.listByTimeFrame(timeFrameId);
        List<EntityBlind> listEntityBlind = entityBlindDao.listByTimeFrame(timeFrameId);

        List<Entity> entities = new ArrayList<Entity>(listEntityBlind);
        entities.addAll(listEntitySubmission);
        return entities;
    }

    @Override
    public List<EntityPlenaryBlock> listPlenaryBlocks(Long timeFrameId) throws RequiredPropertyNullException {
        if (timeFrameId == null) {
            throw new RequiredPropertyNullException();
        }
        return entityPlenaryBlockDao.listByTimeFrame(timeFrameId);
    }

    @Override
    public List<Room> listRooms(Long timeFrameId) throws RequiredPropertyNullException {
        if (timeFrameId == null) {
            throw new RequiredPropertyNullException();
        }
        return roomDao.listRoomsByTimeFrameId(timeFrameId);
    }

    @Override
    public List<EntitySubmission> listSubmissionsBySectionId(Long sectionId) throws RequiredPropertyNullException {
        if (sectionId == null) {
            throw new RequiredPropertyNullException();
        }
        return entitySubmissionDao.listSubmissionsBySectionId(sectionId);
    }

    @Override
    public List<Submission> listUnassignedSubmissions() {
        return entitySubmissionDao.listUnassignedSubmissions();
    }

    @Override
    public List<EntitySubmission> loadBuffer(List<Long> sectionIds) throws NoRecordException, RequiredPropertyNullException {
        if (sectionIds == null) {
            throw new RequiredPropertyNullException();
        }
        return entitySubmissionDao.listBufferSubmission(sectionIds);
    }

    @Override
    public TimeFrame loadTimeFrame(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return timeFrameDao.load(id);
    }

    @Override
    public TimeFrameBlock loadTimeFrameBlock(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null) {
            throw new RequiredPropertyNullException();
        }
        return timeFrameBlockDao.load(id);
    }

    @Override
    public void updateBlock(Long blockId, Long sectionId, String chairUuid) throws NoRecordException, RequiredPropertyNullException {
        if (blockId == null) {
            throw new RequiredPropertyNullException();
        }
        TimeFrameBlock tfb = timeFrameBlockDao.load(blockId);
        Long oldSectionId = tfb.getSection() == null ? null : tfb.getSection().getId();

        if (sectionId == null || sectionId.equals(0l)) {
            tfb.setSection(null);
        } else if (tfb.getSection() == null || !tfb.getSection().getId().equals(sectionId)) {
            Section section = sectionDao.load(sectionId);
            tfb.setSection(section);
        }

        if (chairUuid == null) {
            tfb.setChair(null);
        } else if (tfb.getChair() == null || !tfb.getChair().getUuid().equals(chairUuid)) {
            UserEvent user = userEventService.loadUserEvent(chairUuid);
            tfb.setChair(user);
        }

        timeFrameBlockDao.update(tfb);

        // pokud se id sekci neshoduji blok se vyresetuje
        if ((sectionId == null && oldSectionId != null) || (sectionId != null && oldSectionId == null)
                || (oldSectionId != null && !oldSectionId.equals(sectionId))) {
            List<TimeFrameBlock> blocks = new ArrayList<TimeFrameBlock>();
            blocks.add(tfb);
            entityPlenaryBlockDao.resetInBlocks(blocks);
            entityBlindDao.resetInBlocks(blocks);
            entityDao.resetInBlocks(blocks);
        }
    }

    @Override
    public void updateEntityBufferPosition(final Long entityId) throws NoRecordException, RequiredPropertyNullException {
        if (entityId == null) {
            throw new RequiredPropertyNullException();
        }

        // vynulovani bloku
        Entity entity = entityDao.load(entityId);
        entity.setFromMinutesInBlock(0);
        entity.setTimeFrameBlock(null);
        entityDao.update(entity);

        // smazani plenarek
        List<EntityPlenaryBlock> plenaries = entityPlenaryBlockDao.listByEntity(entityId);
        // TODO: optimalizovat na jediny dotaz
        for (EntityPlenaryBlock epb : plenaries) {
            entityPlenaryBlockDao.delete(epb);
        }

        entityBlindDao.deleteIfBlind(entityId);
    }

    @Override
    public void updateEntityPosition(Long entityId, Long blockId, int timeFrom) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue {
        if (entityId == null || blockId == null) {
            throw new RequiredPropertyNullException();
        }
        Entity entity = entityDao.load(entityId);
        TimeFrameBlock block = entity.getTimeFrameBlock();
        if (block == null || !block.getId().equals(blockId)) {
            block = timeFrameBlockDao.load(blockId);
        }
        ValidateHelper.blockTimeRange(block, timeFrom, timeFrom + entity.getLengthMinutes());
        try {
            // pokud jde o submission, tak ma sekci nastavenou vzdy
            EntitySubmission es = entitySubmissionDao.load(entityId);
            ValidateHelper.sectionCheck(block, es.getSection());
        } catch (NoRecordException ex) {
            // neni to submission
            // TODO: jak kontrolovat sekci blindu?
        }

        // TODO: kontrola plenarek

        entity.setFromMinutesInBlock(timeFrom - block.getTimeFrom());
        entity.setTimeFrameBlock(block);

        entityDao.update(entity);
    }

    @Override
    public void updateEntity(Long entityId, int length, List<Long> roomsId) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue {
        if (entityId == null || roomsId == null) {
            throw new RequiredPropertyNullException();
        }
        Entity entity = entityDao.load(entityId);
        doEntityUpdateActions(entity, roomsId);

        entity.setLengthMinutes(length);
        entityDao.update(entity);
    }

    @Override
    public void updateBlind(Long blindId, int length, String name, List<Long> roomsId) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue {
        if (blindId == null || roomsId == null) {
            throw new RequiredPropertyNullException();
        }
        EntityBlind blind = entityBlindDao.load(blindId);
        doEntityUpdateActions(blind, roomsId);

        blind.setTitle(name);
        blind.setLengthMinutes(length);
        entityBlindDao.update(blind);
    }

    /** provede akce potrebne pro aktualizaci entity (kontrola, plenarky, ...) */
    private void doEntityUpdateActions(Entity entity, List<Long> roomsId) throws NoRecordException, IllegalPropertyValue {
        final boolean isBlind = entity instanceof EntityBlind;
        List<EntityPlenaryBlock> oldPlenaries = entityPlenaryBlockDao.listByEntity(entity.getId());

        Set<Long> newToRooms = new HashSet<Long>(roomsId);
        Set<EntityPlenaryBlock> removeFromRooms = new HashSet<EntityPlenaryBlock>();

        for (EntityPlenaryBlock epb : oldPlenaries) {
            final Long plenaryBlockRoomId = epb.getTimeFrameBlock().getRoom().getId();
            if (!roomsId.contains(plenaryBlockRoomId)) {
                removeFromRooms.add(epb);
            }
            newToRooms.remove(plenaryBlockRoomId);
        }

        // overit delku
        int timeFrom = entity.getTimeFrameBlock().getTimeFrom() + entity.getFromMinutesInBlock();
        ValidateHelper.blockTimeRange(entity.getTimeFrameBlock(), timeFrom, timeFrom + entity.getLengthMinutes());

        // TODO: overit sekci
        // TODO: overit jestli se neprekryva s jinou entitou
        // TODO: jestli se neprekryvaji pripadne plenarky

        // pridat plenarky
        List<TimeFrameBlock> blocks = timeFrameBlockDao.listBlockByRoomIds(entity.getTimeFrameBlock().getTimeFrame().getId(),
                newToRooms, entity.getTimeFrameBlock().getTimeFrom());
        if (blocks.size() != newToRooms.size()) {
            throw new IllegalPropertyValue();
        }
        for (TimeFrameBlock tfb : blocks) {
            EntityPlenaryBlock epb = new EntityPlenaryBlock();
            epb.setEntity(entity);
            epb.setTimeFrameBlock(tfb);
            entityPlenaryBlockDao.create(epb);
        }

        // zrusit plenarky
        for (EntityPlenaryBlock epb : removeFromRooms) {
            entityPlenaryBlockDao.delete(epb);
        }
    }       

      public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    public void setEntityBlindDao(EntityBlindDao entityBlindDao) {
        this.entityBlindDao = entityBlindDao;
    }

    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    public void setEntityPlenaryBlockDao(EntityPlenaryBlockDao entityPlenaryBlockDao) {
        this.entityPlenaryBlockDao = entityPlenaryBlockDao;
    }

    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    public void setSectionDao(SectionDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    
}
