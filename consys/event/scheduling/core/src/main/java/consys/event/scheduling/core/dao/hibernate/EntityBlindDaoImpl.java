package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityBlindDao;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author pepa
 */
public class EntityBlindDaoImpl extends GenericDaoImpl<EntityBlind> implements EntityBlindDao {

    @Override
    public void deleteIfBlind(Long entityId) {
        try {
            EntityBlind eb = load(entityId);
            // je to blind, tak smazat
            delete(eb);
        } catch (NoRecordException ex) {
            // neni to blind, tak se nic nedeje
        }
    }

    @Override
    public void deleteIfBlind(List<Long> entityIds) {
        for (Long entityId : entityIds) {
            try {
                EntityBlind eb = load(entityId);
                // je to blind, tak smazat
                delete(eb);
            } catch (NoRecordException ex) {
                // neni to blind, tak se nic nedeje
            }
        }
    }

    @Override
    public List<EntityBlind> listByTimeFrame(Long timeFrameId) {
        final String query = "from EntityBlind e join fetch e.timeFrameBlock where e.timeFrameBlock.timeFrame.id = :TFID";
        return session().createQuery(query).setLong("TFID", timeFrameId).list();
    }

    @Override
    public List<EntityBlind> listAll(){
            List<EntityBlind> out =  session().
                createQuery("from EntityBlind e")
                .list();
            return out == null ? Collections.EMPTY_LIST : out;
    }

    @Override
    public EntityBlind load(Long l) throws NoRecordException {
        return load(l, EntityBlind.class);
    }

    @Override
    public void resetInBlocks(List<TimeFrameBlock> blocks) {
        List<Long> blockIds = new ArrayList<Long>();
        for (TimeFrameBlock tfb : blocks) {
            blockIds.add(tfb.getId());
        }

        final String query = "from EntityBlind e where e.timeFrameBlock.id in (:IDS)";
        List<EntityBlind> blinds = session().createQuery(query).setParameterList("IDS", blockIds).list();
        for (EntityBlind b : blinds) {
            delete(b);
        }
    }
}
