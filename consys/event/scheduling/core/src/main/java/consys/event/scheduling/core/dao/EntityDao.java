package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface EntityDao extends GenericDao<Entity> {

    /** seznam id entit k nacteni */
    public List<Entity> listByIds(List<Long> entityIds);

    /** vynuluje bloky z entit */
    public void resetInBlocks(List<TimeFrameBlock> blocks);

    public Entity loadEntityByUuid(String uuid) throws NoRecordException;

    void deleteAllEntities();
}
