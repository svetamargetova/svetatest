package consys.event.scheduling.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface SchedulingActionPermissionService {

    /** vrati true pokud je zadany uzivatel administrator */
    public boolean isAdmin(String userEventUuid) throws RequiredPropertyNullException;

    /** vraci true pokud entita patri k sekci, se kterou muze uzivatel pracovat */
    public boolean isInUserSection(Long entityId, String userEventUuid) throws RequiredPropertyNullException, NoRecordException;

    /** vraci true pokud blok patri k sekci, se kterou muze uzivatel pracovat a entita patri do teze sekce */
    public boolean isInUserSection(Long blockId, Long entityId, String userEventUuid) throws RequiredPropertyNullException, NoRecordException;

    /** vyhodi ze seznamu id sekci, ke kterym nema uzivatel pravo, pokud nema ani k jedne vraci false */
    public boolean isUserSection(List<Long> sectionIds, String userEventUuid) throws RequiredPropertyNullException;
}
