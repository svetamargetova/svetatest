package consys.event.scheduling.core.bo;

/**
 *
 * @author pepa
 */
public class EntityBlind extends Entity {

    private static final long serialVersionUID = 7844683013297798430L;
    // data
    private String title;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
