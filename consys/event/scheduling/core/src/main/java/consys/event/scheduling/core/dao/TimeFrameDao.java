package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.scheduling.core.bo.TimeFrame;

/**
 *
 * @author pepa
 */
public interface TimeFrameDao extends GenericDao<TimeFrame> {

    void deleteAllFrames();
}
