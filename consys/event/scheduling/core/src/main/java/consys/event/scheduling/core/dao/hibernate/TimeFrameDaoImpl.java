package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.dao.TimeFrameDao;

/**
 *
 * @author pepa
 */
public class TimeFrameDaoImpl extends GenericDaoImpl<TimeFrame> implements TimeFrameDao {

    @Override
    public TimeFrame load(Long l) throws NoRecordException {
        return load(l, TimeFrame.class);
    }

    @Override
    public void deleteAllFrames() {
        session().createQuery("delete from TimeFrame").executeUpdate();
    }
}
