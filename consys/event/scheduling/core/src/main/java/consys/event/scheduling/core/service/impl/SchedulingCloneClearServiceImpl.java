package consys.event.scheduling.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.scheduling.core.dao.*;
import consys.event.scheduling.core.service.SchedulingCloneClearService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service pro vycisteni naklonovanych dat
 * @author pepa
 */
public class SchedulingCloneClearServiceImpl extends AbstractService implements SchedulingCloneClearService {

    @Autowired
    private EntityDao entityDao;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private TimeFrameDao timeFrameDao;
    @Autowired
    private TimeFrameBlockDao timeFrameBlockDao;
    @Autowired
    private UserEntityDetailsDao userEntityDetailsDao;

    @Override
    public void clearClone() {
        userEntityDetailsDao.deleteAllDetails();
        entityDao.deleteAllEntities();
        timeFrameBlockDao.deleteAllBlocks();
        timeFrameDao.deleteAllFrames();
        sectionDao.deleteAllSections();
    }
}
