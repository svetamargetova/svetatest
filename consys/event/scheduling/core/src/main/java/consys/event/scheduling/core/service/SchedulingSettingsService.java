package consys.event.scheduling.core.service;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.common.core.bo.UserEvent;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pepa
 */
public interface SchedulingSettingsService {

    /*------------------------------------------------------------------------*/
    /* ----  R O O M  ---- */
    /*------------------------------------------------------------------------*/
    /** smaze mistnost */
    public void deleteRoom(Room room) throws NoRecordException, RequiredPropertyNullException;

    /** prida mistnost */
    public Long createRoom(Room room) throws RequiredPropertyNullException;

    /** aktualizuje mistnost */
    public void updateRoom(Room room) throws RequiredPropertyNullException;

    /** vraci mistnost podle id */
    public Room loadRoom(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** vrati seznam vsech mistnosti */
    public List<Room> listRooms();

    /** vraci seznam mistnosti pro casovy ramec */
    public List<Room> listRoomsForTimeFrame(Long timeFrameId) throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  S E C T I O N  ---- */
    /*------------------------------------------------------------------------*/
    /** smaze sekci */
    public void deleteSection(Section section) throws NoRecordException, RequiredPropertyNullException;

    /** prida sekci */
    public Long createSection(Section section) throws RequiredPropertyNullException;

    /** aktualizuje sekci */
    public void updateSection(Section section) throws RequiredPropertyNullException;

    /** nacte sekci */
    public Section loadSection(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** vrati seznam sekci */
    public List<Section> listSections();

    /** vraci seznam uuid uzivatelu s opravnenim k sekci */
    public List<String> listSectionRights(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** nacte seznam id sekci, ke kterym ma zadany uzivatel opravneni */
    public List<Long> listUserSectionRights(String uuid) throws RequiredPropertyNullException;

    /** aktualizuje prirazene submissiony k sekci */
    public void updateSubmissionAssign(Long sectionid, Set<Long> assignedSubmissionIds) throws RequiredPropertyNullException, NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ----  T I M E   F R A M E  ---- */
    /*------------------------------------------------------------------------*/
    /** smaze ramec */
    public void deleteTimeFrame(TimeFrame timeFrame) throws NoRecordException, RequiredPropertyNullException;

    /** prida ramec */
    public Long createTimeFrame(TimeFrame timeFrame) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** aktualizuje ramec */
    public void updateTimeFrame(TimeFrame timeFrame) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** nacte ramec */
    public TimeFrame loadTimeFrame(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** vrati seznam ramcu */
    public List<TimeFrame> listTimeFrames();

    /*------------------------------------------------------------------------*/
    /* ----  T I M E   F R A M E   B L O C K  ---- */
    /*------------------------------------------------------------------------*/
    /** smaze blok */
    public void deleteTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws NoRecordException, RequiredPropertyNullException;

    /** smaze bloky */
    public void deleteTimeFrameBlocks(List<TimeFrameBlock> blocks) throws NoRecordException, RequiredPropertyNullException;

    /** prida blok ramce */
    public Long createTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** aktualizuje blok ramec */
    public void updateTimeFrameBlock(TimeFrameBlock timeFrameBlock) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** aktualizuje cas seznamu bloku ramcu */
    public void updateTimeFrameBlocksTimes(List<TimeFrameBlock> blocks, int timeFrom, int timeTo) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** vrati seznam bloku pro zadany ramec */
    public List<TimeFrameBlock> listTimeFrameBlocks(Long timeFrameId) throws NoRecordException;

    /** zrusi prirazeni zadanych bloku v entitach */
    public void resetEntityForBlocks(List<TimeFrameBlock> blocks) throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  D A L Š Í  ---- */
    /*------------------------------------------------------------------------*/
    /** nacte seznam uzivatelu eventu podle jejich uuid */
    public List<UserEvent> listUserEventByUuids(List<String> uuids) throws RequiredPropertyNullException;
}
