package consys.event.scheduling.core.service;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.conference.core.bo.Submission;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.EntitySubmission;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface SchedulingService {

    /** vytvori blind */
    public Long createEntityBlind(EntityBlind eb) throws RequiredPropertyNullException, IllegalPropertyValue;

    /** nacte entity co jsou prirazeny v casovem ramci */
    public List<Entity> listEntities(Long timeFrameId) throws RequiredPropertyNullException;

    /** nacte plenarni casti pro zadany casovy ramec */
    public List<EntityPlenaryBlock> listPlenaryBlocks(Long timeFrameId) throws RequiredPropertyNullException;

    /** nacte mistnosti pro casovy ramec */
    public List<Room> listRooms(Long timeFrameId) throws RequiredPropertyNullException;

    /** nacte submissiony prirazene k zadane sekci */
    public List<EntitySubmission> listSubmissionsBySectionId(Long sectionId) throws RequiredPropertyNullException;

    /** nacte seznam prijatych submissionu, ktere nejsou nikde prirazene */
    public List<Submission> listUnassignedSubmissions();

    /** nacte buffer polozky pro zadane sekce */
    public List<EntitySubmission> loadBuffer(List<Long> sessionIds) throws NoRecordException, RequiredPropertyNullException;

    /** nacte casovy ramec podle id */
    public TimeFrame loadTimeFrame(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** nacte blok podle id */
    public TimeFrameBlock loadTimeFrameBlock(Long id) throws NoRecordException, RequiredPropertyNullException;

    /** aktualizuje hodnoty bloku z prehledu rozvrhu */
    public void updateBlock(Long blockId, Long sectionId, String chairUuid) throws NoRecordException, RequiredPropertyNullException;

    /** entita premistena do bufferu */
    public void updateEntityBufferPosition(final Long entityId) throws NoRecordException, RequiredPropertyNullException;

    /** entita byla premistena */
    public void updateEntityPosition(Long entityId, Long blockId, int timeFrom) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue;

    /** aktualizuje zakladni parametry entity (delka a jestli je plenarni) */
    public void updateEntity(Long entityId, int length, List<Long> roomsId) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue;

    /** aktualizuje blind (delku, nazev, plenarky) */
    public void updateBlind(Long blindId, int length, String name, List<Long> roomsId) throws NoRecordException, RequiredPropertyNullException, IllegalPropertyValue;
}
