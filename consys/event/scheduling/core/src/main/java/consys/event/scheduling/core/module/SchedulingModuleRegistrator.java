package consys.event.scheduling.core.module;

import consys.common.core.bo.SystemProperty;
import consys.event.common.api.right.Role;
import consys.event.common.core.AbstractEventModuleRegistrator;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.scheduling.api.right.EventScheduleModuleRoleModel;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author pepa
 */
public class SchedulingModuleRegistrator extends AbstractEventModuleRegistrator {

    // konstanty
    public static final String MODULE_NAME = "scheduling";

    @Override
    public String getSchemaName() {
        return MODULE_NAME;
    }

    @Override
    public void doAfterSessionFactoryCreate(Session sn) {
    }

    @Override
    public List<SystemProperty> getModuleProperties() {
        List<SystemProperty> properties = new ArrayList<SystemProperty>();
        return properties;
    }

    @Override
    public Module getModule() {
        Module m = new Module();
        m.setName(MODULE_NAME);

        EventScheduleModuleRoleModel rights = new EventScheduleModuleRoleModel();
        for (Role r : rights.getRoles()) {
            ModuleRight right = new ModuleRight();
            right.setName(r.getName());
            right.setUniqueValue(r.getIdentifier());
            m.getRights().add(right);
        }
        return m;
    }
}
