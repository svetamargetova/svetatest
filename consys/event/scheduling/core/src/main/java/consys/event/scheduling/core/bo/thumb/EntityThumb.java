package consys.event.scheduling.core.bo.thumb;

import consys.event.common.core.bo.UserEvent;
import java.util.Set;

/**
 * Thumb pre entitu ktora obsahuje zjednotene informacie vsetkych entit tak
 * aby boli prezentovatelne jednotne.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EntityThumb {
    private String uuid;
    private String title;
    private String subtitle;
    private String tags;
    private String description;
    private Long blockId;
    private int blockOffset;
    private int entityLength;
    private Set<UserEvent> contributors;
    private TYPE type;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the blockId
     */
    public Long getBlockId() {
        return blockId;
    }

    /**
     * @param blockId the blockId to set
     */
    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }

    /**
     * @return the blockOffset
     */
    public int getBlockOffset() {
        return blockOffset;
    }

    /**
     * @param blockOffset the blockOffset to set
     */
    public void setBlockOffset(int blockOffset) {
        this.blockOffset = blockOffset;
    }

    /**
     * @return the entityLength
     */
    public int getEntityLength() {
        return entityLength;
    }

    /**
     * @param entityLength the entityLength to set
     */
    public void setEntityLength(int entityLength) {
        this.entityLength = entityLength;
    }

    /**
     * @return the contributors
     */
    public Set<UserEvent> getContributors() {
        return contributors;
    }

    /**
     * @param contributors the contributors to set
     */
    public void setContributors(Set<UserEvent> contributors) {
        this.contributors = contributors;
    }

    /**
     * @return the type
     */
    public TYPE getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(TYPE type) {
        this.type = type;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(String tags) {
        this.tags = tags;
    }


    public enum TYPE{
        TALK, BLIND;
    }



}
