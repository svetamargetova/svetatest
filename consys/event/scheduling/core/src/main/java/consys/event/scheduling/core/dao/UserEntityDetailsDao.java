package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.bo.UserEntityDetails;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface UserEntityDetailsDao extends GenericDao<UserEntityDetails> {

    public UserEntityDetails loadForUser(String userUuid, String entityUuid) throws NoRecordException;

    public UserEntityDetails loadForUser(String userUuid, Long entityId) throws NoRecordException;

    public List<UserEntityDetails> listForUserWithEntity(String userUuid);

    void deleteAllDetails();
}
