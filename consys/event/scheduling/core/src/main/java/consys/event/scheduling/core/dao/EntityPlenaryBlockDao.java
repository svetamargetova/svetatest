package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.scheduling.core.bo.EntityPlenaryBlock;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface EntityPlenaryBlockDao extends GenericDao<EntityPlenaryBlock> {

    /** nacte plenarni bloky pro zadanou entitu */
    public List<EntityPlenaryBlock> listByEntity(Long entityId);

    /** nacte plenarni bloky pro zadany casovy ramec */
    public List<EntityPlenaryBlock> listByTimeFrame(Long timeFrameId);

    /** vynuluje bloky plenarek v zadanych blocich */
    public void resetInBlocks(List<TimeFrameBlock> blocks);
}
