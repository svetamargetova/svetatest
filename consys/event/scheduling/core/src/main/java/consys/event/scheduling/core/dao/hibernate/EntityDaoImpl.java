package consys.event.scheduling.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.EntityDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class EntityDaoImpl extends GenericDaoImpl<Entity> implements EntityDao {

    @Override
    public List<Entity> listByIds(List<Long> entityIds) {
        final String query = "from Entity e where e.id in (:IDS)";
        return session().createQuery(query).setParameterList("IDS", entityIds).list();
    }

    @Override
    public Entity load(Long l) throws NoRecordException {
        return load(l, Entity.class);
    }

    @Override
    public void resetInBlocks(List<TimeFrameBlock> blocks) {
        List<Long> blockIds = new ArrayList<Long>();
        for (TimeFrameBlock tfb : blocks) {
            blockIds.add(tfb.getId());
        }

        //final String query = "update Entity e set e.timeFrameBlock = null where e.timeFrameBlock.id in (:IDS)";
        //session().createQuery(query).setParameterList("IDS", blockIds).executeUpdate();

        final String query = "from Entity e where e.timeFrameBlock.id in (:IDS)";
        List<Entity> entities = session().createQuery(query).setParameterList("IDS", blockIds).list();
        for (Entity e : entities) {
            e.setTimeFrameBlock(null);
            update(e);
        }
    }

    @Override
    public Entity loadEntityByUuid(String uuid) throws NoRecordException {
        Entity e = (Entity) session().
                createQuery("select e from Entity e where e.uuid=:E_UUID").
                setString("E_UUID", uuid).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException();
        }
        return e;
    }

    @Override
    public void deleteAllEntities() {
        session().createQuery("delete from EntityPlenaryBlock").executeUpdate();
        session().createQuery("delete from EntityBlind").executeUpdate();
        session().createQuery("delete from EntitySubmission").executeUpdate();
        session().createQuery("delete from Entity").executeUpdate();
    }
}
