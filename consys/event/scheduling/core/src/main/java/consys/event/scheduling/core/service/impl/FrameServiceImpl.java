package consys.event.scheduling.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.impl.AbstractService;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import consys.event.scheduling.core.dao.RoomDao;
import consys.event.scheduling.core.dao.TimeFrameBlockDao;
import consys.event.scheduling.core.service.FrameService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class FrameServiceImpl extends AbstractService implements FrameService{

    private TimeFrameBlockDao timeFrameBlockDao;
    private RoomDao roomDao;


    @Override
    public List<TimeFrameBlock> listTimeFrameBlocksDetailed() throws NoRecordException {
        return timeFrameBlockDao.listAllBlocksDetailed();
    }

    @Override
    public List<Room> listRooms() throws NoRecordException{
        return roomDao.listAll(Room.class);
    }

    /**
     * @return the timeFrameBlockDao
     */
    public TimeFrameBlockDao getTimeFrameBlockDao() {
        return timeFrameBlockDao;
    }

    /**
     * @param timeFrameBlockDao the timeFrameBlockDao to set
     */
    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    /**
     * @return the roomDao
     */
    public RoomDao getRoomDao() {
        return roomDao;
    }

    /**
     * @param roomDao the roomDao to set
     */
    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }
    




}
