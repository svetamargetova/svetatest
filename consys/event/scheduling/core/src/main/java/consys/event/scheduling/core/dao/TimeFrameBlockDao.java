package consys.event.scheduling.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pepa
 */
public interface TimeFrameBlockDao extends GenericDao<TimeFrameBlock> {

    /** smaze bloky se zadanymi id */
    public void delete(List<TimeFrameBlock> blocks);

    /** nacte vsechny bloky (ze vsech ramcu) pro zadanou mistnosti */
    public List<TimeFrameBlock> listBlockByRoom(Room room);

    /** nacte bloky pro zadane id ramce a id mistnosti a zacatek */
    public List<TimeFrameBlock> listBlockByRoomIds(Long timeFrameId, Set<Long> roomIds, int fromTime);

    /** vrati seznam bloku podle zadaneho id casoveho ramce */
    public List<TimeFrameBlock> listBlockByTimeFrameId(Long timeFrameId) throws NoRecordException;
    /**
     * Nacita TimeFrameBlock s dotiahnutou:
     * <ol>     
     *  <li> Sekciou
     *  <li> Chairom
     *  <li> TimeFrameom
     * </ol>
     * @return
     * @throws NoRecordException
     */
    public List<TimeFrameBlock> listAllBlocksDetailed() throws NoRecordException;

    /** aktualizuje cas u zadanych bloku */
    public void updateTime(List<TimeFrameBlock> blocks, int timeFrom, int timeTo);
    
    void deleteAllBlocks();
}
