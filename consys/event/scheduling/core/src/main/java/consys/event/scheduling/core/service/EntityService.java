package consys.event.scheduling.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.scheduling.core.bo.UserEntityDetails;
import consys.event.scheduling.core.bo.thumb.EntityThumb;

import java.util.List;

/**
 * Sluzba ktora je urcena na pracu s entitami vsetkych druhou.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface EntityService {

    public List<EntityThumb> listAllEntitiesWithDetail()
            throws NoRecordException;

    public List<UserEntityDetails> listUserEntityDetails(String userUuid) 
            throws NoRecordException, RequiredPropertyNullException;

    public void updateUserEntityDetails(String userUuid, String entityUuid, boolean favourite, String note)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    public void updateUserEntityDetails(String userUuid, String entityUuid, boolean favourite)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    public void updateUserEntityDetails(String userUuid, String entityUuid, String note)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    public void updateUserEntityDetails(String userUuid, Long entityId, boolean favourite)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    public void updateUserEntityDetails(String userUuid, Long entityId, String note)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

}
