package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author pepa
 */
public class Room implements ConsysObject {

    private static final long serialVersionUID = -5302354529536080886L;
    // data
    private Long id;
    private String name;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
