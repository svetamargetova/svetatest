package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class TimeFrame implements ConsysObject {

    private static final long serialVersionUID = 1517330509761258732L;
    // data
    private Long id;
    private String uuid;
    private Date day;
    private int timeFrom;
    private int timeTo;

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(int timeFrom) {
        this.timeFrom = timeFrom;
    }

    public int getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(int timeTo) {
        this.timeTo = timeTo;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
