package consys.event.scheduling.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author pepa
 */
public class Entity implements ConsysObject {

    private static final long serialVersionUID = 5405554025988278254L;
    // data
    private Long id;
    private String uuid;
    private int fromMinutesInBlock;
    private int lengthMinutes;
    private TimeFrameBlock timeFrameBlock;

    public int getFromMinutesInBlock() {
        return fromMinutesInBlock;
    }

    public void setFromMinutesInBlock(int fromMinutesInBlock) {
        this.fromMinutesInBlock = fromMinutesInBlock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLengthMinutes() {
        return lengthMinutes;
    }

    public void setLengthMinutes(int lengthMinutes) {
        this.lengthMinutes = lengthMinutes;
    }

    public TimeFrameBlock getTimeFrameBlock() {
        return timeFrameBlock;
    }

    public void setTimeFrameBlock(TimeFrameBlock timeFrameBlock) {
        this.timeFrameBlock = timeFrameBlock;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Entity e = (Entity) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }
}
