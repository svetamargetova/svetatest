package consys.event.scheduling.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class SectionDaoTest extends AbstractSchedulingTest {

    // dao
    private SectionDao sectionDao;

    @Autowired(required = true)
    public void setSectionDao(SectionDao sectionDao) {
        this.sectionDao = sectionDao;
    }

    @Test(groups = {"dao"})
    public void createSectionWithoutName() {
        try {
            Section Section = new Section();
            sectionDao.create(Section);
            fail("Vytvorila se sekce bez nazvu");
        } catch (Exception ex) {
            // test prosel
        }
    }

    @Test(groups = {"dao"})
    public void createSectionWithName() {
        try {
            final String SectionName = "Sekce 1";

            Section Section = new Section();
            Section.setName(SectionName);
            sectionDao.create(Section);
            assertNotNull(Section.getId(), "Id sekce je null");

            Section loadSection = sectionDao.load(Section.getId());
            assertEquals(SectionName, loadSection.getName(), "Název sekce se neshoduje");
        } catch (Exception ex) {
            fail("Nepodarilo se vytvorit sekci", ex);
        }
    }

    @Test(groups = {"dao"})
    public void updateSection() {
        try {
            final String SectionName = "Sekce 3";
            final String uSectionName = "Sekce 3.5";

            Section Section = new Section();
            Section.setName(SectionName);
            sectionDao.create(Section);
            final Long id = Section.getId();
            assertNotNull(id, "Id sekce je null");

            Section toEditSection = sectionDao.load(id);
            toEditSection.setName(uSectionName);
            sectionDao.update(toEditSection);

            Section loadSection = sectionDao.load(id);
            assertEquals(uSectionName, loadSection.getName(), "Aktualizovaný název sekce se neshoduje");
        } catch (Exception ex) {
            fail("Neco se pokazilo pri aktualizaci", ex);
        }
    }

    @Test(groups = {"dao"})
    public void deleteSection() {
        try {
            final String SectionName = "Sekce ke smazani";

            Section Section = new Section();
            Section.setName(SectionName);
            sectionDao.create(Section);
            final Long id = Section.getId();
            assertNotNull(id, "Id sekce je null");

            Section loadSection = sectionDao.load(id);
            assertEquals(SectionName, loadSection.getName(), "Název sekce se neshoduje");
            sectionDao.delete(loadSection);

            sectionDao.load(id);
            fail("byl nacteny objekt, ktery mel byt smazany");
        } catch (NoRecordException ex) {
            // v poradku
        } catch (Exception ex) {
            fail("Neco se pokazilo pri mazani", ex);
        }
    }
}
