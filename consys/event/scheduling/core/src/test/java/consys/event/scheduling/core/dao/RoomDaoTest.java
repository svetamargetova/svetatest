package consys.event.scheduling.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class RoomDaoTest extends AbstractSchedulingTest {

    // dao
    private RoomDao roomDao;
    private TimeFrameDao timeFrameDao;
    private TimeFrameBlockDao timeFrameBlockDao;

    @Autowired(required = true)
    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    @Autowired(required = true)
    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    @Autowired(required = true)
    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    @Test(groups = {"dao"})
    public void createRoomWithoutName() {
        try {
            Room room = new Room();
            roomDao.create(room);
            fail("Vytvorila se mistnost bez nazvu");
        } catch (Exception ex) {
            // test prosel
        }
    }

    @Test(groups = {"dao"})
    public void createRoomWithName() {
        try {
            final String roomName = "Mistnost 1";

            Room room = new Room();
            room.setName(roomName);
            roomDao.create(room);
            assertNotNull(room.getId(), "Id mistnosti je null");

            Room loadRoom = roomDao.load(room.getId());
            assertEquals(roomName, loadRoom.getName(), "Název místnosti se neshoduje");
        } catch (Exception ex) {
            fail("Nepodarilo se vytvorit mistnost", ex);
        }
    }

    @Test(groups = {"dao"})
    public void createRoomWithNameAndDescription() {
        try {
            final String roomName = "Mistnost 2";
            final String roomDescription = "Místnost 2 pro 30 lidí, s projektorem a ozvučením";

            Room room = new Room();
            room.setName(roomName);
            room.setDescription(roomDescription);
            roomDao.create(room);
            assertNotNull(room.getId(), "Id mistnosti je null");

            Room loadRoom = roomDao.load(room.getId());
            assertEquals(roomName, loadRoom.getName(), "Název místnosti se neshoduje");
            assertEquals(roomDescription, loadRoom.getDescription(), "Popis místnosti se neshoduje");
        } catch (Exception ex) {
            fail("Nepodarilo se vytvorit mistnost", ex);
        }
    }

    @Test(groups = {"dao"})
    public void updateRoomWithNameAndDescription() {
        try {
            final String roomName = "Mistnost 3";
            final String roomDescription = "Místnost 3 pro 30 lidí, s projektorem a ozvučením";

            final String uRoomName = "Mistnost 3.5";
            final String uRoomDescription = "Místnost 3.5 pro 30 lidí, s projektorem a ozvučením";

            Room room = new Room();
            room.setName(roomName);
            room.setDescription(roomDescription);
            roomDao.create(room);
            final Long id = room.getId();
            assertNotNull(id, "Id mistnosti je null");

            Room toEditRoom = roomDao.load(id);
            toEditRoom.setName(uRoomName);
            toEditRoom.setDescription(uRoomDescription);
            roomDao.update(toEditRoom);

            Room loadRoom = roomDao.load(id);
            assertEquals(uRoomName, loadRoom.getName(), "Aktualizovaný název místnosti se neshoduje");
            assertEquals(uRoomDescription, loadRoom.getDescription(), "Aktualizovaný popis místnosti se neshoduje");
        } catch (Exception ex) {
            fail("Neco se pokazilo pri aktualizaci", ex);
        }
    }

    @Test(groups = {"dao"})
    public void deleteRoom() {
        try {
            final String roomName = "Mistnost ke smazani";

            Room room = new Room();
            room.setName(roomName);
            roomDao.create(room);
            final Long id = room.getId();
            assertNotNull(id, "Id mistnosti je null");

            Room loadRoom = roomDao.load(id);
            assertEquals(roomName, loadRoom.getName(), "Název místnosti se neshoduje");
            roomDao.delete(loadRoom);

            roomDao.load(id);
            fail("byl nacteny objekt, ktery mel byt smazany");
        } catch (NoRecordException ex) {
            // v poradku
        } catch (Exception ex) {
            fail("Neco se pokazilo pri mazani", ex);
        }
    }

    @Test(groups = {"dao"})
    public void listRoomsByTimeFrameId() {
        TimeFrame tf1 = new TimeFrame();
        tf1.setDay(new Date());
        tf1.setTimeFrom(0);
        tf1.setTimeTo(1000);
        tf1.setUuid("1234");
        timeFrameDao.create(tf1);

        TimeFrame tf2 = new TimeFrame();
        tf2.setDay(new Date(50000000l));
        tf2.setTimeFrom(0);
        tf2.setTimeTo(1000);
        tf2.setUuid("4321");
        timeFrameDao.create(tf2);

        List<Room> rooms = new ArrayList<Room>();
        for (int i = 0; i < 2; i++) {
            Room room = new Room();
            room.setDescription("");
            room.setName("Místnost " + i);
            roomDao.create(room);
            rooms.add(room);
        }

        for (int i = 0; i < 3; i++) {
            TimeFrameBlock tfb = new TimeFrameBlock();
            tfb.setRoom(rooms.get(0));
            tfb.setTimeFrame(tf1);
            tfb.setTimeFrom(0);
            tfb.setTimeTo(600);
            timeFrameBlockDao.create(tfb);
        }

        for (int i = 0; i < 5; i++) {
            TimeFrameBlock tfb = new TimeFrameBlock();
            tfb.setRoom(rooms.get(i % 2));
            tfb.setTimeFrame(tf2);
            tfb.setTimeFrom(0);
            tfb.setTimeTo(600);
            timeFrameBlockDao.create(tfb);
        }

        List<Room> dayRooms = roomDao.listRoomsByTimeFrameId(tf1.getId());
        assertEquals(dayRooms.size(), 1);
        assertEquals(dayRooms.get(0).getId(), rooms.get(0).getId());

        List<Room> dayRooms1 = roomDao.listRoomsByTimeFrameId(tf2.getId());
        assertEquals(dayRooms1.size(), 2);
    }
}
