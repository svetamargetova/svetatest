package consys.event.scheduling.core.dao;

import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.Entity;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class EntityDaoTest extends AbstractSchedulingTest {

    // konstanty
    private static final Long[] MANUAL_ENTITY_ID_LIST = {1l, 2l, 3l, 4l};
    // dao
    private EntityDao entityDao;
    private RoomDao roomDao;
    private TimeFrameDao timeFrameDao;
    private TimeFrameBlockDao timeFrameBlockDao;

    @Autowired(required = true)
    public void setEntityDao(EntityDao entityDao) {
        this.entityDao = entityDao;
    }

    @Autowired(required = true)
    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    @Autowired(required = true)
    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    @Autowired(required = true)
    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    @Test(groups = {"dao"})
    public void createEntity() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);

        TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
        timeFrameBlock.setChair(null);
        timeFrameBlock.setRoom(room);
        timeFrameBlock.setSection(null);
        timeFrameBlock.setTimeFrame(timeFrame);
        timeFrameBlock.setTimeFrom(0);
        timeFrameBlock.setTimeTo(1300);
        timeFrameBlockDao.create(timeFrameBlock);

        Entity entity = new Entity();
        entity.setFromMinutesInBlock(0);
        entity.setLengthMinutes(10);
        entity.setTimeFrameBlock(timeFrameBlock);
        entity.setUuid("12441");
        entityDao.create(entity);

        try {
            entityDao.load(entity.getId());
        } catch (Exception ex) {
            fail("Nepodarilo se nacist", ex);
        }
    }

    @Test(groups = {"dao"})
    public void resetEntity() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);

        ArrayList<TimeFrameBlock> blocks = new ArrayList<TimeFrameBlock>();
        for (int i = 0; i < 4; i++) {
            TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
            timeFrameBlock.setChair(null);
            timeFrameBlock.setRoom(room);
            timeFrameBlock.setSection(null);
            timeFrameBlock.setTimeFrame(timeFrame);
            timeFrameBlock.setTimeFrom(0);
            timeFrameBlock.setTimeTo(1300);
            timeFrameBlockDao.create(timeFrameBlock);
            blocks.add(timeFrameBlock);
        }

        for (int i = 0; i < 5; i++) {
            Entity entity = new Entity();
            entity.setFromMinutesInBlock(0);
            entity.setLengthMinutes(10);
            entity.setTimeFrameBlock(blocks.get(i % 2));
            entity.setUuid("12441" + i);
            entityDao.create(entity);
        }

        try {
            List<Entity> list = entityDao.listAll(Entity.class);
            assertEquals(list.size(), 5 + MANUAL_ENTITY_ID_LIST.length, "Spatna velikost seznamu");

            List<TimeFrameBlock> blks = new ArrayList<TimeFrameBlock>();
            blks.add(blocks.get(0));
            List<Entity> entityToReset = new ArrayList<Entity>();

            for (Entity e : list) {
                boolean found = false;
                for (int i = 0; i < MANUAL_ENTITY_ID_LIST.length; i++) {
                    if (e.getId().equals(MANUAL_ENTITY_ID_LIST[i])) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    continue;
                }

                if (e.getTimeFrameBlock() != null && e.getTimeFrameBlock().getId().equals(blks.get(0).getId())) {
                    entityToReset.add(e);
                }
            }

            // reset bloku
            assertEquals(blks.size(), 1, "Spatny pocet id bloku k vyresetovani");
            entityDao.resetInBlocks(blks);

            List<Long> eids = new ArrayList<Long>();
            for (Entity e : entityToReset) {
                eids.add(e.getId());
            }

            List<Entity> ids = entityDao.listByIds(eids);
            assertEquals(ids.size(), 3, "Nebyly nacteny predpokladane entity");
            for (Entity e : ids) {
                if (e.getTimeFrameBlock() != null) {
                    fail("Entita nema null v bloku");
                }
            }
        } catch (Exception ex) {
            fail("Nepodarilo se nacist", ex);
        }
    }
}
