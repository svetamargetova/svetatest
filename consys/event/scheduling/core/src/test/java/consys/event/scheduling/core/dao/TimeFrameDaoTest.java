package consys.event.scheduling.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.TimeFrame;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class TimeFrameDaoTest extends AbstractSchedulingTest {

    // dao
    private TimeFrameDao timeFrameDao;

    @Autowired(required = true)
    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    @Test(groups = {"dao"})
    public void badCreateTimeFrame() {
        try {
            TimeFrame timeFrame = new TimeFrame();
            timeFrame.setUuid("232433");
            timeFrameDao.create(timeFrame);
            fail("Vytvoreno i pres chybejici datum");
        } catch (Exception ex) {
            // v poradku
        }
    }

    @Test(groups = {"dao"})
    public void createTimeFrame() {
        try {
            TimeFrame timeFrame = new TimeFrame();
            timeFrame.setDay(new Date());
            timeFrame.setTimeFrom(0);
            timeFrame.setTimeTo(1440);
            timeFrame.setUuid("2324");
            timeFrameDao.create(timeFrame);
        } catch (Exception ex) {
            fail("Problem s vytvorenim ramce dne");
        }
    }

    @Test(groups = {"dao"})
    public void deleteTimeFrame() {
        try {
            TimeFrame timeFrame = new TimeFrame();
            timeFrame.setDay(new Date());
            timeFrame.setTimeFrom(0);
            timeFrame.setTimeTo(1440);
            timeFrame.setUuid("2324");
            timeFrameDao.create(timeFrame);

            final Long id = timeFrame.getId();

            TimeFrame loadTimeFrame = timeFrameDao.load(id);
            timeFrameDao.delete(loadTimeFrame);

            timeFrameDao.load(id);
            fail("byl nacteny objekt, ktery mel byt smazany");
        } catch (NoRecordException ex) {
            // v poradku
        } catch (Exception ex) {
            fail("Neco se pokazilo pri mazani", ex);
        }
    }
}
