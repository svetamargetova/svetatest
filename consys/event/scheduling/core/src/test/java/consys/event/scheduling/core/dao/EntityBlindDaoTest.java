package consys.event.scheduling.core.dao;

import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.EntityBlind;
import consys.event.scheduling.core.bo.EntitySubmission;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class EntityBlindDaoTest extends AbstractSchedulingTest {

    private EntityBlindDao entityBlindDao;
    private EntitySubmissionDao entitySubmissionDao;

    @Autowired(required = true)
    public void setEntityBlindDao(EntityBlindDao entityBlindDao) {
        this.entityBlindDao = entityBlindDao;
    }

    @Autowired(required = true)
    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    @Test(groups = {"dao"})
    public void deleteIfBlind() {
        try {
            List<EntityBlind> blinds = entityBlindDao.listAll(EntityBlind.class);
            assertEquals(blinds.size(), 0, "nemaji byt vytvoreny zadne blindy");

            EntityBlind eb = new EntityBlind();
            eb.setFromMinutesInBlock(0);
            eb.setLengthMinutes(20);
            eb.setTitle("Blind");
            eb.setUuid("123-123");
            entityBlindDao.create(eb);

            blinds = entityBlindDao.listAll(EntityBlind.class);
            assertEquals(blinds.size(), 1, "ma byt vytvoren jeden blind");

            List<EntitySubmission> submissions = entitySubmissionDao.listAll(EntitySubmission.class);
            final int defaultSize = submissions.size();
            assertTrue(blinds.size() > 0, "musi existovat nejake submissiony");

            entityBlindDao.deleteIfBlind(submissions.get(0).getId());

            submissions = entitySubmissionDao.listAll(EntitySubmission.class);
            assertEquals(submissions.size(), defaultSize, "byl smazat submission i kdyz nemel byt");

            entityBlindDao.deleteIfBlind(eb.getId());
            
            blinds = entityBlindDao.listAll(EntityBlind.class);
            assertEquals(blinds.size(), 0, "nemaji byt vytvoreny zadne blindy");
        } catch (Exception ex) {
            fail("Neco drblo v EntityBlindDaoTest - deleteIfBlind", ex);
        }
    }
}
