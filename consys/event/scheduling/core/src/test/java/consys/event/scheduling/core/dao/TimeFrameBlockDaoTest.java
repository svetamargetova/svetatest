package consys.event.scheduling.core.dao;

import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.TimeFrame;
import consys.event.scheduling.core.bo.TimeFrameBlock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class TimeFrameBlockDaoTest extends AbstractSchedulingTest {

    // dao
    private RoomDao roomDao;
    private TimeFrameDao timeFrameDao;
    private TimeFrameBlockDao timeFrameBlockDao;

    @Autowired(required = true)
    public void setRoomDao(RoomDao roomDao) {
        this.roomDao = roomDao;
    }

    @Autowired(required = true)
    public void setTimeFrameDao(TimeFrameDao timeFrameDao) {
        this.timeFrameDao = timeFrameDao;
    }

    @Autowired(required = true)
    public void setTimeFrameBlockDao(TimeFrameBlockDao timeFrameBlockDao) {
        this.timeFrameBlockDao = timeFrameBlockDao;
    }

    @Test(groups = {"dao"})
    public void badCreateTimeFrameBlock() {
        TimeFrameBlock timeFrameBlock = new TimeFrameBlock();

        try {
            timeFrameBlockDao.create(timeFrameBlock);
            fail("Vytvoril se blok bez pozadovanych hodnot");
        } catch (Exception ex) {
            // v poradku
        }
    }

    @Test(groups = {"dao"})
    public void createTimeFrameBlock() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);

        TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
        timeFrameBlock.setChair(null);
        timeFrameBlock.setRoom(room);
        timeFrameBlock.setSection(null);
        timeFrameBlock.setTimeFrame(timeFrame);
        timeFrameBlock.setTimeFrom(0);
        timeFrameBlock.setTimeTo(1300);
        try {
            timeFrameBlockDao.create(timeFrameBlock);
        } catch (Exception ex) {
            fail("Nepodarilo se vytvorit blok", ex);
        }
    }

    @Test(groups = {"dao"})
    public void listBlockByTimeFrameId() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);
        final Long timeFrameId = timeFrame.getId();

        for (int i = 0; i < 4; i++) {
            TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
            timeFrameBlock.setChair(null);
            timeFrameBlock.setRoom(room);
            timeFrameBlock.setSection(null);
            timeFrameBlock.setTimeFrame(timeFrame);
            timeFrameBlock.setTimeFrom(0);
            timeFrameBlock.setTimeTo(1300);
            timeFrameBlockDao.create(timeFrameBlock);
        }

        try {
            List<TimeFrameBlock> list = timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);
            assertEquals(list.size(), 4, "Spatna velikost seznamu");
        } catch (Exception ex) {
            fail("Nepodarilo se nacist", ex);
        }
    }

    @Test(groups = {"dao"})
    public void deleteBlocksByIds() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);
        final Long timeFrameId = timeFrame.getId();

        ArrayList<TimeFrameBlock> blocks = new ArrayList<TimeFrameBlock>();
        for (int i = 0; i < 4; i++) {
            TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
            timeFrameBlock.setChair(null);
            timeFrameBlock.setRoom(room);
            timeFrameBlock.setSection(null);
            timeFrameBlock.setTimeFrame(timeFrame);
            timeFrameBlock.setTimeFrom(0);
            timeFrameBlock.setTimeTo(1300);
            timeFrameBlockDao.create(timeFrameBlock);
            if (i < 2) {
                blocks.add(timeFrameBlock);
            }
        }

        try {
            List<TimeFrameBlock> list = timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);
            assertEquals(list.size(), 4, "Spatna velikost seznamu pred smazanim");

            assertEquals(blocks.size(), 2, "Spatna velikost seznamu pro smazani");
            timeFrameBlockDao.delete(blocks);

            list = timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);
            assertEquals(list.size(), 2, "Spatna velikost seznamu po smazani");
        } catch (Exception ex) {
            fail("Nepodarilo se dokoncit test", ex);
        }
    }

    @Test(groups = {"dao"})
    public void updateTime() {
        Room room = new Room();
        room.setName("mistnost 1");
        roomDao.create(room);

        TimeFrame timeFrame = new TimeFrame();
        timeFrame.setDay(new Date());
        timeFrame.setUuid("124124");
        timeFrame.setTimeFrom(0);
        timeFrame.setTimeTo(1300);
        timeFrameDao.create(timeFrame);
        final Long timeFrameId = timeFrame.getId();

        for (int i = 0; i < 4; i++) {
            TimeFrameBlock timeFrameBlock = new TimeFrameBlock();
            timeFrameBlock.setChair(null);
            timeFrameBlock.setRoom(room);
            timeFrameBlock.setSection(null);
            timeFrameBlock.setTimeFrame(timeFrame);
            timeFrameBlock.setTimeFrom(100);
            timeFrameBlock.setTimeTo(600);
            timeFrameBlockDao.create(timeFrameBlock);
        }

        try {
            List<TimeFrameBlock> list = timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);

            for (TimeFrameBlock tfb : list) {
                assertEquals(tfb.getTimeFrom(), 100, "Spatny zacatek bloku");
                assertEquals(tfb.getTimeTo(), 600, "Spatny konec bloku");
            }

            timeFrameBlockDao.updateTime(list, 150, 500);

            list = timeFrameBlockDao.listBlockByTimeFrameId(timeFrameId);

            for (TimeFrameBlock tfb : list) {
                assertEquals(tfb.getTimeFrom(), 150, "Spatny zacatek bloku");
                assertEquals(tfb.getTimeTo(), 500, "Spatny konec bloku");
            }
        } catch (Exception ex) {
            fail("Nepodarilo se dokoncit test", ex);
        }
    }
}
