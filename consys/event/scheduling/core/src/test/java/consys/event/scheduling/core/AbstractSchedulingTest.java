package consys.event.scheduling.core;

import consys.event.common.test.AbstractEventDaoTestCase;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author pepa
 */
@ContextConfiguration(locations = {"classpath:/consys/event/common/core/event-common-spring.xml",
"event-scheduling-spring.xml", "classpath:consys/event/common/test/core/service-mocks.xml",
"classpath:/consys/event/conference/core/event-conference-spring.xml"})
public abstract class AbstractSchedulingTest extends AbstractEventDaoTestCase {
}
