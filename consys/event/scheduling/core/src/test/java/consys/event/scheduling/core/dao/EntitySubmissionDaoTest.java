package consys.event.scheduling.core.dao;

import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.EntitySubmission;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class EntitySubmissionDaoTest extends AbstractSchedulingTest {

    // dao
    private EntitySubmissionDao entitySubmissionDao;

    @Autowired(required = true)
    public void setEntitySubmissionDao(EntitySubmissionDao entitySubmissionDao) {
        this.entitySubmissionDao = entitySubmissionDao;
    }

    @Test(groups = {"dao"})
    public void listBufferSubmission() {
        List<Long> sectionIds = new ArrayList<Long>();
        sectionIds.add(1l);
        sectionIds.add(2l);
        List<EntitySubmission> result = entitySubmissionDao.listBufferSubmission(sectionIds);

        assertNotNull(result);
        assertEquals(result.size(), 3);
    }
}
