package consys.event.scheduling.core.service;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.scheduling.core.AbstractSchedulingTest;
import consys.event.scheduling.core.bo.Room;
import consys.event.scheduling.core.bo.Section;
import consys.event.scheduling.core.bo.TimeFrame;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class SchedulingSettingsServiceTest extends AbstractSchedulingTest {

    private SchedulingSettingsService service;

    @Autowired(required = true)
    public void setSchedulingSettingsService(SchedulingSettingsService service) {
        this.service = service;
    }

    @Test(groups = {"service"})
    public void createRoom() {
        Room room = new Room();

        try {
            service.createRoom(room);
            fail("Chyba vytvoril se nepojmenovany objekt");
        } catch (RequiredPropertyNullException ex) {
            // v poradku
        }

        room.setName("Jmeno");
        try {
            service.createRoom(room);
        } catch (RequiredPropertyNullException ex) {
            fail("Chyba nevytvoril se objekt");
        }
    }

    @Test(groups = {"service"})
    public void createSection() {
        Section section = new Section();

        try {
            service.createSection(section);
            fail("Chyba vytvoril se nepojmenovany objekt");
        } catch (RequiredPropertyNullException ex) {
            // v poradku
        }

        section.setName("Jmeno");
        try {
            service.createSection(section);
        } catch (RequiredPropertyNullException ex) {
            fail("Chyba nevytvoril se objekt");
        }
    }

    @Test(groups = {"service"})
    public void createTimeFrame() {
        TimeFrame timeFrame = new TimeFrame();

        try {
            service.createTimeFrame(timeFrame);
            fail("Chyba vytvoril se objekt bez pozadovanych hodnot");
        } catch (RequiredPropertyNullException ex) {
            // v poradku
        } catch (IllegalPropertyValue ex) {
            fail("Chyba vytvoril se objekt s null hodonotama");
        }

        timeFrame.setDay(new Date());
        timeFrame.setUuid("1234");
        timeFrame.setTimeFrom(-1);
        timeFrame.setTimeTo(1500);
        try {
            service.createTimeFrame(timeFrame);
            fail("Chyba vytvoril se objekt bez pozadovanych hodnot");
        } catch (RequiredPropertyNullException ex) {
            fail("Chyba zadne dalsi hodnoty nejsou vyzadovane");
        } catch (IllegalPropertyValue ex) {
            // v poradku
        }

        timeFrame.setTimeFrom(505);
        timeFrame.setTimeTo(500);
        try {
            service.createTimeFrame(timeFrame);
            fail("Chyba vytvoril se objekt bez pozadovanych hodnot");
        } catch (RequiredPropertyNullException ex) {
            fail("Chyba zadne dalsi hodnoty nejsou vyzadovane");
        } catch (IllegalPropertyValue ex) {
            // v poradku
        }

        timeFrame.setTimeFrom(300);
        timeFrame.setTimeTo(1000);
        try {
            service.createTimeFrame(timeFrame);
        } catch (RequiredPropertyNullException ex) {
            fail("Chyba zadne dalsi hodnoty nejsou vyzadovane");
        } catch (IllegalPropertyValue ex) {
            fail("Chyba nevytvoril se objekt se spravne nastavenymi hodnotami");
        }
    }
}
