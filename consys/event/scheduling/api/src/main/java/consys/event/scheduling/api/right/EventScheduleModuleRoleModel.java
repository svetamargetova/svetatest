package consys.event.scheduling.api.right;

import consys.event.common.api.right.ModuleRoleModel;

/**
 *
 * @author pepa
 */
public class EventScheduleModuleRoleModel extends ModuleRoleModel {

    /** Role ze uzivatel je administrator planovani */
    public static final String RIGHT_SCHEDULING_ADMIN = "RIGHT_SCHEDULING_ADMIN";

    public EventScheduleModuleRoleModel() {
        super(4);
    }

    @Override
    public void registerRights() {
        addRole(RIGHT_SCHEDULING_ADMIN, true);
    }
}
