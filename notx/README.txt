This readmefile explains basic deployment, administration and usage of NotX. The level of abstraction is "development" so reader should be skilled developer and it is assumed that he has fair knowledge about business intent and basic notx terminology. Most of business context is presented in academic paper for example the one presented at FedCSIS.

1 Deployment
  1.1 Required prerequisities
  1.2 Optional prerequisities
  1.3 Pre deploy configuration
  1.4 Deployment process
  1.5 Post deploy configuration
  
2 Administration
3 Usage
  3.1 Populating users
  3.1 Template management 
  

------------------------------
 1.1 Required prerequisities
------------------------------
NotX was tested with following tools and theirs' versions.


Internet connection because NotX uses Amazon SQS it has to be installed on machine with internet connection.
Apache Cassandra 0.8.0 for persistance.
Apache Maven  3.0.3 for compiling the source code.
Java JDK 1.6 installed and JAVA_HOME variable set.
Subversion 1.6 to check out the source code. The best way to deploy NotX today is from source code, because constant bugfixes/update will occur.           
Apache Tomcat 7.0.11 for deploying the application.   
jstl.jar in <TOMCAT>/lib directory. This jar are JSTL tags and must be downloaded separately from Tomcat.
standard.jar which contains jakarta-taglibs 'standard': an implementation of
  JSTL
Amazon SQS credentials this should be typical java property file with 2 keys accessKey and secretKey that is provided to every user of Amazon SQS.
            
------------------------------
 1.2 Optional prerequisities
------------------------------
SMTP server credentials with smtp server details for using the mail engine.
SMSBrana.cz credentials implicit SMS engine provides support for SMS brana and 
only needs username and password provided by the smsbrana.cz.

---------------------------
 1.3 Pre deploy configuration
---------------------------
This chapter identifies all possible places of NotX configuration.

When configuring NotX from source code the most important configurations take place in <SRC>/pom.xml which is the main pom of the application. Before continuing with this guide you should review the pom.xml and see comments for each property there. 

These properties in POM are used solely for filtering web.xml file that is located in <NOTX_WAR>/WEB-INF/web.xml feel free to edit them there if you just want deploy time configuration changes.

Next configuration step is to configure concrete engines you want to use. You have to create file <notx.enginesfolder>/engines.properties. This is standard java property file. List of available configuration options follow: 

    mail.engineClass=net.notx.engine.mail.MailEngine
    mail.correlationClass=net.notx.engine.mail.MailEngineCorrelationProcessor
    mail.smtp.host=<HOST>
    mail.transport.protocol=smtp
    mail.smtp.auth=true
    mail.password=<PASS>
    mail.user=<USERNAME>
    mail.from=<FROMADDRESS>
    mail.return-receipt-email=notxwatchdog@gmail.com
    
    sms.engineClass=net.notx.engine.sms.SmsEngineBrana
    sms.correlationClass=net.notx.engine.sms.SmsEngineBranaCorrelator
    sms.smsbrana.username=<SMSUSERNAME>
    sms.smsbrana.password=<SMSPASSWORD>
  
---------------------
 1.4 Deployment
---------------------
After fully configuring the NotX you can deploy the application
'cd' into <SRC> directory and run 'mvn install -DskipTests' this will compile and package NotX along with the engines, deploy it into Tomcat directory and you are ready to go.

Now you have to start cassandra database, then start tomcat server. Now you can fire up your browser and enter implicit Tomcat's address which is 127.0.0.1:8080 if not configured otherwise. Locate the NotX web application and open it the pattern is following: 
   http://localhost:8080/notx-main-1.0.1-SNAPSHOT/
  
When firstly starting the application click on Recreate Cassandra Keyspace. That will create basic structure of the application and add some test data.

-------------------------------
 1.5 Post deploy configuration
-------------------------------
