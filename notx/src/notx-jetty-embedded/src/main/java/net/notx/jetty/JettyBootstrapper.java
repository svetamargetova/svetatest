package net.notx.jetty;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * http://download.eclipse.org/jetty/stable-7/apidocs/index.html?org/eclipse/jetty/server/Server.html
 * http://download.eclipse.org/jetty/stable-7/apidocs/org/eclipse/jetty/webapp/WebAppContext.html#configure%28%29
 * 
 * @author Filip Nguyen
 *
 */
public class JettyBootstrapper {
	private Server jettyServer ;
	private String webxml;
	private String resourceBase;
	private String contextpath;
	private WebAppContext context = new WebAppContext();
	
	public JettyBootstrapper(int port, String webxml, String resourceBase, String contextpath){
		this.webxml= webxml;
		this.resourceBase = resourceBase;
		this.contextpath = contextpath;
		jettyServer = new Server(port);
		jettyServer.setHandler(context);
	}
	
	public void addOverrideDescriptor(String path) {
		List<String> descriptors = new ArrayList<String>();
		descriptors.add(path);
		context.setOverrideDescriptors(descriptors);
	}
	
	public void start() throws Exception{
		context.setDescriptor(webxml);
		context.setResourceBase(resourceBase);
		context.setContextPath(contextpath);
		context.setParentLoaderPriority(true);
		
		jettyServer.start();
	}
	
	public void stop() throws Exception{
		jettyServer.stop();
	}
}
