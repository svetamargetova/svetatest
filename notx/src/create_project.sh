#!/bin/bash
# This script creates new NotX Maven module


if [ -z "$1" ]; then
	echo "Set new project name:"
	read nname
else 
   nname=$1
fi

mvn archetype:create -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=net.notx -DartifactId=$nname
cd $nname
mvn eclipse:eclipse

