#!/bin/bash
# This script updates version of all NotX maven modules  



if [ -z "$1" ]; then
	echo "Enter new NotX version name (1.0.2-SNAPSHOT):"
	read version
else 
   version=$1
fi


mvn versions:set -DnewVersion=$version
