package net.notx.utils;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import net.notx.client.NotxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlDocument {
	private Document document = null;
	private XPathFactory xpathFactory;
	private DocumentBuilder documentBuilder;
	private static Logger logger = LoggerFactory.getLogger(XmlDocument.class);
	
	
	public void setXpathFactory(XPathFactory xpathFactory) {
		this.xpathFactory = xpathFactory;
	}

	public void setDocumentBuilder(DocumentBuilder documentBuilder) {
		this.documentBuilder = documentBuilder;
	}

	public XmlDocument(Document document) {
		this.document = document;
	}

	public XmlDocument(String content) {
		try {
			logger.debug("Loading xml content: "+ content);
			documentBuilder = DocumentBuilderFactory.newInstance()
			.newDocumentBuilder();
			document = documentBuilder.parse(new InputSource(new StringReader(
					content)));
		
				xpathFactory = XPathFactory.newInstance();
		} catch (Exception ex) {
			throw new NotxException(
					"Error while creating xmldocument from string", ex);
		}
	}

	public List<XmlDocument> getElements(String xpathString) {
		XPath xpath = xpathFactory.newXPath();
		List<XmlDocument> documentList = new ArrayList<XmlDocument>();
		XPathExpression xpathExpr;
		try {
			xpathExpr = xpath.compile(xpathString);

			NodeList nodeListResult = null;
			nodeListResult = (NodeList) xpathExpr.evaluate(document,
					XPathConstants.NODESET);

			for (int i = 0; i < nodeListResult.getLength(); i++) {
				Document document = documentBuilder.newDocument();
				Node importedNode = document.importNode(nodeListResult.item(i),
						true);
				document.appendChild(importedNode);
				XmlDocument doc = new XmlDocument(document);
				doc.setDocumentBuilder(documentBuilder);
				doc.setXpathFactory(xpathFactory);
				documentList.add(doc);
			}
		} catch (XPathExpressionException e) {
			throw new NotxException("Error while getting elements using xpath",
					e);
		}
		return documentList;
	}

	public String getTextValue(String xpathString) {
		try {
			XPath xpath = xpathFactory.newXPath();
			XPathExpression xpathExpr = xpath.compile(xpathString);

			String value = ((String) xpathExpr.evaluate(document, XPathConstants.STRING)).trim();
			
			logger.debug("Text value for xpath ["+xpathString+ "] is: "+ value);
			if (value.equals(""))
				return null;
			else return value;
		} catch (XPathExpressionException e) {
			throw new NotxException("Error while getting elements using xpath",
					e);
		}

	}

}
