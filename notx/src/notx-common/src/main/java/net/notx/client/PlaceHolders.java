package net.notx.client;

import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 * POJO for JAXB.
 *
 * Wrapper object for Map of place holders that are inserted into templates. 
 * 
 */
public final class PlaceHolders {

    private final HashMap<String, String> placeHolderVals = Maps.<String, String>newHashMap();

    public PlaceHolders() {
    }

    public PlaceHolders(String... tuples) {        
        if (tuples != null && tuples.length > 0) {
            if (tuples.length % 2 != 0) {
                throw new IllegalArgumentException("Tuples should be inserted in pairs: [key,value]");
            }
            for (int i = 0; i < tuples.length; i += 2) {
                addPlaceHolder(tuples[i], tuples[i + 1]);
            }

        }
    }



    /**
     * @return the placeHolderVals
     */
    public HashMap<String, String> getPlaceHolderVals() {
        return placeHolderVals;
    }

    public PlaceHolders addPlaceHolder(String string, String string2) {
        placeHolderVals.put(string, string2);
        return this;
    }
}
