package net.notx.client.message;

import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * <p>This class is special case of {@link AbstractNotificationMessage}. In
 * opposite of {@link NotificationMessage}, is this message used for notify
 * concrete one anonymous recipient. This class represents discrete notification
 * for one and only one person. </p>
 *
 * <p>This notification message should be used very rarely. Advantage is that
 * target of notification (user) don't have to be regular user. This
 * notification is recorded but not paired with some group.</p>
 *
 * <p>Properties that have to be filled are used for replacing default user
 * routing records.</p>
 */
public class AnonymousNotificationMessage extends AbstractNotificationMessage {

    private static final long serialVersionUID = -1233410474911901235L;
    /**
     * Contact that will be used for engine.
     */
    private String contact;
    /**
     * Optional engine name.
     */
    private String engine;
    /**
     * Language. If no lang then default lang will be selected.
     */
    private String lang;

    public AnonymousNotificationMessage(String contact, String engine, String lang, String msgType, String templateName, Map<String, String> placeholders) {
        super(msgType, templateName, placeholders);
        this.contact = contact;
        this.engine = engine;
        this.lang = lang;
    }

    public AnonymousNotificationMessage(String contact, String msgType, String templateName, Map<String, String> placeholders) {
        super(msgType, templateName, placeholders);
        this.contact = contact;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AnonymousNotificationMessage other = (AnonymousNotificationMessage) obj;
        return new EqualsBuilder().appendSuper(super.equals(obj)).
                append(getLang(), other.getLang()).
                append(getContact(), other.getContact()).
                append(getEngine(), other.getEngine()).
                isEquals();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.contact != null ? this.contact.hashCode() : 0);
        hash = 79 * hash + (this.engine != null ? this.engine.hashCode() : 0);
        hash = 79 * hash + (this.lang != null ? this.lang.hashCode() : 0);
        hash = 79 * hash + super.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return String.format("AnonymousNotificationMessage [uuid=%s contact=%s, lang=%s, engine=%s, msgType=%s, templateName=%s, placeHolderVals=%s]",
                getMsgUuid(), getContact(), getLang(), getEngine(), getMsgType(), getTemplateName(), getPlaceHolderVals());
    }

    /**
     * Contact that will be used for engine.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * Optional engine name.
     *
     * @return the engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * Language. If no lang then default lang will be selected.
     *
     * @return the lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * Language. If no lang then default lang will be selected.
     *
     * @param lang the lang to set
     */
    public void setLang(String lang) {
        this.lang = lang;
    }
}
