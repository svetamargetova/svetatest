package net.notx.utils;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Wrapper mainly for web pres (see formatters and converters)
 * @author kalaz
 *
 */
public class StringMap extends HashMap<String,String> implements Serializable {

	private static final long serialVersionUID = -5337651361786244760L;

}
