package net.notx.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import net.notx.client.NotxException;

/**
 * Helper to load property file from classpath.
 *
 * @author Filip Nguyen
 *
 * TODO to delete
 *
 */
public class PropertyLoader {

    @Deprecated
    public Properties getPropertyFile(String name) throws NotxException {

        InputStream inStream = null;
        try {
            inStream = this.getClass().getClassLoader().getResourceAsStream(name);

            Properties properties = new Properties();

            properties.load(inStream);
            return properties;
        } catch (IOException e) {
            throw new NotxException("Error loading property file " + name,
                    e);
        } finally {
            try {
                if (inStream != null) {
                    inStream.close();
                }
            } catch (IOException e) {
            }
        }
    }
}
