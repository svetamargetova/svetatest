/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.client.message;

import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * This message is endpoint message that holds all properties needed for notification resolution.
 * @author palo
 */
public class UserNotificationMessage extends AbstractNotificationMessage{
    private static final long serialVersionUID = -6029189994650987442L;
    /**
     * User for which this notification will be processed.
     */
    private String userId;

    public UserNotificationMessage(String userId, String msgType, String templateName, Map<String, String> placeholders) {
        super(msgType, templateName, placeholders);
        this.userId = userId;
    }

    /**
     * User for which this notification will be processed.
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * User for which this notification will be processed.
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
     @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserNotificationMessage other = (UserNotificationMessage) obj;
        return new EqualsBuilder().appendSuper(super.equals(obj)).
                append(getUserId(), other.getUserId()).
                isEquals();
    }

    @Override
    public int hashCode() {
        int hash = 6;
        hash = 17 * hash + (this.getUserId() != null ? this.getUserId().hashCode() : 0);
        hash = 17 * hash + super.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        return String.format("UserNotificationMessage [uuid=%s uid=%s, msgType=%s, templateName=%s, placeHolderVals=%s]",
                getMsgUuid(), getUserId(), getMsgType(), getTemplateName(), getPlaceHolderVals());
    }
    
    
    
    
    
}
