package net.notx.client.message;

import org.apache.commons.lang.builder.EqualsBuilder;

public class TagMessage extends AbstractMessage {

    private static final long serialVersionUID = 6981989213808809856L;
    private String userid;
    private String tag;

    public TagMessage(String userId, String tag) {
        this.userid = userId;
        this.tag = tag;       
    }

    public String getUserid() {
        return userid;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return String.format("Tag: userid: %s tag: %s", userid, tag);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tag == null) ? 0 : tag.hashCode());
        result = prime * result + ((userid == null) ? 0 : userid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // if both are referring to the same object
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        TagMessage other = (TagMessage) obj;
        return new EqualsBuilder().
                append(tag, other.tag).
                append(userid, other.userid).
                isEquals();
    }
}
