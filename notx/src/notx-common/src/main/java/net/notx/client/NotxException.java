package net.notx.client;

/**
 * Base exception for all exceptions in NotX.
 * @author kalaz
 *
 */
public class NotxException extends RuntimeException {
 
	public NotxException(String msg, Throwable cause){
		super(msg,cause);
	}
	
	public NotxException(String msg){
		super(msg);
	}
}
