package net.notx.client.message;

import java.util.Date;

/**
 * Correlation message 
 * @author Filip
 */
public class CorrelationMessage extends AbstractMessage {

    private static final long serialVersionUID = 5561193926134290427L;
    /**
     * Unique identifier of correlation ID
     */
    private String correlationID;
    /**
     * Engine name
     */
    private String engine;
    /**
     * Date of correlation
     */
    private Date correlationTime;
    /**
     * Date when correlation was received
     */
    private Date receivedTime;

    /**
     * Unique identifier of correlation ID
     *
     * @return the correlationID
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Unique identifier of correlation ID
     *
     * @param correlationID the correlationID to set
     */
    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    /**
     * Engine name
     *
     * @return the engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * Engine name
     *
     * @param engine the engine to set
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * Date of correlation
     *
     * @return the correlationTime
     */
    public Date getCorrelationTime() {
        return correlationTime;
    }

    /**
     * Date of correlation
     *
     * @param correlationTime the correlationTime to set
     */
    public void setCorrelationTime(Date correlationTime) {
        this.correlationTime = correlationTime;
    }

    /**
     * Date when correlation was received
     *
     * @return the receivedTime
     */
    public Date getReceivedTime() {
        return receivedTime;
    }

    /**
     * Date when correlation was received
     *
     * @param receivedTime the receivedTime to set
     */
    public void setReceivedTime(Date receivedTime) {
        this.receivedTime = receivedTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // if both are referring to the same object
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        CorrelationMessage m = (CorrelationMessage)obj;
        return m.correlationID.equals(correlationID);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (this.correlationID != null ? this.correlationID.hashCode() : 0);
        return hash;
    }
}
