/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.client.message;

import com.google.common.collect.Maps;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Abstract notification message that holds basic notification properties.
 * Subclasses implements concrete notification process type.
 * @see AnonymousNotificationMessage
 * @see UserNotificationMessage
 * @see NotificationMessage
 *
 * @author palo
 */
public class AbstractNotificationMessage extends AbstractMessage {

    private static final long serialVersionUID = -8757805814127024740L;
    /**
     * Type of message. Users of NotX can define its own message types. Message
     * type can be defined for example by source of message: system,
     * administrator, friend. Another example can be message types defined by
     * type of source device: sensors, camera, security administrator.
     */
    private String msgType;
    /**
     * Name of template defines concrete content of notification. For example:
     * registration, activation.
     */
    private String templateName;
    /**
     * NotX itself has many built-in place holders, but in most cases source
     * system that creates notification request have to set placeholders that
     * are predefined in templates. When creating templates in NotX remember all
     * placeholders that you have used and paste this placeholders in source
     * system into message.
     */
    private Map<String, String> placeHolderVals = new HashMap<String, String>();

     
    public AbstractNotificationMessage(String msgType, String templateName) {
        this.msgType = msgType;
        this.templateName = templateName;
    }

    public AbstractNotificationMessage(String msgType, String templateName, Map<String, String> placeholders) {
        this.msgType = msgType;
        this.templateName = templateName;
        this.placeHolderVals = placeholders;
    }

    /**
     * Type of message. Users of NotX can define its own message types. Message
     * type can be defined for example by source of message: system,
     * administrator, friend. Another example can be message types defined by
     * type of source device: sensors, camera, security administrator.
     *
     * @return the msgType
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     * Type of message. Users of NotX can define its own message types. Message
     * type can be defined for example by source of message: system,
     * administrator, friend. Another example can be message types defined by
     * type of source device: sensors, camera, security administrator.
     *
     * @param msgType the msgType to set
     */
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     * Name of template defines concrete content of notification. For example:
     * registration, activation.
     *
     * @return the templateName
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Name of template defines concrete content of notification. For example:
     * registration, activation.
     *
     * @param templateName the templateName to set
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    /**
     * NotX itself has many built-in place holders, but in most cases source
     * system that creates notification request have to set placeholders that
     * are predefined in templates. When creating templates in NotX remember all
     * placeholders that you have used and paste this placeholders in source
     * system into message.
     *
     * @return the placeHolderVals
     */
    public Map<String, String> getPlaceHolderVals() {
        if (placeHolderVals == null) {
            placeHolderVals = Maps.newHashMap();
        }
        return placeHolderVals;
    }

    /**
     * NotX itself has many built-in place holders, but in most cases source
     * system that creates notification request have to set placeholders that
     * are predefined in templates. When creating templates in NotX remember all
     * placeholders that you have used and paste this placeholders in source
     * system into message.
     *
     * @param placeHolderVals the placeHolderVals to set
     */
    public void putPlaceHolder(String key, String value) {
        getPlaceHolderVals().put(key, value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof AbstractNotificationMessage) {
            return false;
        }
        AbstractNotificationMessage other = (AbstractNotificationMessage) obj;
        return new EqualsBuilder().append(msgType, other.msgType).
                append(placeHolderVals, other.placeHolderVals).
                append(templateName, other.templateName).
                isEquals();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.msgType != null ? this.msgType.hashCode() : 0);
        hash = 47 * hash + (this.templateName != null ? this.templateName.hashCode() : 0);
        hash = 47 * hash + (this.placeHolderVals != null ? this.placeHolderVals.hashCode() : 0);
        return hash;
    }
}
