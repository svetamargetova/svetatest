package net.notx.client.message;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Composite tag message - contains multiply tags.
 * @author palo
 */
public class TagsMessage extends AbstractMessage {

    private static final long serialVersionUID = 6981989213808809856L;
    private String userid;
    private String[] tags;

    public TagsMessage(String userId, String[] tags) {
        this.userid = userId;
        this.tags = tags;        
    }

    public String getUserid() {
        return userid;
    }

    public String[] getTags() {
        return tags;
    }        

    @Override
    public String toString() {
        return String.format("Tags: userid: %s tags: %s", userid, StringUtils.join(tags));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        result = prime * result + ((userid == null) ? 0 : userid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // if both are referring to the same object
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        TagsMessage other = (TagsMessage) obj;
        return new EqualsBuilder().
                append(tags, other.tags).
                append(userid, other.userid).
                isEquals();
    }
}
