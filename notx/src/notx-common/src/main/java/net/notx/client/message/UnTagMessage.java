package net.notx.client.message;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * This message will remove user from tag group.
 * @author palo
 */
public class UnTagMessage extends AbstractMessage {

    private static final long serialVersionUID = 3881708833501549050L;
    private String userid;
    private String tag;

    public UnTagMessage(String userId, String tag) {
        this.userid = userId;
        this.tag = tag;        
    }

    public String getUserid() {
        return userid;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return String.format("UnTag: userid: %s tag: %s ", userid ,tag);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tag == null) ? 0 : tag.hashCode());
        result = prime * result + ((userid == null) ? 0 : userid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // if both are referring to the same object
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        UnTagMessage other = (UnTagMessage) obj;
        return new EqualsBuilder().append(tag, other.tag).append(userid, other.userid).isEquals();        
    }
}
