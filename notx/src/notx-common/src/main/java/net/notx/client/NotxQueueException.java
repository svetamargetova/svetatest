/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.client;

/**
 *
 * @author palo
 */
public class NotxQueueException extends NotxException{
    private static final long serialVersionUID = 96406584214023299L;

    public NotxQueueException(String msg) {
        super(msg);
    }

    public NotxQueueException(String msg, Throwable cause) {
        super(msg, cause);
    }        
    
}
