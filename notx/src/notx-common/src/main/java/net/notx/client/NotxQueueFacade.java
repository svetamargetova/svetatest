package net.notx.client;


/**
 * This class is java interface into NotX. NotX leverages many interfaces and this facade 
 * reuses the Web Services API
 * 
 * @author Filip Nguyen
 * 
 * @since 2.0 WebService implementation is placed in its own module notx-client-ws
 *
 */
public interface NotxQueueFacade {
	        
	            
	public void tag(String userId, String ...tags);
	
	public void unTag(String userId, String ...tags);
	
	public void sendNotification(String tag, String msgType, String templateName,PlaceHolders placeholders);
}
