package net.notx.client.message;

import java.io.Serializable;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * <p>This class represents error that occurs when any kind of Handling of jms
 * message fails. When this happens usually the message is pushed into fail
 * column family. It is important that the fail occured while handling JMS
 * message because this fail is then used to generate web reports with option to
 * resend the JMS that caused the error back into the queue. This way one can
 * test that bugfix repaired the problem that caused initial problem. </p>
 */
public class HandlingFailMessage extends AbstractMessage {

    private static final long serialVersionUID = -2983410474911901235L;
    private Exception exception;
    private Serializable sqsMessage;
    private String errorCause = "";
    /**
     * Automatically generated unique id.
     */
    private String uid;

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return this.uid;
    }

    public String getType() {
        return sqsMessage.getClass().getName();
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Serializable getSqsMessage() {
        return sqsMessage;
    }

    public void setSqsMessage(Serializable notificationMessage) {
        this.sqsMessage = notificationMessage;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(String errorCause) {
        this.errorCause = errorCause;
    }

    public String getExceptionInfo() {
        if (exception == null) {
            return "There is no exception";
        }
        StringBuilder sb = new StringBuilder(exception.getClass().getName());
        return sb.append(":").append(exception.getMessage()).toString();
    }

    public String getStackTrace() {
        return ExceptionUtils.getFullStackTrace(exception);
    }

    public HandlingFailMessage() {
    }
}
