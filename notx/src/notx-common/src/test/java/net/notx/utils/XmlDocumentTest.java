package net.notx.utils;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import junit.framework.Assert;
import net.notx.utils.XmlDocument;

import org.junit.Test;
import org.xml.sax.SAXException;

public class XmlDocumentTest {

    @Test
    public void xmldocument() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

        XmlDocument xml = new XmlDocument("<result>\n"
                + "<inbox>\n"
                + "<delivery_sms>\n"
                + "<item>\n"
                + "<number>+420774884136</number>\n"
                + "<time>20091020T164508</time>\n"
                + "<message>test</message>\n"
                + "</item>\n"
                + "</delivery_sms>\n"
                + "<delivery_report>\n"
                + "<item>\n"
                + "<idsms>377339</idsms>\n"
                + "<number>+420774884136</number>\n"
                + "<time>20091020T132528</time>\n"
                + "<status>1</status>\n"
                + "</item>\n"
                + "<item>\n"
                + "<idsms>377340</idsms>\n"
                + "<number>+420774884136</number>\n"
                + "<time>20091020T181102</time>\n"
                + "<status>1</status>\n"
                + "</item>\n"
                + "</delivery_report>\n"
                + "</inbox>\n"
                + "</result>");

        List<XmlDocument> deliveredMsgs = xml.getElements("/result/inbox/delivery_report/item");
        Assert.assertEquals("377339", deliveredMsgs.get(0).getTextValue("/item/idsms"));
        Assert.assertEquals(null, deliveredMsgs.get(0).getTextValue("/item/idsmfsds"));
    }
}
