/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.config;

import java.util.Map;
import org.junit.Test;
import static  org.junit.Assert.*;

/**
 *
 * @author palo
 */
public class NotxPropertiesConfigTests {
    
    
    @Test
    public void testPropertiesConfig(){
        NotXPropertiesConfig config = new NotXPropertiesConfig("classpath:/path");
        assertEquals("1.0.0", config.getVersion());
        assertEquals("1", config.getModuleProperty("notx", "number"));
        assertEquals(1, config.getIntModuleProperty("notx", "number"));
        Map<String,String> engineProps = config.getModuleProperties("engineX");
        assertEquals(3, engineProps.size());
        assertEquals("unique name", engineProps.get("engineX.string"));
        assertEquals("engines", engineProps.get("engineX.string.string"));
        assertEquals("true", engineProps.get("engineX.boolean"));
        
        
    }
}
