package net.notx.core.entities;

import java.io.Serializable;
import net.notx.client.message.AnonymousNotificationMessage;
import net.notx.client.message.NotificationMessage;
import net.notx.client.message.UserNotificationMessage;

/**
 * <p>Routing record is declarative way to specify rule how notifications should
 * be routed to theirs' destinations. </p>
 *
 * An example of routing record: <ul> <li>priority: 3 </li> <li>userid:
 * null</li> <li>source: null</li> <li>msgtype: important</li> <li>destination:
 * mail+sms</li> </ul> <p>This routing record will ensure that for all users
 * (userid null) any notification with msgType "important" will be routed into
 * mail+sms engine. The priority of this record is 3 thus if there is no record
 * of higher priority (1 highest 10 lowest) the notification will be routed
 * using this record.</p>
 *
 * Another example of routing record: <ul> <li>priority: 2</li> <li>userid:
 * 2323</li> <li>source: ConferenceA</li> <li>msgtype: important</li>
 * <li>destination:mail</li> </ul> <p>This record is usually set by user with id
 * 2323 himself (he has user interface to do that). This routing record will
 * ensure that all messages to tag ConferenceA that are of messageType important
 * will be routed to mail.</p>
 *
 * <p>For each notification up to 1 routing record is used according to
 * priority. The priority part of record is there for better extensibility of
 * the system but shouldn't be used too intesively so as not to add too complex
 * rules that would be hard to follow by user.</p>
 *
 * @author Filip Nguyen
 *
 * TODO: we should provide interface for chache system (Memcache or similar)
 * where routing records will be stored because routing record is loaded every
 * time
 *
 */
public class RoutingRecord implements Serializable, Comparable<RoutingRecord> {

    private static final long serialVersionUID = -714949900986426731L;
    private String userid;
    private String id;
    /**
     * 1 highest 10 lowest
     */
    private int priority;
    private String source;
    private String destination;
    private String msgtype;

    /**
     * @return Destination split into discrete engines. For example mail+sms
     * will return collection of 2 elements: mail, sms.
     */
    public String[] getDestinationEngines() {
        if (getDestination() == null || getDestination().equals("")) {
            return new String[0];
        }

        return getDestination().split("\\+");
    }

    /**
     * Finds out whether message can be routed with this routing record. This
     * method doesn't take into account the priority of routing record it just
     * says that routing record can be used.
     */
    public boolean canRouteMessage(UserNotificationMessage notification) {
        if (userid != null && !userid.trim().equals("") && !notification.getUserId().equals(userid)) {
            return false;
        }

        if (msgtype != null && !msgtype.trim().equals("") && !notification.getMsgType().equals(msgtype)) {
            return false;
        }

//        if (source != null && !source.trim().equals("") && !notification.getTo().equals(source)) {
//            return false;
//        }

        return true;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RoutingRecord other = (RoutingRecord) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public String getRecordid() {
        return id;
    }

    public void setRecordid(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RoutingRecord [userid=" + userid + ", id=" + id + ", priority="
                + priority + ", source=" + source + ", destination="
                + destination + ", msgtype=" + msgtype + "]";
    }

    @Override
    public int compareTo(RoutingRecord o) {
        return this.priority - o.getPriority();
    }
}
