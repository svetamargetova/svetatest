package net.notx.core.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import net.notx.utils.StringMap;

public class User implements Serializable {

    private static final long serialVersionUID = 464767566634438099L;
    private String id;
    private String name;
    private String lang;
    private StringMap contacts = new StringMap();
    private Set<String> tags = new HashSet<String>();

    public void setContacts(StringMap contacts) {
        this.contacts = contacts;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void addContact(String contactID, String contact) {
        if (getContacts().containsKey(contactID)) {
            contacts.remove(contactID);
        }

        contacts.put(contactID, contact);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof User)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return ((User) obj).id.equals(id);
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return 0;
        }

        return getId().hashCode();
    }

    public StringMap getContacts() {
        if (contacts == null) {
            contacts = new StringMap();
        }
        return contacts;
    }

    public String getContact(String string) {
        if (getContacts().containsKey(string)) {
            return contacts.get(string);
        }
        return null;


    }

    public String[][] getAllContactsAsStrings() {
        String[][] contacts = new String[getContacts().size()][2];
        int i = 0;
        for (Entry<String, String> contact : getContacts().entrySet()) {
            contacts[i][0] = contact.getKey();
            contacts[i][1] = contact.getValue();
            i++;
        }

        return contacts;
    }

    @Override
    public String toString() {
        return String.format("User [id:%s, name:%s, #cont.: %s]", getId(), getName(), contacts.size());
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the tags
     */
    public Set<String> getTags() {
        return tags;
    }
}
