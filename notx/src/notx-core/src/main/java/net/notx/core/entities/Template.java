package net.notx.core.entities;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.antlr.stringtemplate.StringTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Parts of message that makes a whole notification.</p> <p>Lets take email
 * as an example. Each email consists of <tt>subject</tt>, <tt>html_body</tt>,
 * <tt>text_body</tt>. This triplet is invariably "one thing" because all three
 * has to be changed for each language. Further more this triplet is viable only
 * for one engine, for sms there may be exactly 1 message part -
 * <tt>body</tt></p>
 *
 * @author Filip Nguyen
 *
 */
public class Template implements Serializable {

    private static final long serialVersionUID = -140453135566818196L;
    private static final Logger logger = LoggerFactory.getLogger(Template.class);
    protected Map<String, String> parts = new HashMap<String, String>();
    protected Map<String, String> placeholders;

    /**
     * Injects place-holders into each message part using ANTLR StringTemplate
     * library.
     */
    public void inject(Map<String, String> placeholders) {
        // we save a placeholders copy
        this.placeholders = placeholders;

        for (Entry<String, String> part : parts.entrySet()) {
            final StringTemplate template = new StringTemplate(part.getValue());

            if (placeholders != null) {
                for (Entry<String, String> placeholder : placeholders.entrySet()) {
                    template.setAttribute(placeholder.getKey(), placeholder.getValue());
                }
            }

            part.setValue(template.toString());
        }
    }

    public void put(String partName, String partValue) {
        if (parts.containsKey(partName)) {
            parts.remove(partName);
        }

        parts.put(partName, partValue);

    }

    public Map<String, String> getTemplateParts() {
        return parts;
    }

    public void setTemplateParts(Map<String, String> templateParts) {
        parts = templateParts;
    }

    public String get(String string) {
        return parts.get(string);
    }

    public int size() {
        return parts.size();
    }

    public String getPlaceholder(String key) {
        if (placeholders == null) {
            return null;
        } else {
            return placeholders.get(key);
        }
    }

    public ImmutableList<String> getPlaceholdersWithPrefix(String prefix) {
        if (placeholders == null) {
            return null;
        }
        ImmutableList.Builder<String> b = ImmutableList.builder();
        for (Map.Entry<String, String> e : placeholders.entrySet()) {
            if (e.getKey().startsWith(prefix)) {
                b.add(e.getValue());
            }
        }
        return b.build();
    }
}
