package net.notx.core.entities;

import java.util.*;
import net.notx.client.message.UserNotificationMessage;


/**
 * <p>
 * Routing record collection is just collection of routing records. This
 * collection itself isn't saved in Cassandra DB (hence the constructor) but 
 * is used as business logic object. </p>
 * @author Filip Nguyen
 * 
 * TODO - we should consider whether to create some best routing record hash for 
 * notification and put this hash into cache
 *
 */
public class RoutingRecordCollection {
	private Set<RoutingRecord> records = new HashSet<RoutingRecord>();
	
	
	public RoutingRecordCollection(){
	}
	
	/**
	 * <p>Inspects all records of this collection and finds out if the message is 
	 * routable and takes priority of record into account.</p>
	 * 
	 * @return Routing record that has highest priority (1 highest, 10 lowest) and that can be used 
	 * to route the msg. Null if there is no record that can route the msg. 
	 */
	public RoutingRecord getBestRoutingRecord(UserNotificationMessage msg){	
		RoutingRecord best= null;
		for(RoutingRecord record : records){
			if (record.canRouteMessage(msg))
				if (best == null || record.getPriority() < best.getPriority())
					best = record;
		}		
		return best;
	}

	public void add(RoutingRecord value) {
		records.add(value);		
	}

	public int size() {
		return records.size();
	}
        
        public boolean isEmpty() {
		return records.isEmpty();
	}

	public SortedSet<RoutingRecord> toListByPriority() {
		return (new TreeSet<RoutingRecord>(records));
	}

	public void addAll(List<RoutingRecord> findRecordsFor) {
		records.addAll(findRecordsFor);
	}
	
}
