/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.service;

import java.util.List;
import net.notx.core.entities.SuperTemplate;

/**
 *
 * @author palo
 */
public interface TemplateService {
    public SuperTemplate loadTemplate(String templateName);
    
    public void createTemplate(SuperTemplate template);
    
    public void deleteTemplate(SuperTemplate template);
    
    public List<SuperTemplate> listTemplates();
    
    
    
}
