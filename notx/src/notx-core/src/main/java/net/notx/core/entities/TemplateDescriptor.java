package net.notx.core.entities;

import java.io.Serializable;

/**
 * This descriptor identifies uniquely one Template. Suppose programmer needs
 * template for registration_approval for voice engine in english language. This
 * descriptor allows him to specify exactly that and then he can find this
 * specific Template.
 *
 * TODO jak se to zachova pokud nebude pro dany engine nebo lang dany template?
 * Vymyslet default.
 */
public class TemplateDescriptor implements Serializable {

    private static final long serialVersionUID = -168604341126463443L;
    private String lang = "";
    private String engine = "";

    public TemplateDescriptor(String lang, String engine) {
        this.lang = lang;
        this.engine = engine;
    }

    public TemplateDescriptor() {
        // TODO Auto-generated constructor stub
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof TemplateDescriptor)) {
            return false;
        }        

        TemplateDescriptor t = (TemplateDescriptor) o;
        return t.lang.equals(lang) && t.engine.equals(engine);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.lang != null ? this.lang.hashCode() : 0);
        hash = 97 * hash + (this.engine != null ? this.engine.hashCode() : 0);
        return hash;
    }

    /**
     * Use {@link #setEngine(java.lang.String)}
     */
    @Deprecated 
    public void setEngineName(String engineName) {
        this.engine = engineName;
    }

    @Override
    public String toString() {
        return String.format("TemplateDescriptor [lang=%s, engine=%s]",lang, engine);
    }
}
