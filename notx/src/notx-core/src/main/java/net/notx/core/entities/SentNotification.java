package net.notx.core.entities;

import java.io.Serializable;
import java.util.Date;

import net.notx.client.message.NotificationMessage;

/**
 * This class represent statistical entry for one sent notification.
 *
 */
public class SentNotification implements Serializable, Comparable<SentNotification> {

    /**
     *
     */
    private static final long serialVersionUID = -1178176004714182316L;
    private String userName;
    private String uid;
    private String forTag;
    private Date time;
    private String lang;
    private String engine;
    private String contact;
    private Date correlationTime;
    private Date receivedTime;
    private String correlationID;

    /**
     * Domain is entity identifiable in Tag before first dot. For example in tag
     * Conference1.Attendees.NotPresent the domain is "Conference1". Domains are
     * used to create statistics about usage of NotX.
     *
     * @return String value of domain if it is present, NO_DOMAIN otherwise.
     */
    public String getDomain() {
        if (forTag != null && forTag.indexOf('.') != -1) {
            return forTag.substring(0, forTag.indexOf('.'));
        }

        return "NO_DOMAIN";
    }

    public Date getCorrelationTime() {
        return correlationTime;
    }

    public void setCorrelationTime(Date correlationTime) {
        this.correlationTime = correlationTime;
    }

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getForTag() {
        return forTag;
    }

    public void setForTag(String forTag) {
        this.forTag = forTag;
    }

    public Date getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(Date receivedTime) {
        this.receivedTime = receivedTime;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((correlationID == null) ? 0 : correlationID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SentNotification other = (SentNotification) obj;
        if (correlationID == null) {
            if (other.correlationID != null) {
                return false;
            }
        } else if (!correlationID.equals(other.correlationID)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SentNotification o) {
        if (time == null || o.time == null || time.compareTo(time) == 0) {
            return correlationID.compareTo(o.correlationID);
        }
        return -time.compareTo(o.time);
    }
}
