/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.service;

/**
 * Service provide interface for initialize underlying persistence service. 
 * @author palo
 */
public interface InitService {
 
    public boolean isInitialized();
    
    public void initialize();
    
}
