/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import net.notx.client.message.HandlingFailMessage;
import net.notx.core.entities.SentNotification;

/**
 *
 * @author palo
 */
public interface StatsService {

    public void notificationSent(SentNotification notification);

    public Map<String, Set<SentNotification>> loadStatsByDomain();

    public List<HandlingFailMessage> listFailMessages();

    public HandlingFailMessage loadFailMessages(String id);

    public void deleteFailMessage(HandlingFailMessage failMessage);

    public void createFailMessage(HandlingFailMessage failMessage);
}
