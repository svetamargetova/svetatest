package net.notx;

import net.notx.client.NotxException;

public class JmsHandlingFailException extends NotxException{

	public JmsHandlingFailException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public JmsHandlingFailException(String msg) {
		super(msg);
	}

}
