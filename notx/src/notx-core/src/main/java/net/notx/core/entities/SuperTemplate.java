package net.notx.core.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.notx.client.NotxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Easiest way to explain supertemplate is by example. Suppose NotX want's
 * send greetings to user when he/she arrives to conference. NotX is expected to
 * be able to send this greetings in various formats (mail, voice, sms) and in
 * various languages, but semanitcs of this greetings message is all the same,
 * it is just sort of message that we could call "greetings_on_conference". </p>
 * <p> Here we are getting to the notion of SuperTemplate. SuperTemplate is
 * basically a collection of all combinations for language and format (engine)
 * of the message. Thus SuperTemplate consists of Templates whereas the Template
 * is aimed for specific language and engine. </p>
 */
public class SuperTemplate implements Serializable {

    private static final long serialVersionUID = -6605361717150127241L;
    private static Logger logger = LoggerFactory.getLogger(SuperTemplate.class);
    private String name;
    private String description;
    private Map<TemplateDescriptor, Template> templates = new HashMap<TemplateDescriptor, Template>();

    public Template getTemplate(TemplateDescriptor descriptor) {
        if (!templates.containsKey(descriptor)) {
            logger.warn("There is no template in language of recipient, reverting to implicit english language.");
            descriptor.setLang("en");
            if (!templates.containsKey(descriptor)) {
                throw new NotxException("There is no template for " + name + ": " + descriptor);
            }
        }
        return templates.get(descriptor);
    }

    public void addTemplate(TemplateDescriptor descriptor, Template template) {
        if (templates.containsKey(descriptor)) {
            templates.remove(descriptor);
        }

        templates.put(descriptor, template);
    }

    public List<TemplateDescriptor> getVersions() {
        return new ArrayList<TemplateDescriptor>(templates.keySet());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }

    public void remove(TemplateDescriptor descriptor) {
        templates.remove(descriptor);
    }
}
