/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import net.notx.utils.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of NotX configuration that parses values from .properties
 * file.
 *
 * @author palo
 */
public class NotXPropertiesConfig implements NotxConfig {

    
    private static final Logger logger = LoggerFactory.getLogger(NotXPropertiesConfig.class);
    private final Properties settings;

    /**
     * Constructs NotX configuration by reading notx.properties from
     * <code>notxHomePath</code>/conf/config.properties
     *
     * @param notxHomePath
     */
    public NotXPropertiesConfig(String notxHomePath) {
        try {
            if (StringUtils.isBlank(notxHomePath)) {
                throw new NullPointerException("NotX home path is not set! Please provide path");
            }

            InputStream is = null;

            settings = new Properties();
            
            String home = getConfigPropertiesPath(notxHomePath);
                       
            logger.info("Configuration path: notx.home:{}", home);
            settings.load(FileUtils.getStreamFor(home));

            if (logger.isDebugEnabled()) {
                logger.debug("Loaded properties:");
                for (Map.Entry<Object, Object> e : settings.entrySet()) {
                    logger.debug("Property: {} = {} ", e.getKey(), e.getValue());
                }
            }

            // inject notx.home folder            
            settings.setProperty(propertyName(MODULE, NOTX_HOME), notxHomePath);

            // inject engines folder - if not exists
            if (!settings.containsKey(propertyName(MODULE, ENGINES_HOME))) {
                settings.setProperty(propertyName(MODULE, ENGINES_HOME), notxHomePath+ENGINES_HOME_DEFAULT_VALUE);
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Can't find configuration properties!", ex);
        }
    }

    private static String getConfigPropertiesPath(String home) {
        String path = String.format("%s/conf/config.properties", home);                
        return path;
    }

    /**
     * Helper method for getting property instance for particular module.
     */
    @Override
    public String getModuleProperty(String module, String key) {
        String propertyKey = propertyName(module, key);
        String property = settings.getProperty(propertyKey);
        if (property == null) {
            throw new NullPointerException("Property key '" + propertyKey + "' not found in config.properties");
        }
        return property;
    }

    @Override
    public String getVersion() {
        return getModuleProperty(MODULE, VERSION);
    }

    @Override
    public int getIntModuleProperty(String module, String key) {
        String istr = getModuleProperty(module, key);
        return Integer.parseInt(istr);
    }

    private static String propertyName(String module, String key) {
        return String.format("%s.%s", module, key);
    }

    @Override
    public Map<String, String> getModuleProperties(String module) {
        Builder<String, String> builer = ImmutableMap.builder();
        for (Iterator<Entry<Object, Object>> it = settings.entrySet().iterator(); it.hasNext();) {
            Entry<Object, Object> entry = it.next();
            if (entry.getKey() instanceof String && ((String) entry.getKey()).startsWith(module)) {
                builer.put((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return builer.build();
    }
}
