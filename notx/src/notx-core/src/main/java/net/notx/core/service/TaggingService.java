/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.service;

import java.util.List;

/**
 *
 * @author palo
 */
public interface TaggingService {
    
    public void tagUser(String userId, String tag);
    
    public void unTagUser(String userId, String tag);
    
    public List<String> listTags();
    
}
