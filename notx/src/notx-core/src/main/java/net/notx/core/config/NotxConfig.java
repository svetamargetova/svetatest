package net.notx.core.config;

import java.util.Map;

/**
 *
 * Interface that defines contract for every module configuration. 
 *
 * @author Filip Nguyen
 *
 */
public interface NotxConfig {
    
    static final String MODULE = "notx";    
    static final String VERSION = "version";
    static final String NOTX_HOME = "home";
    static final String LOG_DIR = "log.directory";
    static final String ENGINES_HOME = "engines.home";
    static final String ENGINES_HOME_DEFAULT_VALUE = "/engines";
            
    /**
     * Helper method for getting property instance for particular module. 
     */
    public String getModuleProperty(String module, String key);
    
    /**
     * Method that gets all module properties
     * @param module
     * @return 
     */
    public Map<String,String> getModuleProperties(String module);
    /**
     * Helper method for getting integer property instance for particular module. 
     */
    public int getIntModuleProperty(String module, String key);
    
    public String getVersion();

}
