/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.service;

import java.util.List;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.RoutingRecordCollection;
import net.notx.core.entities.User;

/**
 *
 * @author palo
 */
public interface UserService {
    
    public User loadUser(String userId);
    
    public List<String> loadTaggedUserIds(String tag);       
    
    public List<User> listUsers();
    
    public void deleteUser(String userId);
    
    public void createUser(User user);
    
    public RoutingRecordCollection loadUserRoutingRecords(String userId);
    
    public void createRoute(RoutingRecord route);
    
    public void deleteRoute(RoutingRecord route);
    
}
