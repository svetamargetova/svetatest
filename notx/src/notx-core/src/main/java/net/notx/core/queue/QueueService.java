/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.queue;

import java.util.List;
import net.notx.client.message.AbstractMessage;
import net.notx.core.config.NotxConfig;

/**
 * Interface for implementation of concrete Queue. Queue provider
 * is looked up in Spring context and is initialized by configuration.
 * @author palo
 */
public interface QueueService {
    /**
     * Method that is called right after creation of provider with NotXConfiguration     
     */
    public void initialize(NotxConfig config);
    
    /**
     * Method loads specified or less hent <code>number</code> of messages from queue. 
     * When method is called, returned messages are processed in worker threads.
     * <br/>
     * When zero messages is returned then calling method is repeated after defined 
     * time in configuration <code>notx.queue.repeat</code>
     * @param number count of how many messages have to be loaded
     * @return list of messages or null
     */
    public List<? extends AbstractMessage> loadMessages(int number);
    
    /**
     * Loads one message from queue.
     * @return message or null when no message in queue
     */
    public AbstractMessage loadMessage();
    
    /**
     * Deletes message from Queue right after that message was processed.
     * @param m message 
     */
    public void deleteMessage(AbstractMessage m);
    
    /**
     * Saves message into queue.
     * @param message 
     */
    public void putMessage(AbstractMessage message);
    
    /**
     * Method is called when NotX is going to shutdown.
     * @param config 
     */
    public void destroy(NotxConfig config);
}
