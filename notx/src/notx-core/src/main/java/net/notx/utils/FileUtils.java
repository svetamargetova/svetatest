/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * FileUtils that wraps helper methods wich files and inpustreams
 *
 * @author palo
 */
public class FileUtils {

    private static final String CLASSPATH = "classpath:";

    public static InputStream getStreamFor(String path) throws FileNotFoundException {
        InputStream is = null;
        if (path.startsWith(CLASSPATH)) {
            path = path.substring(CLASSPATH.length());
            is = FileUtils.class.getResourceAsStream(path);
        } else {
            is = new FileInputStream(path);
        }
        return is;

    }
}
