set group=%1%
set name=%2%
set version=%3%
set jar=%4%

mvn install:install-file -Dfile=%jar% -DgroupId=%group%  -DartifactId=%name% -Dversion=%version% -Dpackaging=jar -DlocalRepositoryPath="mvn-local-repository\"