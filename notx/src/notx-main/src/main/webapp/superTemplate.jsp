<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/notx_utility_layout.jsp">

  <stripes:layout-component name="documentReady">
          $('#selectedSuperTemplate').change(function() {
              $('#showinfo').click();
          });
    </stripes:layout-component>
    
    <stripes:layout-component name="contents">
        <h1>Super Template</h1>
        <stripes:errors/>

        <h2>New</h2>
        <table>
        <tbody>
        <stripes:form action="/SuperTemplate.action" focus="">
          <tr><td><stripes:label for="superTemplate.name"/></td><td><stripes:text name="newSuperTemplate.name"/></td></tr>
          <tr><td><stripes:label for="superTemplate.description"/></td><td><stripes:textarea name="newSuperTemplate.description"/></td></tr>    
          <tr><td colspan="2"><stripes:submit name="newSuperTemplate" value="New supertemplate"/></td></tr>
        </stripes:form>
        </tbody>
        </table>
        
        <h2>Find information</h2>        
        <stripes:form id="showInformation" action="/SuperTemplate.action" focus="">
          <div style="display:inline"> 
             <stripes:select id="selectedSuperTemplate" name="selectedSuperTemplate.name">
                <stripes:options-collection collection="${actionBean.allSuperTemplates}"
                                            label="name" value="name"/>
             </stripes:select>
          </div>
          <div style="display:none">
              <stripes:submit id="showinfo" name="show" value="Show info"/>
          </div>
        </stripes:form>
        

        
        <c:if test="${not empty actionBean.selectedSuperTemplate.name}">
         <stripes:label for="superTemplate.description"/> ${actionBean.selectedSuperTemplate.description}
         <br/>
         <h3>Template versions</h3>
         
  				<table class="elegant-table">
  				 <thead>
  					<tr>
  					  <th>Lang</th> 
  						<th>Engine</th>
  						<th></th>
  					</tr>
  				 </thead>
  				 <tbody>
  		    	<c:forEach items="${actionBean.versions}" var="version" varStatus="rowstat">
  			    	<tr class="${rowstat.count mod 2 == 0 ? "even" : "odd"}">
  			      	<td>${version.lang}</td>
  			      	<td>${version.engine}</td>
                <td>  <stripes:link href="/Template.action" event="show">
                        Edit
                        <stripes:param name="superTemplate.name" value="${actionBean.selectedSuperTemplate.name}"/>
                        <stripes:param name="descriptor.lang" value="${version.lang}"/>
                        <stripes:param name="descriptor.engine" value="${version.engine}"/>
                      </stripes:link>
                      <stripes:link href="/Template.action" event="delete">
                        Delete
                        <stripes:param name="superTemplate.name" value="${actionBean.selectedSuperTemplate.name}"/>
                        <stripes:param name="descriptor.lang" value="${version.lang}"/>
                        <stripes:param name="descriptor.engine" value="${version.engine}"/>
                      </stripes:link>
                </td>
  			      </tr>
  					</c:forEach>
  					</tbody>
  				</table> 
  				
  				<stripes:link href="/Template.action" event="preNew">
                        New version
                        <stripes:param name="superTemplate.name" value="${actionBean.selectedSuperTemplate.name}"/>
                        <stripes:param name="superTemplate.description" value="${actionBean.selectedSuperTemplate.description}"/>
          </stripes:link>
				</c:if>
    </stripes:layout-component>
</stripes:layout-render>
