<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
		  	<h1>Errors</h1>
		    <stripes:errors/>
		    <h2>Failed notifications</h2>
				<table class="elegant-table">
						 <thead>
							<tr> 
								<th>Type</th>
								<th>Context</th>
								<th>JMS cause</th>
								<th>Exception info</th>
								<th></th>
							</tr>
						</thead>
			       <tbody>
						    <c:forEach items="${actionBean.notificationFails}" var="nf" varStatus="rowstat">
						     <tr>
						       <td>${nf.type}</td>
						       <td>${nf.errorCause}</td>
						       <td>${nf.sqsMessage}</td>
						       <td>${nf.exceptionInfo}</td>
						       <td>	
						       			<stripes:link href="/ErrorView.action" event="viewDetail">
						        	    	Detail
						       	    	  <stripes:param name="detail.uid" value="${nf.uid}"/>
						          	</stripes:link>
		               </td>
						     </tr>
						    </c:forEach>
					   </tbody>
				</table>
						<stripes:link href="/ErrorView.action" event="resendNotifications">
		            	Send all to MQ again
            </stripes:link>
		    <h2>Others</h2>
				<table  class="elegant-table">
						 <thead>
							<tr> 
								<th>Type</th>
								<th>Error cause</th>
								<th>JMS</th>
								<th>Stack trace</th>
							</tr>
						</thead>
			       <tbody>
						    <c:forEach items="${actionBean.otherFails}" var="of" varStatus="rowstat">
						     <tr>
						       <td>${of.type}</td>
						       <td>${of.errorCause}</td>
						       <td>${of.sqsMessage}</td>
						       <td>${of.stackTrace}</td>
						     </tr>
						    </c:forEach>
					   </tbody>
				</table>
						<stripes:link href="/ErrorView.action" event="resendOthers">
				            	Send all to MQ again
            </stripes:link>
    </stripes:layout-component>
</stripes:layout-render>
