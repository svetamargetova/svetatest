<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
		  	<h1>JMS malfunction</h1>
		    Cannot connect JMS brooker on address: ${actionBean.jmsSettings}. Please check that JMS brooker is working.
		      
	  </stripes:layout-component>
</stripes:layout-render>
