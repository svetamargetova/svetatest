<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>
<%@ page import="java.lang.Math" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
        <h1>Template detail (${actionBean.superTemplate.name})</h1>
        <stripes:form action="/Template.action" focus="">
        <stripes:label for="superTemplate.description"/>: ${actionBean.superTemplate.description} <br/>
        <c:if test="${empty actionBean.descriptor.lang}">
          <stripes:label for="descriptor.lang"/>: <stripes:text name="descriptor.lang"/><br/>
        </c:if>       
        <c:if test="${not empty actionBean.descriptor.lang}">
          <stripes:label for="descriptor.lang"/>: ${actionBean.descriptor.lang}<br/>
        </c:if>
               
        <c:if test="${empty actionBean.descriptor.lang}">
          <stripes:label for="descriptor.engine"/>: <stripes:text name="descriptor.engine"/><br/>
        </c:if>       
               
        <c:if test="${not empty actionBean.descriptor.engine}">
          <stripes:label for="descriptor.engine"/>: ${actionBean.descriptor.engine}<br/>
        </c:if>              
        
        
		    <stripes:errors/>
				
				
				<table width="600px" >
				 <thead>
					<tr>
					  <th>Template part</th> 
						<th>Value</th>
					</tr>
				 </thead>
				 <tbody>
  		    	<c:forEach items="${actionBean.template.templateParts}" var="part" varStatus="rowstat">
  			    	<tr class="${rowstat.count mod 2 == 0 ? "even" : "odd"}">
  			      	<td>${part.key}</td>
  			      	<td><stripes:textarea rows="${fn:length(part.value) div 40 + 2}" cols="${fn:length(part.value) div 100 + 50}" name="template.templateParts[${part.key}]" value="${part.value}"/></td>
  			      </tr>
  					</c:forEach>
  					 <tr><td colspan="2">
  					          <stripes:submit name="save">
                        Save changes
                        <stripes:param name="descriptor.lang" value="${actionBean.descriptor.lang}"/>
                        <stripes:param name="descriptor.engine" value="${actionBean.descriptor.engine}"/>
                        <stripes:param name="superTemplate.name" value="${actionBean.superTemplate.name}"/>
                      </stripes:submit>
            </td></tr>
					</tbody>
				</table>

				</stripes:form>
				
				<table>
				  <tbody>
    				<stripes:form action="/TemplatePart.action" focus="">
               <tr><td><stripes:label for="newPartName"/></td><td> <stripes:text name="newPartName"/></td></tr>
               <tr><td><stripes:label for="newPartValue"/></td><td> <stripes:textarea name="newPartValue"/></td></tr>
               <tr><td colspan="2"><stripes:submit name="save">
                                      Add new template part
                                      <stripes:param name="descriptor.lang" value="${actionBean.descriptor.lang}"/>
                                      <stripes:param name="descriptor.engine" value="${actionBean.descriptor.engine}"/>
                                      <stripes:param name="superTemplate.name" value="${actionBean.superTemplate.name}"/>
                                   </stripes:submit>
                   </td>
               </tr>
    				</stripes:form>
				  </tbody>
				</table> 
    </stripes:layout-component>
</stripes:layout-render>
