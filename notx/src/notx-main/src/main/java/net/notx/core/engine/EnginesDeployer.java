package net.notx.core.engine;

import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import net.notx.client.NotxException;
import net.notx.core.config.NotxConfig;
import net.notx.engine.Engine;
import net.notx.engine.EngineException;
import net.notx.engine.EngineSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> EnginesDeployment is context of deployed engines in Notx. These engines
 * are deployed as jars in "engines" dir with accompanied configuration
 * (engines.properties). This class encapsulates them and provides methods to
 * reaload them and access them by name. </p>
 *
 * <p> It's important to stress that this class only operates with information
 * that can be found in engines folder. Any other information regarding NotX
 * (templates in cassandra, notx settings itself) shouldn't be dragged upon this
 * class as this is already very fragile component of the whole system. Engines
 * themselves depend on TemplateProvider, but this class will be provided
 * elsewhere. </p>
 *
 * @author Filip Nguyen
 *
 */
public class EnginesDeployer implements EngineProvider{

    private static Logger logger = LoggerFactory.getLogger(EnginesDeployer.class);
    private ImmutableMap<String, Engine> engines;

    /**
     * This method reloads config of engines from disk and loads those engines
     * with their's new configuration.
     *
     * It also collects all correlation processors that those engines define.
     *
     * @throws NotxException
     */
    public synchronized void loadEngines(NotxConfig config) throws NotxException {
        EnginesConfig enginesConfig = new EnginesConfig(config);
        List<EngineSettings> settings = enginesConfig.getEngineSettings();
        String engineFolder = config.getModuleProperty(NotxConfig.MODULE, NotxConfig.ENGINES_HOME);
        logger.info("Loading engines from: {}", engineFolder);
        loadEngines(settings, engineFolder);
    }

    protected synchronized void loadEngines(List<EngineSettings> settings, String engineFolder) throws NotxException {
        ImmutableMap.Builder<String, Engine> builder = ImmutableMap.builder();

        for (EngineSettings es : settings) {

            logger.info("Engine [{}] - enabled:{}", es.getEngineName(), es.isEnabled());
            if (es.isEnabled()) {
                // load jar file
                String jarName = es.getEngineJarName();
                if (!jarName.endsWith(".jar")) {
                    jarName = jarName + ".jar";
                }

                File file = new File(String.format("%s/%s", engineFolder, jarName));

                if (!file.exists()) {
                    logger.error("Jar for engine [{}] not found on path: {}", es.getEngineName(), file.getPath());
                    continue;
                }

                // Create engine instance
                Engine engine = null;
                URLClassLoader cloader;
                try {

                    cloader = new URLClassLoader(new URL[]{file.toURI().toURL()}, Engine.class.getClassLoader());
                    Class engineClass = cloader.loadClass(es.getEngineClass());
                    if (engineClass == null) {
                        logger.error("Engine class '{}' was not found in the {}", es.getEngineClass(), file.getPath());
                        continue;
                    }
                    engine = (Engine) engineClass.newInstance();
                    engine.configure(es);
                    builder.put(es.getEngineName(), engine);
                    logger.info("Engine [{}] successfully loaded.", es.getEngineName());

//                    String correlator = settings.getProperty("correlationClass");
//
//                    if (correlator != null) {
//                        logger.debug("Correlator found " + correlator + " in configuration. ");
//                        Class loadedCorrelator = cloader.loadClass(correlator);
//
//                        Object instancedCorrelator = loadedCorrelator.newInstance();
//
//                    }
                } catch (Exception ex) {
                    logger.error("Error while configuring engine: " + es.getEngineName(), ex);
                    continue;
                }
            }

        }

        engines = builder.build();
        if (engines.isEmpty()) {
            logger.error("No engines were found! Please configure engines.");
        }
    }

    /**
     * <p>Gets engine to be used of specified type.</p>
     *
     * <p>This is the place to put load balancing. For example in case of mail
     * engine one can deloy 2 mail engines (same class) but with different
     * configuration. Engines by name should be renamed to "engineTypesByName"
     * and each should be collection of specific engine, the returned engine
     * should be the one that wasn't used for the longest period of time and
     * isn't busy.</p>
     *
     * TODO: should be in different class, maybe every worker thread can have its own engines
     * TODO: what if engine is not thread safe?
     * 
     * @param engineName
     * @return
     */
    @Override
    public synchronized Engine getEngine(String engineName) {        
        Engine engine = engines.get(engineName);
        // test just for null after loading - using containsKey will do one more searching
        if(engine == null){
            throw new EngineException("Engine [" + engineName + "] was not loaded!", null);
        }    
        return engine;
        
        
    }

    public synchronized ImmutableMap<String, Engine> getEngines() {
        return engines;
    }        
}
