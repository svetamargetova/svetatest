package net.notx.web;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;


/**
 *
 * @author Filip Nguyen
 */
public class ErrorActionBean extends NotxActionBean {

    private String cassandraSettings;
    private String jmsSettings;

    public Resolution cassandra() {
        //cassandraSettings = context.getCassandraSetting();
        return new ForwardResolution("/cassandraDown.jsp");
    }

    public Resolution jms() {
        //jmsSettings = context.getJmsSettings();
        return new ForwardResolution("/jmsDown.jsp");
    }

    public String getCassandraSettings() {
        return cassandraSettings;
    }

    public void setCassandraSettings(String cassandraSettings) {
        this.cassandraSettings = cassandraSettings;
    }

    public String getJmsSettings() {
        return jmsSettings;
    }

    public void setJmsSettings(String jmsSettings) {
        this.jmsSettings = jmsSettings;
    }
}
