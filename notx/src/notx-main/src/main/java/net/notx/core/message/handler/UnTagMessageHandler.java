/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import net.notx.client.message.UnTagMessage;
import net.notx.core.message.MessageHandler;
import net.notx.core.service.TaggingService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class UnTagMessageHandler implements MessageHandler<UnTagMessage> {

    @Autowired
    private TaggingService taggingService;

    @Override
    public void handleMessage(UnTagMessage message) {
        taggingService.unTagUser(message.getUserid(), message.getTag());        
    }
}
