package net.notx.core.queue;

import java.util.List;
import net.notx.client.message.AbstractMessage;
import net.notx.core.message.MessageWorkerThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> Thread is started right after NotX is loaded. This thread responsibility
 * is to load messages from custom implementation of queue provider and process
 * them in NotX core. </p>
 *
 * <p>Every message is put into worker pool where idle worker will be assigned
 * into message.</p>
 *
 *
 * @author palo
 */
public class LoadMessagesThread extends Thread {

    public static final String MODULE="notx.queue";            
    public static final String LOAD_MESSAGE_CYCLE_TIME="loadMessageCycleTime";            
    public static final String MESSAGE_COUNT_PER_CYCLE="messageCountPerCycle"; 
    
    private static final Logger logger = LoggerFactory.getLogger(LoadMessagesThread.class);
    private boolean stop = false;
    private final long sleepBetweenCycles;
    private final int messagesPerCycle;
    private QueueService queueProvider;
    private MessageWorkerThreadPool workerThreadPool;

    public LoadMessagesThread(long sleepBetweenCycles, int messagesPerCycle) {
        this.sleepBetweenCycles = sleepBetweenCycles;
        this.messagesPerCycle = messagesPerCycle;
    }

    @Override
    public void run() {
        logger.info("Starting load messages thread");
        if (queueProvider == null) {
            logger.error("Can't start load message thread, missing QueueService");
        } else if (workerThreadPool == null || workerThreadPool.isClosed()) {
            logger.error("Can't start load message thread, missing worker pool or worker pool is not open");
        } else {
            while (!isStopped()) {
                List<? extends AbstractMessage> msgs = queueProvider.loadMessages(messagesPerCycle);
                for (AbstractMessage abstractMessage : msgs) {
                    workerThreadPool.doWork(abstractMessage);
                }
                try {
                    if (sleepBetweenCycles > 0) {
                        Thread.sleep(sleepBetweenCycles);
                    }
                } catch (InterruptedException ex) {
                }
            }
        }
        logger.info("Stopping load messages thread");
    }

    public void setWorkerThreadPool(MessageWorkerThreadPool workerThreadPool) {
        this.workerThreadPool = workerThreadPool;
    }

    public void setQueueProvider(QueueService queueProvider) {
        this.queueProvider = queueProvider;
    }
    
  
    public synchronized void stopThread(){        
        stop = true;
    }
    
    public synchronized boolean isStopped(){
        return stop;
    }
}
