package net.notx.web;

import java.util.Collection;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;

public class NotxActionBeanContext extends ActionBeanContext {

    private Logger logger = LogManager.getLogger(NotxActionBeanContext.class);

    public Resolution createAutocompleteResponse(
            Collection<String> allPossibleAutocompleteStrings, String usersInput) {
        logger.debug("String collection size: " + allPossibleAutocompleteStrings.size());

        JSONArray suggestions = new JSONArray();
        if (usersInput != null) {
            for (String oneString : allPossibleAutocompleteStrings) {
                if (oneString.indexOf(usersInput) != -1) {
                    suggestions.put(oneString);
                }
            }
        }

        return new StreamingResolution("text/plain", suggestions.toString());
    }

    public String convertToAutocomplete(Collection<String> strings) {
        JSONArray array = new JSONArray();
        for (String str : strings) {
            array.put(str);
        }
        return array.toString();

    }
}
