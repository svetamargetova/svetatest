package net.notx.core.message;

import com.google.common.collect.ImmutableMap;
import java.util.List;
import net.notx.client.message.AbstractMessage;
import net.notx.core.queue.QueueService;
import net.notx.core.service.StatsService;
import net.notx.core.utils.GenericUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.pool.PoolableObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class MessageWorkerThreadFactory implements PoolableObjectFactory, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(MessageWorkerThreadFactory.class);
    /**
     * Property that is injected into every message working thread. Every time
     * when thread will complete his work will notify trough this object waiting
     * threads.
     *
     */
    protected Object syncPoolObject;
    /**
     * Registered message handlers that are processing concrete messages.
     */
    @Autowired
    private List<MessageHandler> handlers;
    /**
     * Implementations of queue provider.
     */
    @Autowired
    private QueueService queueService;
    /**
     * Implementation of stats service for acknowledging processing message and
     * reporting fails.
     */
    @Autowired
    private StatsService statsService;
    /**
     * Map of handlers is resolved right after initialization of bean. This map
     * is copied into every thread.
     */
    private ImmutableMap<Class<? extends AbstractMessage>, MessageHandler> handlerMap;

    public void setSyncPoolObject(Object syncPoolObject) {
        this.syncPoolObject = syncPoolObject;
    }

    @Override
    public Object makeObject() {
        return new MessageWorkerThread(syncPoolObject, createHandlerMapCopy(), getQueueService(), getStatsService());
    }

    @Override
    public void destroyObject(Object obj) {
        if (obj instanceof MessageWorkerThread) {
            MessageWorkerThread rt = (MessageWorkerThread) obj;
            if (rt.isAlive()) {
                try {
                    rt.stopThread();
                    logger.debug("Stopping worker: {}", rt);
                    if (rt.isActive()) {
                        // thread is running we will wait for some time                
                        rt.join(5000);
                        logger.error("Thread don't want to stop. Stopping by force! {}", rt);
                        rt.stop();
                    } else {
                        // thread is not active so we will notify thread that something happened
                        synchronized (rt) {
                            rt.notify();
                        }
                    }
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    @Override
    public boolean validateObject(Object obj) {
        if (obj instanceof MessageWorkerThread) {
            MessageWorkerThread rt = (MessageWorkerThread) obj;
            if (rt.isAlive()) {
                if (rt.getThreadGroup() == null) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    /**
     * Will configure and start worker. But onlu when worker is not active and
     * is not stopped.
     *
     * @param obj
     */
    @Override
    public void activateObject(Object obj) {
        if (obj instanceof MessageWorkerThread) {
            MessageWorkerThread rt = (MessageWorkerThread) obj;
            if (!rt.isActive() && rt.isStopped()) {
                // we will set worker thread as deamon thread - can be terminated 
                // by JVM by force and JVM is not waiting till working will be done
                // its prevetion for thread lock
                rt.setDaemon(true);
                rt.start();
            }
        }
    }

    @Override
    public void passivateObject(Object obj) {
        if (obj instanceof MessageWorkerThread) {
            MessageWorkerThread wt = (MessageWorkerThread) obj;
            wt.setMessageToProcess(null);
        }
    }

    /**
     * After properties are set, method will create immutable map of messages
     * type and its handler. This map is copied into every worker thread.
     * Handlers should be thread safe.
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        logger.debug("Resolving message handlers...");
        ImmutableMap.Builder<Class<? extends AbstractMessage>, MessageHandler> builder = ImmutableMap.builder();
        for (MessageHandler messageHandler : getHandlers()) {
            Class<? extends AbstractMessage> messageType = GenericUtils.getMessageTypeFromHandler(messageHandler);
            builder.put(messageType, messageHandler);
            logger.debug("Found handler for: {}", ClassUtils.getShortClassName(messageType));
        }
        handlerMap = builder.build();
        logger.info("Resolved {} message handlers", handlerMap.size());
    }

    /**
     * We can't use native {@link ImmutableMap#copyOf(java.util.Map) } method
     * because it returns not new instance but the same one. But for our needs
     * we need to have new ImmutableMap instance - synchronized Map would be
     * bottle neck.
     *
     * @return fresh copy of handler map
     */
    private ImmutableMap<Class<? extends AbstractMessage>, MessageHandler> createHandlerMapCopy() {
        ImmutableMap.Builder<Class<? extends AbstractMessage>, MessageHandler> b = ImmutableMap.builder();
        if (handlerMap == null) {
            return ImmutableMap.<Class<? extends AbstractMessage>, MessageHandler>builder().build();
        } else {
            return b.putAll(handlerMap).build();
        }
    }

    /**
     * Registered message handlers that are processing concrete messages.
     *
     * @return the handlers
     */
    public List<MessageHandler> getHandlers() {
        return handlers;
    }

    /**
     * Registered message handlers that are processing concrete messages.
     *
     * @param handlers the handlers to set
     */
    public void setHandlers(List<MessageHandler> handlers) {
        this.handlers = handlers;
    }

    /**
     * Implementations of queue provider.
     *
     * @return the queueService
     */
    public QueueService getQueueService() {
        return queueService;
    }

    /**
     * Implementations of queue provider.
     *
     * @param queueService the queueService to set
     */
    public void setQueueService(QueueService queueService) {
        this.queueService = queueService;
    }

    /**
     * Implementation of stats service for acknowledging processing message and
     * reporting fails.
     *
     * @return the statsService
     */
    public StatsService getStatsService() {
        return statsService;
    }

    /**
     * Implementation of stats service for acknowledging processing message and
     * reporting fails.
     *
     * @param statsService the statsService to set
     */
    public void setStatsService(StatsService statsService) {
        this.statsService = statsService;
    }
}