/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message;

import com.google.common.collect.ImmutableList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import net.notx.client.message.AbstractMessage;
import net.notx.core.config.NotxConfig;
import org.apache.commons.collections.list.CursorableLinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> This thread pool is created for NotX special purpose. Worker threads are
 * created and evicted as usual with respect to configuration. When no thread is
 * in IDLE state, pool will create new worker thread only when configuration
 * <code>maxActive</code> allows it. Furthermore when pool is full then caller
 * thread will be waiting.<p>
 *
 * <p>This pool is created to provide maximum flow of processing notifications.
 * Threads have its own meta data which are used to determine whether thread is
 * active or in idle state and amount of time in this state. When thread
 * finishes his work he will mark himself available.</p>
 *
 * <p>When pool is created an eviction timer is started. Worker thread instance
 * is evicted when meet one of two conditions:</p>
 *
 * - IDLE time of worker thread oversteps configuration
 * <code>maxIdleTime</code>
 *
 * - Active working time of worker thread oversteps configuration
 * <code>maxActiveTime</code>
 *
 * <p>Maximum flow is reach by very granular usage of
 * <code>synchronized</code>
 * <code>_pool</code> monitor.</p>
 *
 * <p> Pool is created from double-linked list. Idle worker thread is removed
 * from pool, message is assigned to worker and worker is put into the end of
 * pool list. By this process we can achieve quick search of idle worker threads
 * - active are in the end and idle in the begging.
 *
 *
 *
 *
 *
 * @author palo
 */
public final class MessageWorkerThreadPool implements PoolConfigConstants {

    public MessageWorkerThreadPool(NotxConfig config, MessageWorkerThreadFactory factory) {
        // configuration
        setMaxActive(config.getIntModuleProperty(MODULE, MAX_ACTIVE));
        setMaxIdleTime(config.getIntModuleProperty(MODULE, MAX_IDLE_TIME));
        setMaxWorkingTime(config.getIntModuleProperty(MODULE, MAX_WORKING_TIME));
        setMinIdle(config.getIntModuleProperty(MODULE, MIN_IDLE));
        setTimeBetweenEvictionRunsMillis(config.getIntModuleProperty(MODULE, EVICTION_CYCLE_TIME));
        setEvictionRuTestsCount(config.getIntModuleProperty(MODULE, EVICTION_CYCLE_TESTS));

        // pool have to be stated externally
        closed = true;
        // creating synchonization object
        _syncObject = new Object();
        // setting up factory
        _factory = factory;
        _factory.setSyncPoolObject(_syncObject);
        // creating pool
        _poolSize = new AtomicInteger(0);
        _pool = new CursorableLinkedList();
    }

    /**
     * Initializes pool and starts working threads.
     */
    public void start() {
        allocate();
        startEvictor();
        setClosed(false);
    }

    public void doWork(AbstractMessage message) {
        assertOpen();

        // Worker that will be returned
        MessageWorkerThread worker = null;
        boolean block = false;


        // First, try to find IDLE worker and set him message to process or get pool size
        synchronized (_pool) {
            worker = findIdleWorker();

            if (worker != null) {
                worker.setMessageToProcess(message);
                _pool.addLast(worker);
            }
        }

        // Second, when no IDLE worker, test wheter we can create new thread or block 
        if (worker == null) {
            if (_poolSize.get() < getMaxActive()) {
                // give some time to other threads to pick existing one
                worker = createWorker();                
                activateWorker(worker);
                worker.setMessageToProcess(message);
                synchronized (_pool) {
                    _pool.addLast(worker);
                }
            } else {
                block = true;
            }
        }



        // no worker in pool is empty. we have to block processing and wait
        if (block) {
            while (worker == null) {
                synchronized (_syncObject) {
                    try {
                        _syncObject.wait();
                    } catch (InterruptedException ex) {
                    }
                }
                synchronized (_pool) {
                    worker = findIdleWorker();
                    if (worker != null) {
                        worker.setMessageToProcess(message);
                        _pool.addLast(worker);
                    }
                }
            }
        }

        // notify worker that he has someting to do
        synchronized (worker) {
            worker.notify();
        }
    }

    private MessageWorkerThread findIdleWorker() {
        MessageWorkerThread worker = null;
        CursorableLinkedList.Cursor cursor = _pool.cursor(0);
        while (cursor.hasNext()) {
            worker = (MessageWorkerThread) cursor.next();
            if (!worker.isActive()) {
                cursor.remove();
                break;
            }
            worker = null;
        }
        return worker;
    }

    public int getNumIdle() {
        return getWorkersCount(false);
    }

    public int getNumActive() {
        return getWorkersCount(true);
    }

    private int getWorkersCount(boolean active) {
        synchronized (_pool) {
            int num = 0;
            Iterator it = _pool.iterator();
            while (it.hasNext()) {
                MessageWorkerThread thread = (MessageWorkerThread) it.next();
                if ((active && thread.isActive())
                        || (!active && !thread.isActive())) {
                    num += 1;
                }
            }
            return num;
        }
    }

    public void clear() {
        synchronized (_pool) {
            logger.info("Clearing pool");
            Iterator it = _pool.iterator();
            while (it.hasNext()) {
                MessageWorkerThread thread = (MessageWorkerThread) it.next();
                deleteWorker(thread);
            }
            _pool.clear();
        }
    }

    public synchronized void close() {
        if (!isClosed()) {
            logger.info("Closing pool");
            setClosed(true);
            logger.info("Stopping evictor");
            if (_evictionTimer != null) {
                _evictionTimer.cancel();
            }
            clear();
        }
    }

    //--- private maintenance methods --------------------------------
    private void evict() {
        assertOpen();

        logger.debug("Starting pool evictor");
        ImmutableList.Builder<MessageWorkerThread> builder = ImmutableList.builder();

        if (_evictionCursor == null) {
            _evictionCursor = _pool.cursor();
        }


        synchronized (_pool) {
            if (_pool.isEmpty()) {
                return;
            }

            // reset cursor if we are in the end
            if (!_evictionCursor.hasNext()) {
                _evictionCursor.close();
                _evictionCursor = _pool.cursor();
            }

            for (int i = 0; i < getEvictionRuTestsCount() && _evictionCursor.hasNext(); i++) {

                // load specified number of worker threads to test for eviction
                MessageWorkerThread th = (MessageWorkerThread) _evictionCursor.next();
                builder.add(th);
                _evictionCursor.remove();
            }
        }

        // we will test threads outside of _pool monitor
        List<MessageWorkerThread> toTest = builder.build();
        for (MessageWorkerThread messageWorkerThread : toTest) {
            boolean evict = false;
            if (messageWorkerThread.isActive()) {
                if (messageWorkerThread.getProcessingTime() > getMaxWorkingTime()) {
                    logger.debug("Evict - Processing time: {}", messageWorkerThread);
                    evict = true;
                }
            } else {
                if (messageWorkerThread.getIdleTime() > getMaxIdleTime()) {
                    // we can evict idle thread only when we are above minIdle limit
                    if (_poolSize.get() > getMinIdle()) {
                        logger.debug("Evict - IdleTime: {}", messageWorkerThread);
                        evict = true;
                    }
                }
            }

            // evict thread or put him back into queue
            if (evict) {
                deleteWorker(messageWorkerThread);
            } else {
                boolean sync = false;
                synchronized (_pool) {
                    if (messageWorkerThread.isActive()) {
                        _pool.addLast(messageWorkerThread);
                    } else {
                        _pool.addFirst(messageWorkerThread);
                        sync = true;
                    }

                }
                // we will sync if thread is in idle
                if (sync) {
                    synchronized (_syncObject) {
                        _syncObject.notify();
                    }
                }
            }
        }

        // after eviction we will test if pool is allocated to minIdle        
        if (_poolSize.get() < getMinIdle()) {
            int create = getMinIdle() - _poolSize.get();
            if (create > 0) {
                logger.debug("Rebalancing worker pool - creating {} workers", create);
                MessageWorkerThread[] createThreads = new MessageWorkerThread[create];
                for (int i = 0; i < create; i++) {
                    createThreads[i] = createWorker();
                    activateWorker(createThreads[i]);
                }

                // when are threads ready - append them into the pool and notify
                synchronized (_pool) {
                    for (int i = 0; i < createThreads.length; i++) {
                        _pool.addFirst(createThreads[i]);
                    }

                }
                // notify about new workers
                synchronized (_syncObject) {
                    if (create > 1) {
                        _syncObject.notifyAll();
                    } else {
                        _syncObject.notify();
                    }
                }
            }
        }

    }

    /**
     * Allocates initial idle instances of worker threads - called only once in
     * constructor. No need for synchronization.
     */
    private void allocate() {
        logger.info("Prepopulating pool with {} worker threads", getMinIdle());
        for (int i = 0; i < getMinIdle(); i++) {
            MessageWorkerThread worker = createWorker();
            activateWorker(worker);
            _pool.add(worker);
        }
    }

    private MessageWorkerThread createWorker() {
        MessageWorkerThread w = (MessageWorkerThread) _factory.makeObject();
        _poolSize.incrementAndGet();
        return w;
    }

    private void deleteWorker(MessageWorkerThread thread) {
        _poolSize.decrementAndGet();
        _factory.destroyObject(thread);
    }

    private void activateWorker(MessageWorkerThread worker) {
        if (!worker.isAlive()) {
            _factory.activateObject(worker);
        }
    }

    private void startEvictor() {
        logger.info("Starting evictor");
        _evictionTimer = new Timer(true);
        _evictionTimer.schedule(new Evictor(), getTimeBetweenEvictionRunsMillis(), getTimeBetweenEvictionRunsMillis());
    }

    //--- private helper methods -------------------------------------
    //--- Setter/Getter methods -------------------------------------
    /**
     * The cap on the minimum number of idle instances in the pool.
     *
     * @see #setMinIdle
     * @see #getMinIdle
     * @return the _minIdle
     */
    public int getMinIdle() {
        return _minIdle;
    }

    /**
     * The cap on the minimum number of idle instances in the pool.
     *
     * @see #setMinIdle
     * @see #getMinIdle
     * @param minIdle the _minIdle to set
     */
    public void setMinIdle(int minIdle) {
        this._minIdle = minIdle;
    }

    /**
     * The cap on the total number of active instances from the pool.
     *
     * @see #setMaxActive
     * @see #getMaxActive
     * @return the _maxActive
     */
    public int getMaxActive() {
        return _maxActive;
    }

    /**
     * The cap on the total number of active instances from the pool.
     *
     * @see #setMaxActive
     * @see #getMaxActive
     * @param maxActive the _maxActive to set
     */
    public void setMaxActive(int maxActive) {
        this._maxActive = maxActive;
    }

    /**
     * The maximum amount of time an object may sit idle in the pool before it
     * is eligible for eviction by the idle object evictor
     *
     * @see #setMaxIdleTime
     * @see #getMaxIdleTime
     * @return the _maxIdleTime
     */
    public long getMaxIdleTime() {
        return _maxIdleTime;
    }

    /**
     * The maximum amount of time (in milis) an object may sit idle in the pool
     * before it is eligible for eviction by the idle object evictor
     *
     * @see #setMaxIdleTime
     * @see #getMaxIdleTime
     * @param maxIdleTime the _maxIdleTime to set
     */
    public void setMaxIdleTime(long maxIdleTime) {
        this._maxIdleTime = maxIdleTime;
    }

    /**
     * How long(in milis) can thread process notification. After reaching this
     * limit an evictor will stop thread and remove it from pool.
     *
     * @see #setMaxWorkingTime
     * @see #getMaxWorkingTime
     * @return the _maxWorkingTime
     */
    public long getMaxWorkingTime() {
        return _maxWorkingTime;
    }

    /**
     * How long(in milis) can thread process notification. After reaching this
     * limit an evictor will stop thread and remove it from pool.
     *
     * @see #setMaxWorkingTime
     * @see #getMaxWorkingTime
     * @param maxWorkingTime the _maxWorkingTime to set
     */
    public void setMaxWorkingTime(long maxWorkingTime) {
        this._maxWorkingTime = maxWorkingTime;
    }

    /**
     * The number of sends to sleep between runs of the idle object evictor
     * thread. When non-positive, no idle object evictor thread will be run.
     *
     * @see #setTimeBetweenEvictionRunsMillis
     * @see #getTimeBetweenEvictionRunsMillis
     * @return the _timeBetweenEvictionRunsMillis
     */
    public long getTimeBetweenEvictionRunsMillis() {
        return _timeBetweenEvictionRunsMillis;
    }

    /**
     * The number of sends to sleep between runs of the idle object evictor
     * thread. When non-positive, no idle object evictor thread will be run.
     *
     * @see #setTimeBetweenEvictionRunsMillis
     * @see #getTimeBetweenEvictionRunsMillis
     * @param timeBetweenEvictionRunsMillis the _timeBetweenEvictionRunsMillis
     * to set
     */
    public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
        this._timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public synchronized boolean isClosed() {
        return closed;
    }

    private synchronized void setClosed(boolean closed) {
        this.closed = closed;
    }

    protected final void assertOpen() throws IllegalStateException {
        if (isClosed()) {
            throw new IllegalStateException("Pool not open");
        }
    }

    /**
     * The number of test made in one eviction cycle. Number of tests means the
     * number of thread workers that will be tested.
     *
     * @return the _evictionRuTestsCount
     */
    public int getEvictionRuTestsCount() {
        return _evictionRuTestsCount;
    }

    /**
     * The number of test made in one eviction cycle. Number of tests means the
     * number of thread workers that will be tested.
     *
     * @param evictionRuTestsCount the _evictionRuTestsCount to set
     */
    public void setEvictionRuTestsCount(int evictionRuTestsCount) {
        this._evictionRuTestsCount = evictionRuTestsCount;
    }

    //--- inner classes ----------------------------------------------
    private class Evictor extends TimerTask {

        @Override
        public void run() {
            evict();
        }
    }
    //--- private configurable attributes ----------------------------------
    /**
     * The cap on the minimum number of idle instances in the pool.
     *
     * @see #setMinIdle
     * @see #getMinIdle
     */
    private int _minIdle = 0;
    /**
     * The cap on the total number of active instances from the pool.
     *
     * @see #setMaxActive
     * @see #getMaxActive
     */
    private int _maxActive = 0;
    /**
     * The maximum amount of time an object may sit idle in the pool before it
     * is eligible for eviction by the idle object evictor
     *
     * @see #setMaxIdleTime
     * @see #getMaxIdleTime
     */
    private long _maxIdleTime = 0;
    /**
     * How long(in milis) can thread process notification. After reaching this
     * limit an evictor will stop thread and remove it from pool.
     *
     * @see #setMaxWorkingTime
     * @see #getMaxWorkingTime
     */
    private long _maxWorkingTime = 0;
    /**
     * The number of sends to sleep between runs of the idle object evictor
     * thread. When non-positive, no idle object evictor thread will be run.
     *
     * @see #setTimeBetweenEvictionRunsMillis
     * @see #getTimeBetweenEvictionRunsMillis
     */
    private long _timeBetweenEvictionRunsMillis = 0;
    /**
     * The number of test made in one eviction cycle. Number of tests means the
     * number of thread workers that will be tested.
     */
    private int _evictionRuTestsCount = 0;
    //--- private attributes ---------------------------------------
    private static final Logger logger = LoggerFactory.getLogger(MessageWorkerThreadPool.class);
    /**
     * Double-linked list.
     */
    private final CursorableLinkedList _pool;
    private AtomicInteger _poolSize;
    /**
     * My {@link MessageWorkerThreadFactory}.
     */
    private final MessageWorkerThreadFactory _factory;
    /**
     * Synchronization object that is used for notify requests threads that an
     * new working thread is available.
     */
    private final Object _syncObject;
    /**
     * Flag that pool has been marked as down. After that pool will not provide
     * any other workers.
     */
    private boolean closed = false;
    /**
     * Timer(deamon) that is periodically checking whether worker is processing
     * or is in idle state too long
     */
    private Timer _evictionTimer;
    /**
     * Cursor that is used in eviction process
     */
    private CursorableLinkedList.Cursor _evictionCursor;
}
