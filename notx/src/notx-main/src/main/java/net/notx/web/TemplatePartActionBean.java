package net.notx.web;

import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.TemplateDescriptor;
import net.notx.core.service.TemplateService;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;


/**
 *
 * @author Filip Nguyen
 */
public class TemplatePartActionBean extends NotxActionBean {

    @SpringBean
    private TemplateService templateService;
    private SuperTemplate superTemplate = new SuperTemplate();
    private TemplateDescriptor descriptor = new TemplateDescriptor();
    @Validate(required = true)
    private String newPartName = null;
    @Validate(required = true)
    private String newPartValue = null;

    public Resolution save() {
        superTemplate = templateService.loadTemplate(superTemplate.getName());
        superTemplate.getTemplate(descriptor).put(newPartName, newPartValue);
        templateService.createTemplate(superTemplate);

        ForwardResolution fwres = new ForwardResolution(TemplateActionBean.class, "show");
        fwres.addParameter("superTemplate.name", superTemplate.getName());
        //todo zkusit tam rovnou dat descriptor
        fwres.addParameter("descriptor.lang", descriptor.getLang());
        fwres.addParameter("descriptor.engine", descriptor.getEngine());
        return fwres;
    }

    public SuperTemplate getSuperTemplate() {
        return superTemplate;
    }

    public void setSuperTemplate(SuperTemplate superTemplate) {
        this.superTemplate = superTemplate;
    }

    public TemplateDescriptor getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(TemplateDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    public String getNewPartName() {
        return newPartName;
    }

    public void setNewPartName(String newPartName) {
        this.newPartName = newPartName;
    }

    public String getNewPartValue() {
        return newPartValue;
    }

    public void setNewPartValue(String newPartValue) {
        this.newPartValue = newPartValue;
    }
}
