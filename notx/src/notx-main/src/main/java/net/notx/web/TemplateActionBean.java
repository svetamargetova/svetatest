package net.notx.web;

import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.Template;
import net.notx.core.entities.TemplateDescriptor;
import net.notx.core.service.TemplateService;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;


/**
 *
 * @author Filip Nguyen
 */
public class TemplateActionBean extends NotxActionBean {

    @SpringBean
    private TemplateService templateService;
    // -- Bean properties
    private SuperTemplate superTemplate = new SuperTemplate();
    private TemplateDescriptor descriptor = new TemplateDescriptor();
    private Template template = new Template();

    @DontValidate
    public Resolution show() {
        superTemplate = templateService.loadTemplate(superTemplate.getName());
        template = superTemplate.getTemplate(descriptor);
        return new ForwardResolution("/template.jsp");
    }

    @DontValidate
    public Resolution delete() {
        superTemplate = templateService.loadTemplate(superTemplate.getName());
        superTemplate.remove(descriptor);
        templateService.deleteTemplate(superTemplate);
        templateService.createTemplate(superTemplate);

        ForwardResolution fwres = new ForwardResolution(SuperTemplateActionBean.class, "show");
        fwres.addParameter("selectedSuperTemplate.name", superTemplate.getName());
        return fwres;
    }

    @DontValidate
    public Resolution preNew() {
        return new ForwardResolution("/template.jsp");
    }

    public Resolution save() {
        superTemplate = templateService.loadTemplate(superTemplate.getName());
        superTemplate.addTemplate(descriptor, template);
        templateService.createTemplate(superTemplate);
        return show();
    }

    public SuperTemplate getSuperTemplate() {
        return superTemplate;
    }

    public void setSuperTemplate(SuperTemplate superTemplate) {
        this.superTemplate = superTemplate;
    }

    public TemplateDescriptor getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(TemplateDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }
}
