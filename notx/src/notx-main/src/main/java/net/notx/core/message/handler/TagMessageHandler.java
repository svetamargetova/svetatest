/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import net.notx.client.message.TagMessage;
import net.notx.core.message.MessageHandler;
import net.notx.core.service.TaggingService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class TagMessageHandler implements MessageHandler<TagMessage> {

    @Autowired
    private TaggingService taggingService;

    @Override
    public void handleMessage(TagMessage tagMsg) {
        taggingService.tagUser(tagMsg.getUserid(), tagMsg.getTag());        
    }
}
