/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.web;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract parent for every NotX action beans. 
 * @author palo
 */
public abstract class NotxActionBean implements ActionBean{
     protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private NotxActionBeanContext context;

    @Override
    public void setContext(ActionBeanContext context) {
        this.context = (NotxActionBeanContext) context;
    }

    @Override
    public NotxActionBeanContext getContext() {
        return context;
    }

    
}
