/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.utils;

import net.notx.client.message.AbstractMessage;
import net.notx.core.message.MessageHandler;
import org.springframework.core.GenericTypeResolver;

/**
 *
 * @author palo
 */
public class GenericUtils {

    public static <T extends MessageHandler> Class<? extends AbstractMessage> getMessageTypeFromHandler(T baseObject) {
        Class<? extends AbstractMessage> clazz = (Class<? extends AbstractMessage>) 
                GenericTypeResolver.resolveTypeArgument(baseObject.getClass(), MessageHandler.class);
        return clazz;
    }
}
