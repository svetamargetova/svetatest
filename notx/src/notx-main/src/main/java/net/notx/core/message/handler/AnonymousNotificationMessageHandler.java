/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import java.util.UUID;
import net.notx.client.NotxException;
import net.notx.client.message.AnonymousNotificationMessage;
import net.notx.core.entities.*;
import net.notx.core.message.MessageHandler;
import net.notx.engine.Engine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class AnonymousNotificationMessageHandler extends AbstractNotificationHandler implements MessageHandler<AnonymousNotificationMessage> {

    private static final Logger logger = LoggerFactory.getLogger(UserNotificationMessageHandler.class);

    @Override
    public void handleMessage(AnonymousNotificationMessage message) {


        logger.debug("Anonymous notify: {}", message);
        // load user and best routing record


        /**
         * TODO: This code below is in 95% same as in
         * UserNotificationMessageHandler
         */
        // load template                        
        SuperTemplate template = loadTemplate(message.getTemplateName());
        if (template == null) {
            throw new NotxException("The supertemplate doesn't exist in DB.");
        }



        TemplateDescriptor templateDescriptor = new TemplateDescriptor(message.getLang(), message.getEngine());

        // process notification
        Engine engine = getEngine(message.getEngine());
        String contact = message.getContact();
        if (contact == null) {
            throw new NotxException("User notifiacation failed! User has no contact '" + engine.getRequiredContact() + "' for engine '" + message.getEngine() + "'");
        }

        Template notification = template.getTemplate(templateDescriptor);


        //Create correlation ID for future reference
        String corrID = UUID.randomUUID().toString();
        notification.put("cid", corrID);
        notification.inject(message.getPlaceHolderVals());
        engine.sendNotification(contact, notification);
        notificationSent("anonymous", contact, corrID, message.getEngine(), message.getLang());
    }
}
