package net.notx.web;

import java.util.Locale;
import net.notx.utils.StringMap;
import net.notx.web.utils.StringMapFormatter;
import net.sourceforge.stripes.format.DefaultFormatterFactory;
import net.sourceforge.stripes.format.Formatter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class provides additional notx formatting for String maps. 
 * 
 * Notx uses string maps extensivelly and StringMapFormatter class along with 
 * StringMapConverter class should be used to format/getvalue of 
 * string maps.
 * 
 * @author kalaz
 *
 */
public class NotxFormatterFactory extends DefaultFormatterFactory{
	private Logger logger = LogManager.getLogger(NotxFormatterFactory.class);
	
	@Override
	public Formatter<?> getFormatter(Class<?> clazz, Locale locale, String formatType, String formatPattern) {
		if (clazz.equals(StringMap.class)){
			return new StringMapFormatter();
		} 
		
			
		
		return super.getFormatter(clazz, locale, formatType, formatPattern);
	}
}
