package net.notx.core.engine;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import java.util.*;
import net.notx.core.config.NotxConfig;
import net.notx.engine.EngineSettings;

/**
 * EnginesConfig is responsible for parsing properties that are located in the
 * engines/engines.properties file.
 *
 * @author Filip Nguyen
 * 
 * @since 2.0
 * Access modifier changed from public to package visibility. {@link EnginesDeployer}
 * owns parsed all engine settings. If there is a change in directory then use
 * {@link EnginesDeployer#loadEngines(net.notx.core.config.NotxConfig)}. 
 * 
 * @author Pavol Gressa
 *
 */
class EnginesConfig {

    private static final String ENGINE_MODULE = "engine";
    private NotxConfig notxConfig;

    EnginesConfig(NotxConfig config) {
        this.notxConfig = config;
    }

    /**
     * @return All engines that are present in configuration file.
     */
    public List<EngineSettings> getEngineSettings() {
        List<EngineSettings> engines = Lists.newArrayList();
        for (int i = 0; true; i++) {
            final String enginePrefix = String.format("%s%d", ENGINE_MODULE, i);
            Map<String, String> eProps = notxConfig.getModuleProperties(enginePrefix);            
            if(eProps.isEmpty()){
                break;
            }            
            ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
            for (Map.Entry<String, String> ep : eProps.entrySet()) {
                builder.put(ep.getKey().substring(enginePrefix.length() + 1), ep.getValue()); // +1 dot
            }

            EngineSettings es = new EngineSettings();
            es.setEngineProperties(builder.build());
            engines.add(es);
        }
        return engines;
    }   
}
