/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.engine;

import net.notx.engine.Engine;

/**
 * Instance of this class is responsible for providing engines by its name. 
 *
 * @author palo
 */
public interface EngineProvider {

    public Engine getEngine(String engineName);
}
