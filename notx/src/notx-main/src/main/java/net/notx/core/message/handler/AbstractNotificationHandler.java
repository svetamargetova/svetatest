/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import net.notx.core.entities.SentNotification;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.service.StatsService;
import net.notx.core.service.TemplateService;
import net.notx.engine.Engine;
import org.apache.commons.collections.FastHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This abstract class provides methods for handlers that are processing
 * concrete message notifications.
 *
 * @author palo
 */
public abstract class AbstractNotificationHandler {

    
    
    @Autowired
    private TemplateService templateService;
    @Autowired
    private StatsService statsService;
    
    private Map<String, Engine> engines;

     /**
     * Set engines that will process notifications by defined users routing
     * records. Engines are in {@link FastHashMap} that is optimized for
     * multithread environment and supports two regimes - fast & slow. When slow
     * then all methods are synchronized. FastHashMap is optimized for read
     * operations mostly.
     *
     * @param engines
     */
    public void setEngines(Map<String, Engine> engines) {
        FastHashMap map = null;
        if (this.engines == null) {
            map = new FastHashMap(engines);
            this.engines = map;
        } else {
            map = ((FastHashMap) this.engines);
            map.setFast(false);
            map.clear();
            map.putAll(engines);
        }
        map.setFast(true);
    }
    
    protected Engine getEngine(String engine){
        return engines.get(engine);
    }

    
    public SuperTemplate loadTemplate(String templateName) {
        return templateService.loadTemplate(templateName);
    }

    public void notificationSent(String userId, String contact, String corId, String engine, String lang) {
        //Create correlation ID for future reference
        SentNotification sentNotification = new SentNotification();
        sentNotification.setTime(new Date());
        sentNotification.setUid(userId);
        sentNotification.setContact(contact);
        sentNotification.setCorrelationID(corId);
        sentNotification.setEngine(engine);
        sentNotification.setLang(lang);
        statsService.notificationSent(sentNotification);
    }
}
