package net.notx.web;

import com.googlecode.charts4j.*;
import java.util.Map.Entry;
import java.util.*;
import net.notx.cassandra.CassandraException;
import net.notx.core.entities.SentNotification;
import net.notx.core.service.StatsService;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author Filip Nguyen
 */
public class DomainStatisticsActionBean extends NotxActionBean {

    @SpringBean
    private StatsService statsService;
    
    private Map<String, Set<SentNotification>> statisticsByDomain = new HashMap<String, Set<SentNotification>>();
    private String graph;
    

    public Resolution show() throws CassandraException {
        statisticsByDomain = statsService.loadStatsByDomain();


        setGraph(createGraph(statisticsByDomain));
        return new ForwardResolution("/domainStats.jsp");
    }

    private String createGraph(
            Map<String, Set<SentNotification>> statsByDomain) {
        List<Color> colors = new ArrayList<Color>();
        List<String> engines = new ArrayList<String>(getPresentEngines());
        List<String> domains = new ArrayList<String>(statsByDomain.keySet());
        colors.add(Color.GREEN);
        colors.add(Color.BLACK);
        colors.add(Color.BISQUE);
        colors.add(Color.BLANCHEDALMOND);
        colors.add(Color.CADETBLUE);

        List<BarChartPlot> plots = new ArrayList<BarChartPlot>();

        int i = 0;
        for (String engine : engines) {
            List<Number> data = new ArrayList<Number>(countEnginePresenceByDomain(engine));

            BarChartPlot plot = Plots.newBarChartPlot(
                    Data.newData(data), colors.get(i), engine);

            plots.add(plot);
            i++;
        }

        BarChart chart = GCharts.newBarChart(plots);
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 13, AxisTextAlignment.CENTER);
        AxisLabels score = AxisLabelsFactory.newAxisLabels("Number of notifications", 50.0);
        score.setAxisStyle(axisStyle);
        AxisLabels year = AxisLabelsFactory.newAxisLabels("Domain", 50.0);
        year.setAxisStyle(axisStyle);
        chart.addXAxisLabels(AxisLabelsFactory.newAxisLabels(domains));
        chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, 20));
        chart.addYAxisLabels(score);
        chart.addXAxisLabels(year);

        chart.setSize(600, 450);
        chart.setBarWidth(100);
        chart.setSpaceWithinGroupsOfBars(20);
        chart.setDataStacked(true);
        chart.setTitle("Notification statistics", Color.BLACK, 16);
        chart.setGrid(100, 10, 3, 2);
        chart.setBackgroundFill(Fills.newSolidFill(Color.WHITE));
        LinearGradientFill fill = Fills.newLinearGradientFill(0, Color.WHITE, 100);
        fill.addColorAndOffset(Color.WHITE, 0);
        chart.setAreaFill(fill);

        return chart.toURLForHTML();

    }

    private List<Number> countEnginePresenceByDomain(String engine) {
        List<Number> counts = new ArrayList<Number>();


        for (Entry<String, Set<SentNotification>> stat : statisticsByDomain.entrySet()) {
            int i = 0;
            for (SentNotification sns : stat.getValue()) {
                if (sns.getEngine().equals(engine)) {
                    i++;
                }
            }
            counts.add(i * 5);
        }
        return counts;
    }

    private Set<String> getPresentEngines() {
        Set<String> engines = new HashSet<String>();

        for (Entry<String, Set<SentNotification>> stat : statisticsByDomain.entrySet()) {
            for (SentNotification sns : stat.getValue()) {
                engines.add(sns.getEngine());
            }
        }
        return engines;
    }

    /**
     * @param statisticsByDomain the statisticsByDomain to set
     */
    public void setStatisticsByDomain(Map<String, Set<SentNotification>> statisticsByDomain) {
        this.statisticsByDomain = statisticsByDomain;
    }

    /**
     * @return the statisticsByDomain
     */
    public Map<String, Set<SentNotification>> getStatisticsByDomain() {
        return statisticsByDomain;
    }

    /**
     * @param graph the graph to set
     */
    public void setGraph(String graph) {
        this.graph = graph;
    }

    /**
     * @return the graph
     */
    public String getGraph() {
        return graph;
    }
}
