package net.notx.web;

import java.util.ArrayList;
import java.util.List;
import net.notx.core.entities.User;
import net.notx.core.service.UserService;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

//TODO http://stripesframework.org/display/stripes/State+Management
/**
 * Action to list and modify contact in Cassandra database. This class will be
 * replaced with WS implementation to directly modify cassandra DB.
 * @author Filip Nguyen
 */
public class UsersActionBean extends NotxActionBean {
    
    @SpringBean
    private UserService userService;
    
    private List<User> allUsers = new ArrayList<User>();
        
    
	@DontValidate
	public Resolution show(){
		allUsers = userService.listUsers();
		return new ForwardResolution("/users.jsp");
	}	
	  
	public List<User> getAllUsers() {
		return allUsers;
	}

	public void setAllUsers(List<User> allUsers) {
		this.allUsers = allUsers;
	}	
}
