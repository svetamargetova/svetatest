/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import java.util.Map;
import java.util.UUID;
import net.notx.client.NotxException;
import net.notx.client.message.UserNotificationMessage;
import net.notx.core.entities.*;
import net.notx.core.message.MessageHandler;
import net.notx.core.service.UserService;
import net.notx.engine.Engine;
import org.apache.commons.collections.FastHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Handles notification for concrete user. Loads first template and routing
 * rules for user. After that notification is passed into engine.
 *
 * @author palo
 */
public class UserNotificationMessageHandler extends AbstractNotificationHandler implements MessageHandler<UserNotificationMessage> {

    private static final Logger logger = LoggerFactory.getLogger(UserNotificationMessageHandler.class);
    @Autowired
    private UserService userService;

    @Override
    public void handleMessage(UserNotificationMessage message) {

        logger.debug("Notify: {}", message);
        // load user and best routing record
        User user = userService.loadUser(message.getUserId());
        RoutingRecordCollection routes = userService.loadUserRoutingRecords(user.getId());
        RoutingRecord route = routes.getBestRoutingRecord(message);
        if (routes == null || routes.isEmpty()) {
            throw new NotxException("There are not routing records for " + user);
        }
        if (route == null) {
            throw new NotxException("No suitable route for " + user);
        }

        // load template
        SuperTemplate template = loadTemplate(message.getTemplateName());
        if (template == null) {
            throw new NotxException("The supertemplate doesn't exist in DB.");
        }



        TemplateDescriptor templateDescriptor = new TemplateDescriptor(user.getLang(), "");

        Map<String, String> placeholders = message.getPlaceHolderVals();
        // @Hack
        // we wil push contacts placeholder as native
        logger.debug("Native placeholders:");
        for (Map.Entry<String, String> c : user.getContacts().entrySet()) {            
            logger.debug("{} - {}",c.getKey(),c.getValue());
            placeholders.put(c.getKey(), c.getValue());
        }
        placeholders.put("user_name", user.getName());
        logger.debug("user_name - {}",user.getName());
        if(!placeholders.containsKey("salutation")){
            placeholders.put("salutation", user.getName());
            logger.debug("salutation - {}",user.getName());
        }

        // process notification
        for (String engineName : route.getDestinationEngines()) {
            templateDescriptor.setEngine(engineName);
            Engine engine = getEngine(engineName);
            if (engine == null) {
                logger.error("Missing engine! This destination engine will be ignored! ");
            }

            String contact = user.getContact(engine.getRequiredContact());
            if (contact == null) {
                throw new NotxException("User notifiacation failed! User has no contact '" + engine.getRequiredContact() + "' for engine '" + engineName + "'");
            }

            Template notification = template.getTemplate(templateDescriptor);


            //Create correlation ID for future reference
            String corrID = UUID.randomUUID().toString();
            notification.put("cid", corrID);
            notification.inject(placeholders);

            engine.sendNotification(contact, notification);

            notificationSent(user.getId(), contact, corrID, engineName, user.getLang());
        }
    }
}
