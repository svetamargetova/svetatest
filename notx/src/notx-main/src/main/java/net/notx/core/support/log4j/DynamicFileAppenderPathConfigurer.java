/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.support.log4j;

import java.util.Enumeration;
import net.notx.core.config.NotxConfig;
import org.apache.log4j.Appender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;


/**
 *
 * @author palo
 */
public class DynamicFileAppenderPathConfigurer {

    private NotxConfig config;

    /**
     * @return the config
     */
    public NotxConfig getConfig() {
        return config;
    }

    /**
     * @param config the config to set
     */
    public void setConfig(NotxConfig config) {
        Logger rootLogger = Logger.getRootLogger();
        Enumeration appenders = rootLogger.getAllAppenders();
        
        String newPath = config.getModuleProperty(NotxConfig.MODULE, NotxConfig.LOG_DIR);         
        while (appenders.hasMoreElements()) {
            Appender currAppender = (Appender) appenders.nextElement();
            if (currAppender instanceof DailyRollingFileAppender) {
                DailyRollingFileAppender fa = (DailyRollingFileAppender) currAppender;
                fa.setFile(newPath+fa.getFile());
                fa.activateOptions();                                
            }
        }        
        this.config = config;
    }
}
