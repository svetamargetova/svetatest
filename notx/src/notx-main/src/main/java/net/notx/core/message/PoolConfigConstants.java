/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message;

/**
 *
 * @author palo
 */
public interface PoolConfigConstants {
    static final String MODULE="notx.worker.pool";
    static final String MAX_ACTIVE="maxActive";
    static final String MIN_IDLE="minIdle";
    static final String MAX_IDLE_TIME="maxIdleTime";
    static final String MAX_WORKING_TIME="maxWorkingTime";
    static final String EVICTION_CYCLE_TIME="evictionCycleTime";            
    static final String EVICTION_CYCLE_TESTS="evictionCycleTests";            
    static final String LOAD_MESSAGE_CYCLE_TIME="loadMessageCycleTime";            
    static final String MESSAGE_COUNT_PER_CYCLE="messageCountPerCycle";            
}
