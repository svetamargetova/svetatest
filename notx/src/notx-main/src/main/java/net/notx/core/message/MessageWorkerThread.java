/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message;

import com.google.common.collect.ImmutableMap;
import net.notx.client.message.AbstractMessage;
import net.notx.client.message.HandlingFailMessage;
import net.notx.core.queue.QueueService;
import net.notx.core.service.StatsService;
import org.apache.commons.lang.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public final class MessageWorkerThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(MessageWorkerThread.class);
    /**
     * Flag that worker should stop right after processing notification
     */
    private boolean stopped = true;
    private AbstractMessage messageToProcess;
    private long workerActionTime;
    private final Object syncObject;
    private final ImmutableMap<Class<? extends AbstractMessage>, MessageHandler> handlerMap;
    private final QueueService queueService;
    private final StatsService statsService;

    /**
     * Creates new worker thread with injected synchronization object
     *
     * @param syncObject
     */
    public MessageWorkerThread(Object syncObject, ImmutableMap<Class<? extends AbstractMessage>, MessageHandler> handlerMap, QueueService queueService, StatsService statsService) {
        super(THREAD_GROUP, getThreadName());
        this.syncObject = syncObject;
        this.handlerMap = handlerMap;
        this.queueService = queueService;
        this.statsService = statsService;
        setMessageToProcess(null);
        logger.debug("Initialized {}", getName());
    }

    /**
     * Sets stop flag.
     */
    public void stopThread() {
        synchronized (this) {
            stopped = true;
        }
    }

    /**
     * Returns stop worker flag
     *
     * @return
     */
    public boolean isStopped() {
        synchronized (this) {
            return stopped;
        }
    }

    /**
     * Returns amount of time of how long is worker in idle state
     *
     * @return worker idle time
     */
    public long getIdleTime() {
        return System.currentTimeMillis() - getActionTime();
    }

    /**
     * Returns amount of time of how long is worker processing current message
     *
     * @return worker processing time
     */
    public long getProcessingTime() {
        return System.currentTimeMillis() - getActionTime();
    }

    private long getActionTime() {
        synchronized (this) {
            return workerActionTime;
        }
    }

    public void setMessageToProcess(AbstractMessage message) {
        synchronized (this) {            
            this.messageToProcess = message;
            workerActionTime = System.currentTimeMillis();
        }
    }

    private void workDone() {
        // set default values        
        setMessageToProcess(null);        
        // notifiy some thread
        synchronized (syncObject) {
            syncObject.notify();
        }

    }

    public boolean isActive() {
        synchronized (this) {
            return messageToProcess != null;
        }
    }

    private void processError(Exception ex) {
        logger.error("Process message failed!" + messageToProcess, ex);
        final HandlingFailMessage failmsg = new HandlingFailMessage();
        failmsg.setException(ex);
        failmsg.setSqsMessage(messageToProcess);
        failmsg.setErrorCause(messageToProcess.toString());
        try {
            statsService.createFailMessage(failmsg);
            queueService.deleteMessage(messageToProcess);
        } catch (Exception e) {
            logger.debug("Error occurred while error reporting failed handling of " + failmsg, e);
        }
    }

    private void processMessage() {
        try {
            MessageHandler handler = handlerMap.get(messageToProcess.getClass());
            if (handler == null) {
                throw new NullPointerException("Handler for '" + ClassUtils.getShortClassName(messageToProcess.getClass()) + "' not found!");
            }
            handler.handleMessage(messageToProcess);
            // we will acknowledge that message was proccessed successfully
            queueService.deleteMessage(messageToProcess);
        } catch (Exception e) {
            processError(e);
        }
    }

    @Override
    public void run() {
        logger.debug("Starting {}", getName());
        stopped = false;
        while (!stopped) {
            // wait for setting up a message to process from controll thread
            synchronized (this) {
                while (messageToProcess == null) {
                    // notified because stop
                    if (stopped) {
                        break;
                    }

                    try {
                        wait();
                    } catch (InterruptedException ex) {
                    }
                }
                // notified because stop
                if (stopped) {
                    break;
                }
            }

            // we will process message
            processMessage();
            // mark this work done
            workDone();
        }
        logger.debug("Shuting down {}", getName());
    }

    @Override
    public synchronized String toString() {
        StringBuilder sb = new StringBuilder("MessageWorkerThread[");
        sb.append(" name=").append(getName());
        sb.append(" stopped=").append(stopped);
        sb.append(" active=").append(isActive());
        if (isActive()) {
            sb.append(" message=").append(messageToProcess.toString());
            sb.append(" time=").append(getProcessingTime());
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // if both are referring to the same object
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        MessageWorkerThread th = (MessageWorkerThread) obj;
        return th.getName().equals(getName());
    }

    @Override
    public int hashCode() {
        int hash = 7 * getName().hashCode();
        return hash;
    }
    //--- private utils methods  --------------------------------
    private static final ThreadGroup THREAD_GROUP = new ThreadGroup("MessageWorkerThreadGroup");
    private static int workerNumber = 0;

    private static synchronized String getThreadName() {
        return String.format("MessageWorker-%d", workerNumber++);
    }
}
