/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import net.notx.client.message.TagsMessage;
import net.notx.core.message.MessageHandler;
import net.notx.core.service.TaggingService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class TagsMessageHandler implements MessageHandler<TagsMessage> {

    @Autowired
    private TaggingService taggingService;

    @Override
    public void handleMessage(TagsMessage tagMsg) {
        for (String tag : tagMsg.getTags()) {
            taggingService.tagUser(tagMsg.getUserid(), tag);
        }
    }
}
