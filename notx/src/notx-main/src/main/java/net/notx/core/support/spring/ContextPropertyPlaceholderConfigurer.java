 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.support.spring;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 *
 * @author palo
 */
public class ContextPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(ContextPropertyPlaceholderConfigurer.class);
    private Context myEnv;

    @Override
    protected String resolvePlaceholder(String placeholder, Properties props) {
        try {
            logger.info("Look for: {}", placeholder);
            if (myEnv == null) {
                Context initContext = new InitialContext();
                myEnv = (Context) initContext.lookup("java:comp/env");
            }

            Object obj = myEnv.lookup(placeholder);
            if (obj != null) {
                logger.info("Found: {}:{}", placeholder,obj.toString());
                return obj.toString();
            } else {
                return null;
            }

        } catch (NamingException ex) {
            throw new IllegalArgumentException("Missing", ex);
        }
    }
}