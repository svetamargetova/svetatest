package net.notx.web;

import java.util.ArrayList;
import java.util.List;
import net.notx.cassandra.CassandraException;
import net.notx.client.message.AbstractMessage;
import net.notx.client.message.AbstractNotificationMessage;
import net.notx.client.message.HandlingFailMessage;
import net.notx.core.queue.QueueService;
import net.notx.core.service.StatsService;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 *
 * @author Filip Nguyen
 */
public class ErrorViewActionBean extends NotxActionBean {

    @SpringBean
    private StatsService statsService;
    @SpringBean
    private QueueService queueService;
    private List<HandlingFailMessage> notificationFails = new ArrayList<HandlingFailMessage>();
    private List<HandlingFailMessage> otherFails = new ArrayList<HandlingFailMessage>();
    private HandlingFailMessage detail = new HandlingFailMessage();
    private String theFailStackTrace = null;

    public Resolution show() throws CassandraException {
        List<HandlingFailMessage> hfms = statsService.listFailMessages();
        for (HandlingFailMessage hf : hfms) {
            if (hf.getSqsMessage() instanceof AbstractNotificationMessage) {
                notificationFails.add(hf);
            } else {
                otherFails.add(hf);
            }
        }
        return new ForwardResolution("/errorView.jsp");
    }

    public Resolution resendNotifications() throws CassandraException {
        resendFails(notificationFails);
        return show();
    }

    public Resolution resendOthers() throws CassandraException {
        resendFails(otherFails);
        return show();
    }

    public Resolution viewDetail() {
        detail = statsService.loadFailMessages(detail.getUid());
        theFailStackTrace = ExceptionUtils.getFullStackTrace(detail.getException());
        return new ForwardResolution("/errorViewDetail.jsp");
    }

    /**
     * Sends back to mq message that is viewed in detail.
     *
     * @return
     */
    public Resolution sendNotificationToMQ() {
        detail = statsService.loadFailMessages(detail.getUid());
        queueService.putMessage((AbstractMessage)detail.getSqsMessage());
        statsService.deleteFailMessage(detail);        
        return show();
    }

    /**
     * Takes fails and resends them into MQ.
     *
     * @param fails
     * @throws CassandraException
     */
    public void resendFails(List<HandlingFailMessage> fails) throws CassandraException {

        for (final HandlingFailMessage hf : fails) {
            queueService.putMessage((AbstractMessage) hf.getSqsMessage());
            statsService.deleteFailMessage(hf);
        }
    }

    public HandlingFailMessage getDetail() {
        return detail;
    }

    public void setDetail(HandlingFailMessage detail) {
        this.detail = detail;
    }

    public void setNotificationFails(List<HandlingFailMessage> notificationFails) {
        this.notificationFails = notificationFails;
    }

    public List<HandlingFailMessage> getNotificationFails() {
        return notificationFails;
    }

    public void setOtherFails(List<HandlingFailMessage> otherFails) {
        this.otherFails = otherFails;
    }

    public List<HandlingFailMessage> getOtherFails() {
        return otherFails;
    }

    public void setTheFailStackTrace(String theFailStackTrace) {
        this.theFailStackTrace = theFailStackTrace;
    }

    public String getTheFailStackTrace() {
        return theFailStackTrace;
    }
}
