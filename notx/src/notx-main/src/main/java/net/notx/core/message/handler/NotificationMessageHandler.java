/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import java.util.List;
import net.notx.client.message.NotificationMessage;
import net.notx.client.message.UserNotificationMessage;
import net.notx.core.message.MessageHandler;
import net.notx.core.queue.QueueService;
import net.notx.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Notification is processed and for every user that is defined by {@link NotificationMessage#getTo()}
 * will generate new message
 *
 * @author palo
 */
public class NotificationMessageHandler implements MessageHandler<NotificationMessage> {

    @Autowired
    private QueueService queueService;
    @Autowired
    private UserService userService;

    private void addNotification(String userId, NotificationMessage message) {
        UserNotificationMessage unm = new UserNotificationMessage(userId, message.getMsgType(), message.getTemplateName(), message.getPlaceHolderVals());
        queueService.putMessage(unm);
    }

    @Override
    public void handleMessage(NotificationMessage message) {
        
        // Concrete user ids will be processed first because there is higher propability
        // that their are only few ids instead of tagged group of ids.
        if (message.containsUserIds()) {
            String[] ids = message.getIds();
            for (int i = 0; i < ids.length; i++) {
                addNotification(ids[i], message);
            }
        }

        // process tagged users
        if (message.containsTag()) {
            List<String> taggedIds = userService.loadTaggedUserIds(message.getTag());
            for (String string : taggedIds) {
                addNotification(string, message);
            }
        }                
    }
}
