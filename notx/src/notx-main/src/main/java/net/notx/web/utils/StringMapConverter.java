package net.notx.web.utils;

import java.util.Collection;
import java.util.Locale;
import net.notx.utils.StringMap;
import net.sourceforge.stripes.validation.TypeConverter;
import net.sourceforge.stripes.validation.ValidationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringMapConverter implements TypeConverter<StringMap> {

    private Logger logger = LoggerFactory.getLogger(StringMapConverter.class);

    @Override
    public StringMap convert(String text,
            Class<? extends StringMap> arg1,
            Collection<ValidationError> errors) {
        logger.debug("Converting: {} to map.",text);

        StringMap result = new StringMap();

        for (String placeholder : text.split("\\n")) {
            String[] plc = placeholder.split("=");
            result.put(plc[0].trim(), plc[1].trim());
        }

        logger.debug("Resulting map has " + result.size() + " items. Keys are: " + result.keySet());
        return result;
    }

    @Override
    public void setLocale(Locale arg0) {
        // TODO Auto-generated method stub
    }
}
