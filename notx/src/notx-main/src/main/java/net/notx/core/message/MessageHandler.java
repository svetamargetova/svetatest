/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message;

import net.notx.client.message.AbstractMessage;

/**
 * Implementations of this interface are responsible for processing defined
 * message
 * <code>T</code>.
 *
 * @author palo
 */
public interface MessageHandler<T extends AbstractMessage> {

    /**
     * Processes given message. This method have to be thread safe.
     *
     * @param message to process
     * 
     */
    public abstract void handleMessage(T message);
}
