package net.notx.web;

import java.util.ArrayList;
import java.util.List;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.TemplateDescriptor;
import net.notx.core.service.TemplateService;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

/**
 *
 * @author Filip Nguyen
 */
public class SuperTemplateActionBean extends NotxActionBean {

    @SpringBean
    private TemplateService templateService;
    private List<SuperTemplate> allSuperTemplates = new ArrayList<SuperTemplate>();
    private SuperTemplate selectedSuperTemplate = new SuperTemplate();
    private List<TemplateDescriptor> versions = new ArrayList<TemplateDescriptor>();
    @ValidateNestedProperties({
        @Validate(field = "name", required = true),
        @Validate(field = "description", required = true)
    })
    private SuperTemplate newSuperTemplate = null;

    @DontValidate
    public Resolution show() {
        allSuperTemplates = templateService.listTemplates();

        if (allSuperTemplates.size() > 0 && selectedSuperTemplate.getName() == null) {
            selectedSuperTemplate.setName(allSuperTemplates.get(0).getName());
        }

        if (selectedSuperTemplate.getName() != null) {
            selectedSuperTemplate = templateService.loadTemplate(selectedSuperTemplate.getName());
            versions = selectedSuperTemplate.getVersions();
        }
        return new ForwardResolution("/superTemplate.jsp");
    }

    public Resolution newSuperTemplate() {
        templateService.createTemplate(newSuperTemplate);
        return show();
    }

    public List<SuperTemplate> getAllSuperTemplates() {
        return allSuperTemplates;
    }

    public void setAllSuperTemplates(List<SuperTemplate> allSuperTemplates) {
        this.allSuperTemplates = allSuperTemplates;
    }

    public SuperTemplate getSelectedSuperTemplate() {
        return selectedSuperTemplate;
    }

    public void setSelectedSuperTemplate(SuperTemplate selectedSuperTemplate) {
        this.selectedSuperTemplate = selectedSuperTemplate;
    }

    public List<TemplateDescriptor> getVersions() {
        return versions;
    }

    public void setVersions(List<TemplateDescriptor> versions) {
        this.versions = versions;
    }

    public SuperTemplate getNewSuperTemplate() {
        return newSuperTemplate;
    }

    public void setNewSuperTemplate(SuperTemplate newSuperTemplate) {
        this.newSuperTemplate = newSuperTemplate;
    }
}
