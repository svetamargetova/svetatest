/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message.handler;

import net.notx.client.message.UnTagsMessage;
import net.notx.core.message.MessageHandler;
import net.notx.core.service.TaggingService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class UnTagsMessageHandler implements MessageHandler<UnTagsMessage> {

    @Autowired
    private TaggingService taggingService;

    @Override
    public void handleMessage(UnTagsMessage message) {
        for (String tag : message.getTags()) {
            taggingService.unTagUser(message.getUserid(), tag);
        }
    }
}
