package net.notx.web;

import net.notx.cassandra.CassandraException;
import net.notx.core.entities.User;
import net.notx.core.service.UserService;
import net.notx.web.utils.StringMapConverter;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;


/**
 *
 * @author Filip Nguyen
 */
public class UsersDetailActionBean extends NotxActionBean {

    @SpringBean
    private UserService userService;
    /**
     * Added or edited user.
     */
    @ValidateNestedProperties({
        @Validate(field = "id", required = true),
        @Validate(field = "lang", required = true),
        @Validate(field = "name", required = true),
        @Validate(field = "contacts", converter = StringMapConverter.class)})
    private User user = new User();

    ;
     
    @DontValidate
    public Resolution preEdit() throws CassandraException {
        this.user = userService.loadUser(user.getId());
        return new ForwardResolution("/usersDetail.jsp");
    }

    @DontValidate
    public Resolution delete() {
        userService.deleteUser(user.getId());
        return new ForwardResolution(UsersActionBean.class, "show");
    }

    @DontValidate
    public Resolution preNew() throws CassandraException {
        return new ForwardResolution("/usersDetail.jsp");
    }

    /**
     * This is handler invoked when applying changes to one line of Users'
     * table. When the contact with given name is already present the contact is
     * overwritten.
     *
     * @throws CassandraException
     *
     */
    public Resolution save() throws CassandraException {
        userService.createUser(user);
        return new ForwardResolution(UsersActionBean.class, "show");
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
