/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core;

import java.util.List;
import net.notx.core.config.NotxConfig;
import net.notx.core.engine.EnginesDeployer;
import net.notx.core.message.MessageWorkerThreadPool;
import net.notx.core.message.handler.AbstractNotificationHandler;
import net.notx.core.queue.LoadMessagesThread;
import net.notx.core.queue.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * This class will start whole engine
 *
 * @author palo
 */
public class Bootstrapper implements ApplicationListener<ContextRefreshedEvent>, ApplicationContextAware, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(Bootstrapper.class);
    private boolean alreadySuccessfullyInitialized = false;
    private ApplicationContext ctx;
    @Autowired
    private NotxConfig config;
    @Autowired
    private MessageWorkerThreadPool threadPool;
    @Autowired
    private QueueService queueService;
    @Autowired
    private List<AbstractNotificationHandler> notificationHandlers;
    //--- private properties
    /**
     * Thread that loads messages from queue.     
     */
    private LoadMessagesThread loadMessagesThread;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (!alreadySuccessfullyInitialized) {
            logger.info("Starting NotX with config version: {}", config.getVersion());

            // 1] initialize Queue
            queueService.initialize(config);

            // 2] load engines and set them into notification handlers
            EnginesDeployer deployer = new EnginesDeployer();
            deployer.loadEngines(config);            
            for (AbstractNotificationHandler notif : notificationHandlers) {
                notif.setEngines(deployer.getEngines());
            }

            // 3] start pool
            threadPool.start();

            // 4] start queue thread
            loadMessagesThread = new LoadMessagesThread(
                    config.getIntModuleProperty(LoadMessagesThread.MODULE, LoadMessagesThread.LOAD_MESSAGE_CYCLE_TIME),
                    config.getIntModuleProperty(LoadMessagesThread.MODULE, LoadMessagesThread.MESSAGE_COUNT_PER_CYCLE));
            loadMessagesThread.setQueueProvider(queueService);
            loadMessagesThread.setWorkerThreadPool(threadPool);
            // set priority - MAX=10 NORM= 5 so we choose somewher middle
            loadMessagesThread.setPriority(7);
            loadMessagesThread.start();
            
            alreadySuccessfullyInitialized = true;
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    @Override
    public void destroy() throws Exception {
        // shut down queue thread
        loadMessagesThread.stopThread();
        
        // shut down pool
        threadPool.close();
    }
}
