package net.notx.web;

import net.notx.cassandra.CassandraException;
import net.notx.core.service.InitService;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 * @author Filip Nguyen
 */
public class UtilityActionBean extends NotxActionBean {

    @SpringBean
    private InitService initService;

    @DontValidate
    public Resolution show() throws CassandraException {
        return new ForwardResolution("/index.jsp");
    }

    public boolean isInitialized() {
        return initService.isInitialized();
    }

    public Resolution initialize() {
        initService.initialize();
        return new ForwardResolution("/index.jsp");
    }
}
