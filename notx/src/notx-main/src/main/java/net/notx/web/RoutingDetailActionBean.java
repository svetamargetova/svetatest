package net.notx.web;

import java.util.ArrayList;
import java.util.List;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.User;
import net.notx.core.service.TaggingService;
import net.notx.core.service.UserService;
import net.notx.web.utils.JsonUtils;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

/**
 *
 * @author Filip Nguyen
 */
public class RoutingDetailActionBean extends NotxActionBean {

    @SpringBean
    private UserService userService;
    @SpringBean
    private TaggingService taggingService;
    private NotxActionBeanContext context;
    private List<User> users = new ArrayList<User>();
    private String autocompleteTags = null;
    @ValidateNestedProperties({
        @Validate(field = "priority", required = true, minvalue = 1, maxvalue = 10),
        @Validate(field = "destination", required = true, mask = "(\\w+(\\+\\w+)*)?")})
    private RoutingRecord routingRecord = new RoutingRecord();

    @DontValidate
    public Resolution preNew() {
        autocompleteTags = JsonUtils.convertToAutocomplete(taggingService.listTags());
        users = userService.listUsers();
        return new ForwardResolution("/routingDetail.jsp");
    }

    public Resolution save() {
        userService.createRoute(routingRecord);
        return new ForwardResolution(RoutingActionBean.class, "show");
    }

    @DontValidate
    public Resolution delete() {
        userService.deleteRoute(routingRecord);
        ForwardResolution fwres = new ForwardResolution(RoutingActionBean.class, "show");
        fwres.addParameter("selectedUser.id", routingRecord.getUserid());
        return fwres;
    }

    public String getAutocompleteTags() {
        return autocompleteTags;
    }

    public void setAutocompleteTags(String autocompleteTags) {
        this.autocompleteTags = autocompleteTags;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public RoutingRecord getRoutingRecord() {
        return routingRecord;
    }

    public void setRoutingRecord(RoutingRecord routingRecord) {
        this.routingRecord = routingRecord;
    }
}
