package net.notx.web.utils;

import java.util.Locale;
import java.util.Map.Entry;
import net.notx.utils.StringMap;
import net.sourceforge.stripes.format.Formatter;

public class StringMapFormatter implements Formatter<StringMap> {

    @Override
    public String format(StringMap stringMap) {
        StringBuilder result = new StringBuilder();

        for (Entry<String, String> stringline : stringMap.entrySet()) {
            result.append(stringline.getKey()).append("=").append(stringline.getValue()).append("\n");
        }

        return result.toString();
    }

    @Override
    public void setFormatType(String formatType) {
    }

    @Override
    public void setFormatPattern(String formatPattern) {
    }

    @Override
    public void setLocale(Locale locale) {
    }

    @Override
    public void init() {
    }
}
