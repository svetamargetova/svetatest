package net.notx.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.RoutingRecordCollection;
import net.notx.core.entities.User;
import net.notx.core.service.UserService;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;


/**
 * 
 * @author Filip Nguyen
 */
public class RoutingActionBean extends NotxActionBean {
	
    @SpringBean
    private UserService userService;
	
	private User selectedUser = new User();
	private List<User> users = new ArrayList<User>();
	private Set<RoutingRecord> routingRecords = new TreeSet<RoutingRecord>();
	
	public Resolution show(){
		users = userService.listUsers();
		RoutingRecordCollection rrCollection = userService.loadUserRoutingRecords(selectedUser.getId());
		routingRecords=rrCollection.toListByPriority();
		return new ForwardResolution("/routing.jsp");
	}	
			
	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Set<RoutingRecord> getRoutingRecords() {
		return routingRecords;
	}

	public void setRoutingRecords(Set<RoutingRecord> routingRecords) {
		this.routingRecords = routingRecords;
	}
}

