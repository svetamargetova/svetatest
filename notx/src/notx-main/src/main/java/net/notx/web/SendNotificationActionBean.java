package net.notx.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.notx.cassandra.CassandraException;
import net.notx.client.message.NotificationMessage;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.queue.QueueService;
import net.notx.core.service.TaggingService;
import net.notx.core.service.TemplateService;
import net.notx.utils.StringMap;
import net.notx.web.utils.JsonUtils;
import net.notx.web.utils.StringMapConverter;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;


/**
 * Action that allows programmer to try sending notifications via NotX system.
 * This is just for testing purposes.
 *
 * @author Filip Nguyen
 */
public class SendNotificationActionBean extends NotxActionBean {

    @SpringBean
    private TemplateService templateService;
    @SpringBean
    private TaggingService taggingService;
    @SpringBean
    private QueueService queueService;
    private NotxActionBeanContext context;
    private Set<String> allMessageTypes = new HashSet<String>();
    private Set<String> allTemplateNames = new HashSet<String>();
    private String autocompleteTagsAndIDs = new String();
    private String descriptionsMap = null;
    private boolean sent = false;
    @Validate(required = true)
    private String tag;
    @Validate(required = true)
    private String msgType;
    @Validate(required = true)
    private String templateName;
    private String templateDescription;
    /**
     * Syntax: placeholder1=xyz,placeholder2=asdf,....
     */
    @Validate(converter = StringMapConverter.class)
    private StringMap placeHolders;

    @DontValidate
    public Resolution show() throws CassandraException {
        List<SuperTemplate> superTemplates = templateService.listTemplates();
        descriptionsMap = buildDescriptionsMap(superTemplates);

        templateDescription = superTemplates.get(0).getDescription();

        for (SuperTemplate st : superTemplates) {
            allTemplateNames.add(st.getName());
        }
        //TODO jeste ids
        autocompleteTagsAndIDs = JsonUtils.convertToAutocomplete(taggingService.listTags());

        return new ForwardResolution("/sendNotification.jsp");
    }

    private String buildDescriptionsMap(List<SuperTemplate> superTemplates) {
        String result = "";
        int i = 0;
        for (SuperTemplate st : superTemplates) {
            if (i != 0) {
                result += ",";
            }
            i++;
            String description = "No description";
            if (st.getDescription() != null) {
                description = st.getDescription();
            }

            result += "\"" + st.getName() + "\":\"" + description + "\"";
        }

        return result;
    }

    public Resolution sendNotification() {
        final NotificationMessage jmsReq = new NotificationMessage(tag, msgType, templateName, placeHolders);
        queueService.putMessage(jmsReq);
        return show();
    }

    public boolean getSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public String getTemplateDescription() {
        return templateDescription;
    }

    public void setTemplateDescription(String templateDescription) {
        this.templateDescription = templateDescription;
    }

    public String getDescriptionsMap() {
        return descriptionsMap;
    }

    public void setDescriptionsMap(String descriptionsMap) {
        this.descriptionsMap = descriptionsMap;
    }

    public Set<String> getAllMessageTypes() {
        return allMessageTypes;
    }

    public void setAllMessageTypes(Set<String> allMessageTypes) {
        this.allMessageTypes = allMessageTypes;
    }

    public Set<String> getAllTemplateNames() {
        return allTemplateNames;
    }

    public void setAllTemplateNames(Set<String> allTemplateNames) {
        this.allTemplateNames = allTemplateNames;
    }

    public String getAutocompleteTagsAndIDs() {
        return autocompleteTagsAndIDs;
    }

    public void setAutocompleteTagsAndIDs(String autocompleteTagsAndIDs) {
        this.autocompleteTagsAndIDs = autocompleteTagsAndIDs;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public StringMap getPlaceHolders() {
        return placeHolders;
    }

    public void setPlaceHolders(StringMap placeHolders) {
        this.placeHolders = placeHolders;
    }    
}
