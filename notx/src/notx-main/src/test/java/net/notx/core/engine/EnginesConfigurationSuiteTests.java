/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.engine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Suite test for configurations
 * @author palo
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        EnginesConfigSuite.class,
        EnginesDeployerSuite.class
})
public class EnginesConfigurationSuiteTests {
    
}
