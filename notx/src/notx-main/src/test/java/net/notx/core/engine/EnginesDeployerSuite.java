/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.engine;

import com.google.common.collect.Lists;
import net.notx.engine.EngineSettings;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author palo
 */
// @InSuite(EnginesConfigurationSuiteTests)
public class EnginesDeployerSuite {
    
    @Test
    public void testLoadTestEngine(){        
        EnginesDeployer deployer = new EnginesDeployer();
        EngineSettings settings = mock(EngineSettings.class);
        when(settings.getEngineName()).thenReturn("Engine Test");
        when(settings.getEngineJarName()).thenReturn("notx-testengine-2.0.0-M1.jar");
        when(settings.getEngineClass()).thenReturn("net.notx.engine.test.TestEngine");
        when(settings.getStringProperty("sleep")).thenReturn("1000");
        when(settings.isEnabled()).thenReturn(true);
        
        String workingDir = System.getProperty("user.dir");
        // TODO(someone) - make it work on Windows and other platforms
        String enginesFolder = workingDir+"/src/test/resources/engines";
        
        deployer.loadEngines(Lists.newArrayList(settings), enginesFolder);
        assertEquals(1, deployer.getEngines().size());                                
    }        
}
