/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.engine;

import java.util.List;
import net.notx.core.config.NotXPropertiesConfig;
import net.notx.core.config.NotxConfig;
import net.notx.engine.EngineSettings;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Suite test for engine configuration 
 * @author palo
 */
// @InSuite(EnginesConfigurationSuiteTests)
public class EnginesConfigSuite {
        
    @Test
    public void testLoadEngines(){        
        NotxConfig notxConfig = new NotXPropertiesConfig("classpath:");
        EnginesConfig config = new EnginesConfig(notxConfig);
        List<EngineSettings> engines = config.getEngineSettings();
        assertEquals(2, engines.size());
        assertEquals("engine0name", engines.get(0).getEngineName());
        assertEquals(true, engines.get(0).isEnabled());
        assertEquals("property1", engines.get(0).getStringProperty("custom.property"));
        assertEquals("engine1name", engines.get(1).getEngineName());
        assertEquals(false, engines.get(1).isEnabled());
        assertEquals("property2", engines.get(1).getStringProperty("custom.property"));        
    }
}
