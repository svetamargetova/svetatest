/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.message;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import net.notx.client.message.AbstractMessage;
import net.notx.core.config.NotxConfig;
import net.notx.core.queue.LoadMessagesThread;
import net.notx.core.queue.QueueService;
import net.notx.core.service.StatsService;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class MessageWorkerThreadPoolTests {

    private static final Logger logger = LoggerFactory.getLogger(MessageWorkerThreadPoolTests.class);

    public void testFactory() throws InterruptedException {
        MessageWorkerThreadFactory factory = new MessageWorkerThreadFactory();
        MessageWorkerThread thread = (MessageWorkerThread) factory.makeObject();
        // before activated
        assertFalse(thread.isAlive());
        assertFalse(thread.isActive());
        assertTrue(thread.isStopped());

        // activate thread
        assertTrue(factory.validateObject(thread));
        factory.activateObject(thread);
        Thread.sleep(500);
        assertTrue(thread.isAlive());
        assertFalse(thread.isActive());
        assertFalse(thread.isStopped());

        // destroy thread
        factory.destroyObject(thread);
        Thread.sleep(500);
        assertFalse(thread.isAlive());
        assertFalse(thread.isActive());
        assertTrue(thread.isStopped());
    }
    static int rounds = 4;
    static AtomicInteger processed = new AtomicInteger(0);
    static AtomicInteger msgNumber = new AtomicInteger(0);
    static int msgPerCycle = 15;
    static int msgCycle = 2000;

    @Test
    public void testPool() throws Exception {
        NotxConfig config = new NotxConfig() {

            @Override
            public String getModuleProperty(String module, String key) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public int getIntModuleProperty(String module, String key) {
                if (key.equals(PoolConfigConstants.EVICTION_CYCLE_TIME)) {
                    return 5000;
                }

                if (key.equals(PoolConfigConstants.EVICTION_CYCLE_TESTS)) {
                    return 5;
                }

                if (key.equals(PoolConfigConstants.MAX_ACTIVE)) {
                    return 15;
                }

                if (key.equals(PoolConfigConstants.MAX_IDLE_TIME)) {
                    return 5000;
                }

                if (key.equals(PoolConfigConstants.MAX_WORKING_TIME)) {
                    return 5000;
                }

                if (key.equals(PoolConfigConstants.MIN_IDLE)) {
                    return 5;
                }
                throw new NullPointerException("Not defined in test for: " + key);
            }

            @Override
            public String getVersion() {
                return "1.0.0";
            }

            @Override
            public Map<String, String> getModuleProperties(String module) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };


        int _rounds = rounds;

        QueueService queueServiceMock = mock(QueueService.class);
        when(queueServiceMock.loadMessages(anyInt())).thenAnswer(new Answer<List<? extends AbstractMessage>>() {

            @Override
            public List<? extends AbstractMessage> answer(InvocationOnMock invocation) throws Throwable {
                // we will create 2x15 messages and then stop

                List<TestMessage> messages = Lists.newArrayList();
                if (rounds > 0) {
                    logger.info("Generating next: " + msgPerCycle);
                    for (int i = 0; i < msgPerCycle; i++) {
                        messages.add(new TestMessage("Message "+msgNumber.getAndIncrement()));
                    }
                    rounds -= 1;
                }
                return messages;
            }
        });

        StatsService statsServiceMock = mock(StatsService.class);


        List l = Lists.newArrayList(new TestMessageHandler());

        MessageWorkerThreadFactory factory = new MessageWorkerThreadFactory();
        factory.setQueueService(queueServiceMock);
        factory.setStatsService(statsServiceMock);
        factory.setHandlers(l);
        factory.afterPropertiesSet();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }


        MessageWorkerThreadPool pool = new MessageWorkerThreadPool(config, factory);
        pool.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        LoadMessagesThread thread = new LoadMessagesThread(msgCycle, msgPerCycle);
        thread.setQueueProvider(queueServiceMock);
        thread.setWorkerThreadPool(pool);
        thread.start();

        // wait for processing
        while (processed.get() < (_rounds * msgPerCycle)) {
            logger.info("Testing processed messages: "+processed.get());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
            }
        }
        assertEquals(_rounds*msgPerCycle, processed.get());
        
        Thread.sleep(10000);        
        thread.stopThread();
        pool.close();
        
    }

    public class TestMessageHandler implements MessageHandler<TestMessage> {

        @Override
        public void handleMessage(TestMessage message) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
            logger.info("Processed("+processed.incrementAndGet()+") message: "+message.toString());            
        }
    }

    public class TestMessage extends AbstractMessage {

        private static final long serialVersionUID = 2223780160889736364L;
        
        String name;

        public TestMessage(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
        
        
    }
}
