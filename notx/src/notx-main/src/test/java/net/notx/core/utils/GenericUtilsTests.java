/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.core.utils;

import com.google.common.collect.Maps;
import java.util.Map;
import net.notx.client.message.AbstractMessage;
import net.notx.core.message.MessageHandler;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author palo
 */
public class GenericUtilsTests {
    
    @Test
    public void testGenericUtils(){
    
        // test for extracting handler type
        TestMessageHandler handler = new TestMessageHandler();        
        Class<? extends AbstractMessage> message = GenericUtils.getMessageTypeFromHandler(handler);
        assertTrue(TestMessage.class.equals(message));
        
        // test for map access
        Map<Class<? extends AbstractMessage>,MessageHandler> map = Maps.newHashMap();
        map.put(message, handler);
        
        MessageHandler h = map.get(TestMessage.class);
        assertNotNull(h);
        assertEquals(TestMessageHandler.class, h.getClass());

    }
    
    // test message
    public class TestMessage extends AbstractMessage{
        private static final long serialVersionUID = -6683495159565098217L;
    
    }
    
    // test message handler
    public class TestMessageHandler implements MessageHandler<TestMessage>{

        @Override
        public void handleMessage(TestMessage message) {              
        }    
    }
    
}
