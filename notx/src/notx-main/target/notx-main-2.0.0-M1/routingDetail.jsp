<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="documentReady">
          notxAutocomplete('source',${actionBean.autocompleteTags});      
    </stripes:layout-component>
    <stripes:layout-component name="contents">
        <h1>Routing record</h1>
        <stripes:errors/>
        
         <table>
            <stripes:form action="/RoutingDetail.action" focus="">
              <tbody>
                <tr><td><stripes:label for="routingRecord.userid"/> 
                    </td>
                    <td>
                            <stripes:select name="routingRecord.userid">
                                  <stripes:option value="">Global routing record</stripes:option>
                                  <stripes:options-collection collection="${actionBean.users}" label="name" value="id"/>  
                            </stripes:select>
                    </td>
                </tr>
                <tr><td><stripes:label for="routingRecord.msgtype"/></td><td><stripes:text name="routingRecord.msgtype"/></td></tr>
                <tr><td><stripes:label for="routingRecord.source"/></td><td><stripes:text id="source" name="routingRecord.source"/></td></tr>
                <tr><td><stripes:label for="routingRecord.destination"/></td><td><stripes:text name="routingRecord.destination"/></td></tr>
                <tr><td><stripes:label for="routingRecord.priority"/></td>
                  <td><stripes:text name="routingRecord.priority">
                        <c:if test="${routingRecord.priority=='0'}">5</c:if>
                      </stripes:text>
                  </td>
                </tr>
                <tr><td colspan="2"><stripes:submit name="save" value="Save"/></td></tr>
              </tbody>
            </stripes:form>
        </table>    
       
    </stripes:layout-component>
</stripes:layout-render>
