<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
		  	<h1>All sent notifications</h1>
		    <stripes:errors/>
		    <c:forEach items="${actionBean.statisticsByDomain}" var="stats" varStatus="rowstat">
			    <h2> <td>${stats.key}</td> </h2>
					<table class="elegant-table">
					 <thead>
						<tr> 
							<th>Sent time</th>
							<th>Engine</th>
							<th>Contact</th>
							<th>Correlation time</th>
						</tr>
					</thead>
		      <tbody>
						<c:forEach items="${stats.value}" var="stat" varStatus="rowstat">
					    <tr class="${rowstat.count mod 2 == 0 ? "even" : "odd"} ${empty stat.receivedTime ? "not-correlated" : "correlated"}">
					     	<td><fmt:formatDate value="${stat.time}" pattern="MM.dd HH:mm" /></td>
					     	<td>${stat.engine}</td>
					     	<td>${stat.contact}</td>
					     	<td><fmt:formatDate value="${stat.correlationTime}" pattern="MM.dd HH:mm" /></td>
					    </tr>
				    </c:forEach>
				   </tbody>
					</table>
				</c:forEach>    
				<br/>
				<img src="${actionBean.graph}"/>
    </stripes:layout-component>
</stripes:layout-render>
