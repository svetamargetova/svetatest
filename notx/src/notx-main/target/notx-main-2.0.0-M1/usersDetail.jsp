<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
		  	<h1>User</h1>
		    <stripes:errors/>
			  <table>
				    <stripes:form action="/UsersDetail.action" focus="">
				    <tbody>
			     		<tr><td><stripes:label for="user.id"/> </td><td><stripes:text name="user.id"/></td></tr>
			     		<tr><td><stripes:label for="user.name"/></td><td><stripes:text  name="user.name"/></td></tr>
			        <tr><td><stripes:label for="user.lang"/></td><td><stripes:text  name="user.lang"/></td></tr>
			        <tr><td><stripes:label for="contacts"/> </td><td><stripes:textarea name="user.contacts"/></td></tr>
			        <tr><td colspan="2"><stripes:submit name="save" value="Save"/></td></tr>
			       </tbody>
						</stripes:form>
				</table>    
    </stripes:layout-component>
</stripes:layout-render>
