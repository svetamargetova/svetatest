<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="documentReady">
          descriptions={${actionBean.descriptionsMap}};
    
          notxAutocomplete('tag',${actionBean.autocompleteTagsAndIDs});
          
          $('#templateName').change(function() {
              $('#templateDescription').text(descriptions[$(this).val()]);
          });
    </stripes:layout-component>
    <stripes:layout-component name="contents">
		  	<h1>Sending notification</h1>
		    <stripes:errors/>
		    <c:if test="${actionBean.sent}">
  		    <div class="toast">The notification has been sent to MQ</div>
		    </c:if>
		    
			  <table class="form-table">
				    <stripes:form action="/SendNotification.action" focus="">
				    <tbody>
			     		<tr><td> <stripes:label for="tag"/></td><td><stripes:text id="tag" name="tag"/></td></tr>
			     		<tr><td><stripes:label for="msgType"/></td><td><stripes:text name="msgType"/></td></tr>
			     		<tr><td><stripes:label for="templateName"/></td>
			     		  <td>
	 		     		      <stripes:select id="templateName" name="templateName">
                      <stripes:options-collection collection="${actionBean.allTemplateNames}"/>
                    </stripes:select>
			     		  </td>
			     		</tr>
			     		<tr><td><stripes:label for="templateDescription"/> </td><td><div id="templateDescription">${actionBean.templateDescription}</div></td></tr>
			     		<tr><td><stripes:label for="placeHolders"/><br/><div class="hint-text">key=value per line</div></td><td><stripes:textarea name="placeHolders" rows="10" cols="80"/></td></tr>
			     	  <tr><td colspan="2"><stripes:submit name="sendNotification" value="Send notification"/></td></tr>
			       </tbody>
						</stripes:form>
						
						
				</table>    
    </stripes:layout-component>
</stripes:layout-render>
