<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="documentReady">
          $('#selectedUserID').change(function() {
              $('#formListOfRR').submit();
          });
    </stripes:layout-component>
    <stripes:layout-component name="contents">
        <h1>Routing records</h1>
        <stripes:errors/>
        
        <stripes:link href="/RoutingDetail.action" event="preNew">New routing record</stripes:link>
     
        <h2>List of routing records</h2>
        <stripes:form id="formListOfRR" action="/Routing.action" focus="">
           <stripes:select id="selectedUserID" name="selectedUser.id">
              <stripes:option value="">Global routing records</stripes:option>
              <stripes:options-collection collection="${actionBean.users}"
                                          label="name" value="id"/>
           </stripes:select>
        </stripes:form>
        <table class="elegant-table">
				 <thead>
					<tr>
					  <th>For user</th> 
						<th>Priority</th>
						<th>Source</th>
						<th>Destionation</th>
						<th>Message type</th>
						<th></th>
					</tr>
				 </thead>
				 <tbody>
		    	<c:forEach items="${actionBean.routingRecords}" var="rr" varStatus="rowstat">
			    	<tr class="${rowstat.count mod 2 == 0 ? "even" : "odd"}">
			      	<td>${rr.userid}</td>
			      	<td>${rr.priority}</td>
			      	<td>${rr.source}</td>
			      	<td>${rr.destination}</td>
			      	<td>${rr.msgtype}</td>
			      	<td><stripes:link  href="/RoutingDetail.action" event="delete">
			      	      delete
			      	      <stripes:param name="routingRecord.recordid" value="${rr.recordid}"/>
			      	      <stripes:param name="routingRecord.userid" value="${rr.userid}"/>
			      	    </stripes:link></td>
			      </tr>
					</c:forEach>
					</tbody>
				</table> 
    </stripes:layout-component>
</stripes:layout-render>
