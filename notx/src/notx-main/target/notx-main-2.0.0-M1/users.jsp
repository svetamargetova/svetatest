<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
        <h1>Users</h1>
        <stripes:link href="/UsersDetail.action" event="preNew">New contact </stripes:link>
		    <stripes:errors/>
				<table class="elegant-table">
				 <thead>
					<tr>
					  <th>ID</th> 
						<th>Name</th>
						<th>Language</th>
						<th>Contacts</th>
						<th>Tags</th>
						<th></th>
					</tr>
				 </thead>
				 <tbody>
		    	<c:forEach items="${actionBean.allUsers}" var="user" varStatus="rowstat">
			    	<tr class="${rowstat.count mod 2 == 0 ? "even" : "odd"}">
			      	<td>${user.id}</td>
			      	<td><div style="width:100px">${user.name}</div></td>
			      	<td>${user.lang}</td>
			        <td>	
			            ${user.contacts}
			        </td>
			        <td>
			        		<c:forEach items="${user.tags}" var="tag">${tag} </c:forEach>
			        </td>
			       	<td>
								<stripes:link href="/UsersDetail.action" event="preEdit">
		            	Edit
		             <stripes:param name="user.id" value="${user.id}"/>
		            </stripes:link>
		            <stripes:link href="/UsersDetail.action" event="delete">
                  Delete
                  <stripes:param name="user.id" value="${user.id}"/>
                </stripes:link>
		          </td>
			      </tr>
					</c:forEach>
					</tbody>
				</table> 
    </stripes:layout-component>
</stripes:layout-render>
