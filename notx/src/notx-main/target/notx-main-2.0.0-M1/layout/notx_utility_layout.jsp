<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-definition>
    <html>
        <head>
            <title>NotX Web interface</title>
            <script type="text/javascript" src="${ctx}/layout/jquery-1.5.1.min.js"></script>
            <script type="text/javascript" src="${ctx}/layout/jquery-ui-1.8.13.custom.min.js"></script>
            <script type="text/javascript" src="${ctx}/layout/script.js"></script>
            <link rel="stylesheet" type="text/css" href="${ctx}/layout/jquery-ui-1.8.13.custom.css">
            <link rel="stylesheet" type="text/css" href="${ctx}/layout/styles.css">
  		      <script type="text/javascript">
  		        $(document).ready(function(){
                <stripes:layout-component name="documentReady"/>
				      });
				    </script>
        </head>
        <body>
                <div class="top-panel-item"><stripes:link beanclass="net.notx.web.UtilityActionBean" event="show">Misc utilities</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.UsersActionBean" event="show" >Users management</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.DomainStatisticsActionBean" event="show">Domain statistics</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.ErrorViewActionBean" event="show">Error view</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.SendNotificationActionBean" event="show">Sending</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.SuperTemplateActionBean" event="show">Super templates</stripes:link></div>
        	<div class="top-panel-item"><stripes:link beanclass="net.notx.web.RoutingActionBean" event="show">Routing</stripes:link></div>
          
             <stripes:layout-component name="contents"/>
          
        </body>
    </html>
</stripes:layout-definition>
