<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-render name="/layout/notx_utility_layout.jsp">
    <stripes:layout-component name="contents">
		  	<h1>Detail of fail</h1>
		    <stripes:errors/>
		    <stripes:link href="/ErrorView.action" event="sendNotificationToMQ">Send message back to MQ
		      <stripes:param name="detail.uid" value="${actionBean.detail.uid}"/>
		    </stripes:link>
		    <h2>JMS message that caused error</h2>
				${actionBean.detail.jmsMessage}
				<h2>Stack trace</h2>
				${actionBean.theFailStackTrace}  
	  </stripes:layout-component>
</stripes:layout-render>
