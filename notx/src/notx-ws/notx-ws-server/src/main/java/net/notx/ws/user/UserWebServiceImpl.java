/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.user;

import java.util.Map;
import net.notx.core.entities.User;
import net.notx.core.service.TaggingService;
import net.notx.core.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author palo
 */
@Endpoint
public class UserWebServiceImpl implements UserWebService {

    private static Logger logger = LoggerFactory.getLogger(UserWebServiceImpl.class);
    private UserService userService;
    private TaggingService taggingService;

    
    @PayloadRoot(localPart = "CreateOrUpdateUserRequest", namespace = NAMESPACE)
    @Override
    public CreateOrUpdateUserResponse createOrUpdateUser(CreateOrUpdateUserRequest createOrUpdateUserRequest) {
        CreateOrUpdateUserResponse resp = OBJECT_FACTORY.createCreateOrUpdateUserResponse();
        try {
            User user = new User();
            user.setId(createOrUpdateUserRequest.getUser().getUserId());
            user.setLang(createOrUpdateUserRequest.getUser().getLang());
            user.setName(createOrUpdateUserRequest.getUser().getName());
            for (Contact c : createOrUpdateUserRequest.getUser().getContacts()) {
                user.addContact(c.getKey(), c.getValue());
            }
            logger.debug("Webservices: Creating or updating user {}", createOrUpdateUserRequest);
            userService.createUser(user);

            if (!createOrUpdateUserRequest.getUser().getTags().isEmpty()) {
                for (String tag : createOrUpdateUserRequest.getUser().getTags()) {
                    taggingService.tagUser(user.getId(), tag);
                }
            }
        } catch (Exception e) {
            logger.error("Exception while creating user: ", e);
            resp.setError(e.toString());
        }
        return resp;
    }            

    @PayloadRoot(localPart = "LoadUserRequest", namespace = NAMESPACE)
    @Override
    public LoadUserResponse loadUser(LoadUserRequest createUserRequest) {
        LoadUserResponse resp = OBJECT_FACTORY.createLoadUserResponse();
        try {
            User user = userService.loadUser(createUserRequest.getUserId());
            if (user == null) {
                throw new NullPointerException("User not created:" + user);
            }
            net.notx.ws.user.User notxUser = OBJECT_FACTORY.createUser();
            notxUser.setLang(user.getLang());
            notxUser.setName(user.getName());
            notxUser.setUserId(user.getId());
            for(Map.Entry<String,String> contact : user.getContacts().entrySet()){
                Contact c = OBJECT_FACTORY.createContact();
                c.setKey(contact.getKey());
                c.setValue(contact.getValue());
                notxUser.getContacts().add(c);
            }
            resp.setUser(notxUser);
        } catch (Exception e) {
            logger.error("Exception while loading user: ", e);
            resp.setError(e.toString());
        }
        return resp;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserService getUserService() {
        return userService;
    }

    /**
     * @return the taggingSrevice
     */
    public TaggingService getTaggingService() {
        return taggingService;
    }

    /**
     * @param taggingSrevice the taggingSrevice to set
     */
    public void setTaggingService(TaggingService taggingSrevice) {
        this.taggingService = taggingSrevice;
    }

    
}
