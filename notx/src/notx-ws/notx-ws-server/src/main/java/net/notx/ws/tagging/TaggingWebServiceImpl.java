/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.tagging;

import net.notx.core.service.TaggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author palo
 */
@Endpoint
public class TaggingWebServiceImpl implements TaggingWebService {

    private static Logger logger = LoggerFactory.getLogger(TaggingWebServiceImpl.class);
    private TaggingService taggingService;

    @PayloadRoot(localPart = "TagRequest", namespace = NAMESPACE)
    @Override
    public TagResponse createTags(TagRequest tagRequest) {
        TagResponse resp = OBJECT_FACTORY.createTagResponse();
        try {
            for (String user : tagRequest.getUserIds()) {
                for (String tag : tagRequest.getTags()) {
                    getTaggingService().tagUser(user, tag);
                }
            }
        } catch (Exception e) {
            logger.error("Failed to tag user: ", e);
            resp.setError("Failed to tag user!");
        } finally {
            return resp;
        }
    }

    @PayloadRoot(localPart = "UnTagRequest", namespace = NAMESPACE)
    @Override
    public UnTagResponse removeTags(UnTagRequest unTagRequest) {
        UnTagResponse resp = OBJECT_FACTORY.createUnTagResponse();
        try {
            for (String user : unTagRequest.getUserIds()) {
                for (String tag : unTagRequest.getTags()) {
                    getTaggingService().unTagUser(user, tag);
                }
            }
        } catch (Exception e) {
            logger.error("Failed to untag user: ", e);
            resp.setError("Failed to untag user!");
        } finally {
            return resp;
        }
    }

    /**
     * @return the taggingService
     */
    public TaggingService getTaggingService() {
        return taggingService;
    }

    /**
     * @param taggingService the taggingService to set
     */
    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }
}
