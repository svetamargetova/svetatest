/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.template;

import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.TemplateDescriptor;
import net.notx.core.service.TemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author palo
 */
@Endpoint
public class TemplateWebServiceImpl implements TemplateWebService {

    private static Logger logger = LoggerFactory.getLogger(TemplateWebServiceImpl.class);
    private TemplateService templateService;

    @PayloadRoot(localPart = "CreateTemplateRequest", namespace = NAMESPACE)
    @Override
    public CreateTemplateResponse createTemplate(CreateTemplateRequest createTemplateRequest) {
        CreateTemplateResponse resp = OBJECT_FACTORY.createCreateTemplateResponse();
        try {
            SuperTemplate sp = new SuperTemplate();
            sp.setName(createTemplateRequest.getTemplate().getName());
            sp.setDescription(createTemplateRequest.getTemplate().getDesciption());
            for (EngineMutation mutation : createTemplateRequest.getTemplate().getMutations()) {
                TemplateDescriptor descriptor = new TemplateDescriptor();
                descriptor.setEngine(mutation.getEngine());
                descriptor.setLang(mutation.getLang());

                net.notx.core.entities.Template template = new net.notx.core.entities.Template();
                for (TemplatePart part : mutation.getParts()) {
                    template.put(part.getName(), part.getValue());
                }
                sp.addTemplate(descriptor, template);
            }
            getTemplateService().createTemplate(sp);
        } catch (Exception e) {
            logger.error("Failed to create Template: ", e);
            resp.setError("Failed to create Template!");
        } finally {
            return resp;
        }
    }

    @Override
    public LoadTemplateResponse loadTemplate(LoadTemplateRequest loadTemplateRequest) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @return the templateService
     */
    public TemplateService getTemplateService() {
        return templateService;
    }

    /**
     * @param templateService the templateService to set
     */
    public void setTemplateService(TemplateService templateService) {
        this.templateService = templateService;
    }
}
