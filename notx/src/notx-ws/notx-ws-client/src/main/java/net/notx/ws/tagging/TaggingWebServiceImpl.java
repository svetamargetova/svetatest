/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.tagging;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author palo
 */
public class TaggingWebServiceImpl extends WebServiceGatewaySupport implements TaggingWebService {

    @Override
    public TagResponse createTags(TagRequest tagRequest) {
        return (TagResponse) getWebServiceTemplate().marshalSendAndReceive(tagRequest);
    }

    @Override
    public UnTagResponse removeTags(UnTagRequest unTagRequest) {
        return (UnTagResponse) getWebServiceTemplate().marshalSendAndReceive(unTagRequest);
    }
}
