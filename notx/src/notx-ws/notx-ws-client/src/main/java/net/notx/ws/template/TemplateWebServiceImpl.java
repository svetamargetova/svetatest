/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.template;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author palo
 */
public class TemplateWebServiceImpl extends WebServiceGatewaySupport implements TemplateWebService {

    @Override
    public CreateTemplateResponse createTemplate(CreateTemplateRequest createTemplateRequest) {
        return (CreateTemplateResponse) getWebServiceTemplate().marshalSendAndReceive(createTemplateRequest);
    }

    @Override
    public LoadTemplateResponse loadTemplate(LoadTemplateRequest loadTemplateRequest) {
        return (LoadTemplateResponse) getWebServiceTemplate().marshalSendAndReceive(loadTemplateRequest);
    }

    
}
