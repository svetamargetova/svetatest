/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.user;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author palo
 */
public class UserWebServiceImpl extends WebServiceGatewaySupport implements UserWebService {    

    @Override
    public LoadUserResponse loadUser(LoadUserRequest loadUserRequest) {
        return (LoadUserResponse) getWebServiceTemplate().marshalSendAndReceive(loadUserRequest);
    }

    @Override
    public CreateOrUpdateUserResponse createOrUpdateUser(CreateOrUpdateUserRequest createOrUpdateUserRequest) {
        return (CreateOrUpdateUserResponse) getWebServiceTemplate().marshalSendAndReceive(createOrUpdateUserRequest);
    }
}
