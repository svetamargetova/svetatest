/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.template;



/**
 *
 * @author palo
 */
public interface TemplateWebService {
    public final static String NAMESPACE = "http://notx.net/template";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
        
    public CreateTemplateResponse createTemplate(CreateTemplateRequest createTemplateRequest);
    
    public LoadTemplateResponse loadTemplate(LoadTemplateRequest loadTemplateRequest);
}
