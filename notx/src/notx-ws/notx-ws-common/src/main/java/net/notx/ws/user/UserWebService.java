/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.user;

/**
 *
 * @author palo
 */

public interface UserWebService {

    public final static String NAMESPACE = "http://notx.net/user";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
    
    
    public CreateOrUpdateUserResponse createOrUpdateUser(CreateOrUpdateUserRequest createOrUpdateUserRequest);
    
    public LoadUserResponse loadUser(LoadUserRequest loadUserRequest);
}
