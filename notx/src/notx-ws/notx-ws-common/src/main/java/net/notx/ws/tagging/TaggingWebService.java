/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.ws.tagging;

/**
 *
 * @author palo
 */
public interface TaggingWebService {

    public final static String NAMESPACE = "http://notx.net/tagging";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public TagResponse createTags(TagRequest tagRequest);

    public UnTagResponse removeTags(UnTagRequest unTagRequest);
}
