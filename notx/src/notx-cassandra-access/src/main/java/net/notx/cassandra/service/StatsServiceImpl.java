/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import net.notx.cassandra.dao.FailInfoDao;
import net.notx.cassandra.dao.StatisticsDao;
import net.notx.client.message.HandlingFailMessage;
import net.notx.core.entities.SentNotification;
import net.notx.core.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author palo
 */
@Component(value="statsService")
public class StatsServiceImpl implements StatsService{

    @Autowired
    private StatisticsDao statisticsDao;
    
    @Autowired
    private FailInfoDao failInfoDao;
    
    @Override
    public void notificationSent(SentNotification notification) {        
        statisticsDao.createStatistic(notification);
    }

    @Override
    public Map<String, Set<SentNotification>> loadStatsByDomain() {
        return statisticsDao.findStatisticByDomain();
    }

    @Override
    public List<HandlingFailMessage> listFailMessages() {
        return failInfoDao.findAllFailInformation();
    }

    @Override
    public HandlingFailMessage loadFailMessages(String id) {
        return failInfoDao.findFailInformation(id);
    }

    @Override
    public void deleteFailMessage(HandlingFailMessage failMessage) {
        failInfoDao.deleteFailInformation(failMessage);
    }

    @Override
    public void createFailMessage(HandlingFailMessage failMessage) {
        failInfoDao.createFailInformation(failMessage);
    }
    
}
