/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.service;

import java.util.List;
import net.notx.cassandra.dao.RoutesDao;
import net.notx.cassandra.dao.TagsDao;
import net.notx.cassandra.dao.UsersDao;
import net.notx.client.NotxException;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.RoutingRecordCollection;
import net.notx.core.entities.User;
import net.notx.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author palo
 */
@Component(value="userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersDao usersDao;
    @Autowired
    private TagsDao tagsDao;
    @Autowired
    private RoutesDao routesDao;

    @Override
    public User loadUser(String userId) {
        User user = usersDao.findUserByID(userId);
        if (user == null) {
            throw new NotxException("User with id " + userId + " not exists");
        }
        return user;
    }

    @Override
    public List<String> loadTaggedUserIds(String tag) {
        return usersDao.findUserIdsByTag(tag);
    }

    @Override
    public RoutingRecordCollection loadUserRoutingRecords(String userId) {
        return routesDao.findAllRoutingRecordsWith(userId);
    }

    @Override
    public List<User> listUsers() {
        return usersDao.findAllUsers();
    }

    @Override
    public void deleteUser(String userId) {
        usersDao.deleteUserByID(userId);
    }

    @Override
    public void createUser(User user) {
        usersDao.createUser(user);
    }

    @Override
    public void createRoute(RoutingRecord route) {
        routesDao.addRoute(route);
    }

    @Override
    public void deleteRoute(RoutingRecord route) {
        routesDao.deleteRoute(route.getUserid(), route.getRecordid());
    }
}
