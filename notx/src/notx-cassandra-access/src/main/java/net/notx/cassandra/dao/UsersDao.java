package net.notx.cassandra.dao;

import com.google.common.collect.Lists;
import java.util.Map.Entry;
import java.util.*;
import me.prettyprint.cassandra.serializers.StringSerializer;
import me.prettyprint.hector.api.beans.HColumn;
import me.prettyprint.hector.api.beans.Row;
import me.prettyprint.hector.api.beans.Rows;
import me.prettyprint.hector.api.factory.HFactory;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.config.DBConstants;
import net.notx.cassandra.utils.NotXMutator;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * <p> User in Notx is central entity. To this user notifications are delivered.
 * Notx maintains databse of contacts for this specific user.
 *
 * User's columns: <ul> <li>name</li> <li>lang - textual representation of
 * langauge for this specific user. For example 'en', 'cs'.</li> <li>id</li>
 * <li>c_ - each contact is prefixed with c_ constant.</li> </ul>
 *
 * Indexed by: <ul> <li>IDS_BY_TAG - to easily find users tagged by tag.</li>
 * <li>TAGS_BY_ID - all tags for certain user.</li> <li>CF_ROUTING - routes are
 * saved by id of user that uses these routing records.</li> </ul>
 *
 * <p>From implementation point of view, it is important to understand whether
 * entity is marshalled into database or it is just simply serialized. Any
 * serialized entity has inherent property that it could get "obsolete" in newer
 * versions of NotX as entity changes. Thus one should consider to marshall all
 * entities that hold important business information. </p>
 *
 * @author kalaz
 *
 */
@Repository
public class UsersDao extends AbstractDao {

    @Autowired
    private TagsDao tagsDAO;
    @Autowired
    private RoutesDao routesDAO;

    @Autowired
    public UsersDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    public User findUserByID(String id) {
        HashSet<String> idset = new HashSet<String>();
        idset.add(id);
        List<User> users = findUsersByID(idset);
        if (users.size() > 0) {
            return users.get(0);
        } else {
            throw new NullPointerException("User with ID " + id + " not found!s");
        }
    }

    public List<User> findUsersByID(Set<String> ids) {
        Rows<String, String, String> result = findRows(DBConstants.CF_USERS,
                ids, new StringSerializer());
        List<User> users = parseUsersFromResult(result);
        return users;
    }

    // @Important
    public List<String> findUserIdsByTag(String tag) {

        List<HColumn<String, String>> result = findAllColumns(
                DBConstants.CF_IDS_BY_TAGS, tag, new StringSerializer());

        List<String> ids = Lists.newArrayList();
        for (HColumn<String, String> column : result) {
            ids.add(column.getName());
        }
        return ids;
    }

    public List<User> findUsersByTag(String tag) {

        List<HColumn<String, String>> result = findAllColumns(
                DBConstants.CF_IDS_BY_TAGS, tag, new StringSerializer());
        HashSet<String> userIDs = new HashSet<String>();

        for (HColumn<String, String> column : result) {
            userIDs.add(column.getName());
        }

        return findUsersByID(userIDs);
    }

    public void createUser(User user) {

        NotXMutator userMutator = getNotXMutator(DBConstants.CF_USERS);
        userMutator.addInsertion(
                user.getId(),
                HFactory.createStringColumn(DBConstants.CF_USERS_NAME,
                user.getName()));
        userMutator.addInsertion(
                user.getId(),
                HFactory.createStringColumn(DBConstants.CF_USERS_ID,
                user.getId()));
        userMutator.addInsertion(
                user.getId(),
                HFactory.createStringColumn(DBConstants.CF_USERS_LANG,
                user.getLang()));
        // Add contacts
        for (Entry<String, String> entry : user.getContacts().entrySet()) {
            userMutator.addInsertion(user.getId(), HFactory.createStringColumn(
                    DBConstants.CF_USERS_CONTACT_PREFIX + entry.getKey(),
                    entry.getValue()));
        }

        userMutator.execute();
    }

    public void deleteUserByID(String id) {
        Set<String> tags = tagsDAO.findTagsByID(id);
        User u = new User();
        u.setId(id);
        for (String tag : tags) {
            tagsDAO.deleteTagForUser(tag, u);
        }

        List<RoutingRecord> routes = routesDAO.findRecordsFor(id);

        for (RoutingRecord route : routes) {
            routesDAO.deleteRoute(id, route.getRecordid());
        }

        deleteColumn(DBConstants.CF_USERS, id, null);
    }

    /**
     * Architecture of Cassandra doesn't make it possible to easily implement
     * everything the same way. We have tu use RangeSliceQuery to get all
     * columns.
     *
     * @return
     * @throws CassandraException
     */
    public List<User> findAllUsers() {
        Rows<String, String, String> result = findAllRows(DBConstants.CF_USERS,
                new StringSerializer());
        List<User> users = parseUsersFromResult(result);
        return users;
    }

    /**
     * This method takes result of querying CF_USERS family and converts it into
     * List of users.
     *
     * @param result
     * @return
     * @throws CassandraException
     */
    private List<User> parseUsersFromResult(Rows<String, String, String> result) {

        List<User> users = new ArrayList<User>();

        Iterator<Row<String, String, String>> iterator = result.iterator();
        while (iterator.hasNext()) {
            Row<String, String, String> row = iterator.next();
            User user = new User();

            user.setId(row.getKey());
            List<HColumn<String, String>> columns = row.getColumnSlice().getColumns();

            if (columns.isEmpty()) {
                continue;
            }

            for (HColumn<String, String> column : columns) {
                if (column.getName().equals(DBConstants.CF_USERS_NAME)) {
                    user.setName(column.getValue());
                } else if (column.getName().equals(DBConstants.CF_USERS_ID)) {
                    user.setId(column.getValue());
                } else if (column.getName().equals(DBConstants.CF_USERS_LANG)) {
                    user.setLang(column.getValue());
                } else if (column.getName().startsWith(
                        DBConstants.CF_USERS_CONTACT_PREFIX)) {
                    user.addContact(
                            column.getName().substring(
                            DBConstants.CF_USERS_CONTACT_PREFIX.length()), column.getValue());
                }
            }
            if (user.getId() != null) {
                // TODO test               
                Set<String> values = tagsDAO.findTagsByID(user.getId());
                user.setTags(values);
                users.add(user);
            }

        }
        return users;
    }

    public void setRoutesDAO(RoutesDao routesDAO) {
        this.routesDAO = routesDAO;
    }

    public RoutesDao getRoutesDAO() {
        return routesDAO;
    }

    public void setTagsDAO(TagsDao tagsDAO) {
        this.tagsDAO = tagsDAO;
    }

    public TagsDao getTagsDAO() {
        return tagsDAO;
    }
}
