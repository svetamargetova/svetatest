/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.utils;

import me.prettyprint.cassandra.service.CassandraHostConfigurator;
import me.prettyprint.hector.api.Cluster;
import me.prettyprint.hector.api.ddl.ColumnFamilyDefinition;
import me.prettyprint.hector.api.ddl.ComparatorType;
import me.prettyprint.hector.api.ddl.KeyspaceDefinition;
import me.prettyprint.hector.api.factory.HFactory;
import net.notx.cassandra.config.CassandraNotxConfig;
import net.notx.cassandra.config.DBConstants;

/**
 * Utils class that manages keyspace by providing methods.
 *
 * @author Palo Gressa
 */
public class KeyspaceUtils {

    public static CassandraHostConfigurator createConfiguratorFromSettings(CassandraNotxConfig settings) {
        CassandraHostConfigurator config = new CassandraHostConfigurator(String.format("%s:%s", settings.getHost(), settings.getPort()));
        config.setRetryDownedHosts(true);
        config.setRetryDownedHostsQueueSize(5);
        config.setMaxActive(settings.getMaxActive());
        return config;
    }

    public static boolean isKeyspaceConfigured(CassandraNotxConfig notxConfig) {
        CassandraHostConfigurator config = createConfiguratorFromSettings(notxConfig);


        Cluster c = HFactory.getOrCreateCluster(notxConfig.getClusterName(), config);

        KeyspaceDefinition defdesc = c.describeKeyspace(notxConfig.getKeyspace());
        return defdesc != null;
    }

    /**
     * Cleans up cassandra and inserts fresh keyspace with coresponding column
     * families for notx. <strong>Alert! This method will completely erase
     * cassandra keyspace!</strong>
     *
     */
    public static void recreateKeyspace(CassandraNotxConfig notxConfig) {

        CassandraHostConfigurator config = createConfiguratorFromSettings(notxConfig);


        Cluster c = HFactory.getOrCreateCluster(notxConfig.getClusterName(), config);

        KeyspaceDefinition defdesc = c.describeKeyspace(notxConfig.getKeyspace());
        if (defdesc != null) {
            c.dropKeyspace(notxConfig.getKeyspace());
        }


        KeyspaceDefinition def = HFactory.createKeyspaceDefinition(notxConfig.getKeyspace());
        c.addKeyspace(def);
        ColumnFamilyDefinition cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_USERS, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_IDS_BY_TAGS, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_TAGS_BY_IDS, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_TEMPLATES, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_SENT_NOTIFICATIONS, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_SENT_NOTIFICATIONS_BY_DOMAIN, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_FAILS, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
        cfd = HFactory.createColumnFamilyDefinition(
                notxConfig.getKeyspace(),
                DBConstants.CF_ROUTING, ComparatorType.UTF8TYPE);
        c.addColumnFamily(cfd);
    }
}
