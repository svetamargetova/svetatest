/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra;

import net.notx.cassandra.config.CassandraNotxConfig;
import com.google.common.collect.Maps;
import java.util.Map;
import me.prettyprint.cassandra.connection.HConnectionManager;
import me.prettyprint.cassandra.service.CassandraHostConfigurator;
import me.prettyprint.hector.api.Cluster;
import me.prettyprint.hector.api.Keyspace;
import me.prettyprint.hector.api.factory.HFactory;
import net.notx.cassandra.utils.KeyspaceUtils;
import net.notx.core.config.NotxConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

/**
 * Instance of this class is responsible for all Apache Cassandra initial
 * configurations. This instance is injected into all Dao's classes that will
 * get keyspace connection.
 *
 * @author palo
 */
public class NotXBootstraper implements DisposableBean{

    private static final Logger logger = LoggerFactory.getLogger(NotXBootstraper.class);
    private CassandraNotxConfig cassandraNotxConfig;
    // Cluster is cached in Hector engine
    private Cluster cluster;
    // HFactory.createKeyspace return ThreadSafe keyspace. So we will cache keyspaces
    private Map<String, Keyspace> keyspaceMap;

    public NotXBootstraper(NotxConfig notxConfig) {
        cassandraNotxConfig = new CassandraNotxConfig(notxConfig);
        CassandraHostConfigurator hostConfigurator = KeyspaceUtils.createConfiguratorFromSettings(cassandraNotxConfig);
        cluster = HFactory.getOrCreateCluster(cassandraNotxConfig.getClusterName(), hostConfigurator);                
        keyspaceMap = Maps.newHashMapWithExpectedSize(1);        
    }

    public Keyspace getOrCreateKeyspace(String keyspaceName) {
        if (keyspaceMap.containsKey(keyspaceName)) {
            logger.debug("Returning cached Keyspace instance: {}", keyspaceName);
            return keyspaceMap.get(keyspaceName);
        } else {
            logger.debug("Creating and caching keyspace: {}", keyspaceName);            
            Keyspace keyspace = HFactory.createKeyspace(keyspaceName, cluster);
            keyspaceMap.put(keyspaceName, keyspace);
            return keyspace;
        }
    }
    
    public Keyspace getMainKeyspace(){
        return getOrCreateKeyspace(getCassandraNotxConfig().getKeyspace());
    }

    /**
     * @return the cassandraNotxConfig
     */
    public CassandraNotxConfig getCassandraNotxConfig() {
        return cassandraNotxConfig;
    }

    @Override
    public void destroy() throws Exception {
        logger.debug("Shutting down cassandra boostrapper");
        cluster.getConnectionManager().shutdown();
    }
}
