package net.notx.cassandra.dao;

import java.util.*;
import me.prettyprint.cassandra.serializers.ObjectSerializer;
import me.prettyprint.cassandra.serializers.StringSerializer;
import me.prettyprint.hector.api.beans.HColumn;
import me.prettyprint.hector.api.beans.OrderedRows;
import me.prettyprint.hector.api.beans.Row;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.config.DBConstants;
import net.notx.cassandra.NotXBootstraper;
import net.notx.core.entities.SentNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatisticsDao extends AbstractDao {
    
    @Autowired
    public StatisticsDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    /**
     * @param template
     * @throws CassandraException
     */
    public void createStatistic(SentNotification statistic) {
        if (statistic.getCorrelationID() == null) {
            throw new CassandraException("Cannot create sent notification witout correlation ID");
        }

        addColumn(DBConstants.CF_SENT_NOTIFICATIONS, statistic.getCorrelationID(),
                DBConstants.CF_SENT_NOTIFICATIONS_SERIALIZED, new ObjectSerializer(),
                statistic);

        addColumn(DBConstants.CF_SENT_NOTIFICATIONS_BY_DOMAIN, statistic.getDomain(), statistic.getCorrelationID(),
                new StringSerializer(), statistic.getCorrelationID());

    }

    public SentNotification findStatisticByCorelationID(String correlationID) {
        return findObjectColumn(DBConstants.CF_SENT_NOTIFICATIONS, correlationID, DBConstants.CF_SENT_NOTIFICATIONS_SERIALIZED);
    }

    private Set<SentNotification> findSendNotifications(Set<String> uids) {
        Set<SentNotification> notifications = new TreeSet<SentNotification>();

        for (Row<String, String, Object> row : findRows(DBConstants.CF_SENT_NOTIFICATIONS, uids, new ObjectSerializer())) {
            SentNotification notification = (SentNotification) row.getColumnSlice().getColumnByName(DBConstants.CF_SENT_NOTIFICATIONS_SERIALIZED).getValue();
            notifications.add(notification);
        }


        return notifications;
    }

    /**
     * @param template
     * @throws CassandraException
     */
    public Map<String, Set<SentNotification>> findStatisticByDomain() {
        OrderedRows<String, String, String> allrows = findAllRows(DBConstants.CF_SENT_NOTIFICATIONS_BY_DOMAIN, new StringSerializer());
        Map<String, Set<SentNotification>> result = new HashMap<String, Set<SentNotification>>();

        for (Row<String, String, String> row : allrows.getList()) {
            Set<String> uids = new HashSet<String>();
            for (HColumn<String, String> col : row.getColumnSlice().getColumns()) {
                uids.add(col.getName());
            }
            result.put(row.getKey(), findSendNotifications(uids));
        }

        return result;
    }
}
