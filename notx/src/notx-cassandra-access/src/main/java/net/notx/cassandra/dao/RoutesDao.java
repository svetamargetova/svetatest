package net.notx.cassandra.dao;

import java.util.ArrayList;
import java.util.List;
import me.prettyprint.cassandra.serializers.ObjectSerializer;
import me.prettyprint.hector.api.beans.HColumn;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.config.DBConstants;
import net.notx.cassandra.NotXBootstraper;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.RoutingRecordCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoutesDao extends AbstractDao {

    @Autowired
    public RoutesDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    /**
     * <p> Adds route into routes column family. The key is user for whom this
     * route is created. If it is global route than null is passed as userID.
     * </p>
     *
     * @throws CassandraException
     */
    public void addRoute(RoutingRecord record) {

        if (record.getRecordid() == null || record.getRecordid().equals("")) {
            record.setRecordid(genUUID());
        }

        if (record.getUserid() == null) {
            addColumn(DBConstants.CF_ROUTING,
                    DBConstants.CF_ROUTING_GLOBAL_ROUTES, record.getRecordid(),
                    new ObjectSerializer(), record);
        } else {
            addColumn(DBConstants.CF_ROUTING, record.getUserid(), record.getRecordid(),
                    new ObjectSerializer(), record);
        }

    }

    /**
     * @return Global routes + routes for user with specific id. If id == null
     * only global routing records are returned.
     */
    public RoutingRecordCollection findAllRoutingRecordsWith(String id) {
        RoutingRecordCollection routesCollection = new RoutingRecordCollection();

        List<HColumn<String, Object>> globalRoutes = findAllColumns(
                DBConstants.CF_ROUTING, DBConstants.CF_ROUTING_GLOBAL_ROUTES,
                new ObjectSerializer());


        for (HColumn<String, Object> globalRoute : globalRoutes) {
            routesCollection.add((RoutingRecord) globalRoute.getValue());
        }

        if (id != null) {
            routesCollection.addAll(findRecordsFor(id));
        }

        return routesCollection;
    }

    /**
     * @return Only routing records for specific id.
     */
    public List<RoutingRecord> findRecordsFor(String id) {
        List<RoutingRecord> result = new ArrayList<RoutingRecord>();
        List<HColumn<String, Object>> usersRoutes = findAllColumns(
                DBConstants.CF_ROUTING, id, new ObjectSerializer());

        for (HColumn<String, Object> globalRoute : usersRoutes) {
            result.add((RoutingRecord) globalRoute.getValue());
        }

        return result;
    }

    /**
     * Deletes one route for user. If the route is global you can pass null to
     * userID.
     */
    public void deleteRoute(String userID, String routeID) {
        deleteColumn(DBConstants.CF_ROUTING, DBConstants.CF_ROUTING_GLOBAL_ROUTES, routeID);
        if (userID != null) {
            deleteColumn(DBConstants.CF_ROUTING, userID, routeID);
        }
    }
}
