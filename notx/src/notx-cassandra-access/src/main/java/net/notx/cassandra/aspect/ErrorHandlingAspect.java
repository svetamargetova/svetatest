package net.notx.cassandra.aspect;

import me.prettyprint.hector.api.exceptions.HectorException;
import net.notx.cassandra.CassandraDownException;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.dao.AbstractDao;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ErrorHandlingAspect {
	

	/**
	 * This pointcut matches every public method of any DAO object.
	 */
	@Pointcut("execution(public * * (..)) && within(net.notx.cassandra.dal.*)")
	public void onDAOMethod(){
	}
	
	@AfterThrowing(pointcut = "onDAOMethod()", throwing = "hectorException")
	public void detectPoolDown(JoinPoint jp,HectorException hectorException)
			throws CassandraException {
		if (hectorException.getMessage().contains("All host pools marked down")) {
			throw new CassandraDownException("Can't connect cassandra in method "+jp.getSignature().getName(),
					hectorException);
		} else {
			throw new CassandraException("Problem in method" +jp.getSignature().getName()+"( "+hectorException.getMessage()+")",
					hectorException);
		}
	}
	
	@Before("onDAOMethod()")
	public void daoMethod (JoinPoint jp) throws Throwable{
		
	}

	private String params(Object[] args) {
		StringBuilder sb = new StringBuilder();
		
		if (args == null)
			return "no args";
		
		
		for (int i = 0; i < args.length; i++){
			if (i != 0)
				sb.append(" ");
			
			sb.append(args[i]);
		}
		
		return sb.toString();
	}
}
