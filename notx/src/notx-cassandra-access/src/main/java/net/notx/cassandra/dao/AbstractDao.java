package net.notx.cassandra.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import me.prettyprint.cassandra.serializers.ObjectSerializer;
import me.prettyprint.cassandra.serializers.StringSerializer;
import me.prettyprint.hector.api.Keyspace;
import me.prettyprint.hector.api.Serializer;
import me.prettyprint.hector.api.beans.*;
import me.prettyprint.hector.api.factory.HFactory;
import me.prettyprint.hector.api.query.MultigetSliceQuery;
import me.prettyprint.hector.api.query.RangeSlicesQuery;
import me.prettyprint.hector.api.query.SliceQuery;
import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.utils.NotXMutator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for all data access objects. There is one to one correspondence
 * between data access objects and column families.
 *
 * @author kalaz
 *
 */
public abstract class AbstractDao {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());    
    protected Keyspace keySpace;
    protected int QUERY_MAX_COLUMNS = 3000;
    
    public AbstractDao(NotXBootstraper bootstraper) {
        this.keySpace = bootstraper.getMainKeyspace();        
    }  
 
    protected String genUUID() {
        return UUID.randomUUID().toString();
    }

    protected NotXMutator getNotXMutator(String cf) {
        return new NotXMutator(cf, keySpace);
    }

    /**
     * Finds rows with specific keys.
     *
     * @param <S> type of the columns to find
     * @param <T> type of serializer for result column
     * @param cf column family that is inspected
     * @param keys collections of keys to look for
     * @param resultSerializer implementation of serializer
     * @return All rows with key keys nad with all columns.
     */
    protected <S, T extends Serializer> Rows<String, String, S> findRows(String cf, Iterable<String> keys, T resultSerializer) {
        MultigetSliceQuery<String, String, S> rsq = HFactory.createMultigetSliceQuery(keySpace, new StringSerializer(),
                new StringSerializer(), resultSerializer);
        rsq.setColumnFamily(cf);
        rsq.setKeys(keys);
        rsq.setRange("", "", false, QUERY_MAX_COLUMNS);
        return rsq.execute().get();
    }

    /**
     * Retrieves all rows in specified column family. Same parameters as {@link #findRows(String, Iterable, Serializer)}
     */
    protected <S, T extends Serializer> OrderedRows<String, String, S> findAllRows(String cf, T resultSerializer) {
        RangeSlicesQuery<String, String, S> rsq = HFactory.createRangeSlicesQuery(keySpace, new StringSerializer(),
                new StringSerializer(), resultSerializer);
        rsq.setColumnFamily(cf);
        rsq.setKeys("", "");
        rsq.setRange("", "", false, QUERY_MAX_COLUMNS);
        return rsq.execute().get();
    }

    protected <S> List<S> findAllObjectRows(String cf, String objectColumnName) {
        List<S> result = new ArrayList<S>();

        OrderedRows<String, String, Object> allTemplates = findAllRows(cf, new ObjectSerializer());

        for (Row<String, String, Object> obj : allTemplates.getList()) {
            result.add((S) obj.getColumnSlice().getColumnByName(objectColumnName).getValue());
        }

        return result;
    }

    /**
     * Deletes column.
     *
     * @param key Key of the record to delete from
     * @param column Column to delete, either its name or null to delete all.
     */
    protected void deleteColumn(String cf, String key, String column) {
        NotXMutator templMutator = getNotXMutator(cf);
        templMutator.addDeletion(key, column);
        templMutator.execute();
    }

    /**
     * Gets all columns under key.
     */
    protected <T> List<HColumn<String, T>> findAllColumns(String cf, String key, Serializer serializer) {
        SliceQuery<String, String, T> q = HFactory.createSliceQuery(
                keySpace, new StringSerializer(), new StringSerializer(),
                serializer);
        q.setColumnFamily(cf).setKey(key).setRange("", "", false, QUERY_MAX_COLUMNS);
        ColumnSlice<String, T> columnSlice = q.execute().get();

        return columnSlice.getColumns();
    }

    /**
     * Adds serialized object into database. Parameters are the same as for {@link #findObjectColumn(String, String, String)}
     */
    protected <T> void addColumn(String cf, String key,
            String column, Serializer serializer, T value) {

        NotXMutator templMutator = getNotXMutator(cf);
        templMutator.addObjectInsertion(key,
                HFactory.createColumn(column, value, new StringSerializer(), serializer));
        templMutator.execute();
    }

    /**
     * Finds serialized object that is saved in cassandra database
     *
     * @param <T> Type of the object that is saved in the database
     * @param Column family in which object is saved
     * @param Key under which to look for an object
     * @param Column in which serialized data are present
     */
    protected <T> T findObjectColumn(String cf, String key, String serializedColumnName) {
        SliceQuery<String, String, Object> q = HFactory.createSliceQuery(
                keySpace, new StringSerializer(), new StringSerializer(),
                new ObjectSerializer());
        q.setColumnFamily(cf).setKey(key).setColumnNames(serializedColumnName);

        ColumnSlice<String, Object> column = q.execute().get();

        if (column == null || column.getColumnByName(serializedColumnName) == null) {
            return null;
        }

        return (T) column.getColumnByName(serializedColumnName).getValue();

    }
}
