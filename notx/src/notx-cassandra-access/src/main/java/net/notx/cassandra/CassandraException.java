package net.notx.cassandra;

import net.notx.client.NotxException;

public class CassandraException extends NotxException {

	public CassandraException(String message, Throwable cause) {
		super(message, cause);
	}

	public CassandraException(String message) {
		super(message);
	}


}
