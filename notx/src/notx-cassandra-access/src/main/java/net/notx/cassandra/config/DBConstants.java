package net.notx.cassandra.config;

public interface DBConstants {
	public static final String CF_USERS = "Users";
	public static final String CF_USERS_ID = "id";
	public static final String CF_USERS_CONTACT_PREFIX = "c_";
	public static final String CF_USERS_NAME = "name";
	public static final String CF_USERS_LANG = "lang";
	
	public static final String CF_TAGS_BY_IDS = "TagsByUserID";
	
	public static final String CF_IDS_BY_TAGS = "UserIDsByTag";
	
	public static final String CF_TEMPLATES = "TemplatesByName";
	public static final String CF_TEMPLATES_SERIALIZED = "serialized";
	
	public static final String CF_SENT_NOTIFICATIONS = "SentNot";
	public static final String CF_SENT_NOTIFICATIONS_SERIALIZED = "serialized";
	public static final String CF_SENT_NOTIFICATIONS_BY_DOMAIN = "SentNotByDomain";
	
	public static final String CF_FAILS = "Fails";
	public static final String CF_FAILS_SERIALIZED = "serialized";

	public static final String CF_ROUTING = "Routing";
	public static final String CF_ROUTING_GLOBAL_ROUTES = "globalRoutesKey";
}
