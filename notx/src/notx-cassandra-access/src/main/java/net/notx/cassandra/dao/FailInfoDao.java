package net.notx.cassandra.dao;

import java.util.ArrayList;
import java.util.List;
import me.prettyprint.cassandra.serializers.ObjectSerializer;
import me.prettyprint.hector.api.beans.HColumn;
import me.prettyprint.hector.api.beans.Row;
import me.prettyprint.hector.api.beans.Rows;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.config.DBConstants;
import net.notx.cassandra.NotXBootstraper;
import net.notx.client.message.HandlingFailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FailInfoDao extends AbstractDao {

    @Autowired
    public FailInfoDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    public void createFailInformation(HandlingFailMessage fail) {
        String uid = genUUID();
        fail.setUid(uid);
        addColumn(DBConstants.CF_FAILS, uid, DBConstants.CF_FAILS_SERIALIZED,
                new ObjectSerializer(), fail);
    }

    /**
     * TODO maybe use TIME UUID
     * http://wiki.apache.org/cassandra/FAQ#working_with_timeuuid_in_java
     *
     * @param template
     * @throws CassandraException
     */
    public List<HandlingFailMessage> findAllFailInformation() {
        List<HandlingFailMessage> result = new ArrayList<HandlingFailMessage>();

        Rows<String, String, Object> orderedRows = findAllRows(
                DBConstants.CF_FAILS, new ObjectSerializer());

        // The deleted keys stay in Cassandra
        // http://stackoverflow.com/questions/2981483/when-i-remove-rows-in-cassandra-i-delete-only-columns-not-row-keys
        for (Row<String, String, Object> row : orderedRows) {
            HColumn<String, Object> col = row.getColumnSlice().getColumnByName(
                    DBConstants.CF_FAILS_SERIALIZED);
            if (col != null) {
                result.add((HandlingFailMessage) col.getValue());
            }
        }

        return result;
    }

    public void deleteFailInformation(HandlingFailMessage hf) {
        deleteColumn(DBConstants.CF_FAILS, hf.getUid(), null);
    }

    public HandlingFailMessage findFailInformation(String uid) {
        return findObjectColumn(DBConstants.CF_FAILS, uid, DBConstants.CF_FAILS_SERIALIZED);
    }
}
