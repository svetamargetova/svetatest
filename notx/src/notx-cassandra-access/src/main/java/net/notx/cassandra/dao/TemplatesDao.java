package net.notx.cassandra.dao;

import java.util.List;
import me.prettyprint.cassandra.serializers.ObjectSerializer;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.config.DBConstants;
import net.notx.core.entities.SuperTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TemplatesDao extends AbstractDao {

    @Autowired
    public TemplatesDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    /**
     * Creates supertemplate. Supertemplates are very complex structures hence
     * are stored in serialized from into cassandra.
     *
     * @param template
     * @throws CassandraException
     */
    public void createSuperTemplate(SuperTemplate template) {
        if (template == null || template.getName() == null
                || template.getName().length() == 0) {
            throw new IllegalArgumentException(
                    "Cannot create supertempalte without name! [" + template
                    + "]");
        }

        addColumn(DBConstants.CF_TEMPLATES, template.getName(),
                DBConstants.CF_TEMPLATES_SERIALIZED, new ObjectSerializer(),
                template);
    }

    public List<SuperTemplate> findAllSuperTemplates() {
        return findAllObjectRows(DBConstants.CF_TEMPLATES, DBConstants.CF_TEMPLATES_SERIALIZED);
    }

    public SuperTemplate findSuperTemplateByName(String name) {
        return findObjectColumn(DBConstants.CF_TEMPLATES, name,
                DBConstants.CF_TEMPLATES_SERIALIZED);
    }

    public void deleteSuperTemplate(SuperTemplate template) {
        deleteColumn(DBConstants.CF_TEMPLATES, template.getName(),
                DBConstants.CF_TEMPLATES_SERIALIZED);
    }
}
