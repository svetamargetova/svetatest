package net.notx.cassandra.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import me.prettyprint.cassandra.serializers.StringSerializer;
import me.prettyprint.hector.api.beans.HColumn;
import me.prettyprint.hector.api.beans.OrderedRows;
import me.prettyprint.hector.api.beans.Row;
import me.prettyprint.hector.api.factory.HFactory;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.config.DBConstants;
import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.utils.NotXMutator;
import net.notx.core.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TagsDao extends AbstractDao {
    
    @Autowired
    private UsersDao usersDAO;
    
    @Autowired
    public TagsDao(NotXBootstraper bootstraper) {
        super(bootstraper);
    }

    public void deleteTag(String tag) {
       
        List<User> users = getUsersDAO().findUsersByTag(tag);

        NotXMutator notxmutator = getNotXMutator(DBConstants.CF_TAGS_BY_IDS);
        for (User u : users) {
            notxmutator.addDeletion(u.getId(), tag);
        }
        notxmutator.execute();

        deleteColumn(DBConstants.CF_IDS_BY_TAGS, tag, null);
    }

    public void deleteTagForUser(String tag, User user)
            throws CassandraException {
        List<User> users = new ArrayList<User>();
        users.add(user);
        deleteTagForUsers(tag, users);
    }

    public void createTagForUsers(String tag, List<User> users)
            throws CassandraException {
        NotXMutator tagsbyid = getNotXMutator(DBConstants.CF_TAGS_BY_IDS);
        NotXMutator idbytag = getNotXMutator(DBConstants.CF_IDS_BY_TAGS);

        for (User user : users) {
            tagsbyid.addInsertion(user.getId(),
                    HFactory.createStringColumn(tag, ""));
            idbytag.addInsertion(tag,
                    HFactory.createStringColumn(user.getId(), ""));
        }

        tagsbyid.execute();
        idbytag.execute();
    }

    public void createTagForUser(String tag, User user)
            throws CassandraException {
        List<User> users = new ArrayList();
        users.add(user);
        createTagForUsers(tag, users);
    }

    public Set<String> findTagsByID(String id) {

        List<HColumn<String, String>> result = findAllColumns(
                DBConstants.CF_TAGS_BY_IDS, id, new StringSerializer());

        Set<String> tags = new HashSet<String>();
        for (HColumn<String, String> column : result) {
            String tag = column.getName();
            tags.add(tag);
        }
        return tags;
    }

    public void deleteTagForUsers(String tag, List<User> users) {
        NotXMutator tagsByIDsMutator = getNotXMutator(DBConstants.CF_TAGS_BY_IDS);
        NotXMutator idsbytagsMutator = getNotXMutator(DBConstants.CF_IDS_BY_TAGS);

        for (User u : users) {
            tagsByIDsMutator.addDeletion(u.getId(), tag);
        }

        tagsByIDsMutator.execute();

        for (User u : users) {
            idsbytagsMutator.addDeletion(tag, u.getId());
        }

        idsbytagsMutator.execute();
    }

    public List<String> findAllTags() {
        List<String> result = new ArrayList<String>();
        OrderedRows<String, String, Object> rows = findAllRows(DBConstants.CF_IDS_BY_TAGS, new StringSerializer());
        for (Row<String, String, Object> row : rows) {
            result.add(row.getKey());
        }
        return result;
    }

    /**
     * @return the usersDAO
     */
    public UsersDao getUsersDAO() {
        return usersDAO;
    }

    /**
     * @param usersDAO the usersDAO to set
     */
    public void setUsersDAO(UsersDao usersDAO) {
        this.usersDAO = usersDAO;
    }
}
