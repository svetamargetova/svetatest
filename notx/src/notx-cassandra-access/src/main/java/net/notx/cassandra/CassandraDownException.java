package net.notx.cassandra;

/**
 * This exception is thrown when cassandar is not running.
 * @author palo
 */
public class CassandraDownException extends CassandraException {

    public CassandraDownException(String message, Throwable cause) {
        super(message, cause);
    }

    public CassandraDownException(String message) {
        super(message);
    }
}
