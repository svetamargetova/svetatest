/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.service;

import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.config.CassandraNotxConfig;
import net.notx.cassandra.utils.KeyspaceUtils;
import net.notx.core.service.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author palo
 */
@Component(value="initService")
public class InitServiceImpl implements InitService {

    
    private CassandraNotxConfig cassandraNotxConfig;

    @Autowired
    public InitServiceImpl(NotXBootstraper bootstraper) {
        this.cassandraNotxConfig = bootstraper.getCassandraNotxConfig();
    }

    @Override
    public boolean isInitialized() {
        return KeyspaceUtils.isKeyspaceConfigured(cassandraNotxConfig);
    }

    @Override
    public void initialize() {
        KeyspaceUtils.recreateKeyspace(cassandraNotxConfig);
    }
}
