package net.notx.cassandra.utils;


import me.prettyprint.cassandra.serializers.StringSerializer;
import me.prettyprint.hector.api.Keyspace;
import me.prettyprint.hector.api.beans.HColumn;
import me.prettyprint.hector.api.factory.HFactory;
import me.prettyprint.hector.api.mutation.Mutator;

/**
 * Wrapper over standard Hector mutator.
 * @author Filip Nguyen
 *
 */
public class NotXMutator {
	
	private Keyspace keyspace;
	private String columnFamily;
	Mutator<String> mutator;
	
	
	public NotXMutator(String cf, Keyspace keyspace){
		mutator = HFactory.createMutator(keyspace,
				new StringSerializer());
		this.columnFamily = cf;
		this.keyspace = keyspace;
	}


	public void execute() {
		mutator.execute();
	}
	
	public void addDeletion(String id, String column) {
		mutator = mutator
		.addDeletion(id, columnFamily,column, new StringSerializer());
	}

	public void addInsertion(String id,
			HColumn<String, String> stringColumn) {
		mutator = mutator.addInsertion(id,columnFamily, stringColumn);
	}
	
	public void addObjectInsertion(String id,
			HColumn<String, Object> objectColumn) {
		mutator = mutator.addInsertion(id,columnFamily, objectColumn);
	}
}
