/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.service;

import java.util.List;
import net.notx.cassandra.dao.SuperTemplatesDao;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author palo
 */
@Component(value="templateService")
public class TemplateServiceImpl implements TemplateService{

    @Autowired
    private SuperTemplatesDao superTemplatesDao;
    
    @Override
    public SuperTemplate loadTemplate(String templateName) {
        return superTemplatesDao.findSuperTemplateByName(templateName);
    }

    @Override
    public void createTemplate(SuperTemplate template) {
        superTemplatesDao.createSuperTemplate(template);
    }

    @Override
    public void deleteTemplate(SuperTemplate template) {
        superTemplatesDao.deleteSuperTemplate(template);
    }

    @Override
    public List<SuperTemplate> listTemplates() {
        return superTemplatesDao.findAllSuperTemplates();
    }
    
}
