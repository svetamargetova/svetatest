package net.notx.cassandra.config;

import net.notx.core.config.NotxConfig;

public class CassandraNotxConfig {

    static final String MODULE = "cassandra";
    static final String HOST = "host";
    static final String PORT = "port";
    static final String CLUSTER = "cluster";
    static final String KEYSPACE = "keyspace";
    static final String MAX_ACTIVE = "pool.maxActive";
    private final NotxConfig mainConfig;

    public CassandraNotxConfig(NotxConfig mainConfig) {
        this.mainConfig = mainConfig;
    }

    public String getHost() {
        return mainConfig.getModuleProperty(MODULE, HOST);
    }
    
    public String getPort() {
        return mainConfig.getModuleProperty(MODULE, PORT);
    }
    
    public String getClusterName() {
        return mainConfig.getModuleProperty(MODULE, CLUSTER);
    }
    
    public String getKeyspace() {
        return mainConfig.getModuleProperty(MODULE, KEYSPACE);
    }
    
    public int getMaxActive(){
        return Integer.parseInt(mainConfig.getModuleProperty(MODULE, MAX_ACTIVE));
    }
}
