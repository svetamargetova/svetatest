/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.service;

import java.util.List;
import net.notx.cassandra.dao.TagsDao;
import net.notx.cassandra.dao.UsersDao;
import net.notx.core.entities.User;
import net.notx.core.service.TaggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author palo
 */
@Component(value="taggingService")
public class TaggingServiceImpl implements TaggingService {

    private static final Logger logger = LoggerFactory.getLogger(TaggingServiceImpl.class);
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private TagsDao tagsDao;

    @Override
    public void tagUser(String userId, String tag) {
        logger.debug("Tagging user: {} with tag: {} ", userId, tag);
        User user = usersDao.findUserByID(userId);
        if (user == null) {
            logger.warn("User to tag: {} wasn't found. Nothing tagged", userId);
            return;
        }
        tagsDao.createTagForUser(tag, user);
    }

    @Override
    public void unTagUser(String userId, String tag) {
        logger.debug("Untagging user: {} with tag: {}", userId, tag);
        User user = usersDao.findUserByID(userId);
        if (user == null) {
            logger.warn("User to untag: {} wasn't found. Nothing tagged", userId);
            return;
        }
        tagsDao.deleteTagForUser(tag, user);
    }

    @Override
    public List<String> listTags() {
        return tagsDao.findAllTags();
    }
}
