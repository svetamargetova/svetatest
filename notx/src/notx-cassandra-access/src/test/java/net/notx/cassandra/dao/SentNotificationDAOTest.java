package net.notx.cassandra.dao;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import net.notx.cassandra.CassandraException;
import net.notx.core.entities.SentNotification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SentNotificationDAOTest extends AbstractDaoTest {
	private static StatisticsDao sdao;
	
	@Before
	public void beforeTest(){
		sdao = getBootstraper().getStatisticsDao();
	}
	@Test 
	public void testStatistics() throws CassandraException{
		Date date = new Date();

		SentNotification stat = new SentNotification();
		stat.setUid("13");
		stat.setUserName("filip nguyen");
		stat.setForTag("Conference1.attendees");
		stat.setTime(date);
		stat.setLang("en");
		stat.setEngine("mail");
		stat.setContact("ahoj@ahoj.cz");
		stat.setCorrelationID("1");
		
		SentNotification stat2 = new SentNotification();
		stat2.setUid("14");
		stat2.setUserName("jirka halos");
		stat2.setForTag("Conference1.attendees");
		stat2.setTime(date);
		stat2.setLang("en");
		stat2.setEngine("mail");
		stat2.setContact("ahoj@ahoj.cz");
		stat2.setCorrelationID("2");
		
		SentNotification stat3 = new SentNotification();
		stat3.setUid("15");
		stat3.setUserName("martin serena");
		stat3.setForTag("users");
		stat3.setTime(date);
		stat3.setLang("en");
		stat3.setEngine("mail");
		stat3.setContact("ahoj@ahoj.cz");
		stat3.setCorrelationID("3");
		
		
		Assert.assertEquals("Conference1", stat.getDomain());
		sdao.createStatistic(stat);
		sdao.createStatistic(stat2);
		sdao.createStatistic(stat3);

		Map<String, Set<SentNotification>> stats = sdao.findStatisticByDomain();
		Assert.assertEquals(1, stats.get("NO_DOMAIN").size());
		Assert.assertEquals(2, stats.get("Conference1").size());
		
		Assert.assertEquals("martin serena", sdao.findStatisticByCorelationID("3").getUserName());
		
	}
}
