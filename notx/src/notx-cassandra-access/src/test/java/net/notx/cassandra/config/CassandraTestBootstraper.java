package net.notx.cassandra.config;

import java.util.Map;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.Template;
import net.notx.core.entities.User;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.TemplateDescriptor;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.NotXBootstraper;
import net.notx.cassandra.dao.FailInfoDao;
import net.notx.cassandra.dao.RoutesDao;
import net.notx.cassandra.dao.StatisticsDao;
import net.notx.cassandra.dao.SuperTemplatesDao;
import net.notx.cassandra.dao.TagsDao;
import net.notx.cassandra.dao.TemplatesDao;
import net.notx.cassandra.dao.UsersDao;
import net.notx.client.NotxException;
import net.notx.core.config.NotxConfig;
import net.notx.cassandra.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>This utility class is used to bootstrap Cassandras database schema. It can
 * recreate keyspace with some initial data needed in every cassandra instance
 * </p>
 *
 * <p> This class also has vital methods used for testing. It allows to start up
 * in-memory CassandraDB and create testing datasets</p>
 *
 * TODO: Make all test using Spring context - autowiring
 * 
 * @author Filip Nguyen
 */
public class CassandraTestBootstraper {

    
    private static Logger logger = LoggerFactory.getLogger(CassandraTestBootstraper.class);
        
    private NotXBootstraper notXBootstraper;
    
    private FailInfoDao failInfoDao;
    private RoutesDao routesDao;
    private StatisticsDao statisticsDao;
    private SuperTemplatesDao superTemplatesDao;
    private TagsDao tagsDao;
    private TemplatesDao templatesDao;
    private UsersDao usersDao;

    public CassandraTestBootstraper() {
               
        notXBootstraper = new NotXBootstraper(createConfigStub());
        //Turn off per4j
        System.setProperty("com.prettyprint.cassandra.load_hector_log4j", "false");

        failInfoDao = new FailInfoDao(notXBootstraper);
        
        routesDao = new RoutesDao(notXBootstraper);    
        statisticsDao = new StatisticsDao(notXBootstraper);
        superTemplatesDao = new SuperTemplatesDao(notXBootstraper);
        tagsDao = new TagsDao(notXBootstraper);        
        templatesDao = new TemplatesDao(notXBootstraper);
        usersDao = new UsersDao(notXBootstraper);
        superTemplatesDao = new SuperTemplatesDao(notXBootstraper);
        
        tagsDao.setUsersDAO(usersDao);
        usersDao.setRoutesDAO(routesDao);
        usersDao.setTagsDAO(tagsDao);
    }

    public NotXBootstraper getNotXBootstraper() {
        return notXBootstraper;
    }   
    
    /**
     * We will create configuration stub because Cassandra is running as deamon
     * so we will use propietary configuration settings.
     * @return 
     */
    private static NotxConfig createConfigStub() {        
        return new NotxConfig() {

            @Override
            public String getModuleProperty(String module, String key) {
                if (key.equalsIgnoreCase(CassandraNotxConfig.PORT)) {
                    return "9160";
                }

                if (key.equalsIgnoreCase(CassandraNotxConfig.HOST)) {
                    return "localhost";
                }

                if (key.equalsIgnoreCase(CassandraNotxConfig.CLUSTER)) {
                    return "Test Cluster";
                }

                if (key.equalsIgnoreCase(CassandraNotxConfig.KEYSPACE)) {
                    return "testKSpace";
                }
                
                if (key.equalsIgnoreCase(CassandraNotxConfig.MAX_ACTIVE)) {
                    return "10";
                }

                return null;
            }

            @Override
            public String getVersion() {
                return "1.0.0";
            }

            @Override
            public int getIntModuleProperty(String module, String key) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public Map<String, String> getModuleProperties(String module) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    /**
     * 2 Users, Filip and Tomáš.
     *
     * Tags: Filip - conf1.manager, conf2.attendee Tomáš- conf2.manager,
     * conf1.attendee, conf2.attendee
     *
     * @throws NotxException
     * @throws CassandraException
     */
    public void createDataSet01() throws NotxException, CassandraException {
        User userFilip = new User();
        userFilip.setName("Filipek");
        userFilip.setId("13");
        userFilip.setLang("en");
        userFilip.addContact("telc", "739649759");
        userFilip.addContact("email", "kalaz@seznam.cz");

        User userTomas = new User();
        userTomas.setName("Tomáš");
        userTomas.setId("18");
        userTomas.setLang("cs");
        userTomas.addContact("telc", "4984");
        userTomas.addContact("email", "tom.sezn@seznam.cz");


        getUsersDao().createUser(userFilip);
        getUsersDao().createUser(userTomas);



        getTagsDao().createTagForUser("conf1.manager", userFilip);
        getTagsDao().createTagForUser("conf2.manager", userTomas);

        getTagsDao().createTagForUser("conf1.attendee", userFilip);
        getTagsDao().createTagForUser("conf1.attendee", userTomas);
        getTagsDao().createTagForUser("conf2.attendee", userTomas);
        

        RoutingRecord rr1 = new RoutingRecord();
        RoutingRecord rr2 = new RoutingRecord();
        RoutingRecord rr3 = new RoutingRecord();
        RoutingRecord rr4 = new RoutingRecord();
        RoutingRecord rr5 = new RoutingRecord();
        RoutingRecord rr6 = new RoutingRecord();
        rr1.setUserid(userFilip.getId());
        rr2.setUserid(userFilip.getId());
        rr3.setUserid(userFilip.getId());
        rr4.setUserid(userTomas.getId());
        rr5.setUserid(userTomas.getId());
        rr6.setUserid(userTomas.getId());


        getRoutesDao().addRoute(rr1);
        getRoutesDao().addRoute(rr2);
        getRoutesDao().addRoute(rr3);
        getRoutesDao().addRoute(rr4);
        getRoutesDao().addRoute(rr5);
        getRoutesDao().addRoute(rr6);

        addMailTemplate("account_registration", "templates/account_registration", "For this template you should provide: $salutation$,$activation_key$ and $email$", "Account registration", "Hello $salutation$, your activation key is: $activation_key$ and your email is: $email$", "en");
        addMailTemplate("account_activated", "templates/account_activated", "For this template you should provide: $salutation$ and $email$", "Account activated", "Hello $salutation$, your email is $email$", "en");
        addMailTemplate("invitation_accepted_invited", "templates/invitation_accepted_invited", "For this template you should provide: $salutation$ and $event_name$", "Invitation accepted", "Hello $salutation$ the event accepted is $event_name$", "en");
        addMailTemplate("invitation_accepted_invitor", "templates/invitation_accepted_invitor", "description", "Invitation accepted", "", "en");
        addMailTemplate("invitation_declined_invitor", "templates/invitation_declined_invitor", "description", "Invitation declined", "", "en");
        addMailTemplate("invitation_existing_confirm", "templates/invitation_existing_confirm", "description", "Invitation existing", "", "en");
        addMailTemplate("invitation_existing_without_confirm", "templates/invitation_existing_without_confirm", "description", "Invitation existing", "", "en");
        addMailTemplate("invitation_new_confirm", "templates/invitation_new_confirm", "description", "Invitation new", "", "en");
        addMailTemplate("invitation_new_without_confirm", "templates/invitation_new_without_confirm", "description", "Invitation new", "", "en");
        addMailTemplate("lost_password_changed", "templates/lost_password_changed", "description", "Lost password changed", "", "en");
        addMailTemplate("lost_password_request", "templates/lost_password_request", "description", "Lost password", "", "en");
        addMailTemplate("pending_account_activation", "templates/pending_account_activation", "description", "Pending account activation", "", "en");
    }

    /**
     * This dataset represents an (1) Open mobility conference and (2) One Man
     * Show conference. There are few roles: Attendee (A), Manager (M), Speaker
     * (S)
     *
     * Users: Filip	en	1:MAS, 2:MA Tyler	cs	1:AS 2: AS	NO MAIL Jirka	ger	1:AS
     * 2:A Jana	cs	1:A	NO PHONE Katerina	en	1:M	2:A
     *
     * The templates are following: - from TakePlace: account_registration
     * account_activated invitation_accepted_invited - greetings message used to
     * greet people - message for rescheduling an lecture
     *
     * The routing tables are set so that every body gets emails implicitly for
     * everything. Additionally: - Filip doesn't want to be disturbed by any
     * emails except speakers
     *
     * @throws NotxException
     * @throws CassandraException
     */
    public void createDataSetOpenMobility() throws NotxException, CassandraException {

        /**
         * USERS
         */
        User userFilip = new User();
        userFilip.setName("Filip");
        userFilip.setId("13");
        userFilip.setLang("en");
        userFilip.addContact("phone", "739649759");
        userFilip.addContact("email", "kalaz@seznam.cz");

        User userTyler = new User();
        userTyler.setName("Tyler");
        userTyler.setId("18TY");
        userTyler.setLang("cs");
        userTyler.addContact("phone", "739649759");

        User userJirka = new User();
        userJirka.setName("Jirka");
        userJirka.setId("20JI");
        userJirka.setLang("ger");
        userJirka.addContact("phone", "739649759");
        userJirka.addContact("email", "nguyen.filip@gmail.com");

        User userJana = new User();
        userJana.setName("Jana");
        userJana.setId("22JA");
        userJana.setLang("cs");
        userJana.addContact("email", "nguyen.filip@mail.muni.cz");

        User userKaterina = new User();
        userKaterina.setName("Katerina");
        userKaterina.setId("24K");
        userKaterina.setLang("en");
        userKaterina.addContact("phone", "739649759");
        userKaterina.addContact("email", "nguyen.filip@gmail.com");


        User userPalo = new User();
        userPalo.setName("Palo Gressa");
        userPalo.setId("156481351681");
        userPalo.setLang("en");
        userPalo.addContact("email", "gressa@acemcee.com");


        getUsersDao().createUser(userFilip);
        getUsersDao().createUser(userTyler);
        getUsersDao().createUser(userJirka);
        getUsersDao().createUser(userJana);
        getUsersDao().createUser(userKaterina);
        getUsersDao().createUser(userPalo);

        /**
         * TAGS
         */
        getTagsDao().createTagForUser("openmobility.attendees", userFilip);
        getTagsDao().createTagForUser("openmobility.managers", userFilip);
        getTagsDao().createTagForUser("openmobility.speakers", userFilip);
        getTagsDao().createTagForUser("onemanshow.managers", userFilip);
        getTagsDao().createTagForUser("onemanshow.attendees", userFilip);

        getTagsDao().createTagForUser("openmobility.attendees", userTyler);
        getTagsDao().createTagForUser("openmobility.speakers", userTyler);
        getTagsDao().createTagForUser("onemanshow.attendees", userTyler);
        getTagsDao().createTagForUser("onemanshow.speakers", userTyler);

        getTagsDao().createTagForUser("openmobility.attendees", userJirka);
        getTagsDao().createTagForUser("openmobility.speakers", userJirka);
        getTagsDao().createTagForUser("onemanshow.attendees", userJirka);

        getTagsDao().createTagForUser("openmobility.attendees", userJana);

        getTagsDao().createTagForUser("onemanshow.managers", userKaterina);
        getTagsDao().createTagForUser("onemanshow.attendees", userKaterina);

        getTagsDao().createTagForUser("mobera2015.attendees", userPalo);

        /**
         * ROUTES
         */
        RoutingRecord route_mailEverything = new RoutingRecord();
        route_mailEverything.setDestination("mail");
        route_mailEverything.setMsgtype(null);
        route_mailEverything.setPriority(5);
        route_mailEverything.setSource(null);
        route_mailEverything.setRecordid(null);

        RoutingRecord route_filipNoMails = new RoutingRecord();
        route_filipNoMails.setDestination(null);
        route_filipNoMails.setMsgtype(null);
        route_filipNoMails.setPriority(4);
        route_filipNoMails.setSource(null);
        route_filipNoMails.setRecordid(null);


        getRoutesDao().addRoute(route_mailEverything);

        addMailTemplate("account_registration", "templates/account_registration", "For this template you should provide: $salutation$,$activation_key$ and $email$", "Account registration", "Hello $salutation$, your activation key is: $activation_key$ and your email is: $email$", "en");
        addMailTemplate("account_activated", "templates/account_activated", "For this template you should provide: $salutation$ and $email$", "Account activated", "Hello $salutation$, your email is $email$", "en");
        addMailTemplate("invitation_accepted_invited", "templates/invitation_accepted_invited", "For this template you should provide: $salutation$ and $event_name$", "Invitation accepted", "Hello $salutation$ the event accepted is $event_name$", "en");

        SuperTemplate greetMessage = new SuperTemplate();
        SuperTemplate rescheduleLectureMessage = new SuperTemplate();

        /**
         * The greet template*
         */
        greetMessage.setName("greet");
        greetMessage.setDescription("Template used to greet people on coference. Please provide $date$ of conference and $city$ ");
        String csContent = (new FileUtils()).readTextFileOnClassPath("templates/greet_cs");
        String enContent = (new FileUtils()).readTextFileOnClassPath("templates/greet_en");
        String gerContent = (new FileUtils()).readTextFileOnClassPath("templates/greet_ger");

        Template greet_mail_en = new Template();
        greet_mail_en.put("subject", "Greetings!");
        greet_mail_en.put("html_body", enContent);
        greet_mail_en.put("text_body", "Greetings!");

        Template greet_mail_cs = new Template();
        greet_mail_cs.put("subject", "Pozdrav!");
        greet_mail_cs.put("html_body", csContent);
        greet_mail_cs.put("text_body", "Ahoj!");

        Template greet_mail_ger = new Template();
        greet_mail_ger.put("subject", "GUTEN TAG!");
        greet_mail_ger.put("html_body", gerContent);
        greet_mail_ger.put("text_body", "Halo!");

        Template greet_sms_cs = new Template();
        greet_sms_cs.put("text", "Zdravime na openmobility $date$ ve meste $city$");

        Template greet_sms_en = new Template();
        greet_sms_en.put("text", "Greetings on openmobility $date$ in city $city$");

        Template greet_sms_ger = new Template();
        greet_sms_ger.put("text", "Gutten tag on de kaunference $date$ inslajs cyty $city$");

        greetMessage.addTemplate(new TemplateDescriptor("en", "mail"), greet_mail_en);
        greetMessage.addTemplate(new TemplateDescriptor("cs", "mail"), greet_mail_cs);
        greetMessage.addTemplate(new TemplateDescriptor("ger", "mail"), greet_mail_ger);
        greetMessage.addTemplate(new TemplateDescriptor("en", "sms"), greet_sms_en);
        greetMessage.addTemplate(new TemplateDescriptor("cs", "sms"), greet_sms_cs);
        greetMessage.addTemplate(new TemplateDescriptor("ger", "sms"), greet_sms_ger);

        /**
         * The reschedule lecture template*
         */
        rescheduleLectureMessage.setName("reschedule");
        rescheduleLectureMessage.setDescription("Template used to notify users of rescheduling lecture on coference. Please provide $from_time$, $to_time$, $lecture_name$ ");
        String resch_csContent = (new FileUtils()).readTextFileOnClassPath("templates/reschedule_cs");
        String resch_enContent = (new FileUtils()).readTextFileOnClassPath("templates/reschedule_en");

        Template resch_mail_en = new Template();
        resch_mail_en.put("subject", "The lecture rescheduled!");
        resch_mail_en.put("html_body", resch_enContent);
        resch_mail_en.put("text_body", "Lecture $lecture_name$ rescheduled from $from_time$ to $to_time$");

        Template resch_mail_cs = new Template();
        resch_mail_cs.put("subject", "Přednáška přeložena!");
        resch_mail_cs.put("html_body", resch_csContent);
        resch_mail_cs.put("text_body", "Přednáška $lecture_name$ posunuta z $from_time$ na $to_time$");

        Template resch_sms_cs = new Template();
        resch_sms_cs.put("text", "Prednaska $lecture_name$ posunuta z $from_time$ na $to_time$");

        Template resch_sms_en = new Template();
        resch_sms_en.put("text", "Lecture $lecture_name$ rescheduled from $from_time$ to $to_time$");

        rescheduleLectureMessage.addTemplate(new TemplateDescriptor("en", "mail"), resch_mail_en);
        rescheduleLectureMessage.addTemplate(new TemplateDescriptor("cs", "mail"), resch_mail_cs);
        rescheduleLectureMessage.addTemplate(new TemplateDescriptor("en", "sms"), resch_sms_en);
        rescheduleLectureMessage.addTemplate(new TemplateDescriptor("cs", "sms"), resch_sms_cs);


        getSuperTemplatesDao().createSuperTemplate(greetMessage);
        getSuperTemplatesDao().createSuperTemplate(rescheduleLectureMessage);
    }

    /**
     * Adds english version of html template for mail engine into cassandra.
     *
     * @param facade
     * @param subject Subject of the email
     * @param templateFile HTML content of the email
     * @throws CassandraException
     */
    private void addMailTemplate(
            String templateName,
            String templateFile,
            String descr,
            String subject,
            String textForm,
            String lang) throws CassandraException {
        SuperTemplate st = new SuperTemplate();

        String templateFileContent = (new FileUtils()).readTextFileOnClassPath(templateFile);
        st.setName(templateName);
        st.setDescription(descr);

        Template basicMailTemplate = new Template();
        basicMailTemplate.put("subject", subject);
        basicMailTemplate.put("html_body", templateFileContent);
        basicMailTemplate.put("text_body", textForm);

        st.addTemplate(new TemplateDescriptor(lang, "mail"), basicMailTemplate);

        getSuperTemplatesDao().createSuperTemplate(st);
    }

    /**
     * @return the failInfoDao
     */
    public FailInfoDao getFailInfoDao() {
        return failInfoDao;
    }

    /**
     * @return the routesDao
     */
    public RoutesDao getRoutesDao() {
        return routesDao;
    }

    /**
     * @return the statisticsDao
     */
    public StatisticsDao getStatisticsDao() {
        return statisticsDao;
    }

    /**
     * @return the superTemplatesDao
     */
    public SuperTemplatesDao getSuperTemplatesDao() {
        return superTemplatesDao;
    }

    /**
     * @return the tagsDao
     */
    public TagsDao getTagsDao() {
        return tagsDao;
    }

    /**
     * @return the templatesDao
     */
    public TemplatesDao getTemplatesDao() {
        return templatesDao;
    }

    /**
     * @return the usersDao
     */
    public UsersDao getUsersDao() {
        return usersDao;
    }
            
}
