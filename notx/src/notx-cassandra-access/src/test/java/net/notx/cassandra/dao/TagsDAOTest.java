package net.notx.cassandra.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.notx.cassandra.CassandraException;
import net.notx.core.entities.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TagsDAOTest extends AbstractDaoTest {

    private static TagsDao tagsdao;
    private static UsersDao usersdao;

    @Before
    public void beforeTest() {
        tagsdao = getBootstraper().getTagsDao();
        usersdao = getBootstraper().getUsersDao();
    }

    @Test
    public void testTagDelete() throws CassandraException {
        tagsdao.createTagForUser("conference1.cf", userFilip);
        Assert.assertTrue(usersdao.findUserByID("13").getTags().contains("conference1.cf"));
        tagsdao.deleteTagForUser("conference1.cf", userFilip);
        Assert.assertFalse(usersdao.findUserByID("13").getTags().contains("conference1.cf"));
    }

    @Test
    public void testTagUserAndGetByTag() throws CassandraException {

        List<User> users = new ArrayList<User>();
        users.add(userFilip);

        tagsdao.createTagForUsers("Konference1", users);

        users.add(userTomas);
        tagsdao.createTagForUsers("Konference2.poradatele", users);
        tagsdao.createTagForUsers("Konference2", users);

        List<User> foundUsers = usersdao.findUsersByTag("Konference1");
        Assert.assertEquals(1, foundUsers.size());
        Assert.assertEquals(userFilip.getId(), foundUsers.get(0).getId());

        foundUsers = usersdao.findUsersByTag("Konference2");
        Assert.assertEquals(2, foundUsers.size());

        foundUsers = usersdao.findUsersByTag("Konference2.poradatele");
        Assert.assertEquals(2, foundUsers.size());

        foundUsers = usersdao.findUsersByTag("fasdf");
        Assert.assertEquals("Looking for nonexistent tag", 0, foundUsers.size());
    }

    @Test
    public void testTagUserAndGetByID() throws CassandraException {

        List<User> users = new ArrayList<User>();
        users.add(userFilip);

        Set<String> foundTags = tagsdao.findTagsByID(userFilip.getId());
        Assert.assertEquals(2, foundTags.size());
        
        tagsdao.createTagForUsers("Konference1", users);
        users.add(userTomas);
        tagsdao.createTagForUsers("Konference2.poradatele", users);
        tagsdao.createTagForUsers("Konference2", users);

        foundTags = tagsdao.findTagsByID(userFilip.getId());
        Assert.assertEquals(5, foundTags.size());

        Assert.assertTrue(foundTags.contains("Konference1"));
        Assert.assertTrue(foundTags.contains("Konference2"));
        Assert.assertTrue(foundTags.contains("Konference2.poradatele"));

        foundTags = tagsdao.findTagsByID(userTomas.getId());
        Assert.assertEquals(5, foundTags.size());
        Assert.assertTrue(foundTags.contains("Konference2"));
        Assert.assertTrue(foundTags.contains("Konference2.poradatele"));

        foundTags = tagsdao.findTagsByID("13423");
        Assert.assertEquals("Nonexistent ID", 0, foundTags.size());
    }

    @Test
    public void testTagUserAndDeleteTags() throws CassandraException {
        List<User> users = new ArrayList<User>();
        users.add(userFilip);

        tagsdao.createTagForUsers("Konference1", users);
        users.add(userTomas);
        tagsdao.createTagForUsers("Konference2.poradatele", users);
        tagsdao.createTagForUsers("Konference2", users);

        tagsdao.deleteTag("Konference1");
        tagsdao.deleteTag("Konference2.poradatele");
        tagsdao.deleteTag("Konference2");
        tagsdao.deleteTag("DeletingNonExistentTag");

        Set<String> foundTags = tagsdao.findTagsByID(userFilip.getId());
        Assert.assertEquals(2, foundTags.size());
        foundTags = tagsdao.findTagsByID(userTomas.getId());
        Assert.assertEquals(3, foundTags.size());
        foundTags = tagsdao.findTagsByID("13423");
        Assert.assertEquals("Nonexistent ID", 0, foundTags.size());

        List<User> foundUsers = usersdao.findUsersByTag("Konference1");
        Assert.assertEquals(0, foundUsers.size());
        foundUsers = usersdao.findUsersByTag("Konference2");
        Assert.assertEquals(0, foundUsers.size());
        foundUsers = usersdao.findUsersByTag("Konference2.poradatele");
        Assert.assertEquals(0, foundUsers.size());
        foundUsers = usersdao.findUsersByTag("fasdf");
        Assert.assertEquals("Looking for nonexistent tag", 0, foundUsers.size());
    }
}
