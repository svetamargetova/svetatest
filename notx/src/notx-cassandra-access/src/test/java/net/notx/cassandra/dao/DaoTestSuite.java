/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.cassandra.dao;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import net.notx.cassandra.utils.CassandraInMemoryThread;
import net.notx.cassandra.utils.FileUtils;
import org.apache.cassandra.contrib.utils.service.CassandraServiceDataCleaner;
import org.apache.cassandra.thrift.CassandraDaemon;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This SUITE class will prepare Cassandra for testing. In detail , an instance of 
 * this class will run Cassandra as InMemmory deamon and clean up all data directories.
 * 
 * @author palo
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    FailInfoDAOTest.class,
    RoutesDAOTest.class,
    SentNotificationDAOTest.class,
    TagsDAOTest.class,
    TemplatesDAOTest.class,
    UserDAOTest.class})
public class DaoTestSuite {
    private static final Logger logger = LoggerFactory.getLogger(DaoTestSuite.class);
    private static CassandraDaemon cassandraDaemon = null;
    private static CassandraServiceDataCleaner dscleaner = new CassandraServiceDataCleaner();
    
    private static final String CASSANDRA_YAML_CONFIG = "cassandra.yaml_embeded";
    private static final String LOG4J_CONFIG = "log4j.properties";
    
    @BeforeClass
    public static void startCassandra() throws MalformedURLException, IOException{        
        configureCassandra();
        cleanUpCassandra();
        startInMemmoryCassandra();
    }

    private static void startInMemmoryCassandra() throws IOException {
        logger.info("Starting Cassandra deamon");        
        cassandraDaemon = new CassandraDaemon();        
        cassandraDaemon.activate();
        logger.info("Cassandra started");
    }

    private static void configureCassandra() throws MalformedURLException {
        logger.info("Configure cassandra");
        File yaml = (new FileUtils()).writeResourceToFile(CASSANDRA_YAML_CONFIG, "cassandra.yaml");
        File log4jconfig = (new FileUtils()).writeResourceToFile(LOG4J_CONFIG, "log4j.properties");

        //see http://svn.apache.org/repos/asf/cassandra/trunk/src/java/org/apache/cassandra/service/AbstractCassandraDaemon.java
        System.setProperty("cassandra.config", yaml.toURI().toURL().toString());
        System.setProperty("log4j.configuration", log4jconfig.toURI().toURL().toString());
    }

    private static void cleanUpCassandra() {
        logger.info("Cleaning directories");
        try {
            dscleaner.prepare();
            //  Clean up in case there was prior test. In fact this clean up is not strictly necessary
            //  because we are recreating keyspace before each test.
            dscleaner.cleanupDataDirectories();
        } catch (IOException e1) {
            logger.warn("Error while cleaning up directories and files of InMemory Cassandra");
        }
    }
    
    @Before
    public void cleanUp(){
        cleanUpCassandra();
    }
    
    @AfterClass
    public static void stopCassandra(){
        logger.info("Stopping Cassandra deamon");
        cassandraDaemon.deactivate();
    }
    
    
}
