package net.notx.cassandra.dao;

import java.util.List;
import java.util.Map.Entry;
import net.notx.cassandra.CassandraException;
import net.notx.core.entities.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserDAOTest extends AbstractDaoTest{
	private static UsersDao usersdao;
	
	@Before
	public void beforeTest(){
		usersdao = getBootstraper().getUsersDao();
	}

	/**
	 * <p>
	 * Users are marshalled into Cassandra manualy, therefore its important to check
	 * whether all fields put into DB are successfully persisted.
	 * </p>
	 */
	@Test
	public void testAddUser() throws CassandraException{

		User foundUser = usersdao.findUserByID(userTomas.getId());

		Assert.assertEquals(userTomas.getName(), foundUser.getName());
		Assert.assertEquals(userTomas.getId(), foundUser.getId());
		Assert.assertEquals(userTomas.getContacts().size(), foundUser
				.getContacts().size());
		Assert.assertEquals(userTomas.getLang(), foundUser
				.getLang());

		for (Entry<String, String> entry : userTomas.getContacts().entrySet()) {
			Assert.assertEquals(entry.getValue(),
					foundUser.getContacts().get(entry.getKey()));
		}
	}
	/**
	 * Tags and routes are indexed by UserID so this test should
	 * verify no garbage stays in db after deleting User.
	 * @throws CassandraException
	 */
	@Test
	public void testDeleteUser() throws CassandraException {
		TagsDao tagsdao = getBootstraper().getTagsDao();
		RoutesDao routesdao= getBootstraper().getRoutesDao();
		
		Assert.assertEquals(2,tagsdao.findTagsByID(userFilip.getId()).size());
		Assert.assertEquals(3,routesdao.findRecordsFor(userFilip.getId()).size());
		usersdao.deleteUserByID(userFilip.getId());
		Assert.assertEquals(0,tagsdao.findTagsByID(userFilip.getId()).size());
		Assert.assertEquals(0,routesdao.findRecordsFor(userFilip.getId()).size());
		Assert.assertEquals(3,tagsdao.findTagsByID(userTomas.getId()).size());
		Assert.assertEquals(3,routesdao.findRecordsFor(userTomas.getId()).size());
	}
	
	@Test(expected=NullPointerException.class)
	public void getNonExistentUser() throws CassandraException {
		User u = usersdao.findUserByID("324324");
		Assert.assertNull(u);
	}

	@Test
	public void getAllUsers() throws CassandraException {
		List<User> users = usersdao.findAllUsers();
		Assert.assertEquals("findAllUsers didn't found all users",
				DATASET_SIZE, users.size());
	}
}
