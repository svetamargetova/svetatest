package net.notx.cassandra.dao;

import net.notx.cassandra.CassandraException;
import net.notx.core.entities.SuperTemplate;
import net.notx.core.entities.Template;
import net.notx.core.entities.TemplateDescriptor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class TemplatesDAOTest extends AbstractDaoTest{
	private static SuperTemplatesDao tdao;
	
	@Before
	public void beforeTest(){
		tdao = getBootstraper().getSuperTemplatesDao();
	}
	
	@Test 
	public void testTemplateInDifferentlangs(){
		getBootstraper().createDataSetOpenMobility();
		SuperTemplate greetST = tdao.findSuperTemplateByName("greet");	
		Template partsEN = greetST.getTemplate(new TemplateDescriptor("en", "mail"));
		Template partsCS = greetST.getTemplate(new TemplateDescriptor("cs", "mail"));	
		Assert.assertTrue(partsEN.size() > 0);
		Assert.assertTrue(partsCS.size() > 0);
	}

	@Test
	public void getAllTemplatesTest(){
		Assert.assertTrue(tdao.findAllSuperTemplates().size() > 0);
	}
	
	@Test
	public void testAddTemplate() throws CassandraException{
		SuperTemplate st2 = new SuperTemplate();
		st2.setName("t2");
		SuperTemplate st1 = new SuperTemplate();
		st1.setName("t1");
		SuperTemplate st3 = new SuperTemplate();
		st3.setName("t3");
		st3.setDescription("DESC3");
		

		st1.setDescription("This is description of t1");
		Template basicMailTemplate = new Template();		
		basicMailTemplate.put("subject", "Account registration");
		basicMailTemplate.put("html_body","this is registration body!");
		basicMailTemplate.put("text_body", "XXXX");
		
		st1.addTemplate(new TemplateDescriptor( "cs", "mailengine"), basicMailTemplate);
		tdao.createSuperTemplate(st2);
		tdao.createSuperTemplate(st1);		
		tdao.createSuperTemplate(st3);
		
		SuperTemplate foundt1=tdao.findSuperTemplateByName("t1");
		Template foundTemplate = foundt1.getTemplate(new TemplateDescriptor("cs", "mailengine"));
		Assert.assertEquals(foundTemplate.get("text_body"),"XXXX");
		
		tdao.deleteSuperTemplate(st1);
		tdao.deleteSuperTemplate(st2);
		SuperTemplate foundt3=tdao.findSuperTemplateByName("t3");
		Assert.assertEquals("T3 had loaded with bad description!","DESC3",foundt3.getDescription());
		SuperTemplate nullTemplate1=tdao.findSuperTemplateByName("t1");
		Assert.assertNull("Template 1 wasnt deleted!", nullTemplate1);
	}
}
