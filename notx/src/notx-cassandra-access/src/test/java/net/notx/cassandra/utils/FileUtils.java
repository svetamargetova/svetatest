package net.notx.cassandra.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public  class FileUtils {
	
	/**
	 * Returns text file from classpath as text.
	 */
	public String readTextFileOnClassPath(String fileName) {
		StringBuilder contents = new StringBuilder();
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new InputStreamReader(this.getClass()
					.getClassLoader().getResourceAsStream(fileName),"UTF-8"));
			String text = null;

			while ((text = reader.readLine()) != null) {
				contents.append(text).append(
						System.getProperty("line.separator"));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return contents.toString();
	}

	/**
	 * Returns text file from classpath as text.
	 */
	public String readTextFile(String fileName) {
		StringBuffer contents = new StringBuffer();
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
			String text = null;

			while ((text = reader.readLine()) != null) {
				contents.append(text).append(
						System.getProperty("line.separator"));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return contents.toString();
	}

	
	/**
	 * Writes resource on classpath as file to disk
	 */
	public File writeResourceToFile(String resourceName, String destFile) {
		BufferedWriter bw = null;
		
		try {
			bw = new BufferedWriter(new FileWriter(new File(destFile)));
			bw.write(readTextFileOnClassPath(resourceName));
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return new File(destFile);
	
	}

	public void deleteFile(String path) {
		File file = new File(path);
		file.delete();
	}
}
