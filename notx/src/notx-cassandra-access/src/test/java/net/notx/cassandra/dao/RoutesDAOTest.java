package net.notx.cassandra.dao;


import net.notx.cassandra.CassandraException;
import net.notx.core.entities.RoutingRecord;
import net.notx.core.entities.RoutingRecordCollection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RoutesDAOTest extends AbstractDaoTest {

    private static UsersDao usersdao;

    @Before
    public void beforeTest() {
        usersdao = getBootstraper().getUsersDao();
    }

    @Test
    public void addRoute() throws CassandraException {
        RoutingRecord record1 = new RoutingRecord();
        record1.setMsgtype("important");
        RoutingRecord record2 = new RoutingRecord();
        record1.setMsgtype("important");

        record1.setUserid("filipek");
        getBootstraper().getRoutesDao().addRoute(record1);
        getBootstraper().getRoutesDao().addRoute(record2);

        RoutingRecordCollection rrcol1 = getBootstraper().getRoutesDao().findAllRoutingRecordsWith("filipek");
        RoutingRecordCollection rrcolEmpty = getBootstraper().getRoutesDao().findAllRoutingRecordsWith("filipeka");

        Assert.assertTrue(rrcol1.size() == 2);
        Assert.assertTrue(rrcolEmpty.size() == 1);
    }
}
