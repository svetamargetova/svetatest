package net.notx.cassandra.dao;

import java.io.IOException;
import java.net.MalformedURLException;
import net.notx.cassandra.CassandraException;
import net.notx.cassandra.config.CassandraTestBootstraper;
import net.notx.cassandra.utils.KeyspaceUtils;
import net.notx.client.NotxException;
import net.notx.core.entities.User;
import org.apache.cassandra.contrib.utils.service.CassandraServiceDataCleaner;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This test starts up inmemory Cassandra and tests that CRUD methods on
 * CassandraFacade work as expected.
 */
public class AbstractDaoTest {
    static final Logger logger = LoggerFactory.getLogger(AbstractDaoTest.class);
    protected static int DATASET_SIZE = 2;
    protected static User userFilip;
    protected static User userTomas;
    private static CassandraTestBootstraper bootstraper;    

   
    @Before
    public void prepare() throws NotxException, CassandraException, MalformedURLException {
                                
        bootstraper = new CassandraTestBootstraper();                
        
        KeyspaceUtils.recreateKeyspace(bootstraper.getNotXBootstraper().getCassandraNotxConfig());
        bootstraper.createDataSet01();

        UsersDao usersdao = bootstraper.getUsersDao();
        userFilip = usersdao.findUserByID("13");
        userTomas = usersdao.findUserByID("18");
    }

    public CassandraTestBootstraper getBootstraper() {
        return bootstraper;
    }   
}
