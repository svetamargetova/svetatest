package net.notx.cassandra.dao;

import java.util.List;
import net.notx.cassandra.CassandraException;
import net.notx.client.message.HandlingFailMessage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FailInfoDAOTest extends AbstractDaoTest {
    
	private static FailInfoDao fidao;
	
	@Before
	public void beforeTest(){
		fidao = getBootstraper().getFailInfoDao();
	}
	
	@Test
	public void testFailInformation() throws CassandraException {
		HandlingFailMessage hfm = new HandlingFailMessage();
		fidao.createFailInformation(hfm);
		fidao.createFailInformation(hfm);
		List<HandlingFailMessage> hfms = fidao.findAllFailInformation();
		Assert.assertTrue(hfms.size() == 2);
		fidao.deleteFailInformation(hfm);
		hfms = fidao.findAllFailInformation();
		Assert.assertTrue(hfms.size() == 1);
	}
}
