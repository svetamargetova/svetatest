package net.notx.cassandra.utils;

import java.io.IOException;
import org.apache.cassandra.thrift.CassandraDaemon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CassandraInMemoryThread extends Thread {

    private static Logger logger = LoggerFactory.getLogger(CassandraInMemoryThread.class);

    @Override
    public void run() {
        try {
            CassandraDaemon cassandraDaemon = new CassandraDaemon();
            cassandraDaemon.init(null);
            cassandraDaemon.start();
        } catch (IOException e) {
            logger.error("Couldn't start in memory cassandra daemon!");
        }
    }
}
