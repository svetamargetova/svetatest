import java.util.HashMap;
import java.util.Map;

import net.notx.com.thrift.NotxServer.Client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

public final class PlayProjectClientClient {

//	private static final QName SERVICE_NAME = new QName("http://ws.com.notx.net/",
//			"NotxWebService");
//	private static final QName PORT_NAME = new QName("http://ws.com.notx.net/",
//			"NotxWebServicePort");

	private PlayProjectClientClient() {
		
	}

	public static void main(String args[]) throws Exception {
//		NotxJavaFacade javafacade = new NotxJavaFacade("tcp://localhost:61616");
//		Map<String,String> ph = new HashMap<String,String>();
//		ph.put("salutation", "Hello user");
//		ph.put("activation_key", "ackey");
//		ph.put("email", "mailmail");
//		javafacade.tag("44", "users");
//		javafacade.tag("13", "users");
//		javafacade.tag("18", "users");
		
//		javafacade.untag("13", "Conference1.kkk");
		//javafacade.tag("13", "Conference2.poradatel");
		//javafacade.sendNotification(":13", "standard", "account_registration", ph);
//		javafacade.sendNotification("Conference2.poradatel", "standard", "account_registration", ph);
		
//		CassandraFacade cfacade = new CassandraFacade("Test Cluster", ,"testnotx");
//		cfacade.open();
//		try {
//			System.setProperty("com.prettyprint.cassandra.load_hector_log4j",
//					"false");
//			CassandraHostConfigurator config = new CassandraHostConfigurator(
//					"localhost:9160");
//			config.setRetryDownedHosts(true);
//			config.setRetryDownedHostsDelayInSeconds(10);
//			Cluster c = HFactory.getOrCreateCluster("Test Cluster", config);
//			Keyspace keyspace = HFactory.createKeyspace("testnotx", c);
//
//			SliceQuery<String, String, String> sliceQ = HFactory.createSliceQuery(keyspace, new StringSerializer(), new StringSerializer(), new StringSerializer());
//			
//			sliceQ = sliceQ.setColumnFamily("Users").setKey("13").setColumnNames("name");
//			//Thread.sleep(10000);
//			QueryResult<ColumnSlice<String,String>> qresult = sliceQ.execute();
//			ColumnSlice<String,String> slice = qresult.get();
//			//.getColumnByName("name").getValue();
//			System.out.println("EX NONOONONO CAUGHT");
//			c=null;
//			
//		} catch (Exception ex) {
//			System.out.println("EX CAUGHT" + ex.getMessage());
//		}
//		return;
//		MQProducer producer = new MQProducer("tcp://localhost:61616", "NOTXQUEUE");
//		producer.open();
//		producer.insertMessage(new String("fsdfsdf"));
//		producer.close();
		
//		CassandraBootstraper bootstrapper = new CassandraBootstraper("Test Cluster", "localhost:9160","testnotx");
//		bootstrapper.startInMemoryCassandra();
		
//		CassandraBootstraper bootstrap = new CassandraBootstraper(
//				"Test Cluster", "localhost:9160", "testnotx");
//		bootstrap.recreateKeyspace();
//		bootstrap.createDataSet01();
//		

//		String endpointAddress = "http://localhost:9000/NotxWebService";
//		service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING,
//				endpointAddress);
//		
//		Service service = Service.create(new URL(
//		"http://localhost:9000/NotxWebService?wsdl"), SERVICE_NAME);
//		NotxWebService hw = service.getPort(NotxWebService.class);
////
		
//		// TODO idčko by měl být automatický tag!
//		PlaceHolders ph = new PlaceHolders();
//		ph.addPlaceHolder("salutation", "Usssser");
//		ph.addPlaceHolder("activation_key", "x5f6fsssdsf5ads");
//		ph.addPlaceHolder("email", "usessr@user.com");
//		
//		
//		hw.tag("13", "users");
//		hw.sendNotification("users","standard","account_registration",ph);
//		tagUsingThrift("13", "users");
//		sendUsingThrift("users", "standard", "account_registration",
//				placeHolders);
	}

	private static void tagUsingThrift(String id, String tag) {
		//
		TTransport transport;
		try {
			transport = new TSocket("localhost", 7911);
			TProtocol protocol = new TBinaryProtocol(transport);
			Client client = new Client(protocol);
			transport.open();
			client.tag(id, tag);
			// TODO log
			// System.out.println("Time from server:" + time);
			transport.close();
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
	}

	private static void sendUsingThrift(String tag, String msgType,
			String templateName, HashMap<String, String> placeHolders) {

		TTransport transport;
		try {
			transport = new TSocket("localhost", 7911);
			TProtocol protocol = new TBinaryProtocol(transport);
			Client client = new Client(protocol);
			transport.open();
			client.sendNotification(tag, msgType, templateName, placeHolders);
			// TODO log
			// System.out.println("Time from server:" + time);
			transport.close();
		} catch (TTransportException e) {
			e.printStackTrace();
		} catch (TException e) {
			e.printStackTrace();
		}
	}

}
