package net.notx.engine.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.UUID;

import net.notx.engine.EngineException;
import net.notx.utils.XmlDocument;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

/**
 * There are many actions that can be called upon SMSBrana provider. 
 * See documentation at http://www.smsbrana.cz/dokumenty/smsconnect_http.pdf.
 * 
 * This class encapsulates basic authentication and shields client of this class
 * of these time consuming tasks.
 * @author kalaz
 *
 */
public class SMSBrana {
	DefaultHttpClient httpclient = new DefaultHttpClient();
	private String login;
	private String password;
	private static Logger logger = LoggerFactory.getLogger(SmsEngine.class);

	
	public SMSBrana(String username, String password){
		this.login = username;
		this.password = password; 
	}
	
	/**
	 * 
	 * @param actionName Name of action to perform (For example send_sms)
	 * @param sactionParameters Parameters for this action for exmaple "number:739649..."
	 * @return Content of response from SMSBrana 
	 */
	public String performAction(String actionName, Map<String, String > actionParameters){
		logger.debug("Preparing action "+actionName);
		String textTime = getDateInSpecialFormat();
		String sul = UUID.randomUUID().toString();
		
		List<NameValuePair> qparams = new ArrayList<NameValuePair>();
		qparams.add(new BasicNameValuePair("action", actionName));
		qparams.add(new BasicNameValuePair("login", login));
		qparams.add(new BasicNameValuePair("time", textTime));
		qparams.add(new BasicNameValuePair("sul", sul));
		qparams.add(new BasicNameValuePair("auth", computePHPlikeMD5Hash(password+textTime+sul)));
		
		for (Entry<String,String> param : actionParameters.entrySet())
		{	
			logger.debug("Adding parameter: " + param.getKey()+"-"+param.getValue());
			qparams.add(new BasicNameValuePair(param.getKey(), param.getValue()));
		}
			
		
		HttpGet httpget = createHttpGetToSMSBrana(qparams); 
		
		try {
			HttpResponse response = httpclient.execute(httpget);
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null)
				sb.append(line);
			String result  =  sb.toString();
			
			XmlDocument document  = new XmlDocument(result);
			String err = document.getTextValue("//err");
			if (err != null && !err.equals("0")){
				throw new EngineException("Error while contacting SMS brana with code: "+err, null);
			}
			
			return result;
		} catch (Exception ex) {
			throw new EngineException("Couldn't send request to server. ",ex);
		}
	}
	
	/**
	 * Creates Get action for sms brana URL and adds parameters to it.
	 * @param qparams
	 * @return
	 */
	private HttpGet createHttpGetToSMSBrana(List<NameValuePair> qparams) {
		URI uri = null;
		try {
			uri = URIUtils.createURI("http", "api.smsbrana.cz", -1, "/smsconnect/http.php", 
				    URLEncodedUtils.format(qparams, "UTF-8"), null);
		} catch (URISyntaxException e) {
			throw new EngineException("Uri syntax exception",e);
			
		}
		return new HttpGet(uri);
	}

	/**
	 * @return  date now - formated as specified in SMSBrana documentation.
	 */
	private String getDateInSpecialFormat() {
		Date date = new Date();
		Format formatDate = new SimpleDateFormat("yyyyMMdd");
		Format formatTime = new SimpleDateFormat("HHmmss");
		return formatDate.format(date) + "T" + formatTime.format(date);
	}

	/**
	 * Hashes string in the same way as PHP function MD5.
	 * 
	 * @param string string that will be hashed
	 * @return
	 */
	private String computePHPlikeMD5Hash(String string) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			return new BigInteger(1,md.digest(string.getBytes())).toString(16);
		} catch (NoSuchAlgorithmException e) {
			throw new EngineException("Couldn't create hash",e);
		}
	}

}
