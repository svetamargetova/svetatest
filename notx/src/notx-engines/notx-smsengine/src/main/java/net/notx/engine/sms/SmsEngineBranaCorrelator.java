package net.notx.engine.sms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.notx.engine.EngineSettings;
import net.notx.engine.correlation.ActiveCorrelationProcessor;
import net.notx.engine.correlation.CorrelationInfo;
import net.notx.utils.XmlDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SMS brana has inverted correlation ID. It provides us with 
 * correlation id for sent sms that can be later looked up.
 * 
 * @author kalaz
 *
 */
public class SmsEngineBranaCorrelator implements ActiveCorrelationProcessor {
	private EngineSettings settings;
	private static Logger logger = LoggerFactory.getLogger(SmsEngineBranaCorrelator.class);

	@Override
	public List<CorrelationInfo> tryCorrelate() {
		logger.debug("Correlating sms messages");
		
		SMSBrana smsBrana = new SMSBrana(settings.getStringProperty("smsbrana.username"), settings.getStringProperty("smsbrana.password"));
		String xmlResult = smsBrana.performAction("inbox", new HashMap<String, String>());
		
		List<CorrelationInfo> correlationInfos = new ArrayList<CorrelationInfo>();
		
		XmlDocument document = new XmlDocument(xmlResult);
		List<XmlDocument> deliveredItems = document.getElements("/result/inbox/delivery_report/item");
		
		for (XmlDocument sms:deliveredItems){
			logger.debug("SMS correlation of message ["+sms.getTextValue("/item/idsms")+"] with status: "+sms.getTextValue("/item/status"));
			
			//Status 1 means "delivered"
			if (!sms.getTextValue("/item/status").equals("1"))
				continue;
			
			CorrelationInfo cinfo = new CorrelationInfo();
			cinfo.setCorrelationID("sms-"+sms.getTextValue("/item/idsms"));
			cinfo.setEngine("sms");
			cinfo.setReceivedTime(new Date());
			correlationInfos.add(cinfo);
		}
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("delete","delete");
		smsBrana.performAction("inbox", params );
		return correlationInfos;
	}

	@Override
	public void configure(EngineSettings engineSettings) {
		this.settings =  engineSettings;
	}

}
