package net.notx.engine.sms;

import net.notx.core.entities.Template;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.notx.engine.*;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Documentation of http client: http://hc.apache.org/user-docs.html
 *
 * @author Filip Nguyen
 *
 */
public class SmsEngine implements Engine {

    private EngineSettings settings;
    private static Logger logger = LoggerFactory.getLogger(SmsEngine.class);

    @Override
    public void sendNotification(String contact, Template messageParts)
            throws EngineException {

        String text = messageParts.get("text");
        String correlationID = messageParts.get("cid");

        if (text == null || text.trim().equals("")) {
            throw new EngineException("Text of SMS wasn't supplied!", null);
        }

        if (correlationID == null || correlationID.trim().equals("")) {
            throw new EngineException("Correlation ID for this sms wasnt supplid under key [cid]", null);
        }

        DefaultHttpClient httpclient = new DefaultHttpClient();

        String xml = constructXML(text, correlationID, contact);
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        qparams.add(new BasicNameValuePair("xml", xml));
        String params = URLEncodedUtils.format(qparams, "UTF-8");

        UrlEncodedFormEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(qparams, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new EngineException("Couldn't construct the encoded form entity. ", e);
        }
        HttpPost httpPost = new HttpPost("https://www.sms-operator.cz/webservices/webservice.aspx");
        httpPost.setEntity(entity);


        try {
            HttpResponse response = httpclient.execute(httpPost);
            logger.debug("SMS response: {}", response.getStatusLine().toString());
        } catch (Exception ex) {
            throw new EngineException("Couldn't send request to server. ", ex);
        }
    }

    private String constructXML(String text, String correlationID, String contact) {
        try {
            String sms_template = FileUtils.readFileToString(new File("sms_engine_template.xml"), "UTF-8");
            StringTemplate template = new StringTemplate(sms_template);

            template.setAttribute("cid", correlationID);
            template.setAttribute("text", text);
            template.setAttribute("phone", contact);
            template.setAttribute("password", settings.getStringProperty("password"));
            template.setAttribute("username", settings.getStringProperty("username"));

            logger.debug(template.toString());
            return template.toString();
        } catch (IOException ex) {
            logger.error("Can't read SMS Brana XML request file!", ex);
            throw new NullPointerException();
        }
    }

    @Override
    public void configure(EngineSettings settings) throws EngineException {
        this.settings = settings;
    }

   

    @Override
    public Set<String> getRequiredMessageParts() {
        HashSet<String> set = new HashSet<String>();
        set.add("text");
        return set;
    }
  

    @Override
    public String getRequiredContact() {
        return "phone";
    }
}
