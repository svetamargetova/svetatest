package net.notx.engine.sms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.notx.engine.Engine;
import net.notx.engine.EngineException;
import net.notx.engine.EngineSettings;
import net.notx.core.entities.Template;
import net.notx.utils.XmlDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * SMS brana has inverted correlation ID. It provides us with 
 * correlation id for sent sms that can be later looked up.
 * 
 * @author kalaz
 *
 */
public class SmsEngineBrana implements Engine {
	private EngineSettings settings;
	private static Logger logger = LoggerFactory.getLogger(SmsEngine.class);
	
	@Override
	public void sendNotification(String contact, Template messageParts)
			throws EngineException {
		SMSBrana smsBrana = new SMSBrana(settings.getStringProperty("smsbrana.username"), settings.getStringProperty("smsbrana.password"));
		Map<String,String>  parameters = new HashMap<String,String>();
		parameters.put("number", contact);
		parameters.put("message", messageParts.get("text"));
		parameters.put("delivery_report", "1");
		parameters.put("sender_id", "testid");

		String result = smsBrana.performAction("send_sms", parameters);
		String smsID = (new XmlDocument(result)).getTextValue("//sms_id");
		messageParts.put("cid", "sms-"+smsID.trim());
	}
	
	@Override
	public void configure(EngineSettings settings) throws EngineException {
		this.settings = settings;
	}
	
	
	@Override
	public Set<String> getRequiredMessageParts() {
		Set<String> set = new HashSet<String>();
		set.add("text");
		
		return set;
	}
	
        
	@Override
	public String getRequiredContact() {
		return "phone";
	}

}
