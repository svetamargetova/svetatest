package net.notx.engine.mail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.SharedByteArrayInputStream;
import net.notx.client.NotxException;
import net.notx.engine.correlation.email.EmailCorrelationProcessor;
import net.notx.engine.correlation.email.ProcessedEmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailEngineCorrelationProcessor implements EmailCorrelationProcessor {

    private Logger logger = LoggerFactory.getLogger(MailEngineCorrelationProcessor.class);

    @Override
    public boolean canHandle(Message msg) throws Exception {
        return msg.getContentType().contains("multipart/report");
    }

    @Override
    public ProcessedEmailMessage handle(Message msg) throws Exception {
        ProcessedEmailMessage processed = new ProcessedEmailMessage();

        if (msg.getContentType().contains("multipart/report")) {
            MimeMultipart mime = (MimeMultipart) msg.getContent();

            if (mime.getCount() < 3) {
                throw new NotxException(
                        "Report message have less than 3 parts!");
            }

            BodyPart bodyPart = mime.getBodyPart(2);

            BufferedReader reader = null;
            String correlationID = null;

            try {
                SharedByteArrayInputStream sbis = (SharedByteArrayInputStream) bodyPart.getContent();
                reader = new BufferedReader(new InputStreamReader(sbis, "UTF-8"));

                String corrIDkey = "Notx-Correlation-ID:";
                String line = null;

                while ((line = reader.readLine()) != null) {
                    if (line.toLowerCase().startsWith(corrIDkey.toLowerCase())) {
                        correlationID = line.substring(corrIDkey.length()).trim();
                    }
                }

                if (correlationID == null) {
                    throw new NotxException(
                            "Correlation ID wasn't found in return receipt!");
                }
            } finally {
                reader.close();
            }

            logger.debug("Return receipt detected. The correlation ID: {}", correlationID);
            processed.setEngine("mail");
            processed.setCorrelationID(correlationID);
            processed.setUnprocessedMessage(msg);
            processed.setReceivedTime(msg.getReceivedDate());
        } else {
            logger.warn("The message should've been processed by MailEngineCorrelationProcessor, but wasn't report!");
        }

        return processed;
    }

    @Override
    public String getName() {
        return "Mail engine correlation processor";
    }
}
