package net.notx.engine.mail;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import net.notx.engine.Engine;
import net.notx.engine.EngineException;
import net.notx.engine.EngineSettings;
import net.notx.core.entities.Template;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This engine uses standard Apache Commons Mail implementation to encapsulate
 * email sending. This implementation sends both html and plain text email
 * variant.
 *
 * @author Filip Nguyen
 *
 */
public class MailEngine implements Engine {

    private static Logger logger = LoggerFactory.getLogger(MailEngine.class);
    private static final String HTML_BODY = "html_body";
    private static final String SUBJECT = "subject";
    private static final String TEXT_BODY = "text_body";
    // -- configuration 
    private String host;
    private String user;
    private String pass;
    private boolean ssl;
    // only when ssl is true
    private String sslSmtpPort;
    private boolean tls;
    private int port;
    private String fromEmail;
    private String fromName;
    private boolean hasAttachments;
    private String attachmentsDownloadUrlPrefix;

    public MailEngine() {
        //Hack to avoid UnsupportedType exception when loading activation/jar with different classloaders
        //see 
        // http://blog.hpxn.net/2009/12/02/tomcat-java-6-and-javamail-cant-load-dch/
        // http://stackoverflow.com/questions/1969667/send-a-mail-from-java5-and-java6\	
        Thread.currentThread().setContextClassLoader(com.sun.mail.handlers.multipart_mixed.class.getClassLoader());
//		logger.debug("The multipart_mixed was loaded by: " + com.sun.mail.handlers.multipart_mixed.class.getClassLoader());

    }

    @Override
    public void configure(EngineSettings settings) throws EngineException {
        host = settings.getStringProperty("smtp.host");
        port = settings.getIntProperty("smtp.port");
        user = settings.getStringProperty("smtp.user");
        pass = settings.getStringProperty("smtp.pass");
        ssl = settings.getBoolProperty("smtp.ssl");
        sslSmtpPort = settings.getStringProperty("smtp.ssl.port");
        tls = settings.getBoolProperty("smtp.tls");
        fromEmail = settings.getStringProperty("smtp.fromEmail");
        fromName = settings.getStringProperty("smtp.fromName");
        hasAttachments = settings.getBoolPropertySafe("withAttachments",false);
        attachmentsDownloadUrlPrefix = settings.getStringPropertySafe("attachmentsUrlPrefix",null);

        Object[] params = new Object[]{host, port, user, ssl, sslSmtpPort, tls, fromEmail, fromName};
        logger.info("Mail engine configuration[host={} port={} user={} pass=**** ssl={} sslSmtpPort={} tls={} fromEmail={} fromName={}", params);
    }

    @Override
    public void sendNotification(String contact, Template messageParts)
            throws EngineException {

        if (messageParts.get("cid") == null) {
            throw new EngineException("Message parts must include correlation id under key 'cid'!", null);
        }

        if (contact == null) {
            logger.warn("Email not set. Nothing will be sent.");
            return;
        }

        try {
            // configuring email
            HtmlEmail email = new HtmlEmail();
            Map<String, String> headers = new HashMap<String, String>();
            //headers.put("Disposition-Notification-To", config.getStringProperty("return-receipt-email"));
            //headers.put("Notx-Correlation-ID", messageParts.get("cid"));

            email.setHeaders(headers);
            email.setCharset("UTF-8");
            email.setHostName(host);
            email.setAuthentication(user, pass);
            email.setSSL(ssl);
            if (ssl) {
                email.setSslSmtpPort(sslSmtpPort);
            }
            email.setTLS(tls);
            email.setSmtpPort(port);
            email.setFrom(fromEmail, fromName);

            email.setSubject(messageParts.get(SUBJECT));
            if (messageParts.get(HTML_BODY) == null) {
                logger.debug("Html body is null!");
            } else {
                email.setHtmlMsg(messageParts.get(HTML_BODY));
            }
            if (messageParts.get(TEXT_BODY) == null) {
                logger.debug("Text body is null!");
            } else {
                email.setTextMsg(messageParts.get(TEXT_BODY));
            }
            email.addTo(contact);
            processAttachments(contact, messageParts, email);
            email.send();
        } catch (Exception e) {
            logger.error("Exception sending email", e);
            throw new EngineException("Error while sending email", e);
        }
    }

    protected void processAttachments(String contact, Template template, HtmlEmail email){
        if (hasAttachments) {
            List<String> attachments = template.getPlaceholdersWithPrefix("attachment");
            if (!attachments.isEmpty()) {
                for (String attachment : attachments) {
                    try {
                        final String[] ap = attachment.split("#");
                        if (ap.length != 2) {
                            logger.warn("Attachment '{}' ignored, 2 parts required [Attachment Name]#[Attachment ID]!", attachment);
                            continue;
                        }
                        email.attach(new URL(String.format("%s%s", attachmentsDownloadUrlPrefix, ap[1])), ap[0], "" , EmailAttachment.ATTACHMENT);
                    } catch (Exception ex) {
                        logger.error("Error creating attachmnet!", ex);
                    }
                }
            }
        }
    }

    @Override
    public Set<String> getRequiredMessageParts() {
        Set<String> set = new HashSet<String>();
        set.add(SUBJECT);
        set.add(TEXT_BODY);
        set.add(HTML_BODY);
        return set;
    }

    @Override
    public String getRequiredContact() {
        return "email";
    }
}
