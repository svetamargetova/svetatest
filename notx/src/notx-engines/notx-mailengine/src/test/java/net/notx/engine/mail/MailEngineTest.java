package net.notx.engine.mail;

import com.dumbster.smtp.SimpleSmtpServer;
import java.util.Properties;
import net.notx.engine.EngineSettings;
import net.notx.core.entities.Template;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;


/**
 * This class verifies function of the MailEngine. 
 * This test requires setting in test_config.properties for 
 * smtp server and recipients address (optional). 
 * 
 * This class mainly tests robustness of MailEngine while 
 * sending malformed or badly configured engine. Not 
 * necessary well implemented sending.
 * 
 * @author Filip Nguyen
 *
 */
public class MailEngineTest {
	private static Properties testConfig = null;
	private static SimpleSmtpServer smtpServer= null; 
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@BeforeClass
	public static void SetUp() {
		//TODO setup log4j
		smtpServer = SimpleSmtpServer.start();
	}
	
	@AfterClass
	public static  void TearDown(){
		smtpServer.stop();
	}
	
	
//	@Test
	public void sendRealEmail(){
//		EngineSettings settings = new EngineSettings();
//		settings.put("smtp.host", "smtp.seznam.cz");
//		settings.put("transport.protocol", "smtp");
//		settings.put("smtp.auth", "true");
//		settings.put("password", "maildn10kpl1ska");
//		settings.put("user", "kalaz");
//		settings.put("return-receipt-email", "notxwatchdog@gmail.com");
//		settings.put("from", "kalaz@seznam.cz");
//		
//
//		MailEngine engine = new MailEngine();
//		engine.configure(settings);
//		Template messageParts = new Template();
//		messageParts.put("cid", "1234567");
//		messageParts.put("html_body", "fasdfasf");
//		messageParts.put("subject", "fasdfasf");
//		messageParts.put("text_body", "fasdfasf");
//		engine.sendNotification("gressa@acemcee.com", messageParts);
	}
	
//	@Test
//	public void SendMail() throws EngineException {
//		EngineSettings settings = new EngineSettings();
//		settings.put("smtp.host", "localhost");
//		settings.put("transport.protocol", "smtp");
//		settings.put("smtp.auth", "");
//		settings.put("password", "");
//		settings.put("user", "kalaz");
//		settings.put("from", "fromSomewhere@email.com");
//		User filip = new User();
//		filip.setContact("email", "nguyen.filip@gmail.com");
//		filip.setId("31");
//
//		MailEngine engine = new MailEngine();
//		engine.configure(settings);
//		MessageParts messageParts = new MessageParts();
//		messageParts.put("html_body", "fasdfasf");
//		messageParts.put("subject", "fasdfasf");
//		messageParts.put("text_body", "fasdfasf");
//		engine.sendNotification(filip.getContact("email"), messageParts);
//		
//		Assert.assertTrue(smtpServer.getReceivedEmailSize() == 1);
//		Iterator emailIter = smtpServer.getReceivedEmail();
//		SmtpMessage email = (SmtpMessage) emailIter.next();
//		//TODO assert na to jak vypada vnitrek emailu
//		//System.out.print(email.getBody());
//	}
}
