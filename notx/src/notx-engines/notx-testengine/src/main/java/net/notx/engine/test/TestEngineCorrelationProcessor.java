package net.notx.engine.test;

import net.notx.engine.correlation.CorrelationProcessor;
import net.notx.engine.correlation.ProcessedCorrelationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestEngineCorrelationProcessor implements CorrelationProcessor {

    private Logger logger = LoggerFactory.getLogger(TestEngineCorrelationProcessor.class);
    
    @Override
    public String getName() {
        return "Test engine correlation processor";
    }

    @Override
    public boolean canHandle(Object correlationMessage) throws Exception {
        return true;
    }

    @Override
    public ProcessedCorrelationMessage handle(Object correlationMessage) throws Exception {
        ProcessedCorrelationMessage message = new ProcessedCorrelationMessage();        
        return  message;
    }
}
