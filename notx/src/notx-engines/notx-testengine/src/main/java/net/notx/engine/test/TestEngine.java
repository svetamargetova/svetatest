package net.notx.engine.test;

import java.util.HashSet;
import java.util.Set;
import net.notx.engine.Engine;
import net.notx.engine.EngineException;
import net.notx.engine.EngineSettings;
import net.notx.core.entities.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This engine is intended only to testing purpose - in test to simulate processing
 * message;
 *
 * @author Filip Nguyen
 *
 */
public class TestEngine implements Engine {

    private static Logger logger = LoggerFactory.getLogger(TestEngine.class);

    private long sleep = 0;

    public TestEngine() {
    }
   

    @Override
    public void configure(EngineSettings settings) throws EngineException {

        sleep = Long.parseLong(settings.getStringProperty("sleep"));
    }

    @Override
    public void sendNotification(String contact, Template messageParts)
            throws EngineException {
        try {
            logger.info("Sending with {} message {}",contact,messageParts);
            Thread.sleep(sleep);
        } catch (InterruptedException ex) {
        }

    }

    @Override
    public Set<String> getRequiredMessageParts() {
        Set<String> set = new HashSet<String>();
        set.add("text");
        return set;
    }

    @Override
    public String getRequiredContact() {
        return "c_email";
    }
}
