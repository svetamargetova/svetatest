package net.notx.engine.aws.ses;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import java.util.HashSet;
import java.util.Set;
import net.notx.engine.Engine;
import net.notx.engine.EngineException;
import net.notx.engine.EngineSettings;
import net.notx.core.entities.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AwsSesEngine implements Engine {

    private static Logger logger = LoggerFactory.getLogger(AwsSesEngine.class);
    private AmazonSimpleEmailServiceClient client;
    


    @Override
    public void configure(EngineSettings settings) throws EngineException {

        String awsAccessKey = settings.getStringProperty("awsAccessKey");
        String awsSecretKey = settings.getStringProperty("awsSecretKey");
        
        client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(awsAccessKey,awsSecretKey));                
    }

    @Override
    public void sendNotification(String contact, Template messageParts)
            throws EngineException {
       
               
    }

    @Override
    public Set<String> getRequiredMessageParts() {
        Set<String> set = new HashSet<String>();
        set.add("subject");
        set.add("body_text");
        set.add("body_html");
        return set;
    }

    @Override
    public String getRequiredContact() {
        return "c_mail";
    }
}
