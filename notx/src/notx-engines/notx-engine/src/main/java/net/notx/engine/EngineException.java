package net.notx.engine;

import net.notx.client.NotxException;

public class EngineException extends NotxException {

	public EngineException(String msg, Exception cause){
		super(msg,cause);
	}

	
	
}
