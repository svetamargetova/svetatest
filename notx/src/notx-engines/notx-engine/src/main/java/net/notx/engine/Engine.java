package net.notx.engine;

import java.util.Map;
import java.util.Set;
import net.notx.core.entities.Template;

public interface Engine {

    /**
     * <p>Implement this method to send notification via this Engine. Engine
     * should declare required message parts in getRequiredMessageParts
     * method.</p>
     *
     * <p>Each message can consist of multiple parts for example mail has
     * subject, body and possibly the body will be html and plain text.<p>
     *
     * <p>Message parts always contain 'cid' and each engine should ensure that
     * correlationID is set into message so that provider of notification will
     * sent 'cid' back to communication module and it will be processed as
     * needed.</p>
     *
     * @param contact Value of contact that will be used to send the
     * notification
     * @throws EngineException
     */
    void sendNotification(String contact, Template messageParts) throws EngineException;

    /**
     * Settings to each engine are supplied in engines.properties in engines
     * directory.
     *
     * @param settings
     * @throws EngineException
     */
    void configure(EngineSettings settings) throws EngineException;

    /**
     * Various engines need specific message parts as mentioned in documentation
     * for {@link #sendNotification(String, Map)}. Engine should be able to tell
     * which parts are required by him, e.g. email needs subject, html_part,
     * text_part.
     *
     * @return
     */
    Set<String> getRequiredMessageParts();

       

    /**
     * Name of the contact that is used for sending notifications via this
     * engine.
     */
    String getRequiredContact();
}
