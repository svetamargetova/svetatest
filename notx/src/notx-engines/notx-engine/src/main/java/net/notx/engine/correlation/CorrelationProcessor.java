package net.notx.engine.correlation;


/**
 * Represents mail message processor that can recognize whether
 * he can handle particular mail message and if so he returns
 * information about correlation ID, engine type and time when
 * reception took place.
 * 
 * @author kalaz
 *
 */
public interface CorrelationProcessor<T>{
	boolean canHandle(T correlationMessage) throws Exception;
	ProcessedCorrelationMessage<T> handle(T correlationMessage) throws Exception;
	String getName();
	
}
