package net.notx.engine.correlation.email;

import javax.mail.Message;
import net.notx.engine.correlation.CorrelationProcessor;


public interface EmailCorrelationProcessor extends CorrelationProcessor<Message> {
	
	@Override
	ProcessedEmailMessage handle(Message correlationMessage) throws Exception;
}
