package net.notx.engine.correlation.http;

import javax.servlet.http.HttpServletRequest;
import net.notx.engine.correlation.CorrelationProcessor;


public interface HttpCorrelationProcessor extends CorrelationProcessor<HttpServletRequest>{
	
	@Override
	ProcessedHttpServletRequest handle(HttpServletRequest request);

}
