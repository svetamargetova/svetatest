package net.notx.engine.correlation.http;

import javax.mail.Message;
import javax.servlet.http.HttpServletRequest;


/**
 * Represents mail message processor that can recognize whether
 * he can handle particular mail message and if so he returns
 * information about correlation ID, engine type and time when
 * reception took place.
 * 
 * @author kalaz
 *
 */
public interface HttpRequestCorrelationProcessor {
	boolean canHandle(HttpServletRequest request) throws Exception;
	ProcessedHttpServletRequest handle(HttpServletRequest msg) throws Exception;
	String getName();
	
}
