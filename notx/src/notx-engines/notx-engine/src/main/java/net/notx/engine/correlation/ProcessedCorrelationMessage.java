package net.notx.engine.correlation;

/**
 * Correlation message T that is processed.
 * @author kalaz
 *
 * @param <T>
 */
public class ProcessedCorrelationMessage<T> extends CorrelationInfo {
	private T unprocessedMessage;

	public T getUnprocessedMessage() {
		return unprocessedMessage;
	}	
	

	public void setUnprocessedMessage(T unprocessedMessage) {
		this.unprocessedMessage = unprocessedMessage;
	}
}
