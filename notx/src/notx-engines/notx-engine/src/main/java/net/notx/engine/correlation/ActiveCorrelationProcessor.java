package net.notx.engine.correlation;

import java.util.List;
import net.notx.engine.EngineSettings;

public interface ActiveCorrelationProcessor {
	List<CorrelationInfo> tryCorrelate();
	void configure(EngineSettings engineSettings);
}
