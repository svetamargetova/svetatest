package net.notx.engine.correlation;

import java.util.Date;

public class CorrelationInfo {

    private String correlationID;
    private Date receivedTime;
    private String engine;

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public Date getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(Date receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "CorrelationInfo [correlationID=" + correlationID
                + ", receivedTime=" + receivedTime + ", engine=" + engine + "]";
    }
}
