package net.notx.engine.correlation.email;

import java.util.Date;

import javax.mail.Flags.Flag;
import javax.mail.Message;
import javax.mail.MessagingException;
import net.notx.engine.correlation.ProcessedCorrelationMessage;


/**
 * Correlation email that has been processed by some Mail message processor and
 * all important information has been extracted.
 * 
 * @author kalaz
 *
 */
public class ProcessedEmailMessage extends ProcessedCorrelationMessage<Message>{
	
	/**
	 * Marks original message as read.
	 * @throws MessagingException
	 */
	public void markAsRead() throws MessagingException {
		getUnprocessedMessage().setFlag(Flag.SEEN, true);
	}

	public void markAsUnread() throws MessagingException {
		getUnprocessedMessage().setFlag(Flag.SEEN, false);
	}
	
	
}
