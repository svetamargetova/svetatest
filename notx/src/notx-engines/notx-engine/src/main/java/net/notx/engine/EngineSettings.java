package net.notx.engine;

import com.google.common.collect.ImmutableMap;
import java.util.Map;

/**
 * This class represents settings for one specific engine. There are some
 * mandatory settings that are expressed as fields. Other settings are optional
 * or engine specific. Those are stored as key-value pairs.
 */
public class EngineSettings {

    private static final long serialVersionUID = 6941168751983227802L;
    private static final String ENABLED = "enabled";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String JAR = "engine.jar";
    private static final String CLAZZ = "engine.class";
    private static final String CORRELATION_CLAZZ = "engine.correlation.class";
    private ImmutableMap<String, String> properties;

    public EngineSettings() {
    }

    public ImmutableMap<String, String> getEngineProperties() {
        return properties;
    }

    public void setEngineProperties(ImmutableMap<String, String> engineCustomProperties) {
        this.properties = engineCustomProperties;
    }

    public String getStringProperty(String key) {
        if (properties.containsKey(key)) {
            return properties.get(key);
        } else {
            throw new NullPointerException("Missing engine property: " + key);
        }
    }

    public String getStringPropertySafe(String key, String defaults) {
        try {
            return getStringProperty(key);
        } catch (NullPointerException ex) {
            return defaults;
        }
    }

    public boolean getBoolProperty(String key) {
        return Boolean.parseBoolean(getStringProperty(key));
    }

    public boolean getBoolPropertySafe(String key, boolean defaults) {
        try {
            return Boolean.parseBoolean(getStringProperty(key));
        } catch (NullPointerException ex) {
            return defaults;
        }
    }

    public int getIntProperty(String key) {
        return Integer.parseInt(getStringProperty(key));
    }

    public String getEngineName() {
        return getStringProperty(NAME);
    }

    public String getEngineClass() {
        return getStringProperty(CLAZZ);
    }

    /**
     * Name of java library that implements engine behaviour
     *
     * @return the engineJraName
     */
    public String getEngineJarName() {
        return getStringProperty(JAR);
    }

    /**
     * Name of engine correlaction class. Correlation class is optional
     *
     * @return the engineCorrelationClass
     */
    public String getEngineCorrelationClass() {
        return getStringProperty(CORRELATION_CLAZZ);
    }

    /**
     * Engine short description
     *
     * @return the engineDescription
     */
    public String getEngineDescription() {
        return getStringProperty(DESCRIPTION);
    }

    /**
     * Flag that engine whether engine can process notification or not
     *
     * @return the enabled
     */
    public boolean isEnabled() {
        return Boolean.parseBoolean(getStringProperty(ENABLED));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EngineSettings[ ");
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            sb.append(" ").append(entry.getKey()).append(":").append(entry.getValue());
        }
        sb.append("]");
        return sb.toString();

    }
}
