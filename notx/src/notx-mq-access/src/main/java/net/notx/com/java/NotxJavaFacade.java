package net.notx.com.java;

import java.util.Map;

import javax.jms.JMSException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import net.notx.NotxException;
import net.notx.jms.RequestMessage;
import net.notx.jms.TagMessage;
import net.notx.jms.UnTagMessage;
import net.notx.mq.MQFacade;
import net.notx.mq.MQProducer;

/**
 * This class can be used as direct java client for notx 
 * so as not to interface NotX via webservices or thrift.
 * @author Filip Nguyen
 * 
 */
public class NotxJavaFacade {
	private String brookerAddress = null;
	private static Logger logger = LogManager.getLogger(NotxJavaFacade.class);
	 
	public NotxJavaFacade(String mqBrooker) {
		if (null == mqBrooker || "".equals(mqBrooker)) {
			throw new IllegalArgumentException(
					"mqBrooker address has to be valid URI");
		}
		brookerAddress = mqBrooker;
	}

	private MQProducer ConnectMQ() throws NotxException {
		MQProducer mqfac = new MQProducer(brookerAddress, "notx.request");
		mqfac.open();
		return mqfac;
	}
	
	public boolean tag(String userId, String tag) {
		try {
			MQProducer mq = ConnectMQ();
			TagMessage tagmsg = new TagMessage(userId, tag);
			mq.insertMessage(tagmsg);
			mq.close();
			return true;
		} catch (Exception ex) {
			logger.error("Error while trying to insert tag message into mq", ex);
			return false;
		}
	}

	public boolean sendNotification(String tag, String msgType,
			String templateName, Map<String, String> placeHolderVals) {
		try {
			MQProducer mq = ConnectMQ();
			RequestMessage notif = new RequestMessage(tag,
					msgType, templateName, placeHolderVals);
			mq.insertMessage(notif);
			mq.close();
			return true;
		} catch (Exception ex) {
			logger.error("Error while trying to insert send notification message into mq", ex);
			return false;
		}
	}
	
	public boolean untag(String userId, String tag) {
		try {
			MQProducer mq = ConnectMQ();
			UnTagMessage tagmsg = new UnTagMessage(userId, tag);
			mq.insertMessage(tagmsg);
			mq.close();
			return true;
		} catch (Exception ex) {
			logger.error("Error while trying to insert untag message into mq", ex);
			return false;
		}
	}
	

}
