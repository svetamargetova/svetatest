package net.notx.mq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;

import net.notx.NotxException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * This class is used to receive message from MQ in that is guaranteed way.
 * Message is not deleted from MQ until Acknowledge is called. 
 * 
 * @author Filip Nguyen
 *
 */
public class MQConsumer extends MQFacade {
	private static Logger logger  = LogManager.getLogger(MQConsumer.class);
	private  MessageConsumer consumer = null ;
	private ObjectMessage lastReceivedMessage = null;
	
	public MQConsumer(String brookerAddress, String queueName) {
		super(brookerAddress, queueName);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * This method is used to acknowledge that last received message is successfully processed.
	 * It's important to note that when this method fails the Notx should probably fail. Reason is
	 * that calling this method indicates everything was processed and failing to call acknowledge
	 * would leave the message in MQ itself. 
	 * @throws NotxException If there is MQ internal error.
	 */
	public void acknowledge() throws NotxException{
		if (lastReceivedMessage != null)
		{
			try {
				lastReceivedMessage.acknowledge();
				lastReceivedMessage=null;
				logger.debug("Msg acknowledged successfully");
			} catch (JMSException e) {
				throw new NotxException("Error while acknowledging message", e);				
			}
		}else {
			logger.warn("Trying to acknowledge null message");
		}
	}
	
	//TODO vyzkoušet acknowedlge
	//TODO vyzkoušet co se stane když připojení spadne a ještě se nezavolalo receiveNoWait (tzn že zprávy už u consumera byly ale nebyly vyzvednuty)
	/**
	 * Extracts message from MQ. If there is a connection problem when extracting there is a reconnect
	 * attempt but this method returns silently.
	 */
	public Object pullMessage(long wait)  {
		Message message = null;
		try {
			message = consumer.receive(wait);

			if (message == null) {
				return null;
			}

			if (message instanceof ObjectMessage) {
				lastReceivedMessage = (ObjectMessage) message;
				return lastReceivedMessage.getObject();
			} else {
				logger.warn("There is odd message in mq: "
						+ message.getClass().getName());
				return null;
			}
		} catch (JMSException ex) {
			logger.error("Error while receiving message.", ex);
			tryReconnect();
			return null;
		}
	}
	
	public void open() throws NotxException {
		try {
			connect();
			
			consumer = session.createConsumer(destination);
			
			connection.start();
		} catch (JMSException ex) {
			throw new NotxException("Cannot connect to MQ [" + brookerAddress
					+ "]", ex);
		}
	}

}
