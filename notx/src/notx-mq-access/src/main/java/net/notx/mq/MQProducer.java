package net.notx.mq;

import java.io.Serializable;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import net.notx.NotxException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * <p>
 * MQFacade used to insert messages into MQ. 
 * </p>
 * @author Filip Nguyen
 *
 */
public class MQProducer extends MQFacade {
	private static Logger logger  = LogManager.getLogger(MQProducer.class);
	private  MessageProducer producer = null ;


	public MQProducer(String brookerAddress, String queueName) {
		super(brookerAddress, queueName);
	}

	@Override
	public void open() throws NotxException {
		try{
			
			connect();
			producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);
			connection.start();	
		} catch (JMSException ex){
			throw new NotxException("Cannot connect to MQ ["+brookerAddress+"]",ex);
		}
		
	}
	
	/**
	 * Inserts into JMS. If insertion fails with JMSException this method tries to wait to reconnect to 
	 * MQ for 15 seconds. If the reconnection fails this method let's the JMS exception to propagate.
	 * @param msg
	 * @throws JMSException
	 */
	public void insertMessage(Serializable msg) {
		try{
			ObjectMessage message = session.createObjectMessage(msg);
			producer.send(message);
		} catch (JMSException ex) {
			//TODO silent fail fakt?
			tryReconnect();
			logger.error("Cannot send message. Silent fail.");		
		}
	}
}
