package net.notx.mq;

import java.io.File;

import org.apache.activemq.broker.BrokerService;

/**
 * Manages in memory MQ. This class should delete all it's messages 
 * upon startup.
 * 
 * http://activemq.apache.org/maven/activemq-core/apidocs/index.html
 * @author kalaz
 *
 */
public class MQBootstrapper {
	private  BrokerService broker = new BrokerService();
	
	public MQBootstrapper(String address) throws Exception{
		broker.setDeleteAllMessagesOnStartup(true);
		broker.addConnector(address);
	}
	
	public void startMQ() throws Exception{
		
		broker.start();		
	}
	
	public void stopMQ() throws Exception{
		broker.stop();
	}
}
