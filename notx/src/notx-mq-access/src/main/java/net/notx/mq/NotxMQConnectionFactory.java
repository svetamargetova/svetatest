package net.notx.mq;

import net.notx.config.NotxConfig;




public class NotxMQConnectionFactory extends org.apache.activemq.ActiveMQConnectionFactory {

	public NotxMQConnectionFactory(NotxConfig settings){
		setBrokerURL(settings.getMQAddress());
	}
}
