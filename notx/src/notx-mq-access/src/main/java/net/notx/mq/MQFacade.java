package net.notx.mq;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import net.notx.NotxException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * <p>Facade to MQ, this class aims to simplify access to mq.
 * The usage is simple, just choose one of it's implementations and
 * call open.
 * </p>  
 * @author Filip Nguyen
 */
// TODO ACKNOWLEDGEMENT
public abstract class MQFacade {

	/**
	 * Seconds between connection retry.
	 */
	private static int RETRY_INTERAVAL = 2;

	/**
	 * Timeout for message push and pull.
	 */
	protected static int TIMEOUT = 5;

	private static Logger logger = LogManager.getLogger(MQFacade.class);
	protected Session session = null;
	protected Connection connection = null;
	protected Destination destination = null;
	protected String queueName = null;
	protected String brookerAddress = null;
	private ActiveMQConnectionFactory connectionFactory = null;

	public MQFacade(String brookerAddress, String queueName) {
		this.brookerAddress = brookerAddress;
		this.queueName = queueName;
		logger.debug("The MQ brooker address: " + brookerAddress);
		connectionFactory = new ActiveMQConnectionFactory(brookerAddress);
	}

	protected void connect() throws JMSException {
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

		logger.debug("Connecting to queue: " + queueName);
		destination = session.createQueue(queueName);
		logger.debug("Connection success");
	}

	/**
	 * Opens connection to MQ and allows receive of messages
	 */
	public abstract void open() throws NotxException;

	public void tryReconnect() {
		try {
			open();
		} catch (Exception ex) {
			logger.error("Reconnect failed", ex);
		}
	}

	public void close() {
		try {
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			logger.error("Closing connection", e);
		}
	}
}
