package net.notx.com;

import org.apache.activemq.broker.BrokerService;

public class MQBootstrapper {
	private  BrokerService broker = new BrokerService();
	
	public MQBootstrapper(String address) throws Exception{
		broker.addConnector(address);
	}
	
	public void startMQ() throws Exception{
		broker.start();
	
	}
	
	public void stopMQ() throws Exception{
		broker.stop();
	}
}
