/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.aws.sqs.config;

import net.notx.core.config.NotxConfig;

/**
 * 
 * @author palo
 */
public class SqsNotxConfig {

    static final String MODULE = "queue.aws.sqs";
    static final String AWS_ACCESS_KEY = "accessKey";
    static final String AWS_SECRET_KEY = "secretKey";
    static final String REGION = "region";
    static final String QUEUE_NAME = "queueName";
    static final String VISIBILITY_TIMEOUT = "visibilityTimeout";
    private final NotxConfig mainConfig;

    public SqsNotxConfig(NotxConfig mainConfig) {
        this.mainConfig = mainConfig;
    }

    public String getAwsSecretKey() {
        return mainConfig.getModuleProperty(MODULE, AWS_SECRET_KEY);
    }

    public String getAwsAccessKey() {
        return mainConfig.getModuleProperty(MODULE, AWS_ACCESS_KEY);
    }

    public String getRegion() {
        return mainConfig.getModuleProperty(MODULE, REGION);
    }

    public String getQueueName() {
        return mainConfig.getModuleProperty(MODULE, QUEUE_NAME);
    }

    public int getVisibilityTimout() {
        return Integer.parseInt(mainConfig.getModuleProperty(MODULE, VISIBILITY_TIMEOUT));
    }
}
