package net.notx.aws.sqs;

/**
 * <p>This container is able to receive object messages from SQS queue 
 * in multiple threads and pass them to handlers that will either succeed or
 * throw an exception in which  case the message won't be acknowledged from SQS.
 * </p>
 * <p>
 * Message listener should be thread safe and is standard POJO with onMessage method that 
 * should have 1 parameter only (message to be handled).
 * </p>
 * @author Filip Nguyen
 *
 */
public class SQSMessageContainer implements Runnable {

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
//	private static Logger logger = LogManager.getLogger(SQSMessageContainer.class);
//	private SqsQueueService sqsFacade = null;
//	private String queueName;
//	private volatile boolean stop = false;
//	private int maxConcurrentThreads = 3;
//	private Object messageListener = null;
//	private int listenInterval = 1000;
//	/**
//	 * Maximal allowed running of listener thread.
//	 */
//	private int maxThreadWorkTimeSeconds = 60; 
//	/**
//	 * Threads in which messageListeners receive the messages. 
//	 */
//	private SQSThredCollection listeningThreads = new SQSThredCollection();
//	
//	/**
//	 * Starts listening for queue
//	 */
//	public void start(){
//		stop = false;
//		new Thread(this).start();
//		logger.debug("Listener container started");
//	}
//	
//	public void stop(){
//		logger.debug("Stopping listener container");
//		stop=true;
//	}
//	
//	@Override
//	public void run() {
//		while (!stop){
//			SQSThredCollection succThreads = listeningThreads.getSuccessfullFinished();
//			
//			for (SQSHandleThread succ: succThreads){
//				logger.debug("Successfulll handling of: "+ succ.getHandledMessage().getTheObject());
//				sqsFacade.acknowledge(succ.getHandledMessage());
//			}	
//									
//			SQSThredCollection failedThreads = listeningThreads.getFailedFinished();
//			for (SQSHandleThread fail: failedThreads)
//				logger.warn("Handling of SQS message failed. The message: " +fail.getHandledMessage()+" \nwith exception: \n" +fail.getException(),fail.getException());
//			
//			listeningThreads.deleteFinished();
//			
//			/**
//			 * Kill off any thread that runs too long
//			 */
//			long now = System.currentTimeMillis();
//			for (SQSHandleThread thread: listeningThreads){
//				if (now - thread.getStartTime() > maxThreadWorkTimeSeconds*1000){
//					logger.info("Thread that was handling "+thread.getHandledMessage().getTheObject()+" is running for too long. Killing it." );
//					thread.interrupt();
//				}
//			}
//			
//			int freeThreadCount = maxConcurrentThreads - listeningThreads.size();
//			logger.debug(freeThreadCount +" threads are ready to start message reception"); 
//			List<SQSMessage> msgs = sqsFacade.receiveObjects(freeThreadCount > 10 ? 10 : freeThreadCount, queueName);
//			
//			for (SQSMessage msg : msgs){
//				SQSHandleThread ht = new SQSHandleThread(msg, messageListener);
//				ht.start();
//				listeningThreads.add(ht);
//			}
//			
//			try {
//				Thread.sleep(listenInterval);
//			} catch (Exception e) {
//				logger.warn("Exception while sleeping.");
//			}
//		}
//		logger.debug("Container stopped");
//	}
//	
//	public String getQueueName() {
//		return queueName;
//	}
//
//	public void setQueueName(String queueName) {
//		this.queueName = queueName;
//	}
//
//	public int getMaxThreadWorkTimeSeconds() {
//		return maxThreadWorkTimeSeconds;
//	}
//
//	public void setMaxThreadWorkTimeSeconds(int maxThreadWorkTimeSeconds) {
//		this.maxThreadWorkTimeSeconds = maxThreadWorkTimeSeconds;
//	}
//
//	public int getListenInterval() {
//		return listenInterval;
//	}
//
//	public void setListenInterval(int listenInterval) {
//		this.listenInterval = listenInterval;
//	}
//
//
//
//	public SqsQueueService getSqsFacade() {
//		return sqsFacade;
//	}
//
//	public void setSqsFacade(SqsQueueService sqsFacade) {
//		this.sqsFacade = sqsFacade;
//	}
//
//	public int getMaxConcurrentThreads() {
//		return maxConcurrentThreads;
//	}
//
//	public void setMaxConcurrentThreads(int maxConcurrentThreads) {
//		this.maxConcurrentThreads = maxConcurrentThreads;
//	}
//
//	public Object getMessageListener() {
//		return messageListener;
//	}
//
//	public void setMessageListener(Object messageListener) {
//		this.messageListener = messageListener;
//	}
//

}
