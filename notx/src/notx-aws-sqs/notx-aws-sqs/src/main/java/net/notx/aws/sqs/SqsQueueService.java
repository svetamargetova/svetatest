package net.notx.aws.sqs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import java.util.List;
import net.notx.aws.sqs.config.SqsNotxConfig;
import net.notx.client.NotxQueueException;
import net.notx.client.message.AbstractMessage;
import net.notx.core.config.NotxConfig;
import net.notx.core.queue.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> Basic operations on SQS. This class should encapsulate object message
 * sending and receiving. The configuration of SQS is taken from file. This
 * looks most convenient because even in development environment the credentials
 * to SQS shouldn't be checked in to the repostiory </p>
 *
 * @author kalaz
 *
 */
public class SqsQueueService implements QueueService {

    private static Logger logger = LoggerFactory.getLogger(SqsQueueService.class);
    private SqsDao sqsDao;

    @Override
    public void initialize(NotxConfig config) {
        final SqsNotxConfig sqsNotxConfig = new SqsNotxConfig(config);
        logger.info("Initializing SQS Queue");
        AWSCredentials credentials = new AWSCredentials() {

            @Override
            public String getAWSAccessKeyId() {
                return sqsNotxConfig.getAwsAccessKey();
            }

            @Override
            public String getAWSSecretKey() {
                return sqsNotxConfig.getAwsSecretKey();
            }
        };

        sqsDao = new SqsDao(credentials, sqsNotxConfig.getRegion(), sqsNotxConfig.getQueueName());
        sqsDao.initializeQueue();
    }

    @Override
    public List<? extends AbstractMessage> loadMessages(int number) {
        try {
            List<AbstractMessage> out = sqsDao.loadMessages(number);
            return out;
        } catch (AmazonClientException exception) {
            throw new NotxQueueException("Load messages failed!", exception);
        }
    }

    @Override
    public AbstractMessage loadMessage() {
        try {
            List<AbstractMessage> msgs = sqsDao.loadMessages(1);
            return msgs.isEmpty() ? null : msgs.get(0);
        } catch (AmazonClientException exception) {
            throw new NotxQueueException("Load message failed!", exception);
        }
    }

    @Override
    public void putMessage(AbstractMessage message) {
        try {
            sqsDao.sendMessage(message);
        } catch (AmazonClientException exception) {
            throw new NotxQueueException("Send message failed!", exception);
        }
    }

    @Override
    public void deleteMessage(AbstractMessage m) {
        try {
            sqsDao.deleteMessage(m);
        } catch (AmazonClientException exception) {
            throw new NotxQueueException("Delete messages failed!", exception);
        }
    }

    @Override
    public void destroy(NotxConfig config) {
    }
}
