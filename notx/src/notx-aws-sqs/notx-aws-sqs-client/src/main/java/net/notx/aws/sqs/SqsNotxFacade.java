/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.aws.sqs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.sqs.model.SendMessageResult;
import net.notx.client.NotxQueueException;
import net.notx.client.NotxQueueFacade;
import net.notx.client.PlaceHolders;
import net.notx.client.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p> Basic operations on SQS. This class should encapsulate object message
 * sending and receiving. The configuration of SQS is taken from file. This
 * looks most convenient because even in development environment the credentials
 * to SQS shouldn't be checked in to the repostiory </p>
 *
 * @author kalaz
 *
 * @since 2.0 Extending class to be more independent on whole NotX
 * infrastructure. This class is extended in NotX server library.
 *
 * @author Pavol Gressa
 */
public class SqsNotxFacade implements NotxQueueFacade {

    private static Logger logger = LoggerFactory.getLogger(SqsNotxFacade.class);
    protected SqsDao sqsDao;

    public SqsNotxFacade(AWSCredentials credentials, String endpoint, String queueName) {
        sqsDao = new SqsDao(credentials, endpoint, queueName);
    }

    @Override
    public void tag(String userId, String ...tags) {
        AbstractMessage tagMsg = null;
        if(tags.length == 1){
            tagMsg = new TagMessage(userId, tags[0]);
        }else{
            tagMsg = new TagsMessage(userId, tags);
        }        
        logger.debug("Queue-> {}",tagMsg);
        try {
            sqsDao.sendMessage(tagMsg);            
        } catch (AmazonClientException aws) {
            throw new NotxQueueException("Exception when tagging user:" + userId + " with tag: " + tags, aws);
        }

    }

    @Override
    public void unTag(String userId, String ...tags) {
        AbstractMessage tagMsg = null;
        if(tags.length == 1){
            tagMsg = new UnTagMessage(userId, tags[0]);
        }else{
            tagMsg = new UnTagsMessage(userId, tags);
        }             
        logger.debug("Queue-> {}",tagMsg);
        try {
            sqsDao.sendMessage(tagMsg);            
        } catch (AmazonClientException aws) {
            throw new NotxQueueException("Exception when untagging user:" + userId + " with tag: " + tags, aws);
        }
    }

    @Override
    public void sendNotification(String tag, String msgType,
            String templateName, PlaceHolders placeHolderVals) {
        NotificationMessage notif = new NotificationMessage(tag, msgType,
                templateName, placeHolderVals.getPlaceHolderVals());
        logger.debug("Queue-> {}",notif);
        try {
            sqsDao.sendMessage(notif);            
        } catch (AmazonClientException aws) {
            throw new NotxQueueException("Exception when sending notification :" + notif, aws);
        }
    }
}
