/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.aws.sqs;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import net.notx.client.NotxException;
import net.notx.client.message.AbstractMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class SqsDao {

    private static Logger logger = LoggerFactory.getLogger(SqsNotxFacade.class);
    /**
     * This is constant for SQS that determins after how long the messages is
     * "revisibile" in the queue after not being processed.
     */
    private static final String DEFAULT_VISIBILITY = "50";
    /**
     * SQS has maximum 64kb.
     */
    private static final Integer MAX_MESSAGE_SIZE = 65536;
    /**
     * SQS supports on one receive request to load max 10 messages
     */
    private static final Integer MAX_MESSAGE_RECEIVE_COUNT = 10;
    /**
     * Queue name in SQS
     */
    private final String queueName;
    /**
     * Resolved queue url
     */
    private String queueUrl;
    private ObjectSerializer objectSerializer = new ObjectSerializer(MAX_MESSAGE_SIZE);
    public AmazonSQS sqs = null;

    public SqsDao(AWSCredentials credentials, String endpoint, String queueName) {
        sqs = new AmazonSQSClient(credentials);
        sqs.setEndpoint(endpoint);
        this.queueName = queueName;
        logger.info("Successfully configured amazone SQSClient with queue: {}", queueName);
    }

    /**
     * This method will initialize queue on AWS SQS. Initialization process gets
     * queue URL and will create new queue on specified region if needed.
     */
    public void initializeQueue() {
        queueUrl = getOrCreateQueue(queueName);
    }

    /**
     * Creates a queue. If the queue exists it returns the url of the queue.
     *
     * @param name
     */
    private String getOrCreateQueue(String name) {

        CreateQueueRequest crqr = new CreateQueueRequest(name);
        String queueurl = sqs.createQueue(crqr).getQueueUrl();
        Map<String, String> config = Maps.newHashMap();
        config.put("MaximumMessageSize", MAX_MESSAGE_SIZE.toString());
        config.put("VisibilityTimeout", DEFAULT_VISIBILITY);
        sqs.setQueueAttributes(new SetQueueAttributesRequest().withQueueUrl(queueurl).withAttributes(config));
        logger.debug("Queue {} mapped to {}", name, queueurl);
        return queueurl;
    }

    /**
     * Method will delete DAO managed queue.
     */
    public void deleteQueue() {
        deleteQueue(queueName);
    }

    /**
     * Method will delete queue specified by
     * <code>prefix</code>
     *
     * @param prefix queue prefix name that defines queue which will be deleted
     *
     * @throws NotxException when given prefix has more than one matched queue
     */
    public void deleteQueue(String prefix) {

        List<String> queues = sqs.listQueues(new ListQueuesRequest().withQueueNamePrefix(prefix)).getQueueUrls();

        if (queues.size() > 1) {
            throw new NotxException("More queues with prefix " + prefix + " found while deleting, what should i delete really?");
        }

        if (queues.isEmpty()) {
            return;
        }

        logger.debug("Deleting queue {}", queues.get(0));
        DeleteQueueRequest deleteQueue = new DeleteQueueRequest();
        deleteQueue.setQueueUrl(queues.get(0));
        sqs.deleteQueue(deleteQueue);
    }

    /**
     * Sends list of messages into queue. This operation is <strong>NOT</strong>
     * atomic.
     *
     * @param objects messages
     * @return result
     */
    public SendMessageBatchResult sendMessages(List<? extends AbstractMessage> objects) {
        checkDaoSettings();

        logger.debug("Queuing {} messages to {} ", objects.size(), queueName);

        // we will crete list of requests
        ImmutableList.Builder<SendMessageBatchRequestEntry> builder = ImmutableList.builder();
        for (int i = 0; i < objects.size(); i++) {
            SendMessageBatchRequestEntry entry = new SendMessageBatchRequestEntry();
            entry.setId(String.format("Entry-%d", i));
            entry.setMessageBody(objectSerializer.serialize(objects.get(i)));
            builder.add(entry);
        }

        SendMessageBatchResult result =
                sqs.sendMessageBatch(new SendMessageBatchRequest(queueUrl).withEntries(builder.build()));
        logger.debug("Queued success:{}  failed:{}", result.getSuccessful().size(), result.getFailed().size());
        return result;
    }

    public SendMessageResult sendMessage(AbstractMessage object) {
        checkDaoSettings();
        logger.debug("Queuing {} to {} ", object, queueName);
        SendMessageResult result = sqs.sendMessage(new SendMessageRequest().withQueueUrl(queueUrl).withMessageBody(objectSerializer.serialize(object)));
        return result;
    }

    public void deleteMessage(AbstractMessage message) {
        checkDaoSettings();
        logger.debug("Deleting message: {} in {}", message.getQueueId(), queueName);
        sqs.deleteMessage(new DeleteMessageRequest(queueUrl, message.getQueueId()));
    }

    public void deleteMessages(List<AbstractMessage> messages) {


        checkDaoSettings();

        logger.debug("Deleting {} messages in {} ", messages.size(), queueName);

        if (messages.isEmpty()) {
            return;
        }
        // we will crete list of requests
        ImmutableList.Builder<DeleteMessageBatchRequestEntry> builder = ImmutableList.builder();
        for (int i = 0; i < messages.size(); i++) {
            DeleteMessageBatchRequestEntry entry = new DeleteMessageBatchRequestEntry();
            entry.setId(String.format("Entry-%d", i));
            entry.setReceiptHandle(messages.get(i).getQueueId());
            builder.add(entry);
        }

        DeleteMessageBatchResult result =
                sqs.deleteMessageBatch(new DeleteMessageBatchRequest(queueUrl, builder.build()));
        logger.debug("Delete success:{}  faled:{}", result.getSuccessful().size(), result.getFailed().size());
    }

    public List<AbstractMessage> loadMessages() {
        return loadMessages(MAX_MESSAGE_RECEIVE_COUNT);
    }

    public List<AbstractMessage> loadMessages(int numberOfMessages) {

        checkDaoSettings();

        if (numberOfMessages > MAX_MESSAGE_RECEIVE_COUNT) {
            logger.warn("Maximal allowed number of messages is 10. See: http://docs.amazonwebservices.com/AWSSimpleQueueService/latest/APIReference/Query_QueryReceiveMessage.html");
            numberOfMessages = MAX_MESSAGE_RECEIVE_COUNT;
        } else if (numberOfMessages < 1) {
            logger.warn("Minimal allowed number of messages is 1. See: http://docs.amazonwebservices.com/AWSSimpleQueueService/latest/APIReference/Query_QueryReceiveMessage.html");
            numberOfMessages = 1;
        }



        ReceiveMessageResult rslt = sqs.receiveMessage(
                new ReceiveMessageRequest(queueUrl).withMaxNumberOfMessages(numberOfMessages));

        ImmutableList.Builder<AbstractMessage> builder = ImmutableList.builder();

        for (Message m : rslt.getMessages()) {
            AbstractMessage message = (AbstractMessage) objectSerializer.deserialize(m.getBody());
            message.setQueueId(m.getReceiptHandle());
            builder.add(message);
        }

        ImmutableList<AbstractMessage> msgs = builder.build();

        logger.debug("Loaded {} messages", rslt.getMessages().size());
        return msgs;
    }

    private void checkDaoSettings() throws NotxException {
        if (objectSerializer == null) {
            throw new NotxException("Sqs facade doesn't have objectSerializer. Please provide one via setObjectSerializer method.");
        }

        if (queueUrl == null) {
            initializeQueue();
        }
    }

    public ObjectSerializer getObjectSerializer() {
        return objectSerializer;
    }

    public void setObjectSerializer(ObjectSerializer objectSerializer) {
        this.objectSerializer = objectSerializer;
    }

    public String getDefaultQueue() {
        return queueName;
    }
}
