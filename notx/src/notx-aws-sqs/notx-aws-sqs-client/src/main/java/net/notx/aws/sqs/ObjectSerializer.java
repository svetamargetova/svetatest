package net.notx.aws.sqs;


import java.io.*;
import net.notx.client.NotxException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to serialize objects into string(base64) and vice versa.
 * 
 * @author kalaz
 *
 */
public class ObjectSerializer {
	private int maxSize = -1;
	private static Logger logger = LoggerFactory.getLogger(ObjectSerializer.class);

	/**
	 * 
	 * @param maxSize Maximal size of serialized object in bytes. -1 if unlimited
	 */
	public ObjectSerializer(int maxSize){
		this.maxSize=maxSize;
	}
	
	public String serialize(Object obj) {
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream oos;
			oos = new ObjectOutputStream(byteArrayOutputStream);
			oos.writeObject(obj);
			byte[] bytes = byteArrayOutputStream.toByteArray();
			
			if (bytes.length > maxSize){
				throw new NotxException("The object couldn't be serialized it's size "+ bytes.length +" exceeds maximum allowed " + maxSize);
			}
                        
			String result = new String(new Base64().encode(bytes));
			logger.debug("Size of serialized in base64 is {} characters.",result.length());
			return result;
		} catch (IOException e) {
			throw new NotxException("Error while converting object: "+ obj+" into byte array.",e);
		}
	}
	
	public Object deserialize(String serialized){
		ByteArrayInputStream byteArrayInputStream;
		try {
			byte[] bytes = new Base64().decode(serialized.getBytes());
			if (bytes.length > maxSize){
				throw new NotxException("The object couldn't be deserialized it's serialized size "+ bytes.length +" exceeds maximum allowed " + maxSize);
			}
			byteArrayInputStream = new ByteArrayInputStream(bytes);
			ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream);
			Object result = ois.readObject();
			return result;
		} catch (IOException e) {
			throw new NotxException("Error while deserializing: "+serialized,e); 
		} catch (ClassNotFoundException e) {
			throw new NotxException("Error while deserializing: "+serialized,e);
		}
	}
}
