/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.notx.aws.sqs;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.notx.client.PlaceHolders;
import net.notx.client.message.*;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for NotXFacade. AWS credentials are gained from
 * aws-credentials.properties, where are these properties filtered by Maven on
 * compile time. You have to create properties placeholders in settings.xml as
 * default for proper running this test.
 *
 * Add this in default settings.xml progile: <properties>
 * <amazon.aws.accessKey>XXXXXXXXXXXXX</amazon.aws.accessKey>
 * <amazon.aws.secretKey>XXXXXXXXXXXXX</amazon.aws.secretKey> </properties>
 *
 * <strong>Please, DO NOT commit these resources into shared repositor</strong>
 *
 * @author Pavol Gressa
 */
public class SqsNotXFacadeTests {

    private SqsNotxFacade facade;
    private static final String SQS_TEST_REGION = "sqs.eu-west-1.amazonaws.com";
    private static final String SQS_TEST_QUEUE = "NotXTest";

    @Before
    public void setUp() throws IOException {
        AWSCredentials credentials = new PropertiesCredentials(new File("target/test-classes/aws-credentials.properties"));
        if (StringUtils.isBlank(credentials.getAWSAccessKeyId())
                || credentials.getAWSAccessKeyId().equals("${amazon.aws.accessKey}")
                || StringUtils.isBlank(credentials.getAWSSecretKey())
                || credentials.getAWSSecretKey().equals("${amazon.aws.secretKey}")) {
            throw new NullPointerException("Missing AWS credentials in aws-credentials.properties file. Please configure your Maven settings.xml properly.");
        }
        facade = new SqsNotxFacade(credentials, SQS_TEST_REGION, SQS_TEST_QUEUE);
    }

    /**
     * Test for dao methods. This test is little bit odd because it contains few
     * calling for Thread.sleep method. The reason is that AWS SQS can process
     * message from 5 to 60 seconds before it will be loadable. This is resolved
     * by the pooling process that is limited for 10 times by 5 seconds sleeping.
     */
    @Test
    public void testFacadeAndDao() {
        // we will clear 
        List<AbstractMessage> messages = facade.sqsDao.loadMessages();
        facade.sqsDao.deleteMessages(messages);
        try {
            // we will test
            Thread.sleep(1000);
            facade.tag("1234", "tag");
            Thread.sleep(1000);
            facade.unTag("1234", "tag");
            Thread.sleep(1000);
            facade.sendNotification("tag", "type", "templateName", new PlaceHolders("key1", "value1", "key2", "tuple2"));
            // we have to sleep for a 5 seconds to let AWS to process messages

            // we are trying to load 3 messages 
            for (int j = 0, k = 0; j < 3 ; k++ ) {
                // pooling limited to max 50 seconds
                if(k >= 10){
                    fail("Too many poolings!");
                }
                Thread.sleep(5000);
                // load messages
                messages = facade.sqsDao.loadMessages();
                j+= messages.size();

                for (int i = 0; i < messages.size(); i++) {
                    if (messages.get(i) instanceof TagMessage) {
                        TagMessage tag = (TagMessage) messages.get(i);
                        assertEquals("1234", tag.getUserid());
                        assertEquals("tag", tag.getTag());
                    } else if (messages.get(i) instanceof UnTagMessage) {
                        UnTagMessage untag = (UnTagMessage) messages.get(i);
                        assertEquals("1234", untag.getUserid());
                        assertEquals("tag", untag.getTag());
                    } else if (messages.get(i) instanceof NotificationMessage) {
                        NotificationMessage req = (NotificationMessage) messages.get(i);
                        assertEquals("tag", req.getTo());
                        assertEquals("type", req.getMsgType());
                        assertEquals("templateName", req.getTemplateName());
                    } else {
                        fail("Message can not be handled - not known message " + messages.get(i).toString());
                    }
                    // delete messages
                    facade.sqsDao.deleteMessage(messages.get(i));
                }
            }
        } catch (InterruptedException ex) {
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        // load messages again
        messages = facade.sqsDao.loadMessages();
        assertEquals(0, messages.size());

    }
}
